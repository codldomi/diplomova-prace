## Diplomová práce

- autor: Bc. Dominik Codl
- vedoucí: Ing. Ladislav Šťastný, PhD.
- téma: Rozšíření systému pro sběr dat z OPC UA serverů
- rok: 2024

---

Příloha je rozdělena do částí:

-  adresář ```/text``` obsahující zdrojové soubory písemné části;
-  adresář ```/system``` obsahující nepísemnou část práce.

---

### Adresář ```/text```

- soubor ```DP_Codl_Dominik_2024.pdf``` - text práce ve formátu PDF;
- adresář ```/latex_zdroj``` - LaTeX zdrojové soubory;
- adresář ```/obrazky_zdroj``` - zdrojové soubory obrázků, které jsou exportovány v PDF a použity v projektu LaTeX. Zdrojové soubory obrázků jsou tvořeny skrze projekt [draw.io](https://app.diagrams.net/).

---

### Adresář ```/system```

Adresář obsahuje projekty a adresáře dle struktury vybrané v návrhu systému. Každý projekt obsahuje ```readme.md``` soubor s pokyny. Unifikovaně popisuje, ve kterém jazyce je daná knihovna/aplikace napsána, jak zprovoznit vývojové prostředí, které příkazy spustí generování dokumentace atp. Níže přehled:

- ```/adapter```

  Adresář s Adapter API a jeho implementacemi.

    - ```Adapter_API```
    
      Pojekt obsahující dokumentaci Adapter API a rovněž i implementaci klienta ve Scale.
    
    - ```OPC_UA_Adapter```
  
      Adapter implementující Adapter API pro protokol OPC UA.

- ```/libs```

  Adresář s knihovnami používanými v rámci systémových komponent.

    - ```Scalable_OPC_UA```

      Implementace OPC UA klienta a utilit pro práci s daty.

Z důvodu, že je v písemné části zmíněno testování na OPC UA serverech a že je to nutné pro chod testů, jsou k nepísemné části práce přiloženy i dva projekty s testovacími servery OPC UA, které nejsou součástí práce. Byly součastí bakalářské práce, na kterou tato diplomová navazuje. Těmi jsou:

- ```OPC_UA_Test_Server_-_open62541```

  Testovací server v C, který používá knihovnu open62541.

- ```OPC_UA_Test_Server_-_Eclipse_Milo```

  Testovací server v Jave/Kotlinu, který používá knihovnu Eclipse Milo.
