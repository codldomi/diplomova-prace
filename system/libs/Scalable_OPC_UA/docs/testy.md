# Testy

Testování probíhá většinou proti rozhraní implementace daného modulu. Výjimku tvoří ty pomocné třídy, které poskytují
komplexní funkcionality, a tudíž je riziko vzniku chyby. Součástí projektu je rovněž klient pro OPC UA, jenž pro otestování
vyžaduje běžící OPC UA server. Testy či skupiny testů, které vyžadují běžící OPC UA server, jsou označeny štítkem 
```NeedsRunningServer```.

V sbt shellu (start příkazem ```sbt```, konec příkazem ```exit```) lze spustit všechny testy zavoláním:

```
test
```

Naopak spustit testy bez těch se štítkem ```NeedsRunningServer``` je možné příkazem:

```
testOnly -- -l NeedsRunningServer
```

Testy používají následující servery.

## OPC UA Test Server - open62541

[Projekt](http://gitlab.intranet.modemtec.cz/products/server/pdds/opc-ua-test-server-open62541) vytvořený pomocí 
knihovny [open62541](http://www.open62541.org/) simuluje server z produkčního prostředí. V testech se k němu připojuje
klient skrze anonymní přístup a soustředí se na otestování datových typů a služeb.

## OPC UA Test Server - Eclipse Milo

[Projekt](http://gitlab.intranet.modemtec.cz/products/server/pdds/opc-ua-test-server-eclipse-milo) vytvořený pomocí
knihovny [Eclipse Milo](https://github.com/eclipse/milo) umožňuje testování přihlašování přes uživatelské jméno a heslo, 
s možností šifrování.