# Datový model

Verze [knihoven](https://github.com/open62541/open62541/wiki/List-of-Open-Source-OPC-UA-Implementations) k datu 19.09.2023
podporují vlastní datové typy skrze definování tříd a struktur v daném programovacím jazyce. Což ale sebou nese opakované
generování kódu při každé změně v informačním modelu, který má být podporován. Navíc některé informační modely obsahují 
desítky definic datových typů, např. model pro [IEC61580-7-3](https://reference.opcfoundation.org/IEC61850-7-3/DataTypes/). 
Jedním z cílů projektu Scalable OPC UA je vytvoření obecných struktur pro datové typy definované uživatelem. Tím se umožní
konfigurovat podporu datových typů dané aplikace, aniž by docházelo k opětovné kompilaci zdrojových kódů. Lze k tomuto účelu
využít například [XML soubory obsahují informační modely](https://reference.opcfoundation.org/v104/Core/docs/Part6/B/), 
kde jsou rovněž uloženy definice datových typů.

Definice datových typů jsou knihovnami využívány k obecné manipulaci s daty, tj. implementace dekodérů či alokace paměti
v připadě nižších jazyků. Ačkoliv vyšší jazyky (např. C#, Java) podporují reflexi (například pro zjištění informací o 
programu za jeho běhu či pro modifikaci programu za běhu), jsou nutné definice minimálně pro dekodéry formátů, které neobsahují
informace o obsahu, např. data v binární podobě. Tudíž je modelována jak reprezentace hodnot (dále jen "hodnoty"), tak 
reprezentace definic datových typů (dále jen "datové typy").

## Datový typ

Knihovna v současné verzi podporuje uživatelem definované struktury a výčty, ačkoliv standard umožňuje definovat i 
jednoduché typy (prakticky přejmenování stávajících typů) a unie. Diagram níže zobrazuje návrh tříd pro datové typy. 
[Vestavěné datové typy](https://reference.opcfoundation.org/Core/Part6/v104/5.1.2/) jsou reprezentovány objekty (ve 
Scale definováno jako ```case object```), které implementují třídu ```UaDataType```.

``` mermaid
classDiagram

class UaDataType {
    <<trait>>
    + id : UaId
    + superId : UaId
    + isAbstract : UaBoolean
    + name : UaString
}

class GeneralStructure {
    <<case class>>
    + id : UaId
    + superId : UaId
    + isAbstract : UaBoolean
    + name : UaString
}

GeneralStructure --|> UaDataType

class Field {
    <<case class>>
    + name : UaString
    + typeId : UaId
    + isArray : UaBoolean
    + isOptional : UaBoolean
}

GeneralStructure "1" --> "0..*" Field

class GeneralEnumeration {
    <<case class>>
    + id : UaId
    + superId : UaId
    + isAbstract : UaBoolean
    + name : UaString
    + values : Map~UaString, UaInt32~
}

GeneralEnumeration --|> UaDataType

```

Seznam atributů (```Field```) struktury je seřazený podle pořadí jejich definic v ```NodeSet2.xml``` souboru, kde se 
nalézají definice datových typů dané množiny uzlů/jmenného prostoru. Třída ```GeneralStructure``` obsahuje atribut ```fields```
jakožto typ ```Vector<Field>```.

## Hodnota

Třída ```UaValue``` a její podtřídy reprezentují hodnotu datového typu. Pro uživatelem definované struktury a výčty je 
modelována obecná reprezentace. Zbylé hodnoty jsou reprezentovány konkrétními třídami. V této knihovně i v řadě dalších
(např. [open62541](http://www.open62541.org/) čí [Eclipse Milo](https://github.com/eclipse/milo)) jsou hodnoty modelovány 
dle standardu. Tzn. hodnoty odpovídají vestavěným typům dle následující tabulky.  

| hodnota             | datový typ                 |
| :------------------ | :------------------------- |
| *UaArray*           | -                          |
| UaBoolean           | UaDataType.Boolean         |
| UaSByte             | UaDataType.SByte           |
| UaByte              | UaDataType.Byte            |
| UaInt16             | UaDataType.Int16           |
| UaUInt16            | UaDataType.UInt16          |
| UaInt32             | UaDataType.Int32           |
| UaUInt32            | UaDataType.UInt32          |
| UaInt64             | UaDataType.Int64           |
| UaUInt64            | UaDataType.UInt64          |
| UaFloat             | UaDataType.Float           |
| UaDouble            | UaDataType.Double          |
| UaString            | UaDataType.String          |
| UaDateTime          | UaDataType.DateTime        |
| UaGuid              | UaDataType.Guid            |
| UaByteString        | UaDataType.ByteString      |
| UaXmlElement        | UaDataType.XmlElement      |
| UaNodeId            | UaDataType.NodeId          |
| UaExpandedNodeId    | UaDataType.ExpandedNodeId  | 
| UaStatusCode        | UaDataType.StatusCode      |
| UaQualifiedName     | UaDataType.QualifiedName   |
| UaLocalizedText     | UaDataType.LocalizedText   |
| *UaExtensionObject* | *UaDataType.Structure*     |
| UaDataValue         | UaDataType.DataValue       |
| *UaVariant*         | *UaDataType.BaseDataType*  |
| UaDiagnosticInfo    | UaDataType.DiagnosticInfo  |
| UaNumber            | UaDataType.Number          |
| UaInteger           | -                          |
| UaSInteger          | UaDataType.Integer         |
| UaUInteger          | UaDataType.UInteger        |
| UaEnumeration       | UaDataType.Enumeration     |

Kurzívou jsou označeny zvláštnosti.

- ```UaNull``` = třída představující prázdnou hodnotu. Není v originální hierarchii. Nicméně standard Null hodnotu zná.
- ```UaArray``` =  třída představující pole vybrané hodnoty. Není v originální hierarchii. Nicméně standard reprezentaci 
pole implementuje.
- ```UaExtensionObject``` = třída odpovídající abstraktní třídě Structure z originální hierarchie. Ponecháno kvůli 
převodům mezi datovými formáty/reprezentacemi. OPC UA využívá ExtensionObject pro přenos uživatelem definovaných struktur. 
Nicméně knihovna podporuje i přímý převod.
- ```UaVariant``` = třída odpovídající abstraktní třídě BaseDataType z originální hierarchie. Ponecháno kvůli převodům 
mezi datovými formáty/reprezentacemi. OPC UA využívá Variant jako strukturu obsahující libovolný z vestavěných datových
typů. Nicméně knihovna podporuje i přímý převod do ```UaValue```.