# Datové formáty

Knihovna podporuje kódování z/do níže uvedených formátů.

## ModemTec OPC UA JSON

ModemTec OPC UA JSON představuje pravidla pro převod hodnot (třída ```UaValue``` a její podtřídy) do [JSON formátu](https://www.json.org/json-en.html). 
Narozdíl od existujícího [JSON kódování od OPC Foundation](https://reference.opcfoundation.org/v104/Core/docs/Part6/5.4.1/) 
umožňují tyto pravidla manipulaci s daty bez vazby na konkrétní typ či instanci serveru a bez znalosti definic daných typů. 
Je toho docíleno skrze:

- atribut s hodnotou je vždy přítomen. Pokud hodnota neexistuje, je dosazeno ```null```.
- atribut ```index```, který představuje index jmenného prostoru v rámci pole jmenných prostorů (```NamespaceArray```)
na konkrétní instanci serveru, je nahrazen atributem ```uri```, jenž obsahuje unikátní URI daného jmenného prostoru.  

Následuje legenda k popisu formátu.

```
{
  "name" : [String],
  "friends" : [[String]],
  "age" : [Number],
  "other" : [UaPerson],
  "maybe" : [Number|String]
}
```

Text v hranatých závorkách ```[String]``` označuje název datového typu. Svislá čára mezi typy, např. ```[Number|String]```, 
značí, že datovým typem může být buď jeden, nebo druhý uvedený typ. ```[[String]]``` představuje pole hodnot datového 
typu ```String```. Datový typ s prefixem ```Ua``` odkazuje na mapování daného knihovního typu za účelem zpřehlednění zápisu.

```
{
  "name" : "",
  "friends" : [],
  "age" : 10,
  "other" : null
}
```

Chybějící hodnota je mapována na ```null``` s výjimkou: ```String``` (prázdný řetězec ```""```), pole (prázdné pole ```[]```) .

Převody hodnot jsou následující.

### UaBoolean

Přímý převod na ```[Boolean]```.

### UaByte, UaSByte, UaUInt16, UaInt16, UaUInt32, UaInt32, UaUInt64, UaInt64, UaFloat, UaDouble

Přímý převod na ```[Number]```.

### UaString, UaXmlElement

Přímý převod na ```[String]```.

### UaGuid

Převod na ```[String]``` obsahující hexadecimální znaky ve formátu ```<Data1>-<Data2>-<Data3>-<Data4[0:1]>-<Data4[2:7]>```,
např. ```C496578A-0DFE-4B8F-870A-745238C6AEAE```. Viz [online reference](https://reference.opcfoundation.org/Core/Part6/5.1.3/).
 
### UaByteString

Převod na řetězec obsahující kódování v [Base64](https://en.wikipedia.org/wiki/Base64).

### UaDateTime

Převod na čas podle normy ISO 8601 ve formátu UTC: ```"yyyy-MM-ddTHH:mm:ssZ"```, např. ```"2022-06-27T15:16:00Z"```.

### UaStatusCode

Převod na objekt tvaru:

```
{
  "code" : [Number],
  "symbol" : [String]
}
```

### UaEnumeration

Převod uživatelsky nadefinované enumerace na objekt tvaru:

```
{
  "name" : [String],
  "value" : [Number]
}
``` 

### UaQualifiedName

Převod na objekt tvaru:

```
{
  "name" : [String]
  "uri" : [String]
}
```

### UaLocalizedText

Převod na objekt tvaru:

```
{
  "locale" : [String],
  "text" : [String]
}
```

### UaVariant

Převod na objekt tvaru:

```
{
  "varType" : [String],
  "value" : [BaseDataType]
}
```

kde ```[BaseDataType]``` je OPC UA built-in datový typ dle odpovídajícího ```varType```.

| hodnota "value"   | hodnota "varType" |
| :---------------- | :---------------- |
| [UaNull]            | "NULL"            |
| [UaBoolean]         | "BOOLEAN"         |
| [UaSByte]           | "SBYTE"           |
| [UaByte]            | "BYTE"            |
| [UaInt16]           | "INT16"           |
| [UaUInt16]          | "UINT16"          |
| [UaInt32]           | "INT32"           |
| [UaUInt32]          | "UINT32"          |
| [UaInt64]           | "INT64"           |
| [UaUInt64]          | "UINT64"          |
| [UaFloat]           | "FLOAT"           |
| [UaDouble]          | "DOUBLE"          |
| [UaString]          | "STRING"          |
| [UaDateTime]        | "DATETIME"        |
| [UaGuid]            | "GUID"            |
| [UaByteString]      | "BYTESTRING"      |
| [UaXmlElement]      | "XMLELEMENT"      |
| [UaNodeId]          | "NODEID"          |
| [UaExpandedNodeId]  | "EXPANDEDNODEID"  |
| [UaStatusCode]      | "STATUSCODE"      |
| [UaQualifiedName]   | "QUALIFIEDNAME"   |
| [UaLocalizedText]   | "LOCALIZEDTEXT"   |
| [UaExtensionObject] | "EXTENSIONOBJECT" |
| [UaDataValue]       | "DATAVALUE"       |
| [UaVariant]         | "VARIANT"         |
| [UaDiagnosticInfo]  | "DIAGNOSTICINFO"  |

Hodnota v ```UaVariant``` může být jedna, nebo pole či ```UaNull```.

### UaDiagnosticInfo

Převod na objekt tvaru:

```
{
  "symbolicId" : [Number],
  "namespaceUri" : [Number],
  "localizedText" : [Number],
  "locale" : [Number],
  "additionalInfo" : [String],
  "innerStatusCode" : [UaStatusCode],
  "innerDiagnosticInfo" : [UaDiagnosticInfo]
}
```

kde atributy ```innerStatusCode``` a ```innerDiagnosticInfo``` mohou nabývat ```null``` hodnoty.

### UaDataValue

Převod na objekt tvaru:

```
{
  "value" : [UaVariant],
  "status" : [UaStatusCode],
  "serverTimestamp" : [UaDateTime],
  "serverPicoSeconds" : [Number],
  "sourceTimestamp" : [UaDateTime],
  "sourcePicoSeconds" : [Number]
}
```

### UaStructure

Převod uživatelem nadefinované struktury na objekt tvaru:

```
{
  "name" : [UaValue]
}
```

kde ```name``` je název daného atributu a ```[UaValue]``` jeho hodnota. Atributy struktury jsou atributy JSON objektu.

### UaArray

Pole obsahuje typ, který je potomkem ```UaValue```. Převádí se na pole v JSON, např. dvoudimenzionální pole typu ```UaInt32```:

```
[[1, 2], [3, 4]]
```

### UaNodeId

Identifikátor uzlu ```UaNodeId``` je abstraktní datový typ s podtypy: ```UaNumericNodeId```, ```UaOpaqueNodeId```, ```UaStringNodeId``` a ```UaGuidNodeId```. Výsledkem převodu je objekt tvaru:

```
{
  "idType" : [Number],
  "id" : [String|Number],
  "uri" : [String]
}
```

kde hodnota atributu ```id``` odpovídá typu dle ```idType```.

| datový typ      | hodnota "idType" | hodnota "id"   |
| :-------------- | :--------------- | :------------- |
| UaNumericNodeId | "NUMERIC"        | [Number]       |
| UaGuidNodeId    | "GUID"           | [UaGuid]       |
| UaStringNodeId  | "STRING"         | [String]       |
| UaOpaqueNodeId  | "OPAQUE"         | [UaByteString] |

### UaExtensionObject

Třída ```UaExtensionObject``` je abstraktní datový typ s podtypy: ```UaByteExtensionObject``` a ```UaXmlExtensionObject```. Výsledkem převodu je objekt tvaru:

```
{
  "enType" : [Number],
  "body" : [String],
  "encodingId" : [UaNodeId]
}
```

kde hodnota atributu ```enType``` určuje daný podtyp (při deserializaci lze díky němu určit, zda se v  ```body``` 
nachází hodnota ```UaXmlElement``` či ```UaByteString```).

| datový typ            | hodnota "enType" |
| :-------------------- | :--------------- |
| UaByteExtensionObject | "BYTE"           |
| UaXmlExtensionObject  | "XML             |

### UaExpandedNodeId

Převod na objekt tvaru:

```
{
  "id" : [UaNodeId],
  "serverIndex" : [Number]
}
```


## OPC UA Binary

Nebo též OPC UA Binary DataEncoding, či ve zdrojovém kódu této knihovny se uvádí i OPC UA Standard Binary, je binární 
formát dat. Je definován společností OPC Foundation a lze ho nalézt v [online dokumentaci](https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.1/),
tudíž zde nebude rozepsáno kódovaní hodnot. Knihovna v současné verzi podporuje OPC UA verze 1.04.