# Scalable OPC UA

Knihovna částečně implementující [OPC UA](https://reference.opcfoundation.org/). Soustředí se na klientskou část a práci 
s daty.

Tento dokument shrnuje podstatné myšlenky návrhu a uvádí případné vývojářské manuály.