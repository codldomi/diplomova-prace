## Scalable OPC UA

Version 0.5.0

The library implements the [OPC UA standard](https://reference.opcfoundation.org/). It mainly focuses on the client-side and data manipulating, e.g. data encoding,
metamodel description. It's written in Scala language.

---

### Language

- [Scala](https://www.scala-lang.org/) 3.3.1
- conventions as [Scala Style Guide](https://docs.scala-lang.org/style/)
- building tool [sbt](https://www.scala-sbt.org/) 1.9.6
- JDK 17 [Eclipse Temurin by Adoptium](https://adoptium.net/temurin/releases/)

### Development Environment

- [IntelliJ IDEA](https://www.jetbrains.com/idea/) (Community or Ultimate) with [plugin Scala](https://www.jetbrains.com/help/idea/discover-intellij-idea-for-scala.html)

### Documentation

- in Czech language
- web pages generated from [Markdown](https://www.markdownguide.org/) with tool [MkDocs](https://www.mkdocs.org/)
- in terminal executing ```mkdocs build``` creates directory ```site``` with documentation

### Publishing

- in terminal ```sbt publish``` creates files for publishing to [Maven repository](https://maven.apache.org/index.html) 
specified in ```build.sbt```
- in terminal ```sbt publishLocal``` creates files for publishing to local [Maven repository](https://maven.apache.org/index.html)
often located in ```/home/<user>/.m2/``` (more info [here](https://www.baeldung.com/maven-local-repository))

### Functionality

- OPC UA version 1.04 
- sdk:
  - asynchronous client
  - anonymous client or username plus password
  - encrypting, certificates enabled
- utilities:
  - JsonEncoder
  - JsonDecoder
  - JsonSchemaGenerator
  - JsonValidator
  - BinaryEncoder
  - BinaryDecoder
  - StatusCodeParser
  - NodeSetExtractor
  - TrustListManager
  - ValueCreator
- data:
  - data types (built-in, custom structures and enums)
  - values (general structure and enum without code generating)
- conversion
  - implicit conversion between data types
