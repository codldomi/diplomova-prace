//----------------------------------------------------------------------------------------------------------------------
//- BUILD
//  tutorials and tips about Scala libraries: https://docs.scala-lang.org/overviews/contributors/index.html

scalaVersion := "3.3.1"

//  authentication for Sonatype Nexus Repository Manager
credentials := Seq(Credentials(Path.userHome / ".sbt" / ".credentials"))

resolvers ++= Seq(
  "jitpack.io" at "https://jitpack.io",
)

libraryDependencies ++= Seq(
  //  BASIC
  "org.scalactic" %% "scalactic" % "3.2.15",

  //  OPC UA
  "io.netty" % "netty-all" % "4.1.82.Final",
  "org.eclipse.milo" % "sdk-client" % "0.6.8",
  "org.eclipse.milo" % "sdk-server" % "0.6.8",

  //  JSON
  "net.jimblackler.jsonschemafriend" % "core" % "0.11.4",
  "com.typesafe.play" %% "play-json" % "2.10.0-RC9",

  //  XML
  "org.scala-lang.modules" %% "scala-xml" % "2.2.0",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.14.3",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-xml" % "2.14.3",

  //  LOGGER
  "ch.qos.logback" % "logback-classic" % "1.4.7" % "test",

  //  TEST
  "org.scalatest" %% "scalatest" % "3.2.15" % "test"
)

scalacOptions ++= Seq("-feature", "-language:implicitConversions")

//----------------------------------------------------------------------------------------------------------------------
//- PUBLISH

version := "0.5.0"

//  used as artifactId
name := "scalable-opc-ua"

//  used as groupId
organization := "cz.modemtec"

description := "A library implementing OPC UA standard. It's focused on the client-side and the data handling."

versionScheme := Some("early-semver")

publishMavenStyle := true

licenses := Seq("MIT License" -> url("http://www.opensource.org/licenses/mit-license.php"))

homepage := Some(url("http://gitlab.intranet.modemtec.cz/products/server/pdds/scalable-opc-ua"))
scmInfo :=
  Some(
    ScmInfo(
      url("http://gitlab.intranet.modemtec.cz/products/server/pdds/scalable-opc-ua"),
      "git@gitlab.intranet.modemtec.cz:products/server/pdds/scalable-opc-ua.git"))

developers := List(
  Developer(id="dominik.codl", name="Dominik Codl", email="dominik.codl@modemtec.cz", url=url("https://modemtec.cz/cs/"))
)

//----------------------------------------------------------------------------------------------------------------------