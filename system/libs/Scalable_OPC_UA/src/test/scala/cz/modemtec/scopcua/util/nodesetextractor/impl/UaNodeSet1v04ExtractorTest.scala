package cz.modemtec.scopcua.util.nodesetextractor.impl

import com.ctc.wstx.stax.{WstxInputFactory, WstxOutputFactory}
import com.fasterxml.jackson.dataformat.xml.{XmlFactory, XmlMapper}

import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation
import cz.modemtec.scopcua.data.UaDataType.GeneralStructure.Field
import cz.modemtec.scopcua.data.UaDataType.GeneralEnumeration
import cz.modemtec.scopcua.data.UaDataType.GeneralStructure
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.node.*
import cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel.*

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import javax.xml.stream.XMLInputFactory
import scala.io.Source
import scala.xml.{Node, XML}


class UaNodeSet1v04ExtractorTest extends AnyFunSuite with Matchers with UaNodeSet1v04Extractor:

  private val ModemTec = "http://www.modemtec.cz/PD/"

  private val mapper =
    val input = new WstxInputFactory()
    input.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false)
    new XmlMapper(new XmlFactory(input, new WstxOutputFactory()))

  def loadXml(path: String): Node =
    val text = Source.fromResource(path).mkString
    XML.loadString(text)


  test("DataTypes from zero namespace"):
    val xml = loadXml("nodeset/Opc.Ua.NodeSet2.xml")

    val result = extract(xml)

    val selected = UaDataType.BuiltIns.values

    result.modelUri shouldBe "http://opcfoundation.org/UA/"
    selected.foreach(expected => result.dataTypes should contain value (expected))


  test("DataTypes from IEC-61850-7-3"):
    val typesCount = 51
    val xml = loadXml("nodeset/Opc.Ua.IEC61850-7-3.NodeSet2.xml")
    val result = extract(xml)

    result.dataTypes.toSeq.length shouldBe typesCount

    val selected =
      Seq(
        GeneralEnumeration(
          isAbstract = false,
          name       = "SequenceKind",
          id         = UaId(101, "http://opcfoundation.org/UA/IEC61850-7-3"),
          superId    = Some(UaId(29, "http://opcfoundation.org/UA/")),
          "pos-neg-zero" -> 0,
          "dir-quad-zero" -> 1),
        GeneralStructure(
          isAbstract = false,
          name       = "ScaledValueConfig",
          id         = UaId(107, "http://opcfoundation.org/UA/IEC61850-7-3"),
          superId    = Some(UaId(22, "http://opcfoundation.org/UA/")),
          Field(
            name       = "scaleFactor",
            typeId     = UaId(10, "http://opcfoundation.org/UA/"),
            valueRank  = -1,
            arrayDimensions = Vector.empty,
            isOptional = false,
            ord        = 0),
          Field(
            name       = "offset",
            typeId     = UaId(10, "http://opcfoundation.org/UA/"),
            valueRank  = -1,
            arrayDimensions = Vector.empty,
            isOptional = false,
            ord        = 1)))

    result.modelUri shouldBe "http://opcfoundation.org/UA/IEC61850-7-3"
    selected.foreach(expected => result.dataTypes should contain value expected)


  test("DataTypes from IEC-61850-7-4"):
    val typesCount = 46
    val xml = loadXml("nodeset/Opc.Ua.IEC61850-7-4.NodeSet2.xml")
    val result = extract(xml)

    result.modelUri shouldBe "http://opcfoundation.org/UA/IEC61850-7-4"
    result.dataTypes.toSeq.length shouldBe typesCount


  test("Forbidden from zero namespace"):
    val xml =  loadXml("nodeset/Opc.Ua.NodeSet2.xml")
    val result = extract(xml)

    val forbidden =
      Vector(
        UaId(298, OpcUaFoundation)) // /Root/Types/DataTypes/OPC Binary/Opc.Ua/Argument/Default Binary

    forbidden.foreach: id =>
      result.nodes.keySet should not contain id


  test("Production XMLs: ModemTec"):
    val xml = loadXml("nodeset/ModemTec.PDM1.NodeSet2.xml")
    val result = extract(xml)

    val pdm1 =
      UaObjectNode(
        id          = UaId(5015, ModemTec),      // /Root/Objects/PDM1
        parentId    = Some(UaId(85, OpcUaFoundation)),
        browseName  = "PDM1",
        displayName = "PDM1",
        description = None)

    val setup =
      UaMethodNode(
        id          = UaId(7001, ModemTec),       // /Root/Objects/PDM1/CONF/Calib/setup
        parentId    = Some(UaId(5017, ModemTec)), // /Root/Objects/PDM1/CONF/Calib
        browseName  = "setup",
        displayName = "setup",
        description = None,
        input =
          Vector(
            UaArgument(
              isInput         = true,
              name            = "channel",
              typeId          = UaId(3, OpcUaFoundation), //  UaByte
              valueRank       = -1,
              arrayDimensions = Vector.empty,
              ord           = 0),
            UaArgument(
              isInput         = true,
              name            = "attenuator",
              typeId          = UaId(3, OpcUaFoundation), //  UaByte
              valueRank       = -1,
              arrayDimensions = Vector.empty,
              ord           = 1),
            UaArgument(
              isInput         = true,
              name            = "reference",
              typeId          = UaId(7, OpcUaFoundation), // UaUInt32
              valueRank       = -1,
              arrayDimensions = Vector.empty,
              ord           = 2)),
        output =
          Vector(
            UaArgument(
              isInput         = false,
              name            = "result",
              typeId          = UaId(3002, ModemTec), //  MtRequestResult
              valueRank       = -1,
              arrayDimensions = Vector.empty,
              ord           = 0)))

    val bands =
      UaVariableNode(
        id              = UaId(6028, ModemTec),       // /Root/Objects/PDM1/CONF/bands
        parentId        = Some(UaId(5016, ModemTec)), // /Root/Objects/PDM1/CONF
        browseName      = "bands",
        displayName     = "bands",
        description     = None,
        typeId          = UaId(7, OpcUaFoundation),   //  UAUInt32
        valueRank       = 2,
        arrayDimensions = Vector(0, 8))

    result.nodes(bands.id) shouldBe bands
    result.nodes(setup.id) shouldBe setup
    result.nodes(pdm1.id) shouldBe pdm1

    val forbidden =
      Vector(
        UaId(1010, ModemTec), // /Root/Types/ObjectTypes/BaseObjectType/ModemTecBaseObjectType/CONF
        UaId(5002, ModemTec), // /Root/Types/ObjectTypes/BaseObjectType/ModemTecBaseObjectType/CONF/Calib
        UaId(6159, ModemTec), // /Root/Types/ObjectTypes/BaseObjectType/ModemTecBaseObjectType/CONF/extSync
        UaId(5005, ModemTec), // /Root/Types/ObjectsTypes/BaseObjectType/ModemTecBaseObjectType/CONF/NoiseTh/
        UaId(6137, ModemTec), // /Root/Types/ObjectsTypes/BaseObjectType/ModemTecBaseObjectType/CONF/NoiseTh/coeffs
        UaId(6018, ModemTec), // /Root/Types/DataTypes/BaseDataType/Enumeration/MtGridFreq/EnumStrings
        UaId(6016, ModemTec)) // /Root/Types/DataTypes/OPC Binary/TypeDictionary/NamespaceUri

    result.modelUri shouldBe "http://www.modemtec.cz/PD/"

    forbidden.foreach: id =>
      result.nodes.keySet should not contain id
