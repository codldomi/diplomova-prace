package cz.modemtec.scopcua.test.dataset

import cz.modemtec.scopcua.data.UaDataType
import cz.modemtec.scopcua.data.value.{UaString, UaValue}
import cz.modemtec.scopcua.data.value.number.integer.{UaByte, UaUInt16}
import cz.modemtec.scopcua.conversion.ConvertToUa.FromPair.given Conversion[?, ?]


object DataSet:

  private case class Data(name: String, binaries: Vector[Vector[UaByte]], jsons: Vector[String], values: Vector[UaValue], dataType: UaDataType)

  case class Set(name: String, records: Vector[Record], dataType: UaDataType)
  case class Record(binary: Vector[UaByte], json: String, value: UaValue)

  private val data =
    Vector(
      Data("Bytes",             BinaryDataSet.Bytes,             JsonDataSet.Bytes,             ValueDataSet.Bytes,             UaDataType.Byte),
      Data("SBytes",            BinaryDataSet.SBytes,            JsonDataSet.SBytes,            ValueDataSet.SBytes,            UaDataType.SByte),
      Data("UInt16s",           BinaryDataSet.UInt16s,           JsonDataSet.UInt16s,           ValueDataSet.UInt16s,           UaDataType.UInt16),
      Data("Int16s",            BinaryDataSet.Int16s,            JsonDataSet.Int16s,            ValueDataSet.Int16s,            UaDataType.Int16),
      Data("UInt32s",           BinaryDataSet.UInt32s,           JsonDataSet.UInt32s,           ValueDataSet.UInt32s,           UaDataType.UInt32),
      Data("Int32s",            BinaryDataSet.Int32s,            JsonDataSet.Int32s,            ValueDataSet.Int32s,            UaDataType.Int32),
      Data("UInt64s",           BinaryDataSet.UInt64s,           JsonDataSet.UInt64s,           ValueDataSet.UInt64s,           UaDataType.UInt64),
      Data("Int64s",            BinaryDataSet.Int64s,            JsonDataSet.Int64s,            ValueDataSet.Int64s,            UaDataType.Int64),
      Data("Floats",            BinaryDataSet.Floats,            JsonDataSet.Floats,            ValueDataSet.Floats,            UaDataType.Float),
      Data("Doubles",           BinaryDataSet.Doubles,           JsonDataSet.Doubles,           ValueDataSet.Doubles,           UaDataType.Double),
      Data("DateTimes",         BinaryDataSet.DateTimes,         JsonDataSet.DateTimes,         ValueDataSet.DateTimes,         UaDataType.DateTime),
      Data("ByteStrings",       BinaryDataSet.ByteStrings,       JsonDataSet.ByteStrings,       ValueDataSet.ByteStrings,       UaDataType.ByteString),
      Data("Strings",           BinaryDataSet.Strings,           JsonDataSet.Strings,           ValueDataSet.Strings,           UaDataType.String),
      Data("Guids",             BinaryDataSet.Guids,             JsonDataSet.Guids,             ValueDataSet.Guids,             UaDataType.Guid),
      Data("XmlElements",       BinaryDataSet.XmlElements,       JsonDataSet.XmlElements,       ValueDataSet.XmlElements,       UaDataType.XmlElement),
      Data("QualifiedNames",    BinaryDataSet.QualifiedNames,    JsonDataSet.QualifiedNames,    ValueDataSet.QualifiedNames,    UaDataType.QualifiedName),
      Data("LocalizedTexts",    BinaryDataSet.LocalizedTexts,    JsonDataSet.LocalizedTexts,    ValueDataSet.LocalizedTexts,    UaDataType.LocalizedText),
      Data("NodeIds",           BinaryDataSet.NodeIds,           JsonDataSet.NodeIds,           ValueDataSet.NodeIds,           UaDataType.NodeId),
      Data("DiagnosticInfos",   BinaryDataSet.DiagnosticInfos,   JsonDataSet.DiagnosticInfos,   ValueDataSet.DiagnosticInfos,   UaDataType.DiagnosticInfo),
      Data("ExpandedNodeIds",   BinaryDataSet.ExpandedNodeIds,   JsonDataSet.ExpandedNodeIds,   ValueDataSet.ExpandedNodeIds,   UaDataType.ExpandedNodeId),
      Data("ExtensionObjects",  BinaryDataSet.ExtensionObjects,  JsonDataSet.ExtensionObjects,  ValueDataSet.ExtensionObjects,  UaDataType.Structure),
      Data("Variants",          BinaryDataSet.Variants,          JsonDataSet.Variants,          ValueDataSet.Variants,          UaDataType.BaseDataType),
      Data("DataValues",        BinaryDataSet.DataValues,        JsonDataSet.DataValues,        ValueDataSet.DataValues,        UaDataType.DataValue),
      Data("Qualities",         BinaryDataSet.Qualities,         JsonDataSet.Qualities,         ValueDataSet.Qualities,         DataTypeDataSet.Quality))

  val All: Vector[Set] =
    data.map: data =>
      val records = data
        .values
        .zip(data.binaries)
        .zip(data.jsons)
        .map: (tuple, str) =>
          val value = tuple._1
          val binary = tuple._2
          val json = str
          Record(binary, json, value)
        
      Set(data.name, records, data.dataType)
    

  val Namespaces: Map[UaUInt16, UaString] =
    Map(
          0 -> "http://opcfoundation.org/UA/",
          1 -> "http://opcfoundation.org/UA/IEC61850-7-3",
          2 -> "http://opcfoundation.org/UA/IEC61850-7-4",
        255 -> "ns255",
      32767 -> "ns32767",
      65535 -> "ns65535",
      65407 -> "ns65407")
