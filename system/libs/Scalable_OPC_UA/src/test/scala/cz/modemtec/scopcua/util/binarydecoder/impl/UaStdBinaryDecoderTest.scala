package cz.modemtec.scopcua.util.binarydecoder.impl

import cz.modemtec.scopcua.test.dataset.{DataSet, DataTypeDataSet, StatusCodeDataSet}
import cz.modemtec.scopcua.util.binarydecoder.UaBinaryDecoder

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class UaStdBinaryDecoderTest extends AnyFunSuite with Matchers:

  val data = DataSet.All
  val dataTypes = DataTypeDataSet.All
  val statusCodes = StatusCodeDataSet.All

  val decoder: UaBinaryDecoder = UaStdBinaryDecoder(dataTypes)

  data.foreach: data =>
    test(data.name):
      data.records.foreach: record =>
        val result = decoder.decode(record.binary, data.dataType.id)
        val expected = record.value
        result shouldBe expected


