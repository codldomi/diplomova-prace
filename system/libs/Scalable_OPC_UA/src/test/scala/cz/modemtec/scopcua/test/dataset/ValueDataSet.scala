package cz.modemtec.scopcua.test.dataset

import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation
import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.data.value.number.*
import cz.modemtec.scopcua.data.value.number.integer.*


object ValueDataSet:

  val Booleans: Vector[UaBoolean] = Vector(true, false, false)

  val Bytes: Vector[UaByte] = Vector(0, 10, 255)

  val SBytes: Vector[UaSByte] = Vector(-128, 0, 127)

  val UInt16s: Vector[UaUInt16] = Vector(0, 65535, 32767)

  val Int16s: Vector[UaInt16] = Vector(-32768, 0, 32767)

  val UInt32s: Vector[UaUInt32] = Vector(0, 4294967295L, 2147483647L)

  val Int32s: Vector[UaInt32] = Vector(-2147483648, 0, 2147483647)

  val UInt64s: Vector[UaUInt64] = Vector(BigInt("0"), BigInt("18446744073709551615"), BigInt("9223372036854775807"))

  val Int64s: Vector[UaInt64] = Vector(-9223372036854775807L, 0L, 9223372036854775807L)

  val Floats: Vector[UaFloat] = Vector(1.17549e-38f, 0.0f, 3.40282e38f)

  val Doubles: Vector[UaDouble] = Vector(2.3e-308d, 0.0, 1.7e308d)

  val Strings: Vector[UaString] = Vector("string", "", "helloWorld")

  val DateTimes: Vector[UaDateTime] = Vector(0L, 9223372036850000000L)

  val ByteStrings: Vector[UaByteString] =
    Vector(
      UaByteString(115, 116, 114, 105, 110, 103),
      UaByteString(104, 101, 108, 108, 111, 87, 111, 114, 108, 100),
      UaByteString())

  val Guids: Vector[UaGuid] =
    Vector(
      UaGuid(3298187146L, 3582, 19343, Vector(135, 10, 116, 82, 56, 198, 174, 174)),
      UaGuid(4294967295L, 65535, 65535, Vector(255, 255, 255, 255, 255, 255, 255, 255)),
      UaGuid())

  val XmlElements: Vector[UaXmlElement] =
    Vector(
      UaXmlElement("string"),
      UaXmlElement(),
      UaXmlElement("helloWorld"))

  val QualifiedNames: Vector[UaQualifiedName] =
    Vector(
      UaQualifiedName(
        name           = "lowest",
        namespaceIndex = 0),
      UaQualifiedName(
        name           = "secondHighest",
        namespaceIndex =  32767),
      UaQualifiedName(
        name           = "maximum",
        namespaceIndex = 65535))

  val LocalizedTexts: Vector[UaLocalizedText] =
    Vector(
      UaLocalizedText(
        text   = "lowest",
        locale = "locale"),
      UaLocalizedText(
        text   = "secondHighest",
        locale = "text"),
      UaLocalizedText(
        text   = "",
        locale = ""))

  val NodeIds: Vector[UaNodeId] =
    Vector(
      UaNumericNodeId(
        namespaceIndex = 0,
        identifier     = 255),
      UaNumericNodeId(
        namespaceIndex = 255,
        identifier     = 65535),
      UaNumericNodeId(
        namespaceIndex = 32767,
        identifier     = 2147483647),
      UaStringNodeId(
        namespaceIndex = 32767,
        identifier = "string"),
      UaGuidNodeId(
        namespaceIndex = 65407,
        identifier     = UaGuid(3298187146L, 3582, 19343, Vector(135, 10, 116, 82, 56, 198, 174, 174))),
      UaOpaqueNodeId(
        namespaceIndex = 65535,
        identifier     = UaByteString(115, 116, 114, 105, 110, 103)))

  val DiagnosticInfos: Vector[UaDiagnosticInfo] =
    Vector(
      UaDiagnosticInfo(
        symbolicId          = None,
        namespaceUri        = None,
        localizedText       = None,
        locale              = None,
        additionalInfo      = None,
        innerStatusCode     = None,
        innerDiagnosticInfo = None),
      UaDiagnosticInfo(
        symbolicId          = None,
        namespaceUri        = Some(150),
        localizedText       = None,
        locale              = Some(1000),
        additionalInfo      = Some(""),
        innerStatusCode     = Some(UaStatusCode(0, "Good")),
        innerDiagnosticInfo = None),
      UaDiagnosticInfo(
        symbolicId          = None,
        namespaceUri        = None,
        localizedText       = Some(20),
        locale              = None,
        additionalInfo      = Some("string"),
        innerStatusCode     = None,
        innerDiagnosticInfo =
          Some(
            UaDiagnosticInfo(
              symbolicId          = None,
              namespaceUri        = Some(2147483647),
              localizedText       = Some(-1),
              locale              = None,
              additionalInfo      = None,
              innerStatusCode     = None,
              innerDiagnosticInfo = None))))

  val ExpandedNodeIds: Vector[UaExpandedNodeId] =
    Vector(
      UaExpandedNodeId(
        namespaceUri = OpcUaFoundation,
        nodeId       = UaNumericNodeId(0, 4294967295L),
        serverIndex  = 0),
      UaExpandedNodeId(
        namespaceUri = OpcUaFoundation,
        nodeId       = UaStringNodeId(0, "string"),
        serverIndex  = 4294967295L),
      UaExpandedNodeId(
        namespaceUri = OpcUaFoundation,
        nodeId       = UaNumericNodeId(0, 65535),
        serverIndex  = 2147483647))

  val ExtensionObjects: Vector[UaExtensionObject] =
    Vector(
      UaByteExtensionObject(
        encodingId = UaNumericNodeId(
          namespaceIndex = 0,
          identifier     = 6),
        body = UaByteString(0, 0, 0, 0)),
      UaByteExtensionObject(
        encodingId = UaNumericNodeId(
          namespaceIndex = 0,
          identifier     = 0),
        body = UaByteString()),
      UaXmlExtensionObject(
        encodingId = UaNumericNodeId(
          namespaceIndex = 0,
          identifier     = 12),
        body = "element"))

  val Variants: Vector[UaVariant] =
    Vector(
      UaVariant(
        UaBuiltInId.Int32,
        Some(UaInt32(-2147483648))),
      UaVariant(
        UaBuiltInId.Null,
        None),
      UaVariant(
        UaBuiltInId.Double,
        Some(UaArray(
          UaArray(1d, 2d, 3d),
          UaArray(4d, 5d, 6d),
          UaArray(7d, 8d, 9d)))))

  val DataValues: Vector[UaDataValue] =
    Vector(
      UaDataValue(
        value             = None,
        status            = Some(UaStatusCode(0, "Good")),
        sourceTimestamp   = Some(9223372036850000000L),
        sourcePicoSeconds = Some(6500),
        serverTimestamp   = None,
        serverPicoSeconds = None),
      UaDataValue(
        value             = Some(UaVariant(UaBuiltInId.Int32, Some(UaInt32(-2147483648)))),
        status            = Some(UaStatusCode(0, "Good")),
        sourceTimestamp   = Some(0L),
        sourcePicoSeconds = None,
        serverTimestamp   = Some(9223372036850000000L),
        serverPicoSeconds = Some(1000)),
      UaDataValue(
        value             = None,
        status            = None,
        sourceTimestamp   = None,
        sourcePicoSeconds = None,
        serverTimestamp   = None,
        serverPicoSeconds = None))

  val Qualities: Vector[UaStructure] =
    Vector(
      UaStructure(
        "validity" -> UaEnumeration(1, "invalid"),
        "detailQual" ->
          UaStructure(
            "overflow"     -> UaBoolean(true),
            "outOfRange"   -> UaBoolean(false),
            "badReference" -> UaBoolean(true),
            "oscillatory"  -> UaBoolean(false),
            "failure"      -> UaBoolean(true),
            "oldData"      -> UaBoolean(false),
            "inconsistent" -> UaBoolean(true),
            "inaccurate"   -> UaBoolean(false)),
        "source" -> UaEnumeration(0, "process"),
        "test"-> UaBoolean(false),
        "operatorBlocked" -> UaBoolean(true)))

