package cz.modemtec.scopcua.sdk.client.impl

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.config.{UaClientConfig, UaSecurityPolicy}
import cz.modemtec.scopcua.data.value.number.integer.UaInt32
import cz.modemtec.scopcua.data.value.{UaBoolean, UaEnumeration, UaString, UaStructure, UaValue}
import cz.modemtec.scopcua.test.dataset.DataTypeDataSet
import cz.modemtec.scopcua.test.tag.NeedsRunningServer

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt


class UaAnonymousMiloClientTest extends UaMiloClientTest:

  override def createClient: UaMiloClient = 
    val types = DataTypeDataSet.All
    val config =
      UaClientConfig(
        name = "Scalable Test Client",
        uri = "urn:scalable:test:client",
        timeout = 5000,
        url = "opc.tcp://127.0.0.1:4840",
        auth = None,
        encryption = None,
        policy = UaSecurityPolicy.None)

    UaMiloClient.from(config, types)


  test("Read history of Quality[IEC73] - node /PDM1/SPDC1/Beh/q", NeedsRunningServer):
    val expected =
      Vector(
        UaStructure(
          "validity" -> UaEnumeration(0, "good"),
          "detailQual" ->
            UaStructure(
              "overflow"     -> UaBoolean(false),
              "outOfRange"   -> UaBoolean(false),
              "badReference" -> UaBoolean(false),
              "oscillatory"  -> UaBoolean(false),
              "failure"      -> UaBoolean(false),
              "oldData"      -> UaBoolean(false),
              "inconsistent" -> UaBoolean(false),
              "inaccurate"   -> UaBoolean(false)),
          "source" -> UaEnumeration(0, "process"),
          "test" -> UaBoolean(false),
          "operatorBlocked" -> UaBoolean(false)),
        UaStructure(
          "validity" -> UaEnumeration(1, "invalid"),
          "detailQual" ->
            UaStructure(
              "overflow"     -> UaBoolean(true),
              "outOfRange"   -> UaBoolean(true),
              "badReference" -> UaBoolean(true),
              "oscillatory"  -> UaBoolean(true),
              "failure"      -> UaBoolean(false),
              "oldData"      -> UaBoolean(false),
              "inconsistent" -> UaBoolean(false),
              "inaccurate"   -> UaBoolean(false)),
          "source" -> UaEnumeration(0, "process"),
          "test" -> UaBoolean(true),
          "operatorBlocked" -> UaBoolean(true)),
        UaStructure(
          "validity" -> UaEnumeration(0, "good"),
          "detailQual" ->
            UaStructure(
              "overflow"     -> UaBoolean(false),
              "outOfRange"   -> UaBoolean(false),
              "badReference" -> UaBoolean(false),
              "oscillatory"  -> UaBoolean(false),
              "failure"      -> UaBoolean(false),
              "oldData"      -> UaBoolean(false),
              "inconsistent" -> UaBoolean(false),
              "inaccurate"   -> UaBoolean(false)),
          "source" -> UaEnumeration(0, "process"),
          "test" -> UaBoolean(false),
          "operatorBlocked" -> UaBoolean(false)))

    val result = withClient: client =>
      var collected = Vector.empty[UaValue]
      val id = UaId(6058, "http://www.modemtec.cz/PD/")
      val from = stringToUaDateTime("1601-01-01T00:00:00Z")
      val to = stringToUaDateTime("2025-01-01T00:00:00Z")

      val action = client.readHistory(id, from, to): readValue =>
        collected = collected :+ readValue.value.get

      Await.result(action, 60.second)
      collected

    result shouldBe expected
  

  test("Read data of BehaviourModeKing[IEC74] - node /PDM1/LLN0/Beh/stVal", NeedsRunningServer):
    val expected = UaEnumeration(3, "test")

    val result = withClient: client =>
      val id = UaId(6002, "http://www.modemtec.cz/PD/")
      val readResult = client.readValue(id).asSync
      readResult.value.get

    result shouldBe expected
  

  test("Read data and source time of BehaviourModeKing[IEC74] - node /PDM1/LLN0/Beh/stVal", NeedsRunningServer):
    val result = withClient: client =>
      val id = UaId(6002, "http://www.modemtec.cz/PD/")
      val readResult = client.readValue(id).asSync
      readResult.sourceTime
    

    result.isDefined shouldBe true
  

  test("Read data of Quality[IEC73] - node /PDM1/LLN0/Beh/q", NeedsRunningServer):
    val expected =
      UaStructure(
        "validity" -> UaEnumeration(1, "invalid"),
        "detailQual" ->
          UaStructure(
            "overflow"     -> UaBoolean(true),
            "outOfRange"   -> UaBoolean(false),
            "badReference" -> UaBoolean(true),
            "oscillatory"  -> UaBoolean(false),
            "failure"      -> UaBoolean(true),
            "oldData"      -> UaBoolean(false),
            "inconsistent" -> UaBoolean(true),
            "inaccurate"   -> UaBoolean(false)),
          "source" -> UaEnumeration(0, "process"),
          "test" -> UaBoolean(false),
          "operatorBlocked" -> UaBoolean(true))

    val result = withClient: client =>
      val id = UaId(6001, "http://www.modemtec.cz/PD/")
      val readResult = client.readValue(id).asSync
      readResult.value.get

    result shouldBe expected
  


  test("Call Method /PDM1/Conf/changePass - result OK", NeedsRunningServer):
    val objectId = UaId(5016, "http://www.modemtec.cz/PD/")
    val method = "changePass"

    val input = Map(
      UaString("username") -> UaString("username"),
      UaString("password") -> UaString("password"))

    val expected = Map[UaString, UaValue]("result" -> UaEnumeration(0, "Ok"))

    val result = withClient: client =>
      client.callMethod(objectId, method, input).asSync

    result shouldBe expected
  

  test("Call Method /PDM1/Conf/changePass - result InvalidArgument", NeedsRunningServer):
    val objectId = UaId(5016, "http://www.modemtec.cz/PD/")
    val method = "changePass"

    val input = Map(
      UaString("username") -> UaString(),
      UaString("password") -> UaString("password"))

    val expected = Map[UaString, UaValue]("result" -> UaEnumeration(3, "InvalidArgument"))

    val result = withClient: client =>
      client.callMethod(objectId, method, input).asSync
    
    result shouldBe expected


  test("Write-Read-Write-Read empty value - node /PDM1/SPDC1/Beh/q", NeedsRunningServer):
    val id = UaId(6058, "http://www.modemtec.cz/PD/")

    withClient: client =>
      var value: Option[UaValue] = None
      val origin = client.readValue(id).asSync.value

      client.writeValue(id, None).asSync
      value = client.readValue(id).asSync.value

      value shouldBe None

      client.writeValue(id, origin).asSync
      value = client.readValue(id).asSync.value

      value shouldBe origin


  test("Write-Read-Write-Read data of Quality[IEC73] - node /PDM1/LLN0/Beh/q", NeedsRunningServer):
    val id = UaId(6001, "http://www.modemtec.cz/PD/")

    val written: Option[UaValue] =
      Some(
        UaStructure(
          "validity" -> UaEnumeration(1, "invalid"),
          "detailQual" ->
            UaStructure(
              "overflow"     -> UaBoolean(true),
              "outOfRange"   -> UaBoolean(true),
              "badReference" -> UaBoolean(true),
              "oscillatory"  -> UaBoolean(true),
              "failure"      -> UaBoolean(true),
              "oldData"      -> UaBoolean(true),
              "inconsistent" -> UaBoolean(true),
              "inaccurate"   -> UaBoolean(true)),
          "source" -> UaEnumeration(0, "process"),
          "test"   -> UaBoolean(true),
          "operatorBlocked" -> UaBoolean(true)))

    withClient: client =>
      val origin = client.readValue(id).asSync.value

      client.writeValue(id, written).asSync
      var value = client.readValue(id).asSync.value

      value shouldBe written

      client.writeValue(id, origin).asSync
      value = client.readValue(id).asSync.value

      value shouldBe origin


  test("Subscription of UaInt32 - node /PDM1/SPDC1/OpCnt/stVal", NeedsRunningServer):

    //  todo: why exception "OpcUaSubscriptionManager -- Publish service failure" happens during the subscription
    //        cancellation? The client seems to be functional, but in debug log there is this exception...

    val variableId = UaId(6057, "http://www.modemtec.cz/PD/")

    val expected = Vector[UaInt32](0, 1, 2, 3, 4)

    val result = withClient: client =>
      var values = Vector.empty[UaValue]
      val subId = client.createSubscription(10.0).asSync
      val monId =
        client
          .createMonitoredItem(subId, variableId): 
            case (readValue, _, _) =>
              values = values :+ readValue.value.get
          .asSync

      Thread.sleep(10000) // 10 seconds
        
      client.deleteMonitoredItem(subId, monId).asSync // Monitored Item deleting (just for test)
      client.deleteSubscription(subId).asSync         // Subscription deleting (also deletes all Monitored Item in this Subscription)

      values

    result containsSlice expected
  


