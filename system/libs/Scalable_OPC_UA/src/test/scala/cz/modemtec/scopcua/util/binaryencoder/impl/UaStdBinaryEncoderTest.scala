package cz.modemtec.scopcua.util.binaryencoder.impl

import cz.modemtec.scopcua.test.dataset.{DataSet, DataTypeDataSet}
import cz.modemtec.scopcua.util.binaryencoder.UaBinaryEncoder

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class UaStdBinaryEncoderTest extends AnyFunSuite with Matchers:

  val data = DataSet.All
  val dataTypes = DataTypeDataSet.All

  val encoder: UaBinaryEncoder = UaStdBinaryEncoder(dataTypes)

  data.foreach: data =>
    test(data.name): 
      data.records.foreach: record =>
        val result = encoder.encode(record.value, data.dataType.id)
        val expected = record.binary
        result shouldBe expected
      
        

