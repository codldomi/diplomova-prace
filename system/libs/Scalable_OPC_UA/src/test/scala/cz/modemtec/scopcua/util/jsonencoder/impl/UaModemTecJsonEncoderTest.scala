package cz.modemtec.scopcua.util.jsonencoder.impl

import cz.modemtec.scopcua.test.dataset.DataSet

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json


class UaModemTecJsonEncoderTest extends AnyFunSuite with Matchers:

  val data = DataSet.All
  val namespaces = DataSet.Namespaces

  val encoder: UaModemTecJsonEncoder = UaModemTecJsonEncoder(namespaces)

  data.foreach: data =>
    test(data.name):
      data.records.foreach: record =>
        val result = encoder.encode(record.value)
        val expected = Json.parse(record.json)
        result shouldBe expected
        
