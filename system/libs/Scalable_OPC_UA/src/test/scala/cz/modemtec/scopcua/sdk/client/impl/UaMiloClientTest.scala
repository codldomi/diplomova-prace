package cz.modemtec.scopcua.sdk.client.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}


trait UaMiloClientTest extends AnyFunSuite with Matchers:

  given context: ExecutionContext = ExecutionContext.global

  extension [A](future: Future[A])
    def asSync: A = Await.result(future, 240.seconds)

  def createClient: UaMiloClient

  def withClient[A](code: UaMiloClient => A): A =
    val client = createClient
    var isConnected = false

    try
      client.connect().asSync
      isConnected = true
      code(client)
    finally
      if isConnected then client.disconnect().asSync


