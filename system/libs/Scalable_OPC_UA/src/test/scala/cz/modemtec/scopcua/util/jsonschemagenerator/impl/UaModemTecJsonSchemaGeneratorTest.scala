package cz.modemtec.scopcua.util.jsonschemagenerator.impl

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaDataType.GeneralStructure.Field
import cz.modemtec.scopcua.data.node.{UaArgument, UaMethodNode, UaVariableNode}
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.test.dataset.DataTypeDataSet
import cz.modemtec.scopcua.util.jsonschemagenerator.UaJsonSchemaGenerator

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import play.api.libs.json.Json


class UaModemTecJsonSchemaGeneratorTest extends AnyFunSuite with Matchers:

  /*
      default settings
   */
  val types = DataTypeDataSet.All
  val generator: UaJsonSchemaGenerator = UaModemTecJsonSchemaGenerator(types)


  test("VariableValue schema - 2D array"):
    val node =
      UaVariableNode(
        id              = UaId(1, "http://www.modemtec.cz/PD/"),
        parentId        = None,
        browseName      = "MyVariable",
        displayName     = "MyVariable",
        description     = None,
        typeId          = UaDataType.Int32.id,
        valueRank       = 0,
        arrayDimensions = Vector(7, 8))

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://www.modemtec.cz/PD/foo/MyVariable",
      "title"   -> "VariableValue",
      "type"    -> "array",
      "items" -> Json.obj(
        "type"  -> "array",
        "items" -> Json.obj(
          "$ref" -> "http://opcfoundation.org/UA/Int32"),
        "minItems" -> 8,
        "maxItems" -> 8),
      "minItems" -> 7,
      "maxItems" -> 7)

    val result = generator.generate(node, "/foo/MyVariable")
    result shouldBe expected


  test("VariableValue schema - 1D array"):
    val node =
      UaVariableNode(
        id              = UaId(1, "http://www.modemtec.cz/PD/"),
        parentId        = None,
        browseName      = "MyVariable",
        displayName     = "MyVariable",
        description     = None,
        typeId          = UaDataType.Int32.id,
        valueRank       = 0,
        arrayDimensions = Vector(7))

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id" -> "http://www.modemtec.cz/PD/foo/MyVariable",
      "title" -> "VariableValue",
      "type" -> "array",
      "items" ->  Json.obj(
        "$ref" -> "http://opcfoundation.org/UA/Int32"),
      "minItems" -> 7,
      "maxItems" -> 7)

    val result = generator.generate(node, "/foo/MyVariable")
    result shouldBe expected


  test("MethodInput/Output schema"):
    val node =
      UaMethodNode(
        id          = UaId(1, "http://www.modemtec.cz/PD"),
        parentId    = None,
        browseName  = "MyMethod",
        displayName = "MyMethod",
        description = None,
        input =
          Vector(
            UaArgument(
              name      = "One",
              isInput   = true,
              typeId    = UaDataType.Int32.id,
              valueRank = 0,
              arrayDimensions = Vector(7, 8),
              ord       = 0),
            UaArgument(
              name      = "Two",
              isInput   = true,
              typeId    = UaDataType.Int32.id,
              valueRank = 0,
              arrayDimensions = Vector.empty,
              ord       = 1)),
        output = Vector.empty)

    val input = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://www.modemtec.cz/PD/foo/MyMethod/Input",
      "title"   -> "MethodInput",
      "type"    -> "object",
      "properties" -> Json.obj(
        "One" -> Json.obj(
          "type" -> "array",
          "items" -> Json.obj(
            "type" -> "array",
            "items" -> Json.obj(
              "$ref" -> "http://opcfoundation.org/UA/Int32"),
            "minItems" -> 8,
            "maxItems" -> 8),
          "minItems" -> 7,
          "maxItems" -> 7),
        "Two" -> Json.obj(
          "$ref" -> "http://opcfoundation.org/UA/Int32")),
      "required" -> Json.arr("One", "Two")
    )

    val expected = (Some(input), None)
    val result = generator.generate(node, "/foo/MyMethod")

    result shouldBe expected


  test("Schema of UInt32"):
    val result = generator.generate(UaDataType.UInt32)

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://opcfoundation.org/UA/UInt32",
      "title"   -> "UInt32",
      "type"    -> "number")

    result shouldBe expected


  test("Schema of NodeId"):
    val result = generator.generate(UaDataType.NodeId)

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://opcfoundation.org/UA/NodeId",
      "title"   -> "NodeId",
      "type"    -> "object",
      "properties" -> Json.obj(
        "uri" -> Json.obj(
          "$ref" -> "http://opcfoundation.org/UA/String"),
        "idType" -> Json.obj(
          "enum" -> Json.arr("NUMERIC", "GUID", "OPAQUE", "STRING"),
          "type" -> "string"),
        "id" -> Json.obj(
          "anyOf" -> Json.arr(
            Json.obj("$ref" -> "http://opcfoundation.org/UA/UInt32"),
            Json.obj("$ref" -> "http://opcfoundation.org/UA/String"),
            Json.obj("$ref" -> "http://opcfoundation.org/UA/ByteString"),
            Json.obj("$ref" -> "http://opcfoundation.org/UA/Guid")))),
      "required" -> Json.arr("idType", "uri", "id"))

    result shouldBe expected


  test("Schema of DateTime"):
    val result = generator.generate(UaDataType.DateTime)

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://opcfoundation.org/UA/DateTime",
      "title"   -> "DateTime",
      "type"    -> "string",
      "format"  -> "date-time")

    result shouldBe expected


  test("Schema of DataValue"):
    val result = generator.generate(UaDataType.DataValue)

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://opcfoundation.org/UA/DataValue",
      "title"   -> "DataValue",
      "type"    -> "object",
      "properties" -> Json.obj(
        "value"             -> Json.obj("$ref" -> "http://opcfoundation.org/UA/BaseDataType"),
        "status"            -> Json.obj("$ref" -> "http://opcfoundation.org/UA/StatusCode"),
        "serverTimestamp"   -> Json.obj("$ref" -> "http://opcfoundation.org/UA/DateTime"),
        "serverPicoSeconds" -> Json.obj("$ref" -> "http://opcfoundation.org/UA/UInt16"),
        "sourceTimestamp"   -> Json.obj("$ref" -> "http://opcfoundation.org/UA/DateTime"),
        "sourcePicoSeconds" -> Json.obj("$ref" -> "http://opcfoundation.org/UA/UInt16")),
      "required" -> Json.arr())

    result shouldBe expected


  test("Schema of AnalogueValue"):
    val result = generator.generate(DataTypeDataSet.AnalogValue)

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://opcfoundation.org/UA/IEC61850-7-3/AnalogValue",
      "title"   -> "AnalogValue",
      "type"    -> "object",
      "properties" -> Json.obj(
        "i" -> Json.obj("$ref" -> "http://opcfoundation.org/UA/Int32"),
        "f" -> Json.obj("$ref" -> "http://opcfoundation.org/UA/Float")),
      "required" -> Json.arr())

    result shouldBe expected


  test("Number of schema uris"):
    val schemas = types.map: (id, dataType) => 
      (id, generator.generate(dataType)) 
      
    val expected = schemas.size

    val result =
      schemas
        .values
        .map(schema => (schema \ "$id").as[String])
        .toSet
        .size

    result shouldBe expected


  test("Recursive custom DataType"):
    val types = UaDataType.BuiltIns

    val dt =
      UaDataType.GeneralStructure(
        isAbstract = false,
        name = "RecStruct",
        id = UaId(1, "http://opcfoundation.org/UA/Test"),
        superId = Some(UaDataType.Structure.id),
        Field(
          name       = "a",
          typeId     = UaDataType.Int32.id,
          valueRank  = -1,
          arrayDimensions = Vector.empty,
          isOptional = false,
          ord        = 0),
        Field(
          name       = "b",
          typeId     = UaId(1, "http://opcfoundation.org/UA/Test"),
          valueRank  = -1,
          arrayDimensions = Vector.empty,
          isOptional = false,
          ord        = 1))

    val generator = UaModemTecJsonSchemaGenerator(types + (dt.id -> dt))
    val result = generator.generate(dt)

    val expected = Json.obj(
      "$schema" -> "https://json-schema.org/draft/2020-12/schema",
      "$id"     -> "http://opcfoundation.org/UA/Test/RecStruct",
      "title"   -> "RecStruct",
      "type"    -> "object",
      "properties" -> Json.obj(
        "a" -> Json.obj("$ref" -> "http://opcfoundation.org/UA/Int32"),
        "b" -> Json.obj("$ref" -> "#")),
      "required" -> Json.arr("a", "b"))

    result shouldBe expected


  test("Built-ins without exception"):
    types.values.map: dataType =>
      generator.generate(dataType)

