package cz.modemtec.scopcua.test.dataset

import cz.modemtec.scopcua.data.value.{UaStatusCode, UaString}
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32


object StatusCodeDataSet:

  val All: Map[UaUInt32, UaStatusCode] = Map(UaUInt32(0) -> UaStatusCode(UaUInt32(0), UaString("Good")))

