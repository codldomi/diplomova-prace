package cz.modemtec.scopcua.util.statuscodeparser.impl

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import cz.modemtec.scopcua.data.value.{UaStatusCode, UaString}
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

import scala.io.Source


class UaCsvStatusCodeParserTest extends AnyFunSuite with Matchers with UaCsvStatusCodeParser:

  private def loadText(): String = Source.fromResource("StatusCode.csv").mkString


  test("Selected status codes."):
    val text = loadText()
    val result = parse(text)

    result(UaUInt32(0))           shouldBe UaStatusCode(UaUInt32(0), UaString("Good"))
    result(UaUInt32(2161442816L)) shouldBe UaStatusCode(UaUInt32(2161442816L), UaString("BadAggregateNotSupported"))
    result(UaUInt32(67633152L))   shouldBe UaStatusCode(UaUInt32(67633152L), UaString("GoodInitiateFaultState"))


  // Helping code for generating built-in status codes
  /*
  {
    val text = loadText()
    val codes = _parser.parse(text)

    val sortedCodes =
      codes
        .values
        .toVector
        .sortWith(_.symbol.value < _.symbol.value)        // sort status codes by name
        .groupBy(status =>                                // set priority in order to sort groups (good ones, bad ones...)
          if (status.symbol.value.startsWith("Good")) 0
          else if (status.symbol.value.startsWith("Bad")) 1
          else 2
        )
        .toVector
        .sortWith(_._1 < _._1)                            // sort by priority
        .flatMap(_._2)                                    // flatten sorted groups

    sortedCodes.foreach(status => {                              // write status codes
      val name = status.symbol.value
      val value = status.code.value
      println(s"val $name: UaStatusCode = UaStatusCode(UaUInt32(0x${value.toHexString.toUpperCase}L), UaString(\"$name\"))")
    })

    println("----------------")

    sortedCodes.foreach(status => println("UaStatusCode." + status.symbol.value  + ","))
  }
  */
