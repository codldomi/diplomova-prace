package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.conversion.ConvertToUa.FromInt.intToUaInt32

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import java.util


class UaArrayTest extends AnyFunSuite with Matchers:

  case class Data(name: String, array: UaArray, dimensions: Vector[Int], flatten: UaArray)

  val dataSet: Seq[Data] =
    Seq(
      Data(
        "empty array",
        UaArray(),
        Vector.empty,
        UaArray()),

      Data(
        "one-dimensional - 2",
        UaArray(1, 2),
        Vector(2),
        UaArray(1, 2)),

      Data(
        "two-dimensional - 3x3",
        UaArray(
          UaArray(1, 2, 3),
          UaArray(4, 5, 6),
          UaArray(7, 8, 9)),
        Vector(3, 3),
        UaArray(1, 2, 3, 4, 5, 6, 7, 8, 9)),

      Data(
        "three-dimensional - 3x2x2",
        UaArray(
          UaArray(
            UaArray(1, 2),
            UaArray(3, 4)),
          UaArray(
            UaArray(5, 6),
            UaArray(7, 8)),
          UaArray(
            UaArray(9, 10),
            UaArray(11, 12))),
        Vector(3, 2, 2),
        UaArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)))


  def methodFlatten(data: Data): Unit = 
    test(s"flatten: ${data.name}"):
      val expected = data.flatten
      val result = data.array.flatten
      result should equal (expected)
  

  def methodDimensions(data: Data): Unit = 
    test(s"dimensions: ${data.name}"):
      val expected = data.dimensions
      val result = data.array.dimensions
      result should equal (expected)
  
  
  def methodOfDim(data: Data): Unit = 
    test(s"ofDim: ${data.name}"):
      val expected = data.array
      val result = UaArray.ofDim(data.dimensions: _*)(data.flatten.values: _*)
      result should equal (expected)


  dataSet.foreach(methodFlatten)
  dataSet.foreach(methodDimensions)
  dataSet.foreach(methodOfDim)


