package cz.modemtec.scopcua.test.tag

import org.scalatest.Tag

/** A test needs running OPC UA server.
  */
object NeedsRunningServer extends Tag("NeedsRunningServer")
