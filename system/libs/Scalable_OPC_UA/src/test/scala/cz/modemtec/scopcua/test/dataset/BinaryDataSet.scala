package cz.modemtec.scopcua.test.dataset

import cz.modemtec.scopcua.data.value.number.integer.UaByte
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]


object BinaryDataSet {

  val Booleans: Vector[Vector[UaByte]] =
    Vector(
      Vector(1),
      Vector(0),
      Vector(0))

  val Bytes: Vector[Vector[UaByte]] =
    Vector(
      Vector(0),
      Vector(10),
      Vector(255))

  val SBytes: Vector[Vector[UaByte]] =
    Vector(
      Vector(128),
      Vector(0),
      Vector(127))

  val Int16s: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 128),
      Vector(0, 0),
      Vector(255, 127))

  val UInt16s: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 0),
      Vector(255, 255),
      Vector(255, 127))

  val Int32s: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 0, 0, 128),
      Vector(0, 0, 0, 0),
      Vector(255, 255, 255, 127))

  val UInt32s: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 0, 0, 0),
      Vector(255, 255, 255, 255),
      Vector(255, 255, 255, 127))

  val Int64s: Vector[Vector[UaByte]] =
    Vector(
      Vector(1, 0, 0, 0, 0, 0, 0, 128),
      Vector(0, 0, 0, 0, 0, 0, 0, 0),
      Vector(255, 255, 255, 255, 255, 255, 255, 127))

  val UInt64s: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 0, 0, 0, 0, 0, 0, 0),
      Vector(255, 255, 255, 255, 255, 255, 255, 255),
      Vector(255, 255, 255, 255, 255, 255, 255, 127))

  val Floats: Vector[Vector[UaByte]] =
    Vector(
      Vector(225, 255, 127, 0),
      Vector(0, 0, 0, 0),
      Vector(238, 255, 127, 127))

  val Doubles: Vector[Vector[UaByte]] =
    Vector(
      Vector(176, 202, 110, 71, 237, 137, 16, 0),
      Vector(0, 0, 0, 0, 0, 0, 0, 0),
      Vector(118, 59, 119, 48, 209, 66, 238, 127))

  val DateTimes: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 0, 0, 0, 0, 0, 0, 0),
      Vector(128, 32, 183, 255, 255, 255, 255, 127))

  val ByteStrings: Vector[Vector[UaByte]] =
    Vector(
      Vector(6, 0, 0, 0, 115, 116, 114, 105, 110, 103),
      Vector(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100),
      Vector(255, 255, 255, 255))

  val Strings: Vector[Vector[UaByte]] =
    Vector(
      Vector(6, 0, 0, 0, 115, 116, 114, 105, 110, 103),
      Vector(255, 255, 255, 255),
      Vector(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100))

  val Guids: Vector[Vector[UaByte]] =
    Vector(
      Vector(138, 87, 150, 196, 254, 13, 143, 75, 135, 10, 116, 82, 56, 198, 174, 174),
      Vector(255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255),
      Vector(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

  val XmlElements: Vector[Vector[UaByte]] =
    Vector(
      Vector(6, 0, 0, 0, 115, 116, 114, 105, 110, 103),
      Vector(255, 255, 255, 255),
      Vector(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100))

  val QualifiedNames: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 0, 6, 0, 0, 0, 108, 111, 119, 101, 115, 116),
      Vector(255, 127, 13, 0, 0, 0, 115, 101, 99, 111, 110, 100, 72, 105, 103, 104, 101, 115, 116),
      Vector(255, 255, 7, 0, 0, 0, 109, 97, 120, 105, 109, 117, 109))

  val LocalizedTexts: Vector[Vector[UaByte]] =
    Vector(
      Vector(3, 6, 0, 0, 0, 108, 111, 99, 97, 108, 101, 6, 0, 0, 0, 108, 111, 119, 101, 115, 116),
      Vector(3, 4, 0, 0, 0, 116, 101, 120, 116, 13, 0, 0, 0, 115, 101, 99, 111, 110, 100, 72, 105, 103, 104, 101, 115, 116),
      Vector(3, 255, 255, 255, 255, 255, 255, 255, 255))

  val NodeIds: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 255),
      Vector(1, 255, 255, 255),
      Vector(2, 255, 127, 255, 255, 255, 127),
      Vector(3, 255, 127, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103),
      Vector(4, 127, 255, 138, 87, 150, 196, 254, 13, 143, 75, 135, 10, 116, 82, 56, 198, 174, 174),
      Vector(5, 255, 255, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103))

  val DiagnosticInfos: Vector[Vector[UaByte]] =
    Vector(
      Vector(0),
      Vector(58, 150, 0, 0, 0, 232, 3, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0),
      Vector(84, 20, 0, 0, 0, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103, 6, 255, 255, 255, 127, 255, 255, 255, 255))

  val ExpandedNodeIds: Vector[Vector[UaByte]] =
    Vector(
      Vector(130, 0, 0, 255, 255, 255, 255, 28, 0, 0, 0, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47),
      Vector(195, 0, 0, 6, 0, 0, 0, 115, 116, 114, 105, 110, 103, 28, 0, 0, 0, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47, 255, 255, 255, 255),
      Vector(193, 0, 255, 255, 28, 0, 0, 0, 104, 116, 116, 112, 58, 47, 47, 111, 112, 99, 102, 111, 117, 110, 100, 97, 116, 105, 111, 110, 46, 111, 114, 103, 47, 85, 65, 47, 255, 255, 255, 127))

  val ExtensionObjects: Vector[Vector[UaByte]] =
    Vector(
      Vector(0, 6, 1, 4, 0, 0, 0, 0, 0, 0, 0),
      Vector(0, 0, 0),
      Vector(0, 12, 2, 7, 0, 0, 0, 101, 108, 101, 109, 101, 110, 116))

  val Variants: Vector[Vector[UaByte]] =
    Vector(
      Vector(6, 0, 0, 0, 128),
      Vector(0),
      Vector(203, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 8, 64, 0, 0, 0, 0, 0, 0, 16, 64, 0, 0, 0, 0, 0, 0, 20, 64, 0, 0, 0, 0, 0, 0, 24, 64, 0, 0, 0, 0, 0, 0, 28, 64, 0, 0, 0, 0, 0, 0, 32, 64, 0, 0, 0, 0, 0, 0, 34, 64, 2, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 0))

  val DataValues: Vector[Vector[UaByte]] =
    Vector(
      Vector(22, 0, 0, 0, 0, 128, 32, 183, 255, 255, 255, 255, 127, 100, 25),
      Vector(47, 6, 0, 0, 0, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 32, 183, 255, 255, 255, 255, 127, 232, 3),
      Vector(0))

  val Qualities: Vector[Vector[UaByte]] =
    Vector(
      Vector(1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1))

}
