package cz.modemtec.scopcua.util.jsonvalidator.impl

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.test.dataset.{DataSet, DataTypeDataSet}
import cz.modemtec.scopcua.util.jsonschemagenerator.impl.UaModemTecJsonSchemaGenerator
import cz.modemtec.scopcua.util.jsonvalidator.UaJsonValidator

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import play.api.libs.json.{JsValue, Json}


class UaFriendJsonValidatorTest extends AnyFunSuite with Matchers:

  val data = DataSet.All
  val dataTypes = DataTypeDataSet.All
  val generator: UaModemTecJsonSchemaGenerator = UaModemTecJsonSchemaGenerator(dataTypes)

  val schemas: Map[UaId, JsValue] =
    dataTypes
      .values
      .map(dataType => dataType.id -> generator.generate(dataType))
      .toMap

  val validator: UaJsonValidator = UaFriendJsonValidator(schemas)

  data.foreach: set =>
    val typeId = set.dataType.id
    test(set.name):
      set.records.foreach: record =>
        val json = Json.parse(record.json)
        val result = validator.validate(json, typeId)
        result shouldBe true

