package cz.modemtec.scopcua.test.dataset


object JsonDataSet:

  val Booleans: Vector[String] = Vector("true", "false", "false")

  val Bytes: Vector[String] = Vector("0", "10", "255")

  val SBytes: Vector[String] = Vector("-128", "0", "127")

  val UInt16s: Vector[String] = Vector("0", "65535", "32767")

  val Int16s: Vector[String] = Vector("-32768", "0", "32767")

  val UInt32s: Vector[String] = Vector("0", "4294967295", "2147483647")

  val Int32s: Vector[String] = Vector("-2147483648", "0", "2147483647")

  val UInt64s: Vector[String] = Vector("0", "18446744073709551615", "9223372036854775807")

  val Int64s: Vector[String] = Vector("-9223372036854775807", "0", "9223372036854775807")

  val Floats: Vector[String] = Vector("1.1754900067970481E-38", "0.0", "3.402820018375656E38")

  val Doubles: Vector[String] = Vector("2.3E-308", "0.0", "1.7E308")

  val Strings: Vector[String] = Vector("\"string\"", "\"\"", "\"helloWorld\"")

  val DateTimes: Vector[String] = Vector(""""1601-01-01T00:00:00Z"""", """"30828-09-14T02:48:05Z"""")

  val ByteStrings: Vector[String] =
    Vector(
      "\"c3RyaW5n\"",
      "\"aGVsbG9Xb3JsZA==\"",
      "\"\"")

  val Guids: Vector[String] =
    Vector(
      """"C496578A-0DFE-4B8F-870A-745238C6AEAE"""",
      """"FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF"""",
      """"00000000-0000-0000-0000-000000000000"""")

  val XmlElements: Vector[String] = Vector("\"string\"", "\"\"", "\"helloWorld\"")

  val QualifiedNames: Vector[String] =
    Vector(
      """{
        | "name" : "lowest",
        | "uri"  : "http://opcfoundation.org/UA/"
        |}""",
      """{
        | "name" : "secondHighest",
        | "uri"  : "ns32767"
        |}""",
      """{
        | "name" : "maximum",
        | "uri"  : "ns65535"
        |}""")
      .map(_.stripMargin)

  val LocalizedTexts: Vector[String] =
    Vector(
      """{
        | "text"   : "lowest",
        | "locale" : "locale"
        |}""",
      """{
        | "text"   : "secondHighest",
        | "locale" : "text"
        |}""",
      """{
        | "text"   : "",
        | "locale" : ""
        |}""")
      .map(_.stripMargin)

  val NodeIds: Vector[String] =
    Vector(
      """{
        | "idType" : "NUMERIC",
        | "uri"    : "http://opcfoundation.org/UA/",
        | "id"     : 255
        |}""",
      """{
        | "idType" : "NUMERIC",
        | "uri"    : "ns255",
        | "id"     : 65535
        |}""",
      """{
        | "idType" : "NUMERIC",
        | "uri"    : "ns32767",
        | "id"     : 2147483647
        |}""",
      """{
        | "idType" : "STRING",
        | "uri"    : "ns32767",
        | "id"     : "string"
        |}""",
      """{
        | "idType" : "GUID",
        | "uri"    : "ns65407",
        | "id"     : "C496578A-0DFE-4B8F-870A-745238C6AEAE"
        |}""",
      """{
        | "idType" : "OPAQUE",
        | "uri"    : "ns65535",
        | "id"     : "c3RyaW5n"
        |}""")
      .map(_.stripMargin)

  val DiagnosticInfos: Vector[String] =
    Vector(
      """{
        |}""",
      """{
        | "namespaceUri"        : 150,
        | "locale"              : 1000,
        | "additionalInfo"      : "",
        | "innerStatusCode"     : { "code": 0, "symbol": "Good" }
        |}""",
      """{
        | "localizedText"       : 20,
        | "additionalInfo"      : "string",
        | "innerDiagnosticInfo" : {
        |   "namespaceUri"        : 2147483647,
        |   "localizedText"       : -1
        | }
        |}""")
      .map(_.stripMargin)

  val ExpandedNodeIds: Vector[String] =
    Vector(
      """{
        | "serverIndex" : 0,
        | "id" : {
        |   "idType" : "NUMERIC",
        |   "uri" : "http://opcfoundation.org/UA/",
        |   "id" : 4294967295
        | }
        |}""",
      """{
        | "serverIndex" : 4294967295,
        | "id" : {
        |   "idType" : "STRING",
        |   "uri" : "http://opcfoundation.org/UA/",
        |   "id" : "string"
        | }
        |}""",
      """{
        | "serverIndex" : 2147483647,
        | "id" : {
        |   "idType" : "NUMERIC",
        |   "uri" : "http://opcfoundation.org/UA/",
        |   "id" : 65535
        | }
        |}""")
      .map(_.stripMargin)

  val ExtensionObjects: Vector[String] =
    Vector(
      """{
        | "enType" : "BYTE",
        | "encodingId" : {
        |   "idType" : "NUMERIC",
        |   "uri" : "http://opcfoundation.org/UA/",
        |   "id" : 6
        |  },
        |  "body" : "AAAAAA=="
        |}""",
      """{
        | "enType" : "BYTE",
        | "encodingId" : {
        |   "idType" : "NUMERIC",
        |   "uri" : "http://opcfoundation.org/UA/",
        |   "id" : 0
        |  },
        |  "body" : ""
        |}""",
      """{
        | "enType" : "XML",
        | "encodingId" : {
        |   "idType" : "NUMERIC",
        |   "uri" : "http://opcfoundation.org/UA/",
        |   "id" : 12
        |  },
        |  "body" : "element"
        |}""")
      .map(_.stripMargin)

  val Variants: Vector[String] =
    Vector(
      """{
        | "varType" : "INT32",
        | "value" : -2147483648
        |}""",
      """{
        | "varType" : "NULL"
        |}""",
      """{
        | "varType" : "DOUBLE",
        | "value" : [ [1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0] ]
        |}""")
      .map(_.stripMargin)

  val DataValues: Vector[String] =
    Vector(
      """{
        | "status"            : { "code" : 0, "symbol" : "Good" },
        | "sourceTimestamp"   : "30828-09-14T02:48:05Z",
        | "sourcePicoSeconds" : 6500
        |}""",
      """{
        | "value"             : { "varType" : "INT32", "value" : -2147483648 },
        | "status"            : { "code" : 0, "symbol" : "Good" },
        | "sourceTimestamp"   : "1601-01-01T00:00:00Z",
        | "serverTimestamp"   : "30828-09-14T02:48:05Z",
        | "serverPicoSeconds" : 1000
        |}""",
      """{
        |}""")
      .map(_.stripMargin)

  val Qualities: Vector[String] =
    Vector(
      """{
        | "validity" : { "value": 1, "name": "invalid" },
        | "detailQual" : {
        |   "overflow"     : true,
        |   "outOfRange"   : false,
        |   "badReference" : true,
        |   "oscillatory"  : false,
        |   "failure"      : true,
        |   "oldData"      : false,
        |   "inconsistent" : true,
        |   "inaccurate"   : false
        | },
        | "source" : { "value": 0, "name": "process" },
        | "test"  : false,
        | "operatorBlocked" : true
        |}""")
      .map(_.stripMargin)

