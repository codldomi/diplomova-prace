package cz.modemtec.scopcua.conversion

import cz.modemtec.scopcua.test.dataset.ValueDataSet

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class ConvertToUaTest extends AnyFunSuite with Matchers with ConvertToUa.FromString:

  private val data =
    ValueDataSet.Guids
      .zip(
        Vector(
          "C496578A-0DFE-4B8F-870A-745238C6AEAE",
          "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF",
          "00000000-0000-0000-0000-000000000000"))

  data.foreach: (guid, str) =>
    test(str + " to Guid"):
      val result = stringToUaGuid(str)
      val expected = guid
      result shouldBe expected
