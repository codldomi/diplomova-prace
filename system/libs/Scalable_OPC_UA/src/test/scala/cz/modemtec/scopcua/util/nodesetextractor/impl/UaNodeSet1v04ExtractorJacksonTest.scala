package cz.modemtec.scopcua.util.nodesetextractor.impl

import com.ctc.wstx.stax.{WstxInputFactory, WstxOutputFactory}
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.dataformat.xml.*
import cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel.*
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import javax.xml.stream.XMLInputFactory


class UaNodeSet1v04ExtractorJacksonTest extends AnyFunSuite with Matchers:

  private val mapper =
    val input = new WstxInputFactory()
    input.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, java.lang.Boolean.FALSE)
    new XmlMapper(new XmlFactory(input, new WstxOutputFactory()))


  test("Extract DataType"):
    val xml =
      """<UADataType NodeId="ns=1;i=3003" BrowseName="1:MtGridFreq">
        |  <DisplayName>MtGridFreq</DisplayName>
        |  <References>
        |    <Reference ReferenceType="HasProperty">ns=1;i=6018</Reference>
        |    <Reference ReferenceType="HasSubtype" IsForward="false">i=29</Reference>
        |  </References>
        |  <Definition Name="1:MtGridFreq">
        |    <Field SymbolicName="S50Hz" Name="50Hz" Value="0"/>
        |    <Field SymbolicName="S60Hz" Name="60Hz" Value="1"/>
        |  </Definition>
        |</UADataType>
        |""".stripMargin

    val result = mapper.readValue(xml, classOf[UADataType])

    println(result)


  test("Extract Variable"):
    val xml =
      """    <UAVariable DataType="Argument" ParentNodeId="ns=1;i=7017" ValueRank="1" NodeId="ns=1;i=6142" ArrayDimensions="3" BrowseName="InputArguments">
        |        <DisplayName>InputArguments</DisplayName>
        |        <References>
        |            <Reference ReferenceType="HasModellingRule">i=78</Reference>
        |            <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
        |            <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=7017</Reference>
        |        </References>
        |        <Value>
        |            <uax:ListOfExtensionObject>
        |                <uax:ExtensionObject>
        |                    <uax:TypeId>
        |                        <uax:Identifier>i=297</uax:Identifier>
        |                    </uax:TypeId>
        |                    <uax:Body>
        |                        <uax:Argument>
        |                            <uax:Name>channel</uax:Name>
        |                            <uax:DataType>
        |                                <uax:Identifier>i=3</uax:Identifier>
        |                            </uax:DataType>
        |                            <uax:ValueRank>-1</uax:ValueRank>
        |                            <uax:ArrayDimensions/>
        |                            <uax:Description/>
        |                        </uax:Argument>
        |                    </uax:Body>
        |                </uax:ExtensionObject>
        |                <uax:ExtensionObject>
        |                    <uax:TypeId>
        |                        <uax:Identifier>i=297</uax:Identifier>
        |                    </uax:TypeId>
        |                    <uax:Body>
        |                        <uax:Argument>
        |                            <uax:Name>attenuator</uax:Name>
        |                            <uax:DataType>
        |                                <uax:Identifier>i=3</uax:Identifier>
        |                            </uax:DataType>
        |                            <uax:ValueRank>-1</uax:ValueRank>
        |                            <uax:ArrayDimensions/>
        |                            <uax:Description/>
        |                        </uax:Argument>
        |                    </uax:Body>
        |                </uax:ExtensionObject>
        |                <uax:ExtensionObject>
        |                    <uax:TypeId>
        |                        <uax:Identifier>i=297</uax:Identifier>
        |                    </uax:TypeId>
        |                    <uax:Body>
        |                        <uax:Argument>
        |                            <uax:Name>reference</uax:Name>
        |                            <uax:DataType>
        |                                <uax:Identifier>i=7</uax:Identifier>
        |                            </uax:DataType>
        |                            <uax:ValueRank>-1</uax:ValueRank>
        |                            <uax:ArrayDimensions/>
        |                            <uax:Description/>
        |                        </uax:Argument>
        |                    </uax:Body>
        |                </uax:ExtensionObject>
        |            </uax:ListOfExtensionObject>
        |        </Value>
        |    </UAVariable>""".stripMargin

    val result = mapper.readValue(xml, classOf[UAVariable])

    println(result)


  test("Extract Argument"):
    val xml =
      """<uax:Argument xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:uax="http://opcfoundation.org/UA/2008/02/Types.xsd" xmlns="http://opcfoundation.org/UA/2011/03/UANodeSet.xsd" xmlns:s1="http://www.modemtec.cz/PD/Types.xsd" xmlns:s2="http://opcfoundation.org/UA/IEC61850-7-4/Types.xsd" xmlns:s3="http://opcfoundation.org/UA/IEC61850-7-3/Types.xsd" xmlns:ua="http://unifiedautomation.com/Configuration/NodeSet.xsd" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        |                            <uax:Name>channel</uax:Name>
        |                            <uax:DataType>
        |                                <uax:Identifier>i=3</uax:Identifier>
        |                            </uax:DataType>
        |                            <uax:ValueRank>-1</uax:ValueRank>
        |                            <uax:ArrayDimensions/>
        |                            <uax:Description/>
        |                        </uax:Argument>""".stripMargin

    val result = mapper.readValue(xml, classOf[Argument])

    println(result)



