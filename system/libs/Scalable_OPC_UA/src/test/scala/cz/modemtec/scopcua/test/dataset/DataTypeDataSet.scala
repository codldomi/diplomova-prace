package cz.modemtec.scopcua.test.dataset

import cz.modemtec.scopcua.data.UaDataType.{GeneralEnumeration, GeneralStructure}
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaDataType.GeneralStructure.*
import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation


object DataTypeDataSet:

  val IEC_61580_7_3 = "http://opcfoundation.org/UA/IEC61850-7-3"
  val IEC_61580_7_4 = "http://opcfoundation.org/UA/IEC61850-7-4"
  val MODEMTEC = "http://www.modemtec.cz/PD/"

  val DetailQual: GeneralStructure =
    GeneralStructure(
      isAbstract = false,
      name = "DetailQual",
      id = UaId(23, IEC_61580_7_3),
      superId = Some(UaId(22, OpcUaFoundation)),
      Field(
        name       = "overflow",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 0),
      Field(
        name       = "outOfRange",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 1),
      Field(
        name       = "badReference",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 2),
      Field(
        name       = "oscillatory",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 3),
      Field(
        name       = "failure",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 4),
      Field(
        name       = "oldData",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 5),
      Field(
        name       = "inconsistent",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 6),
      Field(
        name       = "inaccurate",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 7))

  val ValidityKind: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "ValidityKind",
      id         = UaId(22, IEC_61580_7_3),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "good"         -> 0,
      "invalid"      -> 1,
      "reserved"     -> 2,
      "questionable" -> 3)

  val SourceKind: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "SourceKind",
      id         = UaId(24, IEC_61580_7_3),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "process"     -> 0,
      "substituted" -> 1)

  val Quality: GeneralStructure =
    GeneralStructure(
      isAbstract = false,
      name       = "Quality",
      id         = UaId(17, IEC_61580_7_3),
      superId    = Some(UaId(22, OpcUaFoundation)),
      Field(
        name       = "validity",
        typeId     = UaId(22, IEC_61580_7_3),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 0),
      Field(
        name       = "detailQual",
        typeId     = UaId(23, IEC_61580_7_3),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 1),
      Field(
        name       = "source",
        typeId     = UaId(24, IEC_61580_7_3),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 2),
      Field(
        name       = "test",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 3),
      Field(
        name       = "operatorBlocked",
        typeId     = UaId(1, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 4))

  val Timestamp: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "TimeStamp",
      id         = UaId(5, IEC_61580_7_3),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "source"  -> 0,
      "server"  -> 1,
      "both"    -> 2,
      "neither" -> 3,
      "invalid" -> 4)

  val SiUnitKind: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "SIUnitKind",
      id         = UaId(97, IEC_61580_7_3),
      superId    = Some(UaId(29, OpcUaFoundation)),
      ""         -> 1,
      "meter"    -> 2,
      "kilogram" -> 3,
      "second"   -> 4,
      "amper"    -> 5,
      "kelvin"   -> 6,
      "mole"     -> 7,
      "candela"  -> 8,
      "degree"   -> 9,
      "radian"   -> 10)

  val MultiplierKind: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "MultiplierKind",
      id         = UaId(81, IEC_61580_7_3),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "mili"  -> -3,
      "centi" -> -2,
      "deci"  -> -1,
      ""      -> 0,
      "deca"  -> 1,
      "hecto" -> 2,
      "kilo"  -> 3)

  val AnalogValue: GeneralStructure =
    GeneralStructure(
      isAbstract = false,
      name       = "AnalogValue",
      id         = UaId(117, IEC_61580_7_3),
      superId    = Some(UaId(22, OpcUaFoundation)),
      Field(
        name       = "i",
        typeId     = UaId(6, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = true,
        ord        = 0),
      Field(
        name       = "f",
        typeId     = UaId(10, OpcUaFoundation),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = true,
        ord        = 1))

  val Unit: GeneralStructure =
    GeneralStructure(
      isAbstract = false,
      name       = "Unit",
      id         = UaId(133, IEC_61580_7_3),
      superId    = Some(UaId(22, OpcUaFoundation)),
      Field(
        name       = "SIUnit",
        typeId     = UaId(97, IEC_61580_7_3),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = false,
        ord        = 0),
      Field(
        name       = "multiplier",
        typeId     = UaId(81, IEC_61580_7_3),
        valueRank  = -1,
        arrayDimensions = Vector.empty,
        isOptional = true,
        ord        = 1))

  val BehaviourModeKind: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "BehaviourModeKind",
      id         = UaId(5, IEC_61580_7_4),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "on"          -> 1,
      "blocked"     -> 2,
      "test"        -> 3,
      "testBlocked" -> 4,
      "off"         -> 5)

  val HealthKind: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "HealthKind",
      id         = UaId(25, IEC_61580_7_4),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "ok"      -> 1,
      "warning" -> 2,
      "alarm"   -> 3)

  val MtRequestResult: GeneralEnumeration =
    GeneralEnumeration(
      isAbstract = false,
      name       = "MtRequestResult",
      id         = UaId(3002, MODEMTEC),
      superId    = Some(UaId(29, OpcUaFoundation)),
      "Ok"      -> 0,
      "Error"   -> 1,
      "Busy"    -> 2,
      "InvalidArgument" -> 3,
      "Timeout" -> 4,
      "Access"  -> 5)

  val All: Map[UaId, UaDataType] = 

    val custom =
      Map(
        Quality.id           -> Quality,
        DetailQual.id        -> DetailQual,
        ValidityKind.id      -> ValidityKind,
        SourceKind.id        -> SourceKind,
        Timestamp.id         -> Timestamp,
        SiUnitKind.id        -> SiUnitKind,
        MultiplierKind.id    -> MultiplierKind,
        AnalogValue.id       -> AnalogValue,
        Unit.id              -> Unit,
        BehaviourModeKind.id -> BehaviourModeKind,
        HealthKind.id        -> HealthKind,
        MtRequestResult.id   -> MtRequestResult)

    UaDataType.BuiltIns ++ custom
  