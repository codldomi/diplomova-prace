package cz.modemtec.scopcua.sdk.client.impl

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.config.{UaAuthConfig, UaClientConfig, UaEncryptionConfig, UaSecurityPolicy}
import cz.modemtec.scopcua.data.value.{UaArray, UaString}
import cz.modemtec.scopcua.test.dataset.DataTypeDataSet
import cz.modemtec.scopcua.test.tag.NeedsRunningServer
import cz.modemtec.scopcua.util.trustlistmanager.impl.UaMemoryTrustListManager

import org.eclipse.milo.opcua.stack.core.util.CertificateUtil

import java.security.{KeyPair, KeyStore, PrivateKey}
import java.security.cert.X509Certificate


class UaSecureMiloClientTest extends UaMiloClientTest:

  override def createClient: UaMiloClient = 
    val keyStorePasswd = "password".toCharArray
    val keyStoreAlias = "secureClient"

    //  Encoding Configuration

    val types = DataTypeDataSet.All

    //  Encryption Configuration

    val ks = KeyStore.getInstance("PKCS12")
    val is = getClass.getClassLoader.getResourceAsStream("client/ks.p12")
    ks.load(is, keyStorePasswd)
    val certificate =
      ks
        .getCertificate(keyStoreAlias)
        .asInstanceOf[X509Certificate]
    val chain =
      ks
        .getCertificateChain(keyStoreAlias)
        .map(_.asInstanceOf[X509Certificate])
        .toVector
    val clientPrivateKey = ks.getKey(keyStoreAlias, keyStorePasswd)
    val clientPublicKey = certificate.getPublicKey
    val keyPair = new KeyPair(clientPublicKey, clientPrivateKey.asInstanceOf[PrivateKey])

    //  Trust List

    val trustList = new UaMemoryTrustListManager()
    val serverCertStream = getClass.getClassLoader.getResourceAsStream("client/pki/trusted/certs/milo_demo_server.crt")
    val serverCert = CertificateUtil.decodeCertificate(serverCertStream)
    trustList.addTrustedCertificate(serverCert)

    //  Final Creation

    val config =
      UaClientConfig(
        name = "Secure Scalable Client",
        uri = "urn:secure:scalable:client",
        timeout = 5000,
        url = "opc.tcp://127.0.0.1:6254/milo",
        policy = UaSecurityPolicy.Basic256Sha256,
        auth =
          Some(UaAuthConfig(
            username = "user1",
            password = "password")),
        encryption =
          Some(UaEncryptionConfig(
            keyPair = keyPair,
            certificate = certificate,
            chain = chain,
            trustList = trustList)))

    UaMiloClient.from(config, types)
  

  test("Read Namespace Array - node with i=2255;ns=“http://opcfoundation.org/UA/”", NeedsRunningServer):
    val id = UaId(2255, "http://opcfoundation.org/UA/")

    val result = withClient: client =>
      val readResult = client.readValue(id).asSync
      readResult.value.get

    val expected =
      UaArray(
        UaString("http://opcfoundation.org/UA/"),
        UaString("urn:eclipse:milo:opcua:server:db520941-be4e-4934-8367-2b2a899762f1"),
        UaString("urn:eclipse:milo:opcua:server:demo"))

    result shouldBe expected
  

/*
  todo: code to generate valid OPC UA self-signed certificate (valid cert contains app uri == client app uri)

  private def cs(): Unit = {

    val keyStore = KeyStore.getInstance("PKCS12")
    keyStore.load(null, "password".toCharArray)

    val keyPair = SelfSignedCertificateGenerator.generateRsaKeyPair(2048)
    val builder = new SelfSignedCertificateBuilder(keyPair)
      .setCommonName("Secure Scalable Client")
      .setOrganization("modemtec")
      .setOrganizationalUnit("dev")
      .setLocalityName("Prague")
      .setStateName("CZ")
      .setCountryCode("CZ")
      .setApplicationUri("urn:secure:scalable:client")
      .addDnsName("localhost")
      .addIpAddress("127.0.0.1")

    val certificate = builder.build();

    keyStore.setKeyEntry("secureClient", keyPair.getPrivate, "password".toCharArray, Array(certificate))

    val out = new FileOutputStream("ks.p12")
    keyStore.store(out, "password".toCharArray)
  }
*/

