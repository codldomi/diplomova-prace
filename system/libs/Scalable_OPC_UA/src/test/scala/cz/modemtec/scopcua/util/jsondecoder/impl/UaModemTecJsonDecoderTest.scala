package cz.modemtec.scopcua.util.jsondecoder.impl

import cz.modemtec.scopcua.test.dataset.{DataSet, DataTypeDataSet}

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json


class UaModemTecJsonDecoderTest extends AnyFunSuite with Matchers:

  val data = DataSet.All
  val nsMap = DataSet.Namespaces
  val dataTypes = DataTypeDataSet.All

  val decoder: UaModemTecJsonDecoder = UaModemTecJsonDecoder(dataTypes, nsMap.map(_.swap))

  data.foreach: data =>
    test(data.name):
      data.records.foreach: record =>
        val json = Json.parse(record.json)
        val result = decoder.decode(json, data.dataType.id)
        val expected = record.value
        result shouldBe expected
        


