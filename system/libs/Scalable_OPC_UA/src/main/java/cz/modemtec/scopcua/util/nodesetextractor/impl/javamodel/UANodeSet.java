package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/* XML Schema UANodeSet
 *
 *  <xs:element name="UANodeSet">
 *    <xs:complexType>
 *      <xs:sequence>
 *        <xs:element name="NamespaceUris" type="UriTable" minOccurs="0" />
 *        <xs:element name="ServerUris" type="UriTable" minOccurs="0" />
 *        <xs:element name="Models" type="ModelTable" minOccurs="0" />
 *        <xs:element name="Aliases" type="AliasTable" minOccurs="0" />
 *        <xs:element name="Extensions" type="ListOfExtensions" minOccurs="0" />
 *        <xs:choice minOccurs="0" maxOccurs="unbounded">
 *          <xs:element name="UAObject" type="UAObject" />
 *          <xs:element name="UAVariable" type="UAVariable" />
 *          <xs:element name="UAMethod" type="UAMethod" />
 *          <xs:element name="UAView" type="UAView" />
 *          <xs:element name="UAObjectType" type="UAObjectType" />
 *          <xs:element name="UAVariableType" type="UAVariableType" />
 *          <xs:element name="UADataType" type="UADataType" />
 *          <xs:element name="UAReferenceType" type="UAReferenceType" />
 *        </xs:choice>
 *      </xs:sequence>
 *      <xs:attribute name="LastModified" type="xs:dateTime" />
 *    </xs:complexType>
 *  </xs:element>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UANodeSet {

    //  fields
    @JsonProperty("NamespaceUris")
    private List<Uri> NamespaceUris;

    @JsonProperty("Models")
    private List<Model> Models;

    /*
        problem with more unwrapped arrays
        - see: https://github.com/FasterXML/jackson-dataformat-xml/issues/275
        - workaround: setters must add all elements, not just set the new object. As on the Github:
          if (cats == null){
            cats = new ArrayList<Cat>(value.size());
          }
          cats.addAll(value);
     */

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "UAObject")
    private List<UAObject> UAObject;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "UAVariable")
    private List<UAVariable> UAVariable;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "UAMethod")
    private List<UAMethod> UAMethod;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "UAObjectType")
    private List<UAObjectType> UAObjectType;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "UAVariableType")
    private List<UAVariableType> UAVariableType;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "UADataType")
    private List<UADataType> UADataType;

    public UANodeSet() {
        NamespaceUris = new ArrayList<>();
        Models = new ArrayList<>();
        this.UAObject = new ArrayList<>();
        this.UAVariable = new ArrayList<>();
        this.UAMethod = new ArrayList<>();
        this.UAObjectType = new ArrayList<>();
        this.UAVariableType = new ArrayList<>();
        this.UADataType = new ArrayList<>();
    }

    public UANodeSet(List<Uri> namespaceUris, List<Model> models, List<UAObject> UAObject, List<UAVariable> UAVariable, List<UAMethod> UAMethod, List<UAObjectType> UAObjectType, List<UAVariableType> UAVariableType, List<UADataType> UADataType) {
        NamespaceUris = namespaceUris;
        Models = models;
        this.UAObject = UAObject;
        this.UAVariable = UAVariable;
        this.UAMethod = UAMethod;
        this.UAObjectType = UAObjectType;
        this.UAVariableType = UAVariableType;
        this.UADataType = UADataType;
    }

    public List<Uri> getNamespaceUris() {
        return NamespaceUris;
    }

    public void setNamespaceUris(List<Uri> namespaceUris) {
        NamespaceUris = namespaceUris;
    }

    public List<Model> getModels() {
        return Models;
    }

    public void setModels(List<Model> models) {
        Models = models;
    }

    public List<UAObject> getUAObject() {
        return UAObject;
    }

    public void setUAObject(List<UAObject> newOnes) {
        UAObject.addAll(newOnes);
    }

    public List<UAVariable> getUAVariable() {
        return UAVariable;
    }

    public void setUAVariable(List<UAVariable> newOnes) {
        UAVariable.addAll(newOnes);
    }

    public List<UAMethod> getUAMethod() {
        return UAMethod;
    }

    public void setUAMethod(List<UAMethod> newOnes) {
       UAMethod.addAll(newOnes);
    }

    public List<UAObjectType> getUAObjectType() {
        return UAObjectType;
    }

    public void setUAObjectType(List<UAObjectType> newOnes) {
        UAObjectType.addAll(newOnes);
    }

    public List<UAVariableType> getUAVariableType() {
        return UAVariableType;
    }

    public void setUAVariableType(List<UAVariableType> newOnes) {
        UAVariableType.addAll(newOnes);
    }

    public List<UADataType> getUADataType() {
        return UADataType;
    }

    public void setUADataType(List<UADataType> newOnes) {
        UADataType.addAll(newOnes);
    }
}
