package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Uri {

    //  field
    @JacksonXmlText
    private String Uri;

    public Uri() {
        Uri = null;
    }

    public Uri(String uri) {
        Uri = uri;
    }

    public String getUri() {
        return Uri;
    }

    public void setUri(String uri) {
        Uri = uri;
    }

}
