package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/* XML Schema UADataType
 *
 * <xs:complexType name="UADataType">
 *   <xs:complexContent>
 *     <xs:extension base="UAType">
 *       <xs:sequence>
 *         <xs:element name="Definition" type="DataTypeDefinition" minOccurs="0" />
 *       </xs:sequence>
 *       <xs:attribute name="Purpose" type="DataTypePurpose" default="Normal" />
 *     </xs:extension>
 *   </xs:complexContent>
 * </xs:complexType>
 * 
 * XML Schema UAType
 *
 * <xs:complexType name="UAType">
 *   <xs:complexContent>
 *     <xs:extension base="UANode">
 *       <xs:attribute name="IsAbstract" type="xs:boolean" default="false" />
 *     </xs:extension>
 *   </xs:complexContent>
 * </xs:complexType>
 * 
 * XML Schema UANode
 *
 * <xs:complexType name="UANode">
 *   <xs:sequence>
 *     <xs:element name="DisplayName" type="LocalizedText" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Description" type="LocalizedText" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Category" type="xs:string" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Documentation" type="xs:string" minOccurs="0" />
 *     <xs:element name="References" type="ListOfReferences" minOccurs="0" />
 *     <xs:element name="RolePermissions" type="ListOfRolePermissions" minOccurs="0" />
 *     <xs:element name="Extensions" type="ListOfExtensions" minOccurs="0" />
 *   </xs:sequence>
 *   <xs:attribute name="NodeId" type="NodeId" use="required" />
 *   <xs:attribute name="BrowseName" type="QualifiedName" use="required" />
 *   <xs:attribute name="WriteMask" type="WriteMask" default="0" />
 *   <xs:attribute name="UserWriteMask" type="WriteMask" default="0" />
 *   <xs:attribute name="AccessRestrictions" type="AccessRestriction" use="optional" />
 *   <xs:attribute name="HasNoPermissions" type="xs:boolean" default="false" />
 *   <xs:attribute name="SymbolicName" type="SymbolicName" />
 *   <xs:attribute name="ReleaseStatus" type="ReleaseStatus" default="Released" />
 * </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UADataType {

    //  fields
    @JsonProperty("DisplayName")
    private String DisplayName; // nullable

    @JsonProperty("Description")
    private String Description; // nullable

    @JsonProperty("References")
    private List<Reference> References;

    @JsonProperty("Definition")
    private DataTypeDefinition Definition; // nullable
    
    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("NodeId")
    private String NodeId;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("BrowseName")
    private String BrowseName;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("SymbolicName")
    private String SymbolicName;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("IsAbstract")
    private Boolean IsAbstract;

    public UADataType() {
        DisplayName = null;
        Description = null;
        References = new ArrayList<>();
        Definition = null;
        NodeId = null;
        BrowseName = null;
        SymbolicName = null;
        IsAbstract = false;
    }

    public UADataType(String displayName, String description, List<Reference> references, DataTypeDefinition definition, String nodeId, String browseName, String symbolicName, Boolean isAbstract) {
        DisplayName = displayName;
        Description = description;
        References = references;
        Definition = definition;
        NodeId = nodeId;
        BrowseName = browseName;
        SymbolicName = symbolicName;
        IsAbstract = isAbstract;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Reference> getReferences() {
        return References;
    }

    public void setReferences(List<Reference> references) {
        References = references;
    }

    public DataTypeDefinition getDefinition() {
        return Definition;
    }

    public void setDefinition(DataTypeDefinition definition) {
        Definition = definition;
    }

    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getBrowseName() {
        return BrowseName;
    }

    public void setBrowseName(String browseName) {
        BrowseName = browseName;
    }

    public String getSymbolicName() {
        return SymbolicName;
    }

    public void setSymbolicName(String symbolicName) {
        SymbolicName = symbolicName;
    }

    public Boolean getAbstract() {
        return IsAbstract;
    }

    public void setAbstract(Boolean anAbstract) {
        IsAbstract = anAbstract;
    }
    
}
