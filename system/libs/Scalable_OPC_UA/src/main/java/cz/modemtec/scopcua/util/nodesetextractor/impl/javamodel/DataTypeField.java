package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/* XML Schema DataTypeField
 *
 * <xs:complexType name="DataTypeField">
 *   <xs:sequence>
 *     <xs:element name="DisplayName" type="LocalizedText" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Description" type="LocalizedText" minOccurs="0" maxOccurs="unbounded" />
 *   </xs:sequence>
 *   <xs:attribute name="Name" type="xs:string" use="required" />
 *   <xs:attribute name="SymbolicName" type="SymbolicName" />
 *   <xs:attribute name="DataType" type="NodeId" default="i=24" />
 *   <xs:attribute name="ValueRank" type="ValueRank" default="-1" />
 *   <xs:attribute name="ArrayDimensions" type="ArrayDimensions" default="" />
 *   <xs:attribute name="MaxStringLength" type="xs:unsignedInt" default="0" />
 *   <xs:attribute name="Value" type="xs:int" default="-1" />
 *   <xs:attribute name="IsOptional" type="xs:boolean" default="false" />
 *   <xs:attribute name="AllowSubTypes" type="xs:boolean" default="false" />
 * </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataTypeField {

    //  fields
    @JsonProperty("DisplayName")
    private String DisplayName; // nullable
    
    @JsonProperty("Description")
    private String Description; // nullable

    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("Value")
    private int Value;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("Name")
    private String Name;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("SymbolicName")
    private String SymbolicName;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("DataType")
    private String DataType;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ValueRank")
    private int ValueRank;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ArrayDimensions")
    private String ArrayDimensions;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("IsOptional")
    private boolean IsOptional;

    public DataTypeField() {
        DisplayName = null;
        Description = null;
        Name = null;
        SymbolicName = null;
        DataType = "i=24";
        ValueRank = -1;
        ArrayDimensions = "";
        IsOptional = false;
        Value = -1;
    }

    public DataTypeField(int value, String displayName, String description, String name, String symbolicName, String dataType, int valueRank, String arrayDimensions, boolean isOptional) {
        DisplayName = displayName;
        Description = description;
        Name = name;
        SymbolicName = symbolicName;
        DataType = dataType;
        ValueRank = valueRank;
        ArrayDimensions = arrayDimensions;
        IsOptional = isOptional;
        Value = value;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSymbolicName() {
        return SymbolicName;
    }

    public void setSymbolicName(String symbolicName) {
        SymbolicName = symbolicName;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String dataType) {
        DataType = dataType;
    }

    public int getValueRank() {
        return ValueRank;
    }

    public void setValueRank(int valueRank) {
        ValueRank = valueRank;
    }

    public String getArrayDimensions() {
        return ArrayDimensions;
    }

    public void setArrayDimensions(String arrayDimensions) {
        ArrayDimensions = arrayDimensions;
    }

    public boolean isOptional() {
        return IsOptional;
    }

    public void setOptional(boolean optional) {
        IsOptional = optional;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

}
