package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

/* XML Schema Alias
 *
 * <xs:complexType name="AliasTable">
 *    <xs:sequence>
 *      <xs:element name="Alias" type="NodeIdAlias" minOccurs="0" maxOccurs="unbounded" />
 *    </xs:sequence>
 *  </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Alias {

    //  field
    @JacksonXmlText
    private String Value;

    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("Alias")
    private String Alias;

    public Alias() {
        Value = null;
        Alias = null;
    }

    public Alias(String value, String alias) {
        Value = value;
        Alias = alias;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

}
