package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

/* XML Schema Reference
 *
 * <xs:complexType name="Reference">
 *   <xs:simpleContent>
 *     <xs:extension base="NodeId">
 *       <xs:attribute name="ReferenceType" type="NodeId" use="required" />
 *       <xs:attribute name="IsForward" type="xs:boolean" default="true" />
 *     </xs:extension>
 *   </xs:simpleContent>
 * </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reference {

    //  field
    @JacksonXmlText
    private String To;
    
    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ReferenceType")
    private String ReferenceType;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("IsForward")
    private boolean IsForward;

    public Reference() {
        To = null;
        ReferenceType = null;
        IsForward = true;
    }

    public Reference(String to, String referenceType, boolean isForward) {
        To = to;
        ReferenceType = referenceType;
        IsForward = isForward;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public String getReferenceType() {
        return ReferenceType;
    }

    public void setReferenceType(String referenceType) {
        ReferenceType = referenceType;
    }

    public boolean isForward() {
        return IsForward;
    }

    public void setForward(boolean forward) {
        IsForward = forward;
    }

}
