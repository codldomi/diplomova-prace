package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import java.util.ArrayList;
import java.util.List;

/* XML Schema UAVariable
 *
 * <xs:complexType name="UAVariable">
 *   <xs:complexContent>
 *     <xs:extension base="UAInstance">
 *       <xs:sequence>
 *         <xs:element name="Value" minOccurs="0">
 *           <xs:complexType>
 *             <xs:sequence>
 *               <xs:any minOccurs="0" processContents="lax" />
 *             </xs:sequence>
 *           </xs:complexType>
 *         </xs:element>
 *         <xs:element name="Translation" type="TranslationType" minOccurs="0" maxOccurs="unbounded" />
 *       </xs:sequence>
 *       <xs:attribute name="DataType" type="NodeId" default="i=24" />
 *       <xs:attribute name="ValueRank" type="ValueRank" default="-1" />
 *       <xs:attribute name="ArrayDimensions" type="ArrayDimensions" default="" />
 *       <xs:attribute name="AccessLevel" type="AccessLevel" default="1" />
 *       <xs:attribute name="UserAccessLevel" type="AccessLevel" default="1" />
 *       <xs:attribute name="MinimumSamplingInterval" type="Duration" default="0" />
 *       <xs:attribute name="Historizing" type="xs:boolean" default="false" />
 *     </xs:extension>
 *   </xs:complexContent>
 * </xs:complexType>
 * 
 * XML Schema UAInstance
 * 
 * <xs:complexType name="UAInstance">
 *   <xs:complexContent>
 *     <xs:extension base="UANode">
 *       <xs:attribute name="ParentNodeId" type="NodeId" />
 *     </xs:extension>
 *   </xs:complexContent>
 * </xs:complexType>
 *
 * XML Schema UANode
 *
 * <xs:complexType name="UANode">
 *   <xs:sequence>
 *     <xs:element name="DisplayName" type="LocalizedText" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Description" type="LocalizedText" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Category" type="xs:string" minOccurs="0" maxOccurs="unbounded" />
 *     <xs:element name="Documentation" type="xs:string" minOccurs="0" />
 *     <xs:element name="References" type="ListOfReferences" minOccurs="0" />
 *     <xs:element name="RolePermissions" type="ListOfRolePermissions" minOccurs="0" />
 *     <xs:element name="Extensions" type="ListOfExtensions" minOccurs="0" />
 *   </xs:sequence>
 *   <xs:attribute name="NodeId" type="NodeId" use="required" />
 *   <xs:attribute name="BrowseName" type="QualifiedName" use="required" />
 *   <xs:attribute name="WriteMask" type="WriteMask" default="0" />
 *   <xs:attribute name="UserWriteMask" type="WriteMask" default="0" />
 *   <xs:attribute name="AccessRestrictions" type="AccessRestriction" use="optional" />
 *   <xs:attribute name="HasNoPermissions" type="xs:boolean" default="false" />
 *   <xs:attribute name="SymbolicName" type="SymbolicName" />
 *   <xs:attribute name="ReleaseStatus" type="ReleaseStatus" default="Released" />
 * </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UAVariable {

    //  fields
    @JsonProperty("DisplayName")
    private String DisplayName; // nullable

    @JsonProperty("Description")
    private String Description; // nullable

    @JsonProperty("References")
    private List<Reference> References;

    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("NodeId")
    private String NodeId;

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ParentNodeId")
    private String ParentNodeId; // nullable

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("BrowseName")
    private String BrowseName;

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("SymbolicName")
    private String SymbolicName;

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("DataType")
    private String DataType;

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ValueRank")
    private int ValueRank;

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ArrayDimensions")
    private String ArrayDimensions;

    public UAVariable() {
        DisplayName = null;
        Description = null;
        References = new ArrayList<>();
        NodeId = null;
        BrowseName = null;
        SymbolicName = null;
        ParentNodeId = null;
        DataType = "i=24";
        ValueRank = -1;
        ArrayDimensions = "";
    }

    public UAVariable(String displayName, String description, List<Reference> references, String nodeId, String parentNodeId, String browseName, String symbolicName, String dataType, int valueRank, String arrayDimensions) {
        DisplayName = displayName;
        Description = description;
        References = references;
        NodeId = nodeId;
        ParentNodeId = parentNodeId;
        BrowseName = browseName;
        SymbolicName = symbolicName;
        DataType = dataType;
        ValueRank = valueRank;
        ArrayDimensions = arrayDimensions;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Reference> getReferences() {
        return References;
    }

    public void setReferences(List<Reference> references) {
        References = references;
    }

    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getParentNodeId() {
        return ParentNodeId;
    }

    public void setParentNodeId(String parentNodeId) {
        ParentNodeId = parentNodeId;
    }

    public String getBrowseName() {
        return BrowseName;
    }

    public void setBrowseName(String browseName) {
        BrowseName = browseName;
    }

    public String getSymbolicName() {
        return SymbolicName;
    }

    public void setSymbolicName(String symbolicName) {
        SymbolicName = symbolicName;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String dataType) {
        DataType = dataType;
    }

    public int getValueRank() {
        return ValueRank;
    }

    public void setValueRank(int valueRank) {
        ValueRank = valueRank;
    }

    public String getArrayDimensions() {
        return ArrayDimensions;
    }

    public void setArrayDimensions(String arrayDimensions) {
        ArrayDimensions = arrayDimensions;
    }
}
