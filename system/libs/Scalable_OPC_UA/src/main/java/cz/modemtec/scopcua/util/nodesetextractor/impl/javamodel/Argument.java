package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/* XML Schema Argument
 *
 * <xs:complexType name="Argument">
 *   <xs:sequence>
 *     <xs:element name="Name" type="xs:string" minOccurs="0" nillable="true" />
 *     <xs:element name="DataType" type="ua:NodeId" minOccurs="0" nillable="true" />
 *     <xs:element name="ValueRank" type="xs:int" minOccurs="0" />
 *     <xs:element name="ArrayDimensions" type="ua:ListOfUInt32" minOccurs="0" nillable="true" />
 *     <xs:element name="Description" type="ua:LocalizedText" minOccurs="0" nillable="true" />
 *   </xs:sequence>
 * </xs:complexType>
 * <xs:element name="Argument" type="tns:Argument" />
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Argument {

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("ValueRank")
    private int ValueRank;

    @JsonProperty("ArrayDimensions")
    private String ArrayDimensions;

    @JsonProperty("DataType")
    private DataType DataType;

    public Argument() {
        Name = null;
        ValueRank = -1;
        ArrayDimensions = "";
        DataType = null;
    }

    public Argument(String name, int valueRank, String arrayDimensions, cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel.DataType dataType) {
        Name = name;
        ValueRank = valueRank;
        ArrayDimensions = arrayDimensions;
        DataType = dataType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getValueRank() {
        return ValueRank;
    }

    public void setValueRank(int valueRank) {
        ValueRank = valueRank;
    }

    public String getArrayDimensions() {
        return ArrayDimensions;
    }

    public void setArrayDimensions(String arrayDimensions) {
        ArrayDimensions = arrayDimensions;
    }

    public cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel.DataType getDataType() {
        return DataType;
    }

    public void setDataType(cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel.DataType dataType) {
        DataType = dataType;
    }

}
