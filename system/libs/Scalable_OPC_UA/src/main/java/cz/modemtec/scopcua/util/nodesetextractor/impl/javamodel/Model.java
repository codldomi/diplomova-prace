package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import org.bouncycastle.math.raw.Mod;

import java.util.ArrayList;
import java.util.List;

/* XML Schema Model
 *
 * <xs:complexType name="ModelTableEntry">
 *    <xs:sequence>
 *      <xs:element name="RolePermissions" type="ListOfRolePermissions" minOccurs="0" />
 *      <xs:element name="RequiredModel" type="ModelTableEntry" minOccurs="0" maxOccurs="unbounded" />
 *    </xs:sequence>
 *    <xs:attribute name="ModelUri" type="xs:string" use="required" />
 *    <xs:attribute name="XmlSchemaUri" type="xs:string" use="optional" />
 *    <xs:attribute name="Version" type="xs:string" />
 *    <xs:attribute name="PublicationDate" type="xs:dateTime" />
 *    <xs:attribute name="AccessRestrictions" type="AccessRestriction" default="0" />
 * </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Model {

    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("ModelUri")
    private String ModelUri;

    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("Version")
    private String Version;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("RequiredModel")
    private List<Model> RequiredModel;

    public Model() {
        ModelUri = null;
        Version = null;
        RequiredModel = new ArrayList<>();
    }

    public Model(String modelUri, String version, List<Model> requiredModel) {
        ModelUri = modelUri;
        Version = version;
        RequiredModel = requiredModel;
    }

    public String getModelUri() {
        return ModelUri;
    }

    public void setModelUri(String modelUri) {
        ModelUri = modelUri;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public List<Model> getRequiredModel() {
        return RequiredModel;
    }

    public void setRequiredModel(List<Model> requiredModel) {
        RequiredModel = requiredModel;
    }
}
