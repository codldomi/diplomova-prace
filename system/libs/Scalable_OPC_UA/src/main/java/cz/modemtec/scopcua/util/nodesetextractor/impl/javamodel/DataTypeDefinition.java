package cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

/* XML Schema DataTypeDefinition
 *
 * <xs:complexType name="DataTypeDefinition">
 *   <xs:sequence>
 *     <xs:element name="Field" type="DataTypeField" minOccurs="0" maxOccurs="unbounded" />
 *   </xs:sequence>
 *   <xs:attribute name="Name" type="QualifiedName" use="required" />
 *   <xs:attribute name="SymbolicName" type="SymbolicName" use="optional" />
 *   <xs:attribute name="IsUnion" type="xs:boolean" default="false" />
 *   <xs:attribute name="IsOptionSet" type="xs:boolean" default="false" />
 *
 *   <!-- BaseType is obsolete and no longer used. Left in for backwards compatibility. -->
 *   <xs:attribute name="BaseType" type="QualifiedName" default="" />
 * </xs:complexType>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataTypeDefinition {

    //  fields
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<DataTypeField> Field;
    
    //  attributes
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("Name")
    private String Name;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("SymbolicName")
    private String SymbolicName;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("IsUnion")
    private boolean IsUnion;
    
    @JacksonXmlProperty(isAttribute=true)
    @JsonProperty("IsOptionSet")
    private boolean IsOptionSet;

    public DataTypeDefinition() {
        Field = new ArrayList<>();
        Name = null;
        SymbolicName = null;
        IsUnion = false;
        IsOptionSet = false;
    }

    public DataTypeDefinition(List<DataTypeField> fields, String name, String symbolicName, boolean isUnion, boolean isOptionSet) {
        Field = fields;
        Name = name;
        SymbolicName = symbolicName;
        IsUnion = isUnion;
        IsOptionSet = isOptionSet;
    }

    public List<DataTypeField> getField() {
        return Field;
    }

    public void setField(List<DataTypeField> fields) {
        Field = fields;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSymbolicName() {
        return SymbolicName;
    }

    public void setSymbolicName(String symbolicName) {
        SymbolicName = symbolicName;
    }

    public boolean isUnion() {
        return IsUnion;
    }

    public void setUnion(boolean union) {
        IsUnion = union;
    }

    public boolean isOptionSet() {
        return IsOptionSet;
    }

    public void setOptionSet(boolean optionSet) {
        IsOptionSet = optionSet;
    }

}
