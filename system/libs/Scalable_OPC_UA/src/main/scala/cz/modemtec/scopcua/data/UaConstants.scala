package cz.modemtec.scopcua.data

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.{stringToUaString, intToUaUInt32}
import cz.modemtec.scopcua.data.value.UaString


object UaConstants:

  val OpcUaFoundation: UaString = "http://opcfoundation.org/UA/"

  object Id:
    
    val ObjectFolder: UaId = UaId(85, OpcUaFoundation)
  


