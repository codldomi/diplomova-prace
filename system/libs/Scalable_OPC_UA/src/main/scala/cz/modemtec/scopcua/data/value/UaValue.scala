package cz.modemtec.scopcua.data.value

/** A representation for OPC UA value.
  */
trait UaValue