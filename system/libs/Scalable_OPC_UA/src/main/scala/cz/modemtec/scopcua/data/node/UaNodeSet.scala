package cz.modemtec.scopcua.data.node

import cz.modemtec.scopcua.data.{UaDataType, UaId}


case class UaNodeSet(
  modelUri: String,
  uris: Set[String],
  dataTypes: Map[UaId, UaDataType],
  nodes: Map[UaId, UaNode]
)
