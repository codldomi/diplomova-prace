package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


trait NodeModel:
  def id: String
  def parentId: Option[String]
  def browseName: String
  def displayName: String
  def description: Option[String]
  def references: Seq[ReferenceModel]
  def symbolicName: Option[String]
