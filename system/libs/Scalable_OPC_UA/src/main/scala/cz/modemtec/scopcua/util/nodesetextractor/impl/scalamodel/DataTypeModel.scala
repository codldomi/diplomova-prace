package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class DataTypeModel(
  id: String,
  browseName: String,
  displayName: String,
  description: Option[String],
  references: Seq[ReferenceModel],
  definition: Option[DefinitionModel],
  isAbstract: Boolean,
  symbolicName: Option[String]
) extends NodeModel:

  override def parentId: Option[String] = None
