package cz.modemtec.scopcua.util.jsonvalidator.impl

import com.fasterxml.jackson.databind.ObjectMapper

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.util.jsonvalidator.UaJsonValidator

import net.jimblackler.jsonschemafriend.{SchemaStore, Validator}
import play.api.libs.json.{JsValue, Json}

import java.net.URI
import scala.util.{Failure, Success, Try}


/** JSON Validator implemented with JSON Schema Friend library.
  */
class UaFriendJsonValidator(val schemas: Map[UaId, JsValue]) extends UaJsonValidator:

  private val store =
    schemas.values.foldLeft(new SchemaStore()): (store, schema) =>
      val json = Json.stringify(schema)
      val uri = ref(schema)
      store.store(URI.create(uri), new ObjectMapper().readValue(json, classOf[Object]))
      store
  
  
  private def ref(schema: JsValue): String = (schema \ "$id").as[String]

  
  override def validate(value: JsValue, typeId: UaId): Boolean = 
    val json = Json.stringify(value)
    val uri = ref(schemas(typeId))
    val schema = store.loadSchema(URI.create(uri))

    val validator = new Validator()

    val result = Try:
      validator.validateJson(schema, json)
    
    result match 
      case Failure(_) => false
      case Success(_) => true
    




