package cz.modemtec.scopcua.util.binarydecoder.impl.util

import cz.modemtec.scopcua.data.value.number.*
import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]

import io.netty.buffer.UnpooledByteBufAllocator

import java.nio.charset.StandardCharsets

import scala.util.Using.Releasable


/** Encapsulates reading/writing methods on Netty buffer.
  *
  * @note Standard Binary DataEncoding uses the little endian format. OPC UA byte is unsigned 8-bit integer. On the
  *       other hand Java's byte is signed 8-bit integer.
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.2/ Part 6, OPC UA Binary Data Encoding, Built-in Types]]
  * @param statusCodes Supported StatusCodes.
  */
final class BinaryReadBuffer(val statusCodes: Map[UaUInt32, UaStatusCode]):

  private val buffer = UnpooledByteBufAllocator.DEFAULT.buffer()

  def writeBytes(bytes: Seq[UaByte]): Unit =
    bytes
      .map(_.value)
      .foreach(value => buffer.writeByte(value))

  def writeByteString(byteString: UaByteString): Unit =
    byteString
      .bytes
      .foreach(byte => buffer.writeByte(byte.value))

  def release(): Unit = buffer.release()
  /*
      Logic
      -----

      A Boolean value shall be encoded as a single byte where a value of 0 (zero) is false and any non-zero value
      is true.

      Encoders shall use the value of 1 to indicate a true value; however, decoders shall treat any non-zero value
      as true.
   */
  def readBoolean(): UaBoolean = buffer.readBoolean()
  /*
      Integer
      -------

      All integer types shall be encoded as little endian values where the least significant byte appears first
      in the stream.
   */
  def readByte(): UaByte = buffer.readUnsignedByte()
  def readSByte(): UaSByte = buffer.readByte()
  def readUInt16(): UaUInt16 = buffer.readUnsignedShortLE()
  def readInt16(): UaInt16 = buffer.readShortLE()
  def readUInt32(): UaUInt32 = buffer.readUnsignedIntLE()
  def readInt32(): UaInt32 = buffer.readIntLE()
  def readUInt64(): UaUInt64 =
    val signedLong = buffer.readLongLE()
    (BigInt(signedLong >>> 1) << 1) + (signedLong & 1)

  def readInt64(): UaInt64 = buffer.readLongLE()
  /*
      Floating-Point Number
      ---------------------

      All floating-point values shall be encoded with the appropriate IEEE-754 binary representation which has three
      basic components: the sign, the exponent, and the fraction. The bit ranges assigned to each component depend
      on the width of the type. Table 4 lists the bit ranges for the supported floating point types.

      +--------+--------------+----------+-----------------+
      | Name	 | Width (bits) | Fraction | Exponent | Sign |
      +--------+--------------+----------+----------+------+
      | Float  | 32           | 0-22     | 23-30    | 31   |
      | Double | 64           | 0-51     | 52-62    | 63   |
      +--------+--------------+----------+----------+------+

      In addition, the order of bytes in the stream is significant. All floating point values shall be encoded with
      the least significant byte appearing first (i.e. little endian).

      The floating-point type supports positive and negative infinity and not-a-number (NaN). The IEEE specification
      allows for multiple NaN variants; however, the encoders/decoders may not preserve the distinction. Encoders
      shall encode a NaN value as an IEEE quiet-NAN (000000000000F8FF) or (0000C0FF). Any unsupported types such
      as denormalized numbers shall also be encoded as an IEEE quiet-NAN. Any test for equality between NaN values
      always fails.
   */
  def readFloat(): UaFloat = buffer.readFloatLE()
  def readDouble(): UaDouble = buffer.readDoubleLE()
  /*
      String and XmlElement
      ---------------------

      All String values are encoded as a sequence of UTF-8 characters without a null terminator and preceded by the
      length in bytes.

      The length in bytes is encoded as Int32. A value of −1 is used to indicate a ‘null’ string.

      An XmlElement is an XML fragment serialized as UTF-8 string and then encoded as ByteString
   */
  def readString(): UaString =
    val length = readInt32()
    if length.value == -1 then
      return UaString()

    buffer
      .readCharSequence(length.value, StandardCharsets.UTF_8)
      .toString

  def readXmlElement(): UaXmlElement = readString().value
  /*
      DateTime
      --------

      A DateTime value shall be encoded as a 64-bit signed integer which represents the number of 100 nanosecond
      intervals since January 1, 1601 (UTC).
   */
  def readDateTime(): UaDateTime = buffer.readLongLE()
  /*
      ByteString
      ----------

      A ByteString is encoded as sequence of bytes preceded by its length in bytes. The length is encoded as a 32-bit
      signed integer.

      If the length of the byte string is −1 then the byte string is ‘null’.
   */
  def readByteString(): UaByteString =
    val length = readInt32()
    if length.value == -1 then
      return UaByteString()

    val bytes = for _ <- 0 until length.value yield readByte()
    UaByteString(bytes.toVector)

  /*
      GUID
      ----

      Data 1 - 4B, Data 2 - 2B, Data 3 - 2B, Data 4 - 8B. Fields are encoded sequentially according to the data
      type for field.
  */
  def readGuid(): UaGuid =
    val data1 = readUInt32()
    val data2 = readUInt16()
    val data3 = readUInt16()

    val length = 8
    val data4 = for _ <- 0 until length yield readByte()

    UaGuid(data1, data2, data3, data4.toVector)

  /*
      StatusCode
      ----------

      A StatusCode is encoded as a UInt32.
   */
  def readStatusCode(): UaStatusCode = statusCodes(readUInt32())
  /*
      QualifiedName
      -------------

      The abstract QualifiedName structure is defined in OPC 10000-3.
  */
  def readQualifiedName(): UaQualifiedName =
    val namespaceIndex = readUInt16()
    val name = readString()
    UaQualifiedName(name, namespaceIndex)

  /*
      DiagnosticInfo
      --------------

      A DiagnosticInfo structure is described in OPC 10000-4. It specifies a number of fields that could be missing.
      For that reason, the encoding uses a bit mask to indicate which fields are actually present in the encoded form.

      As described in OPC 10000-4, the SymbolicId, NamespaceUri, LocalizedText and Locale fields are indexes in
      a string table which is returned in the response header. Only the index of the corresponding string in
      the string table is encoded. An index of −1 indicates that there is no value for the string.

      DiagnosticInfo allows unlimited nesting which could result in stack overflow errors even if the message size
      is less than the maximum allowed. Decoders shall support at least 100 nesting levels. Decoders shall report
      an error if the number of nesting levels exceeds what it supports.
  */
  def readDiagnosticInfo(): UaDiagnosticInfo =
    val mask = readByte()

    val symbolicId          = if (mask.value & 0x01) == 0x01 then Some(readInt32()) else None
    val namespaceUri        = if (mask.value & 0x02) == 0x02 then Some(readInt32()) else None
    val localizedText       = if (mask.value & 0x04) == 0x04 then Some(readInt32()) else None
    val locale              = if (mask.value & 0x08) == 0x08 then Some(readInt32()) else None
    val additionalInfo      = if (mask.value & 0x10) == 0x10 then Some(readString()) else None
    val innerStatusCode     = if (mask.value & 0x20) == 0x20 then Some(readStatusCode()) else None
    val innerDiagnosticInfo = if (mask.value & 0x40) == 0x40 then Some(readDiagnosticInfo()) else None

    UaDiagnosticInfo(
      symbolicId,
      namespaceUri,
      localizedText,
      locale,
      additionalInfo,
      innerStatusCode,
      innerDiagnosticInfo)

  /*
      DataValue
      ---------

      A DataValue is always preceded by a mask that indicates which fields are present in the stream.
  */
  def readDataValue(): UaDataValue =
    val mask = readByte()

    val value             = if (mask.value & 0x01) == 0x01 then Some(readVariant())    else None
    val status            = if (mask.value & 0x02) == 0x02 then Some(readStatusCode()) else None
    val sourceTimestamp   = if (mask.value & 0x04) == 0x04 then Some(readDateTime())   else None
    val sourcePicoSeconds = if (mask.value & 0x10) == 0x10 then Some(readUInt16())     else None
    val serverTimestamp   = if (mask.value & 0x08) == 0x08 then Some(readDateTime())   else None
    val serverPicoSeconds = if (mask.value & 0x20) == 0x20 then Some(readUInt16())     else None

    UaDataValue(
      value,
      status,
      sourceTimestamp,
      sourcePicoSeconds,
      serverTimestamp,
      serverPicoSeconds)

  /*
      LocalizedText
      -------------

      A LocalizedText structure contains two fields that could be missing. For that reason, the encoding uses a bit
      mask to indicate which fields are actually present in the encoded form.

      The abstract LocalizedText structure is defined in OPC 10000-3.
  */
  def readLocalizedText(): UaLocalizedText =
    val mask = readByte()
    val locale = if (mask.value & 0x01) == 0x01 then readString() else UaString()
    val text   = if (mask.value & 0x02) == 0x02 then readString() else UaString()
    UaLocalizedText(text, locale)

  /*
      NodeId
      ------

      The components of a NodeId are described:

      +----------------+-------------+------------------------------------------------------------------------------------+
      | Namespace      | UInt16      | The index for a namespace URI. An index of 0 is used for OPC UA defined NodeIds.   |
      |                |             |                                                                                    |
      | IdentifierType | Enumeration | The format and data type of the identifier. The value may be one of the following: |
      |                |             | NUMERIC - the value is an UInteger;                                                |
      |                |             | STRING - the value is String;                                                      |
      |                |             | GUID - the value is a Guid;                                                        |
      |                |             | OPAQUE - the value is a ByteString;                                                |
      |                |             |                                                                                    |
      | Value          | *           | The identifier for a node in the address space of an OPC UA Server.                |
      +----------------+-------------+------------------------------------------------------------------------------------+

      The DataEncoding of a NodeId varies according to the contents of the instance. For that reason, the first byte of
      the encoded form indicates the format of the rest of the encoded NodeId. The possible DataEncoding formats are
      shown in the following table. It describes the structure of each possible format (they exclude the byte
      which indicates the format).

      +-------------------+-------+------------------------------------------------------------------------------+
      | Name              | Value | Description                                                                  |
      +-------------------+-------+------------------------------------------------------------------------------+
      | Two Byte          | 0x00  | A numeric value that fits into the two-byte representation.                  |
      | Four Byte         | 0x01  | A numeric value that fits into the four-byte representation.                 |
      | Numeric           | 0x02  | A numeric value that does not fit into the two or four byte representations. |
      | String            | 0x03  | A String value.                                                              |
      | Guid              | 0x04  | A Guid value.                                                                |
      | ByteString	      | 0x05  | An opaque (ByteString) value.                                                |
      | NamespaceUri Flag | 0x80	| See discussion of ExpandedNodeId.                                            |
      | ServerIndex Flag  | 0x40  | See discussion of ExpandedNodeId.                                            |
      +-------------------+-------+------------------------------------------------------------------------------+

      Standard:  Encoding [Byte], NamespaceIndex [UInt16], Identifier [UInt32, String, ByteString, Guid]
      Two Byte:  Encoding [Byte], Identifier [Byte] (namespace is 0)
      Four Byte: Encoding [Byte], NamespaceIndex [Byte], Identifier [UInt16]
  */
  def readNodeId(optionalMask: Option[UaByte] = None): UaNodeId =
    val mask = optionalMask match
      case Some(value) => value
      case None        => readByte()

    mask.value & 0x0F match
      case 0x00 => UaNumericNodeId(UaUInt16(0), UaUInt32(readByte().value))
      case 0x01 => UaNumericNodeId(UaUInt16(readByte().value), UaUInt32(readUInt16().value))
      case 0x02 => UaNumericNodeId(readUInt16(), readUInt32())
      case 0x03 => UaStringNodeId(readUInt16(), readString())
      case 0x04 => UaGuidNodeId(readUInt16(), readGuid())
      case 0x05 => UaOpaqueNodeId(readUInt16(), readByteString())
      case _ => throw new IllegalArgumentException("Not supported NodeId format.")


  /*
      ExpandedNodeId
      --------------

      An ExpandedNodeId extends the NodeId structure by allowing the NamespaceUri to be explicitly specified instead
      of using the NamespaceIndex. The NamespaceUri is optional. If it is specified, then the NamespaceIndex inside
      the NodeId shall be ignored.

      The ExpandedNodeId is encoded by first encoding a NodeId and then encoding NamespaceUri as a String.

      An instance of an ExpandedNodeId may still use the NamespaceIndex instead of the NamespaceUri. In this case,
      the NamespaceUri is not encoded in the stream. The presence of the NamespaceUri in the stream is indicated by
      setting the NamespaceUri flag in the encoding format byte for the NodeId.

      If the NamespaceUri is present, then the encoder shall encode the NamespaceIndex as 0 in the stream when the
      NodeId portion is encoded. The unused NamespaceIndex is included in the stream for consistency.

      An ExpandedNodeId may also have a ServerIndex which is encoded as a UInt32 after the NamespaceUri. The ServerIndex
      flag in the NodeId encoding byte indicates whether the ServerIndex is present in the stream. The ServerIndex is
      omitted if it is equal to zero.
   */
  def readExpandedNodeId(): UaExpandedNodeId =
    val mask = readByte()

    val isUriPresent = (mask.value & 0x80) == 0x80
    val isServerIndexPresent = (mask.value & 0x40) == 0x40

    val nodeId = readNodeId(Some(mask))
    val namespaceUri = if isUriPresent then readString() else UaString()
    val serverIndex  = if isServerIndexPresent then readUInt32() else UaUInt32(0)
    UaExpandedNodeId(namespaceUri, nodeId, serverIndex)

  /*
      Extension Object
      ----------------

      An ExtensionObject is encoded as sequence of bytes prefixed by the NodeId of its DataTypeEncoding and the number
      of bytes encoded.

      An ExtensionObject may be encoded by the application which means it is passed as a ByteString or an XmlElement to
      the encoder. In this case, the encoder will be able to write the number of bytes in the object before it encodes
      the bytes. However, an ExtensionObject may know how to encode/decode itself which means the encoder shall calculate
      the number of bytes before it encodes the object or it shall be able to seek backwards in the stream and update
      the length after encoding the body.

      When a decoder encounters an ExtensionObject it shall check if it recognizes the DataTypeEncoding identifier. If
      it does, then it can call the appropriate function to decode the object body. If the decoder does not recognize
      the type it shall use the Encoding to determine if the body is a ByteString or an XmlElement and then decode
      the object body or treat it as opaque data and skip over it.

      ExtensionObjects are used in two contexts: as values contained in Variant structures or as parameters
      in OPC UA Messages.

      A decoder may choose to parse an XmlElement body after decoding; if an unrecoverable parsing error occurs then
      the decoder should try to continue processing the stream. For example, if the ExtensionObject is the body of
      a Variant or an element in an array that is the body of Variant then this error can be reported by setting value
      of the Variant to the StatusCode Bad_DecodingError.
  */
  def readExtensionObject(): UaExtensionObject =
    val encodingId = readNodeId()
    val format = readByte()

    format.value match
      case 0x00 => UaByteExtensionObject(encodingId, UaByteString())
      case 0x01 => UaByteExtensionObject(encodingId, readByteString())
      case 0x02 => UaXmlExtensionObject(encodingId, readXmlElement())


  /*
      Variant
      -------

      A Variant is a union of the built-in types.
  */
  def readVariant(): UaVariant =

    def readMethodFor(id: Int): (UaBuiltInId, BinaryReadBuffer => UaValue) =
      import UaBuiltInId._
      id match
        case  1 => (Boolean,         _.readBoolean())
        case  2 => (SByte,           _.readSByte())
        case  3 => (Byte,            _.readByte())
        case  4 => (Int16,           _.readInt16())
        case  5 => (UInt16,          _.readUInt16())
        case  6 => (Int32,           _.readInt32())
        case  7 => (UInt32,          _.readUInt32())
        case  8 => (Int64,           _.readInt64())
        case  9 => (UInt64,          _.readUInt64())
        case 10 => (Float,           _.readFloat())
        case 11 => (Double,          _.readDouble())
        case 12 => (String,          _.readString())
        case 13 => (DateTime,        _.readDateTime())
        case 14 => (Guid,            _.readGuid())
        case 15 => (ByteString,      _.readByteString())
        case 16 => (XmlElement,      _.readXmlElement())
        case 17 => (NodeId,          _.readNodeId())
        case 18 => (ExpandedNodeId,  _.readExpandedNodeId())
        case 19 => (StatusCode,      _.readStatusCode())
        case 20 => (QualifiedName,   _.readQualifiedName())
        case 21 => (LocalizedText,   _.readLocalizedText())
        case 22 => (ExtensionObject, _.readExtensionObject())
        case 23 => (DataValue,       _.readDataValue())
        case 24 => (Variant,         _.readVariant())
        case 25 => (DiagnosticInfo,  _.readDiagnosticInfo())
        case _ => throw new IllegalArgumentException("Bad built-in type ID.")


    def readArray(read: BinaryReadBuffer => UaValue): UaArray =
      val length = readInt32()
      if length.value == -1 then
        return UaArray()

      val values = for _ <- 0 until length.value yield read(this)
      UaArray(values.toVector)

    def readDimensions(): List[Int] =
      val length = readInt32()
      if length.value == -1 then
        return List.empty

      val dimensions = for _ <- 0 until length.value yield readInt32()
      dimensions.map(_.value).toList


    val mask = readByte()

    if mask.value == 0 then
      return UaVariant()

    val isArray = (mask.value & 0x80) == 0x80
    val isDimensionEncoded = (mask.value & 0x40) == 0x40
    val (id, readMethod): (UaBuiltInId, BinaryReadBuffer => UaValue) = readMethodFor(mask.value & 0x3F)

    val value = if isArray then readArray(readMethod) else readMethod(this)
    
    if isDimensionEncoded then
      val dims = readDimensions()
      val array = UaArray.ofDim(dims*)(value.asInstanceOf[UaArray].values*)
      UaVariant(id, Some(array)) 
    else
      UaVariant(id, Some(value))


object BinaryReadBuffer:
  given releasable: Releasable[BinaryReadBuffer] = (resource: BinaryReadBuffer) => resource.release()

