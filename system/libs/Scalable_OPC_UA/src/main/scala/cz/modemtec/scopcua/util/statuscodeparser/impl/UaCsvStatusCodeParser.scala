package cz.modemtec.scopcua.util.statuscodeparser.impl

import cz.modemtec.scopcua.data.value.{UaStatusCode, UaString}
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32
import cz.modemtec.scopcua.util.statuscodeparser.UaStatusCodeParser


object UaCsvStatusCodeParser extends UaCsvStatusCodeParser


/** Parses StatusCodes stored in CSV format.
  *
  * Line format: "Symbol,Code[,Description]\n". Code is hexadecimal value starting with "0x".
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/A.2/ Part 6, Annex A (normative) Constants, StatusCodes]]
  */
trait UaCsvStatusCodeParser extends UaStatusCodeParser:

  /** Parses text and creates mapping (code value -> StatusCode).
    *
    * @param text Lines of StatusCode records.
    * @return Pairs of (code value, StatusCode).
    */
  override def parse(text: String): Map[UaUInt32, UaStatusCode] =
    text
      .split('\n')
      .foldLeft(Map.empty[UaUInt32, UaStatusCode]): (result, line) => 
        val pattern = "([^,]+),([^,]+)".r
        val matches =
          pattern
            .findFirstMatchIn(line)
            .getOrElse(throw new IllegalArgumentException("Bad StatusCode CSV line format."))

        val symbol = parseSymbol(matches.group(1))
        val code = parseCode(matches.group(2))

        result + (code -> UaStatusCode(code, symbol))
    

  private def parseSymbol(text: String): UaString = UaString(text.trim)

  
  private def parseCode(text: String): UaUInt32 = 
    val hexadecimal = text.trim.substring(2)
    val value = BigInt(hexadecimal, 16)
    UaUInt32(value.toLong)

