package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class ArgumentModel(
  name: String,
  typeId: String,
  valueRank: Int,
  arrayDimensions: Array[Int]
)
