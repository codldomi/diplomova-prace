package cz.modemtec.scopcua.data.value.number.integer

import cz.modemtec.scopcua.data.value.number.UaNumber

/** An integer value.
  */
trait UaInteger extends UaNumber
