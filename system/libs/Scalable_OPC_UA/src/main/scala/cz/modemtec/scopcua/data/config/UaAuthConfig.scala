package cz.modemtec.scopcua.data.config


case class UaAuthConfig(
  username: String,
  password: String
)
