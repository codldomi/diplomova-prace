package cz.modemtec.scopcua.sdk.client.impl.util

import cz.modemtec.scopcua.conversion.ConvertToUa.FromInt.given Conversion[?, ?]
import cz.modemtec.scopcua.conversion.ConvertFromUa.ToTimestamp.given Conversion[?, ?]
import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.integer.{UaByte, UaInt32, UaUInt32}
import cz.modemtec.scopcua.util.binarydecoder.UaBinaryDecoder
import cz.modemtec.scopcua.util.binaryencoder.UaBinaryEncoder

import io.netty.buffer.{ByteBuf, UnpooledByteBufAllocator}

import org.eclipse.milo.opcua.sdk.client.OpcUaClient
import org.eclipse.milo.opcua.stack.core.serialization.{OpcUaBinaryStreamDecoder, OpcUaBinaryStreamEncoder}
import org.eclipse.milo.opcua.stack.core.types.builtin.{DateTime, NodeId, Variant}
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger
import org.eclipse.milo.opcua.stack.core.types.enumerated.IdType

import scala.util.{Failure, Success, Using}
import scala.util.Using.Releasable


/** An utility for converting between Milo types and Scalable OPC UA types.
  *
  * @param client Milo's client.
  * @param decoder A decoder for OPC UA BinaryEncoding used by the server.
  * @param encoder An encoder for OPC UA BinaryEncoding used by the server.
  * @param typeOf Data types definitions.
  */
class ValueConverter(
  val client: OpcUaClient,
  val decoder: UaBinaryDecoder,
  val encoder: UaBinaryEncoder,
  val typeOf: PartialFunction[UaId, UaDataType]
):

  /** Converts UaId to UaNodeId
    *
    * @param id Universal Scalable OPC UA identifier.
    * @return Server specific identifier.
    */
  def uaIdToUaNodeId(id: UaId): UaNodeId =
    val nsIndex =
      client
        .getNamespaceTable
        .getIndex(id.namespaceUri.value)
        .intValue()

    UaNumericNodeId(nsIndex, id.index)

  /** Converts UaId to NodeId.
    *
    * @param id Scalable OPC UA identifier.
    * @return Milo's NodeId.
    */
  def uaIdToNodeId(id: UaId): NodeId =
    val index = UInteger.valueOf(id.index.value)
    val nsIndex =
      client
        .getNamespaceTable
        .getIndex(id.namespaceUri.value)

    new NodeId(nsIndex, index)

  /** Converts NodeId to UaId.
    *
    * @note Currently supports only Numeric NodeIds.
    * @param nodeId Milo's NodeId.
    * @return Scalable OPC UA identifier.
    */
  def nodeIdToUaId(nodeId: NodeId): UaId =
    val isNumeric = nodeId.getType match
      case IdType.Numeric => true
      case _ => false

    require(isNumeric, "NodeId is not a numeric one.")

    val nsIndex = nodeId.getNamespaceIndex
    val uri = client.getNamespaceTable.getUri(nsIndex)
    val index = nodeId.getIdentifier.asInstanceOf[UInteger].longValue()

    UaId(UaUInt32(index), UaString(uri))

  /** Converts UaDateTime to DateTime.
    *
    * @param dateTime Scalable OPC UA UaDateTime.
    * @return Milo's DateTime.
    */
  def uaDateTimeToDateTime(dateTime: UaDateTime): DateTime = new DateTime(dateTime.toInstant)

  /** Converts DateTime to UaDateTime.
    *
    * @param dateTime Eclipse Milo DateTime.
    * @return Scalable OPC UA UaDateTime.
    */
  def dateTimeToUaDateTime(dateTime: DateTime): Option[UaDateTime] =
    if dateTime == null then
      None
    else
      val nanoSecIntervals = dateTime.getUtcTime
      Some(UaDateTime(nanoSecIntervals))

  /** Converts UaValue to Milo's Variant.
    *
    * @param value Scalable OPC UA UaValue.
    * @param typeId DataType's identifier.
    * @return Milo's Variant.
    */
  def uaValueToVariant(value: UaValue, typeId: UaId): Variant =

    def asBuiltIn(value: UaValue): UaValue = value match
      case UaEnumeration(code, _) => code

      case UaArray(values) => UaArray(values.map(asBuiltIn))

      case struct: UaStructure =>
        val bytes = encoder.encodeStructure(struct, typeId)
        val encodingId = uaIdToUaNodeId(typeId)
        UaByteExtensionObject(encodingId, UaByteString(bytes))

      case other => other


    def decodeMiloVariant(bytes: Seq[UaByte]): Variant =
      given releasable: Releasable[ByteBuf] = (resource: ByteBuf) => resource.release()

      val result = Using(UnpooledByteBufAllocator.DEFAULT.buffer()): buffer =>
        bytes.foreach(byte => buffer.writeByte(byte.value))

        val context = client.getStaticSerializationContext
        val reader = new OpcUaBinaryStreamDecoder(context).setBuffer(buffer)

        reader.readVariant()

      result match
        case Success(variant)   => variant
        case Failure(exception) => throw exception

    val builtInId = typeOf(typeId) match
      case UaDataType.Boolean               => UaBuiltInId.Boolean
      case UaDataType.SByte                 => UaBuiltInId.SByte
      case UaDataType.Byte                  => UaBuiltInId.Byte
      case UaDataType.Int16                 => UaBuiltInId.Int16
      case UaDataType.UInt16                => UaBuiltInId.UInt16
      case UaDataType.Int32                 => UaBuiltInId.Int32
      case UaDataType.UInt32                => UaBuiltInId.UInt32
      case UaDataType.Int64                 => UaBuiltInId.Int64
      case UaDataType.UInt64                => UaBuiltInId.UInt64
      case UaDataType.Float                 => UaBuiltInId.Float
      case UaDataType.Double                => UaBuiltInId.Double
      case UaDataType.String                => UaBuiltInId.String
      case UaDataType.DateTime              => UaBuiltInId.DateTime
      case UaDataType.Guid                  => UaBuiltInId.Guid
      case UaDataType.ByteString            => UaBuiltInId.ByteString
      case UaDataType.XmlElement            => UaBuiltInId.XmlElement
      case UaDataType.NodeId                => UaBuiltInId.NodeId
      case UaDataType.ExpandedNodeId        => UaBuiltInId.ExpandedNodeId
      case UaDataType.StatusCode            => UaBuiltInId.StatusCode
      case UaDataType.QualifiedName         => UaBuiltInId.QualifiedName
      case UaDataType.LocalizedText         => UaBuiltInId.LocalizedText
      case UaDataType.Structure             => UaBuiltInId.ExtensionObject
      case UaDataType.DataValue             => UaBuiltInId.DataValue
      case UaDataType.BaseDataType          => UaBuiltInId.Variant
      case UaDataType.DiagnosticInfo        => UaBuiltInId.DiagnosticInfo

      case _: UaDataType.GeneralEnumeration => UaBuiltInId.Int32
      case _: UaDataType.GeneralStructure   => UaBuiltInId.ExtensionObject

      case _ => throw new IllegalArgumentException("Unsupported data type.")

    val builtIn = asBuiltIn(value)
    val variant = UaVariant(builtInId, Some(builtIn))
    val bytes = encoder.encodeVariant(variant)
    decodeMiloVariant(bytes)

  /** Converts Milo's Variant to a pure UaValue.
    *
    * @param variant Eclipse Milo Variant.
    * @param typeId DataType's identifier.
    * @return Scalable OPC UA UaValue.
    */
  def variantToUaValue(variant: Variant, typeId: UaId): Option[UaValue] =

    def asUaVariant: UaVariant =
      given releasable: Releasable[ByteBuf] = (resource: ByteBuf) => resource.release()

      val result = Using(UnpooledByteBufAllocator.DEFAULT.buffer()): buffer =>
        val context = client.getStaticSerializationContext
        val writer = new OpcUaBinaryStreamEncoder(context).setBuffer(buffer)
        writer.writeVariant(variant)

        var bytes = Vector.empty[UaByte]
        while buffer.isReadable do
          val byte = buffer.readUnsignedByte()
          bytes = bytes :+ UaByte(byte)

        bytes

      result match
        case Failure(exception) => throw exception
        case Success(bytes)     => decoder.decodeVariant(bytes)


    def decodeValue(value: UaValue): UaValue = value match
      case UaArray(values) => UaArray(values.map(decodeValue))

      case UaByteExtensionObject(_, body) => decoder.decode(body.bytes, typeId)

      case int32: UaInt32 => typeOf(typeId) match
        case UaDataType.GeneralEnumeration(_, _, _, _, values) =>
          values
            .find: 
              (_, value) => value == int32
            .map: 
              (name, value) => UaEnumeration(value, name)
            .get
        case _ => int32

      case other => other


    if variant == null then None else asUaVariant.value.map(decodeValue)


