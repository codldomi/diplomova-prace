package cz.modemtec.scopcua.data.value

/** A representation for a custom structure defined by an user.
  */
case class UaStructure(fields: Map[UaString, UaValue]) extends UaValue:
  
  def get(name: String): UaValue = fields(UaString(name))
  def getAs[A <: UaValue](name: String): A = get(name).asInstanceOf[A]


object UaStructure:
  def apply(pairs: (UaString, UaValue)*): UaStructure = new UaStructure(pairs.toMap)
