package cz.modemtec.scopcua.data.value

/** A sequence of Unicode characters.
  */
case class UaString(value: String = "") extends UaValue:

  def length: Int = value.length
  def isEmpty: Boolean = value.isEmpty
  def nonEmpty: Boolean = value.nonEmpty
