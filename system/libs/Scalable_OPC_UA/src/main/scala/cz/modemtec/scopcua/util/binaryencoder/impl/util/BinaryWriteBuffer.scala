package cz.modemtec.scopcua.util.binaryencoder.impl.util

import io.netty.buffer.UnpooledByteBufAllocator

import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.conversion.ConvertToUa.FromInt.given Conversion[?, ?]

import scala.annotation.tailrec
import scala.util.Using.Releasable


/** Encapsulates writing methods on Netty buffer.
  *
  * @note Standard Binary DataEncoding uses the little endian format. OPC UA byte is unsigned 8-bit integer. On the
  *       other hand Java's byte is signed 8-bit integer.
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.2/ Part 6, OPC UA Binary Data Encoding, Built-in Types]]
  */
final class BinaryWriteBuffer:

  private val buffer = UnpooledByteBufAllocator.DEFAULT.buffer()

  def writeBytes(bytes: Seq[UaByte]): Unit =
    bytes
      .map(_.value)
      .foreach(value => buffer.writeByte(value))

  def readBytes(): Vector[UaByte] = 
    var bytes = Vector.empty[UaByte]
    while buffer.isReadable do
      val byte = buffer.readUnsignedByte()
      bytes = bytes :+ UaByte(byte)
    bytes

  def release(): Unit = buffer.release()
  /*
      Logic
      -----

      A Boolean value shall be encoded as a single byte where a value of 0 (zero) is false and any non-zero value
      is true.

      Encoders shall use the value of 1 to indicate a true value; however, decoders shall treat any non-zero value
      as true.
   */
  def writeBoolean(boolean: UaBoolean): Unit = buffer.writeBoolean(boolean.value)
  /*
      Integer
      -------

      All integer types shall be encoded as little endian values where the least significant byte appears first
      in the stream.
   */
  def writeSByte(sbyte: UaSByte): Unit = buffer.writeByte(sbyte.value)
  def writeByte(byte: UaByte): Unit = buffer.writeByte(byte.value)
  def writeInt16(int16: UaInt16): Unit = buffer.writeShortLE(int16.value)
  def writeUInt16(uint16: UaUInt16): Unit = buffer.writeShortLE(uint16.value)
  def writeInt32(int32: UaInt32): Unit = buffer.writeIntLE(int32.value)
  def writeUInt32(uint32: UaUInt32): Unit = buffer.writeIntLE(uint32.value.intValue())
  def writeInt64(int64: UaInt64): Unit = buffer.writeLongLE(int64.value)
  def writeUInt64(uint64: UaUInt64): Unit = buffer.writeLongLE(uint64.value.longValue)
  /*
      Floating-Point Number
      ---------------------

      All floating-point values shall be encoded with the appropriate IEEE-754 binary representation which has three
      basic components: the sign, the exponent, and the fraction. The bit ranges assigned to each component depend
      on the width of the type. Table 4 lists the bit ranges for the supported floating point types.

      +--------+--------------+----------+-----------------+
      | Name	 | Width (bits) | Fraction | Exponent | Sign |
      +--------+--------------+----------+----------+------+
      | Float  | 32           | 0-22     | 23-30    | 31   |
      | Double | 64           | 0-51     | 52-62    | 63   |
      +--------+--------------+----------+----------+------+

      In addition, the order of bytes in the stream is significant. All floating point values shall be encoded with
      the least significant byte appearing first (i.e. little endian).

      The floating-point type supports positive and negative infinity and not-a-number (NaN). The IEEE specification
      allows for multiple NaN variants; however, the encoders/decoders may not preserve the distinction. Encoders
      shall encode a NaN value as an IEEE quiet-NAN (000000000000F8FF) or (0000C0FF). Any unsupported types such
      as denormalized numbers shall also be encoded as an IEEE quiet-NAN. Any test for equality between NaN values
      always fails.
   */
  def writeFloat(float: UaFloat): Unit = buffer.writeFloatLE(float.value)
  def writeDouble(double: UaDouble): Unit = buffer.writeDoubleLE(double.value)
  /*
      String and XmlElement
      ---------------------

      All String values are encoded as a sequence of UTF-8 characters without a null terminator and preceded by the
      length in bytes.

      The length in bytes is encoded as Int32. A value of −1 is used to indicate a ‘null’ string.

      An XmlElement is an XML fragment serialized as UTF-8 string and then encoded as ByteString
   */
  def writeString(string: UaString): Unit = 
    if string.isEmpty then
      writeInt32(-1)
    else
      val length = UaInt32(string.value.length)
      writeInt32(length)
      string.value.foreach(char => buffer.writeByte(char))

  def writeXmlElement(xmlElement: UaXmlElement): Unit = writeString(UaString(xmlElement.value))
  /*
      GUID
      ----

      Data 1 - 4B, Data 2 - 2B, Data 3 - 2B, Data 4 - 8B. Fields are encoded sequentially according to the data
      type for field.
  */
  def writeGuid(guid: UaGuid): Unit = 
    writeUInt32(guid.data1)
    writeUInt16(guid.data2)
    writeUInt16(guid.data3)
    writeBytes(guid.data4)
    
  /*
      ByteString
      ----------

      A ByteString is encoded as sequence of bytes preceded by its length in bytes. The length is encoded as a 32-bit
      signed integer.

      If the length of the byte string is −1 then the byte string is ‘null’.
   */
  def writeByteString(byteString: UaByteString): Unit = 
    if byteString.isEmpty then
      writeInt32(-1)
    else 
      writeInt32(byteString.length)
      byteString.bytes.foreach(writeByte)
    
  /*
      DateTime
      --------

      A DateTime value shall be encoded as a 64-bit signed integer which represents the number of 100 nanosecond
      intervals since January 1, 1601 (UTC).
   */
  def writeDateTime(dateTime: UaDateTime): Unit = buffer.writeLongLE(dateTime.value)
  /*
      StatusCode
      ----------

      A StatusCode is encoded as a UInt32.
   */
  def writeStatusCode(statusCode: UaStatusCode): Unit = writeUInt32(statusCode.code)
  /*
      QualifiedName
      -------------

      The abstract QualifiedName structure is defined in OPC 10000-3.
  */
  def writeQualifiedName(qualifiedName: UaQualifiedName) : Unit = 
    writeUInt16(qualifiedName.namespaceIndex)
    writeString(qualifiedName.name)
    
  /*
      LocalizedText
      -------------

      A LocalizedText structure contains two fields that could be missing. For that reason, the encoding uses a bit
      mask to indicate which fields are actually present in the encoded form.

      The abstract LocalizedText structure is defined in OPC 10000-3.
  */
  def writeLocalizedText(lt: UaLocalizedText) : Unit = 
    buffer.writeByte(0x01 | 0x02)
    writeString(lt.locale)
    writeString(lt.text)
    
  /*
      DiagnosticInfo
      --------------

      A DiagnosticInfo structure is described in OPC 10000-4. It specifies a number of fields that could be missing.
      For that reason, the encoding uses a bit mask to indicate which fields are actually present in the encoded form.

      As described in OPC 10000-4, the SymbolicId, NamespaceUri, LocalizedText and Locale fields are indexes in
      a string table which is returned in the response header. Only the index of the corresponding string in
      the string table is encoded. An index of −1 indicates that there is no value for the string.

      DiagnosticInfo allows unlimited nesting which could result in stack overflow errors even if the message size
      is less than the maximum allowed. Decoders shall support at least 100 nesting levels. Decoders shall report
      an error if the number of nesting levels exceeds what it supports.
   */
  @tailrec
  def writeDiagnosticInfo(info: UaDiagnosticInfo): Unit = 
    var mask = 0x7F

    if info.symbolicId.isEmpty then mask ^= 0x01
    if info.namespaceUri.isEmpty then mask ^= 0x02
    if info.localizedText.isEmpty then mask ^= 0x04
    if info.locale.isEmpty then mask ^= 0x08
    if info.additionalInfo.isEmpty then mask ^= 0x10
    if info.innerStatusCode.isEmpty then mask ^= 0x20
    if info.innerDiagnosticInfo.isEmpty then mask ^= 0x40

    buffer.writeByte(mask)

    if info.symbolicId.isDefined then writeInt32(info.symbolicId.get)
    if info.namespaceUri.isDefined then writeInt32(info.namespaceUri.get)
    if info.localizedText.isDefined then writeInt32(info.localizedText.get)
    if info.locale.isDefined then writeInt32(info.locale.get)
    if info.additionalInfo.isDefined then writeString(info.additionalInfo.get)
    if info.innerStatusCode.isDefined then writeStatusCode(info.innerStatusCode.get)
    if info.innerDiagnosticInfo.isDefined then writeDiagnosticInfo(info.innerDiagnosticInfo.get)
    
  /*
      DataValue
      ---------

      A DataValue is always preceded by a mask that indicates which fields are present in the stream.
  */
  def writeDataValue(dataValue: UaDataValue) : Unit = 
    var mask = 0x00

    if dataValue.value.isDefined then mask |= 0x01
    if dataValue.status.isDefined then mask |= 0x02
    if dataValue.sourceTimestamp.isDefined then mask |= 0x04
    if dataValue.sourcePicoSeconds.isDefined then mask |= 0x10
    if dataValue.serverTimestamp.isDefined then mask |= 0x08
    if dataValue.serverPicoSeconds.isDefined then mask |= 0x20

    buffer.writeByte(mask)

    if dataValue.value.isDefined then writeVariant(dataValue.value.get)
    if dataValue.status.isDefined then writeStatusCode(dataValue.status.get)
    if dataValue.sourceTimestamp.isDefined then writeDateTime(dataValue.sourceTimestamp.get)
    if dataValue.sourcePicoSeconds.isDefined then writeUInt16(dataValue.sourcePicoSeconds.get)
    if dataValue.serverTimestamp.isDefined then writeDateTime(dataValue.serverTimestamp.get)
    if dataValue.serverPicoSeconds.isDefined then writeUInt16(dataValue.serverPicoSeconds.get)
    
  /*
      NodeId
      ------

      The components of a NodeId are described:

      +----------------+-------------+------------------------------------------------------------------------------------+
      | Namespace      | UInt16      | The index for a namespace URI. An index of 0 is used for OPC UA defined NodeIds.   |
      |                |             |                                                                                    |
      | IdentifierType | Enumeration | The format and data type of the identifier. The value may be one of the following: |
      |                |             | NUMERIC - the value is an UInteger;                                                |
      |                |             | STRING - the value is String;                                                      |
      |                |             | GUID - the value is a Guid;                                                        |
      |                |             | OPAQUE - the value is a ByteString;                                                |
      |                |             |                                                                                    |
      | Value          | *           | The identifier for a node in the address space of an OPC UA Server.                |
      +----------------+-------------+------------------------------------------------------------------------------------+

      The DataEncoding of a NodeId varies according to the contents of the instance. For that reason, the first byte of
      the encoded form indicates the format of the rest of the encoded NodeId. The possible DataEncoding formats are
      shown in the following table. It describes the structure of each possible format (they exclude the byte
      which indicates the format).

      +-------------------+-------+------------------------------------------------------------------------------+
      | Name              | Value | Description                                                                  |
      +-------------------+-------+------------------------------------------------------------------------------+
      | Two Byte          | 0x00  | A numeric value that fits into the two-byte representation.                  |
      | Four Byte         | 0x01  | A numeric value that fits into the four-byte representation.                 |
      | Numeric           | 0x02  | A numeric value that does not fit into the two or four byte representations. |
      | String            | 0x03  | A String value.                                                              |
      | Guid              | 0x04  | A Guid value.                                                                |
      | ByteString	      | 0x05  | An opaque (ByteString) value.                                                |
      | NamespaceUri Flag | 0x80	| See discussion of ExpandedNodeId.                                            |
      | ServerIndex Flag  | 0x40  | See discussion of ExpandedNodeId.                                            |
      +-------------------+-------+------------------------------------------------------------------------------+

      Standard:  Encoding [Byte], NamespaceIndex [UInt16], Identifier [UInt32, String, ByteString, Guid]
      Two Byte:  Encoding [Byte], Identifier [Byte] (namespace is 0)
      Four Byte: Encoding [Byte], NamespaceIndex [Byte], Identifier [UInt16]
  */
  def writeNodeId(nodeId: UaNodeId, flag: Int = 0x00): Unit = 

    def writeNumericNodeId(id: UaNumericNodeId): Unit = 
      val namespaceIndex = id.namespaceIndex.value
      val identifier = id.identifier.value

      if namespaceIndex == 0 && identifier >= 0 && identifier <= 255 then
        buffer.writeByte(flag | 0x00)
        buffer.writeByte(identifier.intValue())
      else if namespaceIndex >= 0 && namespaceIndex <= 255 && identifier <= 65535 then
        buffer.writeByte(flag | 0x01)
        buffer.writeByte(namespaceIndex)
        buffer.writeShortLE(identifier.intValue())
      else 
        buffer.writeByte(flag | 0x02)
        buffer.writeShortLE(namespaceIndex)
        buffer.writeIntLE(identifier.intValue())

    def writeStringNodeId(id: UaStringNodeId): Unit = 
      buffer.writeByte(flag | 0x03)
      writeUInt16(id.namespaceIndex)
      writeString(id.identifier)

    def writeGuidNodeId(id: UaGuidNodeId): Unit = 
      buffer.writeByte(flag | 0x04)
      writeUInt16(id.namespaceIndex)
      writeGuid(id.identifier)

    def writeOpaqueNodeId(id: UaOpaqueNodeId): Unit = 
      buffer.writeByte(flag | 0x05)
      writeUInt16(id.namespaceIndex)
      writeByteString(id.identifier)
    
    nodeId match 
      case id: UaNumericNodeId => writeNumericNodeId(id)
      case id: UaStringNodeId  => writeStringNodeId(id)
      case id: UaGuidNodeId    => writeGuidNodeId(id)
      case id: UaOpaqueNodeId  => writeOpaqueNodeId(id)
    
  /*
      ExpandedNodeId
      --------------

      An ExpandedNodeId extends the NodeId structure by allowing the NamespaceUri to be explicitly specified instead
      of using the NamespaceIndex. The NamespaceUri is optional. If it is specified, then the NamespaceIndex inside
      the NodeId shall be ignored.

      The ExpandedNodeId is encoded by first encoding a NodeId and then encoding NamespaceUri as a String.

      An instance of an ExpandedNodeId may still use the NamespaceIndex instead of the NamespaceUri. In this case,
      the NamespaceUri is not encoded in the stream. The presence of the NamespaceUri in the stream is indicated by
      setting the NamespaceUri flag in the encoding format byte for the NodeId.

      If the NamespaceUri is present, then the encoder shall encode the NamespaceIndex as 0 in the stream when the
      NodeId portion is encoded. The unused NamespaceIndex is included in the stream for consistency.

      An ExpandedNodeId may also have a ServerIndex which is encoded as a UInt32 after the NamespaceUri. The ServerIndex
      flag in the NodeId encoding byte indicates whether the ServerIndex is present in the stream. The ServerIndex is
      omitted if it is equal to zero.
  */
  def writeExpandedNodeId(exNodeId: UaExpandedNodeId): Unit = 
    var flag = 0
    val isUriPresent = exNodeId.namespaceUri.nonEmpty
    val isServerIndexPresent = exNodeId.serverIndex.value != 0
    
    if isUriPresent then flag |= 0x80
    if isServerIndexPresent then flag |= 0x40

    writeNodeId(exNodeId.nodeId, flag)

    if isUriPresent then writeString(exNodeId.namespaceUri)
    if isServerIndexPresent then writeUInt32(exNodeId.serverIndex)
    
  /*
      Extension Object
      ----------------

      An ExtensionObject is encoded as sequence of bytes prefixed by the NodeId of its DataTypeEncoding and the number
      of bytes encoded.

      An ExtensionObject may be encoded by the application which means it is passed as a ByteString or an XmlElement to
      the encoder. In this case, the encoder will be able to write the number of bytes in the object before it encodes
      the bytes. However, an ExtensionObject may know how to encode/decode itself which means the encoder shall calculate
      the number of bytes before it encodes the object or it shall be able to seek backwards in the stream and update
      the length after encoding the body.

      When a decoder encounters an ExtensionObject it shall check if it recognizes the DataTypeEncoding identifier. If
      it does, then it can call the appropriate function to decode the object body. If the decoder does not recognize
      the type it shall use the Encoding to determine if the body is a ByteString or an XmlElement and then decode
      the object body or treat it as opaque data and skip over it.

      ExtensionObjects are used in two contexts: as values contained in Variant structures or as parameters
      in OPC UA Messages.

      A decoder may choose to parse an XmlElement body after decoding; if an unrecoverable parsing error occurs then
      the decoder should try to continue processing the stream. For example, if the ExtensionObject is the body of
      a Variant or an element in an array that is the body of Variant then this error can be reported by setting value
      of the Variant to the StatusCode Bad_DecodingError.
  */
  def writeExtensionObject(eo: UaExtensionObject): Unit = 
    writeNodeId(eo.encodingId)

    eo match 
      case UaByteExtensionObject(_, body) if body.isEmpty =>
        buffer.writeByte(0x00)

      case UaByteExtensionObject(_, body) if body.nonEmpty =>
        buffer.writeByte(0x01)
        writeByteString(body)

      case UaXmlExtensionObject(_, body) if body.isEmpty =>
        buffer.writeByte(0x00)

      case UaXmlExtensionObject(_, body) if body.nonEmpty =>
        buffer.writeByte(0x02)
        writeXmlElement(body)

      case _ => throw new IllegalArgumentException("The body of ExtensionObject should not be null.")
    
  /*
      Variant
      -------

      A Variant is a union of the built-in types.
  */
  def writeVariant(variant: UaVariant) : Unit = 

    def valueOf(id: UaBuiltInId): Int = 
      import UaBuiltInId._
      id match 
        case Null             =>  0
        case Boolean          =>  1
        case SByte            =>  2
        case Byte             =>  3
        case Int16            =>  4
        case UInt16           =>  5
        case Int32            =>  6
        case UInt32           =>  7
        case Int64            =>  8
        case UInt64           =>  9
        case Float            => 10
        case Double           => 11
        case String           => 12
        case DateTime         => 13
        case Guid             => 14
        case ByteString       => 15
        case XmlElement       => 16
        case NodeId           => 17
        case ExpandedNodeId   => 18
        case StatusCode       => 19
        case QualifiedName    => 20
        case LocalizedText    => 21
        case ExtensionObject  => 22
        case DataValue        => 23
        case Variant          => 24
        case DiagnosticInfo   => 25
    
    def writeValue(value: UaValue): Unit = value match 
      case x: UaBoolean         => writeBoolean(x)
      case x: UaSByte           => writeSByte(x)
      case x: UaByte            => writeByte(x)
      case x: UaInt16           => writeInt16(x)
      case x: UaUInt16          => writeUInt16(x)
      case x: UaInt32           => writeInt32(x)
      case x: UaUInt32          => writeUInt32(x)
      case x: UaInt64           => writeInt64(x)
      case x: UaUInt64          => writeUInt64(x)
      case x: UaFloat           => writeFloat(x)
      case x: UaDouble          => writeDouble(x)
      case x: UaString          => writeString(x)
      case x: UaDateTime        => writeDateTime(x)
      case x: UaGuid            => writeGuid(x)
      case x: UaByteString      => writeByteString(x)
      case x: UaXmlElement      => writeXmlElement(x)
      case x: UaNodeId          => writeNodeId(x)
      case x: UaExpandedNodeId  => writeExpandedNodeId(x)
      case x: UaStatusCode      => writeStatusCode(x)
      case x: UaQualifiedName   => writeQualifiedName(x)
      case x: UaLocalizedText   => writeLocalizedText(x)
      case x: UaExtensionObject => writeExtensionObject(x)
      case x: UaDataValue       => writeDataValue(x)
      case x: UaVariant         => writeVariant(x)
      case x: UaDiagnosticInfo  => writeDiagnosticInfo(x)
      case _ => throw new IllegalArgumentException("The value is not a built-in type.")
    
    def writeArray(): Unit = 
      val array = variant.value.get.asInstanceOf[UaArray]
      val elements = array.flatten
      val length = elements.length

      writeInt32(length)
      elements
        .values
        .foreach(writeValue)

    def writeDimensions(): Unit = 
      val array = variant.value.get.asInstanceOf[UaArray]
      val dimensions = array.dimensions
      val length = dimensions.length

      writeInt32(length)
      dimensions.foreach(dim => writeInt32(dim))


    var mask = valueOf(variant.id)

    if variant.id == UaBuiltInId.Null then 
      writeByte(mask)
      return

    val isArray = variant.isArray
    val isMultiDimensional =
      if (!isArray)
        false
      else
        variant
          .value
          .get
          .asInstanceOf[UaArray]
          .isMultiDimensional

    if isArray then mask |= 64
    if isMultiDimensional then mask |= 128

    writeByte(mask)
    if isArray then writeArray() else writeValue(variant.value.get)
    if isMultiDimensional then writeDimensions()


object BinaryWriteBuffer:
  given releasable: Releasable[BinaryWriteBuffer] = (resource: BinaryWriteBuffer) => resource.release()
