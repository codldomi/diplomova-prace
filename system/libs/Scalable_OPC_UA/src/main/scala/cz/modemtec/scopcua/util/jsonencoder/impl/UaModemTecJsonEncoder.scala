package cz.modemtec.scopcua.util.jsonencoder.impl

import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.data.value.{UaArray, UaBoolean, UaByteString, UaDataValue, UaDateTime, UaDiagnosticInfo, UaEnumeration, UaExpandedNodeId, UaExtensionObject, UaGuid, UaLocalizedText, UaNodeId, UaQualifiedName, UaStatusCode, UaString, UaStructure, UaValue, UaVariant, UaXmlElement}
import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.util.jsonencoder.UaJsonEncoder
import cz.modemtec.scopcua.conversion.ConvertFromUa.ToString.given Conversion[?, ?]
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.integer.UaInteger

import play.api.libs.json.*

import java.util.Base64
import scala.util.chaining.scalaUtilChainingOps


/** Implements ModemTec OPC UA JSON format.
  */
class UaModemTecJsonEncoder(val uriOf: PartialFunction[UaUInt16, UaString]) extends UaJsonEncoder:

  override def encode(value: UaValue): JsValue = jsonValue(value)

  override def encode(value: Option[UaValue]): JsValue = value match
    case Some(value) => jsonValue(value)
    case None        => JsNull


  private def jsonValue(value: UaValue): JsValue =

    def jsonField(name: String, value: UaValue): JsObject = Json.obj(name -> jsonValue(value))

    def jsonOptField(name: String, value: Option[UaValue]): JsObject = value match
      case Some(value) => jsonField(name, value)
      case None        => Json.obj()

    def jsonObject(fields: (String, JsValue)*): JsObject =
      fields
        .toMap
        .map: (name, value) =>
          (name, value)
        .pipe: seq =>
          JsObject(seq)

    def jsonNamespaceUri(index: UaUInt16): JsString = jsonString(uriOf(index))

    def jsonBoolean(bool: UaBoolean): JsBoolean = JsBoolean(bool.value)

    def jsonInteger(int: UaInteger): JsNumber = int match
      case UaByte(value)   => JsNumber(BigDecimal(value))
      case UaSByte(value)  => JsNumber(BigDecimal(value))
      case UaUInt16(value) => JsNumber(BigDecimal(value))
      case UaInt16(value)  => JsNumber(BigDecimal(value))
      case UaUInt32(value) => JsNumber(BigDecimal(value))
      case UaInt32(value)  => JsNumber(BigDecimal(value))
      case UaUInt64(value) => JsNumber(BigDecimal(value))
      case UaInt64(value)  => JsNumber(BigDecimal(value))
      case _ => throw new RuntimeException("There should be JSON for all integers.")

    def jsonFloat(float: UaFloat): JsNumber = JsNumber(BigDecimal(float.value))

    def jsonDouble(double: UaDouble): JsNumber = JsNumber(BigDecimal(double.value))

    def jsonString(string: UaString): JsString = JsString(string.value)

    def jsonDateTime(dateTime: UaDateTime): JsString =
      val str = stringFromUaDateTime(dateTime)
      JsString(str)

    def jsonByteString(byteString: UaByteString): JsString =
      val string = byteString.bytes.map(_.value.toChar).mkString
      val base64String = Base64.getEncoder.encodeToString(string.getBytes)
      JsString(base64String)

    def jsonGuid(guid: UaGuid): JsString = JsString(guid)

    def jsonXmlElement(xml: UaXmlElement): JsString = JsString(xml.value)

    def jsonStatusCode(statusCode: UaStatusCode): JsObject =
      jsonField("code", statusCode.code) ++
      jsonField("symbol", statusCode.symbol)

    def jsonQualifiedName(qualifiedName: UaQualifiedName): JsObject =
      jsonField("name", qualifiedName.name) ++
      jsonField("uri", uriOf(qualifiedName.namespaceIndex))

    def jsonLocalizedText(localizedText: UaLocalizedText): JsObject =
      jsonField("locale", localizedText.locale) ++
      jsonField("text", localizedText.text)

    def jsonNodeId(nodeId: UaNodeId): JsObject =
      val (namespaceUri, idType, id) = nodeId match
        case UaNumericNodeId(nsIndex, id) => (jsonNamespaceUri(nsIndex), JsString("NUMERIC"), jsonInteger(id))
        case UaGuidNodeId(nsIndex,    id) => (jsonNamespaceUri(nsIndex), JsString("GUID"),    jsonGuid(id))
        case UaStringNodeId(nsIndex,  id) => (jsonNamespaceUri(nsIndex), JsString("STRING"),  jsonString(id))
        case UaOpaqueNodeId(nsIndex,  id) => (jsonNamespaceUri(nsIndex), JsString("OPAQUE"),  jsonByteString(id))
      jsonObject("idType" -> idType, "uri" -> namespaceUri, "id" -> id)

    def jsonExtensionObject(obj: UaExtensionObject): JsObject =
      val (body, enType, encodingId) = obj match
        case UaByteExtensionObject(id, body) => (jsonByteString(body), JsString("BYTE"), jsonNodeId(id))
        case UaXmlExtensionObject(id,  body) => (jsonXmlElement(body), JsString("XML"),  jsonNodeId(id))
      jsonObject("body" -> body, "enType" -> enType, "encodingId" -> encodingId)

    def jsonVariant(variant: UaVariant): JsValue =
      val varType = variant.id match
        case UaBuiltInId.Null            => "NULL"
        case UaBuiltInId.Boolean         => "BOOLEAN"
        case UaBuiltInId.SByte           => "SBYTE"
        case UaBuiltInId.Byte            => "BYTE"
        case UaBuiltInId.Int16           => "INT16"
        case UaBuiltInId.UInt16          => "UINT16"
        case UaBuiltInId.Int32           => "INT32"
        case UaBuiltInId.UInt32          => "UINT32"
        case UaBuiltInId.Int64           => "INT64"
        case UaBuiltInId.UInt64          => "UINT64"
        case UaBuiltInId.Float           => "FLOAT"
        case UaBuiltInId.Double          => "DOUBLE"
        case UaBuiltInId.String          => "STRING"
        case UaBuiltInId.DateTime        => "DATETIME"
        case UaBuiltInId.Guid            => "GUID"
        case UaBuiltInId.ByteString      => "BYTESTRING"
        case UaBuiltInId.XmlElement      => "XMLELEMENT"
        case UaBuiltInId.NodeId          => "NODEID"
        case UaBuiltInId.ExpandedNodeId  => "EXPANDEDNODEID"
        case UaBuiltInId.StatusCode      => "STATUSCODE"
        case UaBuiltInId.QualifiedName   => "QUALIFIEDNAME"
        case UaBuiltInId.LocalizedText   => "LOCALIZEDTEXT"
        case UaBuiltInId.ExtensionObject => "EXTENSIONOBJECT"
        case UaBuiltInId.DataValue       => "DATAVALUE"
        case UaBuiltInId.Variant         => "VARIANT"
        case UaBuiltInId.DiagnosticInfo  => "DIAGNOSTICINFO"

      jsonObject("varType" -> JsString(varType)) ++ jsonOptField("value", variant.value)

    def jsonDiagnosticInfo(info: UaDiagnosticInfo): JsObject =
      jsonOptField("symbolicId", info.symbolicId) ++
      jsonOptField("namespaceUri", info.namespaceUri) ++
      jsonOptField("localizedText", info.localizedText) ++
      jsonOptField("locale", info.locale) ++
      jsonOptField("additionalInfo", info.additionalInfo) ++
      jsonOptField("innerStatusCode", info.innerStatusCode) ++
      jsonOptField("innerDiagnosticInfo", info.innerDiagnosticInfo)

    def jsonDataValue(dataValue: UaDataValue): JsObject =
      jsonOptField("value", dataValue.value) ++
      jsonOptField("status", dataValue.status) ++
      jsonOptField("serverTimestamp", dataValue.serverTimestamp) ++
      jsonOptField("serverPicoSeconds", dataValue.serverPicoSeconds) ++
      jsonOptField("sourceTimestamp", dataValue.sourceTimestamp) ++
      jsonOptField("sourcePicoSeconds", dataValue.sourcePicoSeconds)

    def jsonExpandedNodeId(expNodeId: UaExpandedNodeId): JsObject =
      val id = jsonNodeId(expNodeId.nodeId)
      val serverIndex = JsNumber(expNodeId.serverIndex.value)
      jsonObject("id" -> id, "serverIndex" -> serverIndex)

    def jsonStructure(struct: UaStructure): JsObject =
      val fields = struct.fields.transform: (_, value) =>
        jsonValue(value)
      val jsonFields = fields.map: (name, value) =>
        (name.value, value)
      jsonObject(jsonFields.toSeq*)

    def jsonEnumeration(enumeration: UaEnumeration): JsObject =
      val value = jsonInteger(enumeration.value)
      val name = jsonString(enumeration.name)
      jsonObject("value" -> value, "name" -> name)

    def jsonArray(array: UaArray): JsArray =
      val jsonValues =
        array
          .values
          .map(value => jsonValue(value))
          .toList
      JsArray(jsonValues)

    value match
      case v: UaInteger          => jsonInteger(v)
      case v: UaBoolean          => jsonBoolean(v)
      case v: UaFloat            => jsonFloat(v)
      case v: UaDouble           => jsonDouble(v)
      case v: UaString           => jsonString(v)
      case v: UaDateTime         => jsonDateTime(v)
      case v: UaGuid             => jsonGuid(v)
      case v: UaByteString       => jsonByteString(v)
      case v: UaXmlElement       => jsonXmlElement(v)
      case v: UaNodeId           => jsonNodeId(v)
      case v: UaExpandedNodeId   => jsonExpandedNodeId(v)
      case v: UaStatusCode       => jsonStatusCode(v)
      case v: UaQualifiedName    => jsonQualifiedName(v)
      case v: UaLocalizedText    => jsonLocalizedText(v)
      case v: UaExtensionObject  => jsonExtensionObject(v)
      case v: UaDataValue        => jsonDataValue(v)
      case v: UaVariant          => jsonVariant(v)
      case v: UaDiagnosticInfo   => jsonDiagnosticInfo(v)
      case v: UaEnumeration      => jsonEnumeration(v)
      case v: UaStructure        => jsonStructure(v)
      case v: UaArray            => jsonArray(v)
      case _ => throw new IllegalArgumentException("Unsupported data type.")


