package cz.modemtec.scopcua.util.statuscodeparser

import cz.modemtec.scopcua.data.value.UaStatusCode
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32


/** A parser for OPC UA status codes.
  */
trait UaStatusCodeParser:

  /** Parses text and creates mapping (code value -> StatusCode).
    *
    * @param text Textual representation of StatusCodes.
    * @return Pairs of (code value, StatusCode).
    */
  def parse(text: String): Map[UaUInt32, UaStatusCode]

