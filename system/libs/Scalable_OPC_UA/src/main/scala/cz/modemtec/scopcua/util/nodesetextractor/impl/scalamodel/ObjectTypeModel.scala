package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class ObjectTypeModel(
  id: String,
  browseName: String,
  displayName: String,
  description: Option[String],
  references: Seq[ReferenceModel],
  symbolicName: Option[String]
) extends NodeModel:

  override def parentId: Option[String] = None
