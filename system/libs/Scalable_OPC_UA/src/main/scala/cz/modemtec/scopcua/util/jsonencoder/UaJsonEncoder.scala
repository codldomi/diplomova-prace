package cz.modemtec.scopcua.util.jsonencoder

import cz.modemtec.scopcua.data.value.UaValue

import play.api.libs.json.JsValue


/** Encoder from Value to implemented JSON format.
  */
trait UaJsonEncoder:

  /** Encodes value to the JSON format.
    *
    * @param value Value representation.
    * @return JSON.
    */
  def encode(value: UaValue): JsValue

  /** Encodes value to the JSON format, but allows null value.
    *
    * @param value Value representation.
    * @return JSON.
    */
  def encode(value: Option[UaValue]): JsValue

