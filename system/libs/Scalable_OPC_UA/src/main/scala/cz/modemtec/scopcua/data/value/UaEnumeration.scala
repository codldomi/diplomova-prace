package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaInt32

/** A representation for a custom enumeration defined by an user.
  */
case class UaEnumeration(value: UaInt32, name: UaString) extends UaValue
