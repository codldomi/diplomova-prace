package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.{UaUInt16, UaUInt32}

/** An identifier for a node in the address space of an OPC UA Server.
  */
sealed trait UaNodeId extends UaValue:

  /** The index for a namespace URI.
    *
    * @note The namespace is a URI that identifies the naming authority responsible for assigning the identifier
    *       element of the NodeId. Naming authorities include the local Server, the underlying system, standards bodies
    *       and consortia. It is expected that most Nodes will use the URI of the Server or of the underlying system.
    *
    *       Using a namespace URI allows multiple OPC UA Servers attached to the same underlying system to use the same
    *       identifier to identify the same Object. This enables Clients that connect to those Servers to recognise
    *       Objects that they have in common.
    *
    *       Namespace URIs, like Server names, are identified by numeric values in OPC UA Services to permit more
    *       efficient transfer and processing (e.g. table lookups). The numeric values used to identify namespaces
    *       correspond to the index into the NamespaceArray. The NamespaceArray is a Variable that is part of the Server
    *       Object in the AddressSpace.
    *
    *       The URI for the OPC UA namespace is: “http://opcfoundation.org/UA/” Its corresponding index in the namespace
    *       table is 0. The namespace URI is case sensitive.
    */
  def namespaceIndex: UaUInt16


/** The most common type of NodeId. It is expressed by a numeric value.
  *
  * @param namespaceIndex The index for a namespace URI.
  * @param identifier Numeric value.
  */
case class UaNumericNodeId(namespaceIndex: UaUInt16, identifier: UaUInt32) extends UaNodeId

/** Normally the scope of NodeIds is the Server in which they are defined. For certain types of NodeIds, NodeIds can
  * uniquely identify a Node within a system, or across systems (e.g. GUIDs). System-wide and globally-unique
  * identifiers allow Clients to track Nodes, such as work orders, as they move between OPC UA Servers as they progress
  * through the system.
  *
  * @param namespaceIndex The index for a namespace URI.
  * @param identifier Globally Unique Identifier.
  */
case class UaGuidNodeId(namespaceIndex: UaUInt16, identifier: UaGuid) extends UaNodeId

/** String identifiers are case sensitive. That is, Clients shall consider them case sensitive. Servers are allowed to
  * provide alternative NodeIds and using this mechanism severs can handle NodeIds as case insensitive.
  *
  * @param namespaceIndex The index for a namespace URI.
  * @param identifier String value.
  */
case class UaStringNodeId(namespaceIndex: UaUInt16, identifier: UaString) extends UaNodeId

/** Opaque identifiers are identifiers that are free-format byte strings that might or might not be human interpretable.
  *
  * @param namespaceIndex The index for a namespace URI.
  * @param identifier Namespace specific format.
  */
case class UaOpaqueNodeId(namespaceIndex: UaUInt16, identifier: UaByteString) extends UaNodeId
