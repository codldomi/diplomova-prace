package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaInt32

/** A structure that contains detailed error and diagnostic information associated with a StatusCode.
  *
  * @param symbolicId          The symbolicId shall be used to identify a vendor-specific error or condition; typically
  *                            the result of some Server internal operation. The maximum length of this string is 32
  *                            characters. Servers wishing to return a numeric return code should convert the return
  *                            code into a string and use this string as symbolicId (e.g., “0xC0040007” or “-4”). This
  *                            symbolic identifier string is conveyed to the Client in the stringTable parameter of the
  *                            ResponseHeader parameter. The symbolicId parameter contains the index into the
  *                            stringTable for this string. -1 indicates that no string is specified. The symbolicId
  *                            shall not contain StatusCodes. If the localizedText contains a translation for the
  *                            description of a StatusCode, the symbolicId is -1.
  *
  * @param namespaceUri        The symbolicId is defined within the context of a namespace. This namespace is
  *                            represented as a string and is conveyed to the Client in the stringTable parameter of
  *                            the ResponseHeader parameter. The namespaceIndex parameter contains the index into the
  *                            stringTable for this string. -1 indicates that no string is specified. The namespaceUri
  *                            shall not be the standard OPC UA namespace. There are no symbolicIds provided for
  *                            standard StatusCodes.
  *
  * @param localizedText       A vendor-specific localized text string describes the symbolic id. The maximum length of
  *                            this text string is 256 characters. This localized text string is conveyed to the Client
  *                            in the stringTable parameter of the ResponseHeader parameter. The localizedText parameter
  *                            contains the index into the stringTable for this string. -1 indicates that no string is
  *                            specified. The localizedText refers to the symbolicId if present or the string that
  *                            describes the standard StatusCode if the Server provides translations. If the index is
  *                            -1, the Server has no translation to return and the Client should use the invariant
  *                            StatusCode description from the specification.
  *
  * @param locale              The locale part of the vendor-specific localized text describing the symbolic id. This
  *                            localized text string is conveyed to the Client in the stringTable parameter of the
  *                            ResponseHeader parameter. The locale parameter contains the index into the stringTable
  *                            for this string. -1 indicates that no string is specified.
  *
  * @param additionalInfo      Vendor-specific diagnostic information.
  *
  * @param innerStatusCode     The StatusCode from the inner operation. Many applications will make calls into
  *                            underlying systems during OPC UA request processing. An OPC UA Server has the option of
  *                            reporting the status from the underlying system in the diagnostic info.
  *
  * @param innerDiagnosticInfo The diagnostic info associated with the inner StatusCode.
  */
case class UaDiagnosticInfo(
  symbolicId: Option[UaInt32],
  namespaceUri: Option[UaInt32],
  localizedText: Option[UaInt32],
  locale: Option[UaInt32],
  additionalInfo: Option[UaString],
  innerStatusCode: Option[UaStatusCode],
  innerDiagnosticInfo: Option[UaDiagnosticInfo]
) extends UaValue
