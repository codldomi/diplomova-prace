package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

/** A numeric identifier for an error or condition that is associated with a value or an operation.
  *
  * A StatusCode in OPC UA is numerical value that is used to report the outcome of an operation performed by an OPC UA
  * Server. This code may have associated diagnostic information that describes the status in more detail; however, the
  * code by itself is intended to provide Client applications with enough information to make decisions on how to process
  * the results of an OPC UA Service.The StatusCode is a 32-bit unsigned integer. The top 16 bits represent the numeric
  * value of the code that shall be used for detecting specific errors or conditions. The bottom 16 bits are bit flags
  * that contain additional information but do not affect the meaning of the StatusCode.
  *
  * All OPC UA Clients shall always check the StatusCode associated with a result before using it. Results that have an
  * uncertain/warning status associated with them shall be used with care since these results might not be valid in all
  * situations. Results with a bad/failed status shall never be used.
  *
  * OPC UA Servers should return good/success StatusCodes if the operation completed normally and the result is always
  * valid. Different StatusCode values can provide additional information to the Client.
  *
  * OPC UA Servers should use uncertain/warning StatusCodes if they could not complete the operation in the manner
  * requested by the Client, however, the operation did not fail entirely.
  *
  * The list of StatusCodes is managed by OPC UA. The complete list of StatusCodes is defined in OPC 10000-6. Servers
  * shall not define their own StatusCodes. OPC UA companion working groups may request additional StatusCodes from the
  * OPC Foundation to be added to the list in OPC 10000-6.
  *
  * @param symbol The name of the StatusCode.
  *
  * @param code Bits of StatusCode's value.
  *
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | Field            | Bit Range | Description                                                            |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | Severity         | 30:31     | Indicates whether the StatusCode represents a good, bad or uncertain   |
  *            |                  |           | condition. These bits have the following meanings:                     |
  *            |                  |           |   Good Success (00) - Indicates that the operation was successful and  |
  *            |                  |           |                       the associated results may be used. Uncertain    |
  *            |                  |           |   Warning (01) - Indicates that the operation was partially successful |
  *            |                  |           |                  and that associated results might not be suitable for |
  *            |                  |           |                  some purposes.                                        |
  *            |                  |           |   Bad Failure (10) - Indicates that the operation failed and any       |
  *            |                  |           |                      associated results cannot be used.                |
  *            |                  |           |   Reserved (11) - Reserved for future use. All Clients should treat    |
  *            |                  |           |                   a StatusCode with this severity as “Bad”.            |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | Reserved         | 29:29     | Reserved for use in OPC UA application specific APIs. This bit shall   |
  *            |                  |           | always be zero on the wire but may be used by OPC UA application       |
  *            |                  |           | specific APIs for API specific status codes.                           |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | Reserved         | 28:28     | Reserved for future use. Shall always be zero.                         |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | SubCode          | 16:27     | The code is a numeric value assigned to represent different conditions.|
  *            |                  |           | Each code has a symbolic name and a numeric value. All descriptions in |
  *            |                  |           | the OPC UA specification refer to the symbolic name. OPC 10000-6 maps  |
  *            |                  |           | the symbolic names onto a numeric value.                               |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | StructureChanged | 15:15     | Indicates that the structure of the associated data value has changed  |
  *            |                  |           | since the last Notification. Clients should not process the data value |
  *            |                  |           | unless they re-read the metadata.                                      |
  *            |                  |           |                                                                        |
  *            |                  |           | Servers shall set this bit if the DataTypeEncoding used for a Variable |
  *            |                  |           | changes.                                                               |
  *            |                  |           |                                                                        |
  *            |                  |           | Servers shall also set this bit if the EnumStrings Property of the     |
  *            |                  |           | DataType of the Variable changes. This bit is provided to warn Clients |
  *            |                  |           | that parse complex data values that their parsing routines could fail  |
  *            |                  |           | because the serialized form of the data value has changed.             |
  *            |                  |           |                                                                        |
  *            |                  |           | This bit has meaning only for StatusCodes returned as part of a data   |
  *            |                  |           | change Notification or the HistoryRead. StatusCodes used in other      |
  *            |                  |           | contexts shall always set this bit to zero.                            |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | SemanticsChanged | 14:14     | Indicates that the semantics of the associated data value have changed.|
  *            |                  |           | Clients should not process the data value until they re-read the       |
  *            |                  |           | metadata associated with the Variable.                                 |
  *            |                  |           |                                                                        |
  *            |                  |           | Servers should set this bit if the metadata has changed in way that    |
  *            |                  |           | could cause application errors if the Client does not re-read the      |
  *            |                  |           | metadata. For example, a change to the engineering units could create  |
  *            |                  |           | problems if the Client uses the value to perform calculations.         |
  *            |                  |           |                                                                        |
  *            |                  |           | OPC 10000-8 defines the conditions where a Server shall set this bit   |
  *            |                  |           | for a DA Variable. Other specifications may define additional          |
  *            |                  |           | conditions. A Server may define other conditions that cause this bit   |
  *            |                  |           | to be set.                                                             |
  *            |                  |           |                                                                        |
  *            |                  |           | This bit has meaning only for StatusCodes returned as part of a data   |
  *            |                  |           | change Notification or the HistoryRead. StatusCodes used in other      |
  *            |                  |           | contexts shall always set this bit to zero.                            |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | Reserved         | 12:13     | Reserved for future use. Shall always be zero.                         |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | InfoType         | 10:11     | The type of information contained in the info bits. These bits have the|
  *            |                  |           | following meanings:                                                    |
  *            |                  |           |   NotUsed (00) - The info bits are not used and shall be set to zero.  |
  *            |                  |           |   DataValue (01) - The StatusCode and its info bits are associated     |
  *            |                  |           |                    with a data value returned from the Server.         |
  *            |                  |           |   Reserved (1X) - Reserved for future use. The info bits shall be      |
  *            |                  |           |                   ignored.                                             |
  *            +------------------+-----------+------------------------------------------------------------------------+
  *            | InfoBits         | 0:9       | Additional information bits that qualify the StatusCode. The structure |
  *            |                  |           | of these bits depends on the Info Type field.                          |
  *            +------------------+-----------+------------------------------------------------------------------------+
  */
case class UaStatusCode(code: UaUInt32, symbol: UaString) extends UaValue:

  def isGood: Boolean = symbol.value.startsWith("Good")
  def isBad: Boolean = symbol.value.startsWith("Bad")
  def isUncertain: Boolean = symbol.value.startsWith("Uncertain")


object UaStatusCode:

  val Good: UaStatusCode = UaStatusCode(UaUInt32(0x0L), UaString("Good"))
  val GoodCallAgain: UaStatusCode = UaStatusCode(UaUInt32(0xA90000L), UaString("GoodCallAgain"))
  val GoodCascade: UaStatusCode = UaStatusCode(UaUInt32(0x4090000L), UaString("GoodCascade"))
  val GoodCascadeInitializationAcknowledged: UaStatusCode = UaStatusCode(UaUInt32(0x4010000L), UaString("GoodCascadeInitializationAcknowledged"))
  val GoodCascadeInitializationRequest: UaStatusCode = UaStatusCode(UaUInt32(0x4020000L), UaString("GoodCascadeInitializationRequest"))
  val GoodCascadeNotInvited: UaStatusCode = UaStatusCode(UaUInt32(0x4030000L), UaString("GoodCascadeNotInvited"))
  val GoodCascadeNotSelected: UaStatusCode = UaStatusCode(UaUInt32(0x4040000L), UaString("GoodCascadeNotSelected"))
  val GoodClamped: UaStatusCode = UaStatusCode(UaUInt32(0x300000L), UaString("GoodClamped"))
  val GoodCommunicationEvent: UaStatusCode = UaStatusCode(UaUInt32(0xA70000L), UaString("GoodCommunicationEvent"))
  val GoodCompletesAsynchronously: UaStatusCode = UaStatusCode(UaUInt32(0x2E0000L), UaString("GoodCompletesAsynchronously"))
  val GoodDataIgnored: UaStatusCode = UaStatusCode(UaUInt32(0xD90000L), UaString("GoodDataIgnored"))
  val GoodDependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0xE00000L), UaString("GoodDependentValueChanged"))
  val GoodEdited: UaStatusCode = UaStatusCode(UaUInt32(0xDC0000L), UaString("GoodEdited"))
  val GoodEdited_DependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x1160000L), UaString("GoodEdited_DependentValueChanged"))
  val GoodEdited_DominantValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x1170000L), UaString("GoodEdited_DominantValueChanged"))
  val GoodEdited_DominantValueChanged_DependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x1180000L), UaString("GoodEdited_DominantValueChanged_DependentValueChanged"))
  val GoodEntryInserted: UaStatusCode = UaStatusCode(UaUInt32(0xA20000L), UaString("GoodEntryInserted"))
  val GoodEntryReplaced: UaStatusCode = UaStatusCode(UaUInt32(0xA30000L), UaString("GoodEntryReplaced"))
  val GoodFaultStateActive: UaStatusCode = UaStatusCode(UaUInt32(0x4070000L), UaString("GoodFaultStateActive"))
  val GoodInitiateFaultState: UaStatusCode = UaStatusCode(UaUInt32(0x4080000L), UaString("GoodInitiateFaultState"))
  val GoodLocalOverride: UaStatusCode = UaStatusCode(UaUInt32(0x960000L), UaString("GoodLocalOverride"))
  val GoodMoreData: UaStatusCode = UaStatusCode(UaUInt32(0xA60000L), UaString("GoodMoreData"))
  val GoodNoData: UaStatusCode = UaStatusCode(UaUInt32(0xA50000L), UaString("GoodNoData"))
  val GoodNonCriticalTimeout: UaStatusCode = UaStatusCode(UaUInt32(0xAA0000L), UaString("GoodNonCriticalTimeout"))
  val GoodOverload: UaStatusCode = UaStatusCode(UaUInt32(0x2F0000L), UaString("GoodOverload"))
  val GoodPostActionFailed: UaStatusCode = UaStatusCode(UaUInt32(0xDD0000L), UaString("GoodPostActionFailed"))
  val GoodResultsMayBeIncomplete: UaStatusCode = UaStatusCode(UaUInt32(0xBA0000L), UaString("GoodResultsMayBeIncomplete"))
  val GoodRetransmissionQueueNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0xDF0000L), UaString("GoodRetransmissionQueueNotSupported"))
  val GoodShutdownEvent: UaStatusCode = UaStatusCode(UaUInt32(0xA80000L), UaString("GoodShutdownEvent"))
  val GoodSubscriptionTransferred: UaStatusCode = UaStatusCode(UaUInt32(0x2D0000L), UaString("GoodSubscriptionTransferred"))
  val Bad: UaStatusCode = UaStatusCode(UaUInt32(0x80000000L), UaString("Bad"))
  val BadAggregateConfigurationRejected: UaStatusCode = UaStatusCode(UaUInt32(0x80DA0000L), UaString("BadAggregateConfigurationRejected"))
  val BadAggregateInvalidInputs: UaStatusCode = UaStatusCode(UaUInt32(0x80D60000L), UaString("BadAggregateInvalidInputs"))
  val BadAggregateListMismatch: UaStatusCode = UaStatusCode(UaUInt32(0x80D40000L), UaString("BadAggregateListMismatch"))
  val BadAggregateNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0x80D50000L), UaString("BadAggregateNotSupported"))
  val BadAlreadyExists: UaStatusCode = UaStatusCode(UaUInt32(0x81150000L), UaString("BadAlreadyExists"))
  val BadApplicationSignatureInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80580000L), UaString("BadApplicationSignatureInvalid"))
  val BadArgumentsMissing: UaStatusCode = UaStatusCode(UaUInt32(0x80760000L), UaString("BadArgumentsMissing"))
  val BadAttributeIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80350000L), UaString("BadAttributeIdInvalid"))
  val BadBoundNotFound: UaStatusCode = UaStatusCode(UaUInt32(0x80D70000L), UaString("BadBoundNotFound"))
  val BadBoundNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0x80D80000L), UaString("BadBoundNotSupported"))
  val BadBrowseDirectionInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x804D0000L), UaString("BadBrowseDirectionInvalid"))
  val BadBrowseNameDuplicated: UaStatusCode = UaStatusCode(UaUInt32(0x80610000L), UaString("BadBrowseNameDuplicated"))
  val BadBrowseNameInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80600000L), UaString("BadBrowseNameInvalid"))
  val BadCertificateChainIncomplete: UaStatusCode = UaStatusCode(UaUInt32(0x810D0000L), UaString("BadCertificateChainIncomplete"))
  val BadCertificateHostNameInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80160000L), UaString("BadCertificateHostNameInvalid"))
  val BadCertificateInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80120000L), UaString("BadCertificateInvalid"))
  val BadCertificateIssuerRevocationUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x801C0000L), UaString("BadCertificateIssuerRevocationUnknown"))
  val BadCertificateIssuerRevoked: UaStatusCode = UaStatusCode(UaUInt32(0x801E0000L), UaString("BadCertificateIssuerRevoked"))
  val BadCertificateIssuerTimeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80150000L), UaString("BadCertificateIssuerTimeInvalid"))
  val BadCertificateIssuerUseNotAllowed: UaStatusCode = UaStatusCode(UaUInt32(0x80190000L), UaString("BadCertificateIssuerUseNotAllowed"))
  val BadCertificatePolicyCheckFailed: UaStatusCode = UaStatusCode(UaUInt32(0x81140000L), UaString("BadCertificatePolicyCheckFailed"))
  val BadCertificateRevocationUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x801B0000L), UaString("BadCertificateRevocationUnknown"))
  val BadCertificateRevoked: UaStatusCode = UaStatusCode(UaUInt32(0x801D0000L), UaString("BadCertificateRevoked"))
  val BadCertificateTimeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80140000L), UaString("BadCertificateTimeInvalid"))
  val BadCertificateUntrusted: UaStatusCode = UaStatusCode(UaUInt32(0x801A0000L), UaString("BadCertificateUntrusted"))
  val BadCertificateUriInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80170000L), UaString("BadCertificateUriInvalid"))
  val BadCertificateUseNotAllowed: UaStatusCode = UaStatusCode(UaUInt32(0x80180000L), UaString("BadCertificateUseNotAllowed"))
  val BadCommunicationError: UaStatusCode = UaStatusCode(UaUInt32(0x80050000L), UaString("BadCommunicationError"))
  val BadConditionAlreadyDisabled: UaStatusCode = UaStatusCode(UaUInt32(0x80980000L), UaString("BadConditionAlreadyDisabled"))
  val BadConditionAlreadyEnabled: UaStatusCode = UaStatusCode(UaUInt32(0x80CC0000L), UaString("BadConditionAlreadyEnabled"))
  val BadConditionAlreadyShelved: UaStatusCode = UaStatusCode(UaUInt32(0x80D10000L), UaString("BadConditionAlreadyShelved"))
  val BadConditionBranchAlreadyAcked: UaStatusCode = UaStatusCode(UaUInt32(0x80CF0000L), UaString("BadConditionBranchAlreadyAcked"))
  val BadConditionBranchAlreadyConfirmed: UaStatusCode = UaStatusCode(UaUInt32(0x80D00000L), UaString("BadConditionBranchAlreadyConfirmed"))
  val BadConditionDisabled: UaStatusCode = UaStatusCode(UaUInt32(0x80990000L), UaString("BadConditionDisabled"))
  val BadConditionNotShelved: UaStatusCode = UaStatusCode(UaUInt32(0x80D20000L), UaString("BadConditionNotShelved"))
  val BadConfigurationError: UaStatusCode = UaStatusCode(UaUInt32(0x80890000L), UaString("BadConfigurationError"))
  val BadConnectionClosed: UaStatusCode = UaStatusCode(UaUInt32(0x80AE0000L), UaString("BadConnectionClosed"))
  val BadConnectionRejected: UaStatusCode = UaStatusCode(UaUInt32(0x80AC0000L), UaString("BadConnectionRejected"))
  val BadContentFilterInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80480000L), UaString("BadContentFilterInvalid"))
  val BadContinuationPointInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x804A0000L), UaString("BadContinuationPointInvalid"))
  val BadDataEncodingInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80380000L), UaString("BadDataEncodingInvalid"))
  val BadDataEncodingUnsupported: UaStatusCode = UaStatusCode(UaUInt32(0x80390000L), UaString("BadDataEncodingUnsupported"))
  val BadDataLost: UaStatusCode = UaStatusCode(UaUInt32(0x809D0000L), UaString("BadDataLost"))
  val BadDataTypeIdUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x80110000L), UaString("BadDataTypeIdUnknown"))
  val BadDataUnavailable: UaStatusCode = UaStatusCode(UaUInt32(0x809E0000L), UaString("BadDataUnavailable"))
  val BadDeadbandFilterInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x808E0000L), UaString("BadDeadbandFilterInvalid"))
  val BadDecodingError: UaStatusCode = UaStatusCode(UaUInt32(0x80070000L), UaString("BadDecodingError"))
  val BadDependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x80E30000L), UaString("BadDependentValueChanged"))
  val BadDeviceFailure: UaStatusCode = UaStatusCode(UaUInt32(0x808B0000L), UaString("BadDeviceFailure"))
  val BadDialogNotActive: UaStatusCode = UaStatusCode(UaUInt32(0x80CD0000L), UaString("BadDialogNotActive"))
  val BadDialogResponseInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80CE0000L), UaString("BadDialogResponseInvalid"))
  val BadDisconnect: UaStatusCode = UaStatusCode(UaUInt32(0x80AD0000L), UaString("BadDisconnect"))
  val BadDiscoveryUrlMissing: UaStatusCode = UaStatusCode(UaUInt32(0x80510000L), UaString("BadDiscoveryUrlMissing"))
  val BadDominantValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x80E10000L), UaString("BadDominantValueChanged"))
  val BadDuplicateReferenceNotAllowed: UaStatusCode = UaStatusCode(UaUInt32(0x80660000L), UaString("BadDuplicateReferenceNotAllowed"))
  val BadEdited_OutOfRange: UaStatusCode = UaStatusCode(UaUInt32(0x81190000L), UaString("BadEdited_OutOfRange"))
  val BadEdited_OutOfRange_DominantValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x811C0000L), UaString("BadEdited_OutOfRange_DominantValueChanged"))
  val BadEdited_OutOfRange_DominantValueChanged_DependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x811E0000L), UaString("BadEdited_OutOfRange_DominantValueChanged_DependentValueChanged"))
  val BadEncodingError: UaStatusCode = UaStatusCode(UaUInt32(0x80060000L), UaString("BadEncodingError"))
  val BadEncodingLimitsExceeded: UaStatusCode = UaStatusCode(UaUInt32(0x80080000L), UaString("BadEncodingLimitsExceeded"))
  val BadEndOfStream: UaStatusCode = UaStatusCode(UaUInt32(0x80B00000L), UaString("BadEndOfStream"))
  val BadEntryExists: UaStatusCode = UaStatusCode(UaUInt32(0x809F0000L), UaString("BadEntryExists"))
  val BadEventFilterInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80470000L), UaString("BadEventFilterInvalid"))
  val BadEventIdUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x809A0000L), UaString("BadEventIdUnknown"))
  val BadEventNotAcknowledgeable: UaStatusCode = UaStatusCode(UaUInt32(0x80BB0000L), UaString("BadEventNotAcknowledgeable"))
  val BadExpectedStreamToBlock: UaStatusCode = UaStatusCode(UaUInt32(0x80B40000L), UaString("BadExpectedStreamToBlock"))
  val BadFilterElementInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80C40000L), UaString("BadFilterElementInvalid"))
  val BadFilterLiteralInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80C50000L), UaString("BadFilterLiteralInvalid"))
  val BadFilterNotAllowed: UaStatusCode = UaStatusCode(UaUInt32(0x80450000L), UaString("BadFilterNotAllowed"))
  val BadFilterOperandCountMismatch: UaStatusCode = UaStatusCode(UaUInt32(0x80C30000L), UaString("BadFilterOperandCountMismatch"))
  val BadFilterOperandInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80490000L), UaString("BadFilterOperandInvalid"))
  val BadFilterOperatorInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80C10000L), UaString("BadFilterOperatorInvalid"))
  val BadFilterOperatorUnsupported: UaStatusCode = UaStatusCode(UaUInt32(0x80C20000L), UaString("BadFilterOperatorUnsupported"))
  val BadHistoryOperationInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80710000L), UaString("BadHistoryOperationInvalid"))
  val BadHistoryOperationUnsupported: UaStatusCode = UaStatusCode(UaUInt32(0x80720000L), UaString("BadHistoryOperationUnsupported"))
  val BadIdentityChangeNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0x80C60000L), UaString("BadIdentityChangeNotSupported"))
  val BadIdentityTokenInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80200000L), UaString("BadIdentityTokenInvalid"))
  val BadIdentityTokenRejected: UaStatusCode = UaStatusCode(UaUInt32(0x80210000L), UaString("BadIdentityTokenRejected"))
  val BadIndexRangeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80360000L), UaString("BadIndexRangeInvalid"))
  val BadIndexRangeNoData: UaStatusCode = UaStatusCode(UaUInt32(0x80370000L), UaString("BadIndexRangeNoData"))
  val BadInitialValue_OutOfRange: UaStatusCode = UaStatusCode(UaUInt32(0x811A0000L), UaString("BadInitialValue_OutOfRange"))
  val BadInsufficientClientProfile: UaStatusCode = UaStatusCode(UaUInt32(0x807C0000L), UaString("BadInsufficientClientProfile"))
  val BadInternalError: UaStatusCode = UaStatusCode(UaUInt32(0x80020000L), UaString("BadInternalError"))
  val BadInvalidArgument: UaStatusCode = UaStatusCode(UaUInt32(0x80AB0000L), UaString("BadInvalidArgument"))
  val BadInvalidSelfReference: UaStatusCode = UaStatusCode(UaUInt32(0x80670000L), UaString("BadInvalidSelfReference"))
  val BadInvalidState: UaStatusCode = UaStatusCode(UaUInt32(0x80AF0000L), UaString("BadInvalidState"))
  val BadInvalidTimestamp: UaStatusCode = UaStatusCode(UaUInt32(0x80230000L), UaString("BadInvalidTimestamp"))
  val BadInvalidTimestampArgument: UaStatusCode = UaStatusCode(UaUInt32(0x80BD0000L), UaString("BadInvalidTimestampArgument"))
  val BadLicenseExpired: UaStatusCode = UaStatusCode(UaUInt32(0x810E0000L), UaString("BadLicenseExpired"))
  val BadLicenseLimitsExceeded: UaStatusCode = UaStatusCode(UaUInt32(0x810F0000L), UaString("BadLicenseLimitsExceeded"))
  val BadLicenseNotAvailable: UaStatusCode = UaStatusCode(UaUInt32(0x81100000L), UaString("BadLicenseNotAvailable"))
  val BadMaxAgeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80700000L), UaString("BadMaxAgeInvalid"))
  val BadMaxConnectionsReached: UaStatusCode = UaStatusCode(UaUInt32(0x80B70000L), UaString("BadMaxConnectionsReached"))
  val BadMessageNotAvailable: UaStatusCode = UaStatusCode(UaUInt32(0x807B0000L), UaString("BadMessageNotAvailable"))
  val BadMethodInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80750000L), UaString("BadMethodInvalid"))
  val BadMonitoredItemFilterInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80430000L), UaString("BadMonitoredItemFilterInvalid"))
  val BadMonitoredItemFilterUnsupported: UaStatusCode = UaStatusCode(UaUInt32(0x80440000L), UaString("BadMonitoredItemFilterUnsupported"))
  val BadMonitoredItemIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80420000L), UaString("BadMonitoredItemIdInvalid"))
  val BadMonitoringModeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80410000L), UaString("BadMonitoringModeInvalid"))
  val BadNoCommunication: UaStatusCode = UaStatusCode(UaUInt32(0x80310000L), UaString("BadNoCommunication"))
  val BadNoContinuationPoints: UaStatusCode = UaStatusCode(UaUInt32(0x804B0000L), UaString("BadNoContinuationPoints"))
  val BadNoData: UaStatusCode = UaStatusCode(UaUInt32(0x809B0000L), UaString("BadNoData"))
  val BadNoDataAvailable: UaStatusCode = UaStatusCode(UaUInt32(0x80B10000L), UaString("BadNoDataAvailable"))
  val BadNoDeleteRights: UaStatusCode = UaStatusCode(UaUInt32(0x80690000L), UaString("BadNoDeleteRights"))
  val BadNoEntryExists: UaStatusCode = UaStatusCode(UaUInt32(0x80A00000L), UaString("BadNoEntryExists"))
  val BadNoMatch: UaStatusCode = UaStatusCode(UaUInt32(0x806F0000L), UaString("BadNoMatch"))
  val BadNoSubscription: UaStatusCode = UaStatusCode(UaUInt32(0x80790000L), UaString("BadNoSubscription"))
  val BadNoValidCertificates: UaStatusCode = UaStatusCode(UaUInt32(0x80590000L), UaString("BadNoValidCertificates"))
  val BadNodeAttributesInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80620000L), UaString("BadNodeAttributesInvalid"))
  val BadNodeClassInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x805F0000L), UaString("BadNodeClassInvalid"))
  val BadNodeIdExists: UaStatusCode = UaStatusCode(UaUInt32(0x805E0000L), UaString("BadNodeIdExists"))
  val BadNodeIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80330000L), UaString("BadNodeIdInvalid"))
  val BadNodeIdRejected: UaStatusCode = UaStatusCode(UaUInt32(0x805D0000L), UaString("BadNodeIdRejected"))
  val BadNodeIdUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x80340000L), UaString("BadNodeIdUnknown"))
  val BadNodeNotInView: UaStatusCode = UaStatusCode(UaUInt32(0x804E0000L), UaString("BadNodeNotInView"))
  val BadNonceInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80240000L), UaString("BadNonceInvalid"))
  val BadNotConnected: UaStatusCode = UaStatusCode(UaUInt32(0x808A0000L), UaString("BadNotConnected"))
  val BadNotExecutable: UaStatusCode = UaStatusCode(UaUInt32(0x81110000L), UaString("BadNotExecutable"))
  val BadNotFound: UaStatusCode = UaStatusCode(UaUInt32(0x803E0000L), UaString("BadNotFound"))
  val BadNotImplemented: UaStatusCode = UaStatusCode(UaUInt32(0x80400000L), UaString("BadNotImplemented"))
  val BadNotReadable: UaStatusCode = UaStatusCode(UaUInt32(0x803A0000L), UaString("BadNotReadable"))
  val BadNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0x803D0000L), UaString("BadNotSupported"))
  val BadNotTypeDefinition: UaStatusCode = UaStatusCode(UaUInt32(0x80C80000L), UaString("BadNotTypeDefinition"))
  val BadNotWritable: UaStatusCode = UaStatusCode(UaUInt32(0x803B0000L), UaString("BadNotWritable"))
  val BadNothingToDo: UaStatusCode = UaStatusCode(UaUInt32(0x800F0000L), UaString("BadNothingToDo"))
  val BadNumericOverflow: UaStatusCode = UaStatusCode(UaUInt32(0x81120000L), UaString("BadNumericOverflow"))
  val BadObjectDeleted: UaStatusCode = UaStatusCode(UaUInt32(0x803F0000L), UaString("BadObjectDeleted"))
  val BadOperationAbandoned: UaStatusCode = UaStatusCode(UaUInt32(0x80B30000L), UaString("BadOperationAbandoned"))
  val BadOutOfMemory: UaStatusCode = UaStatusCode(UaUInt32(0x80030000L), UaString("BadOutOfMemory"))
  val BadOutOfRange: UaStatusCode = UaStatusCode(UaUInt32(0x803C0000L), UaString("BadOutOfRange"))
  val BadOutOfRange_DominantValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x811B0000L), UaString("BadOutOfRange_DominantValueChanged"))
  val BadOutOfRange_DominantValueChanged_DependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x811D0000L), UaString("BadOutOfRange_DominantValueChanged_DependentValueChanged"))
  val BadOutOfService: UaStatusCode = UaStatusCode(UaUInt32(0x808D0000L), UaString("BadOutOfService"))
  val BadParentNodeIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x805B0000L), UaString("BadParentNodeIdInvalid"))
  val BadProtocolVersionUnsupported: UaStatusCode = UaStatusCode(UaUInt32(0x80BE0000L), UaString("BadProtocolVersionUnsupported"))
  val BadQueryTooComplex: UaStatusCode = UaStatusCode(UaUInt32(0x806E0000L), UaString("BadQueryTooComplex"))
  val BadReferenceLocalOnly: UaStatusCode = UaStatusCode(UaUInt32(0x80680000L), UaString("BadReferenceLocalOnly"))
  val BadReferenceNotAllowed: UaStatusCode = UaStatusCode(UaUInt32(0x805C0000L), UaString("BadReferenceNotAllowed"))
  val BadReferenceTypeIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x804C0000L), UaString("BadReferenceTypeIdInvalid"))
  val BadRefreshInProgress: UaStatusCode = UaStatusCode(UaUInt32(0x80970000L), UaString("BadRefreshInProgress"))
  val BadRequestCancelledByClient: UaStatusCode = UaStatusCode(UaUInt32(0x802C0000L), UaString("BadRequestCancelledByClient"))
  val BadRequestCancelledByRequest: UaStatusCode = UaStatusCode(UaUInt32(0x805A0000L), UaString("BadRequestCancelledByRequest"))
  val BadRequestHeaderInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x802A0000L), UaString("BadRequestHeaderInvalid"))
  val BadRequestInterrupted: UaStatusCode = UaStatusCode(UaUInt32(0x80840000L), UaString("BadRequestInterrupted"))
  val BadRequestNotAllowed: UaStatusCode = UaStatusCode(UaUInt32(0x80E40000L), UaString("BadRequestNotAllowed"))
  val BadRequestNotComplete: UaStatusCode = UaStatusCode(UaUInt32(0x81130000L), UaString("BadRequestNotComplete"))
  val BadRequestTimeout: UaStatusCode = UaStatusCode(UaUInt32(0x80850000L), UaString("BadRequestTimeout"))
  val BadRequestTooLarge: UaStatusCode = UaStatusCode(UaUInt32(0x80B80000L), UaString("BadRequestTooLarge"))
  val BadRequestTypeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80530000L), UaString("BadRequestTypeInvalid"))
  val BadResourceUnavailable: UaStatusCode = UaStatusCode(UaUInt32(0x80040000L), UaString("BadResourceUnavailable"))
  val BadResponseTooLarge: UaStatusCode = UaStatusCode(UaUInt32(0x80B90000L), UaString("BadResponseTooLarge"))
  val BadSecureChannelClosed: UaStatusCode = UaStatusCode(UaUInt32(0x80860000L), UaString("BadSecureChannelClosed"))
  val BadSecureChannelIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80220000L), UaString("BadSecureChannelIdInvalid"))
  val BadSecureChannelTokenUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x80870000L), UaString("BadSecureChannelTokenUnknown"))
  val BadSecurityChecksFailed: UaStatusCode = UaStatusCode(UaUInt32(0x80130000L), UaString("BadSecurityChecksFailed"))
  val BadSecurityModeInsufficient: UaStatusCode = UaStatusCode(UaUInt32(0x80E60000L), UaString("BadSecurityModeInsufficient"))
  val BadSecurityModeRejected: UaStatusCode = UaStatusCode(UaUInt32(0x80540000L), UaString("BadSecurityModeRejected"))
  val BadSecurityPolicyRejected: UaStatusCode = UaStatusCode(UaUInt32(0x80550000L), UaString("BadSecurityPolicyRejected"))
  val BadSempahoreFileMissing: UaStatusCode = UaStatusCode(UaUInt32(0x80520000L), UaString("BadSempahoreFileMissing"))
  val BadSensorFailure: UaStatusCode = UaStatusCode(UaUInt32(0x808C0000L), UaString("BadSensorFailure"))
  val BadSequenceNumberInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80880000L), UaString("BadSequenceNumberInvalid"))
  val BadSequenceNumberUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x807A0000L), UaString("BadSequenceNumberUnknown"))
  val BadServerHalted: UaStatusCode = UaStatusCode(UaUInt32(0x800E0000L), UaString("BadServerHalted"))
  val BadServerIndexInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x806A0000L), UaString("BadServerIndexInvalid"))
  val BadServerNameMissing: UaStatusCode = UaStatusCode(UaUInt32(0x80500000L), UaString("BadServerNameMissing"))
  val BadServerNotConnected: UaStatusCode = UaStatusCode(UaUInt32(0x800D0000L), UaString("BadServerNotConnected"))
  val BadServerUriInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x804F0000L), UaString("BadServerUriInvalid"))
  val BadServiceUnsupported: UaStatusCode = UaStatusCode(UaUInt32(0x800B0000L), UaString("BadServiceUnsupported"))
  val BadSessionClosed: UaStatusCode = UaStatusCode(UaUInt32(0x80260000L), UaString("BadSessionClosed"))
  val BadSessionIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80250000L), UaString("BadSessionIdInvalid"))
  val BadSessionNotActivated: UaStatusCode = UaStatusCode(UaUInt32(0x80270000L), UaString("BadSessionNotActivated"))
  val BadShelvingTimeOutOfRange: UaStatusCode = UaStatusCode(UaUInt32(0x80D30000L), UaString("BadShelvingTimeOutOfRange"))
  val BadShutdown: UaStatusCode = UaStatusCode(UaUInt32(0x800C0000L), UaString("BadShutdown"))
  val BadSourceNodeIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80640000L), UaString("BadSourceNodeIdInvalid"))
  val BadStateNotActive: UaStatusCode = UaStatusCode(UaUInt32(0x80BF0000L), UaString("BadStateNotActive"))
  val BadStructureMissing: UaStatusCode = UaStatusCode(UaUInt32(0x80460000L), UaString("BadStructureMissing"))
  val BadSubscriptionIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80280000L), UaString("BadSubscriptionIdInvalid"))
  val BadSyntaxError: UaStatusCode = UaStatusCode(UaUInt32(0x80B60000L), UaString("BadSyntaxError"))
  val BadTargetNodeIdInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80650000L), UaString("BadTargetNodeIdInvalid"))
  val BadTcpEndpointUrlInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80830000L), UaString("BadTcpEndpointUrlInvalid"))
  val BadTcpInternalError: UaStatusCode = UaStatusCode(UaUInt32(0x80820000L), UaString("BadTcpInternalError"))
  val BadTcpMessageTooLarge: UaStatusCode = UaStatusCode(UaUInt32(0x80800000L), UaString("BadTcpMessageTooLarge"))
  val BadTcpMessageTypeInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x807E0000L), UaString("BadTcpMessageTypeInvalid"))
  val BadTcpNotEnoughResources: UaStatusCode = UaStatusCode(UaUInt32(0x80810000L), UaString("BadTcpNotEnoughResources"))
  val BadTcpSecureChannelUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x807F0000L), UaString("BadTcpSecureChannelUnknown"))
  val BadTcpServerTooBusy: UaStatusCode = UaStatusCode(UaUInt32(0x807D0000L), UaString("BadTcpServerTooBusy"))
  val BadTicketInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x81200000L), UaString("BadTicketInvalid"))
  val BadTicketRequired: UaStatusCode = UaStatusCode(UaUInt32(0x811F0000L), UaString("BadTicketRequired"))
  val BadTimeout: UaStatusCode = UaStatusCode(UaUInt32(0x800A0000L), UaString("BadTimeout"))
  val BadTimestampNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0x80A10000L), UaString("BadTimestampNotSupported"))
  val BadTimestampsToReturnInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x802B0000L), UaString("BadTimestampsToReturnInvalid"))
  val BadTooManyArguments: UaStatusCode = UaStatusCode(UaUInt32(0x80E50000L), UaString("BadTooManyArguments"))
  val BadTooManyMatches: UaStatusCode = UaStatusCode(UaUInt32(0x806D0000L), UaString("BadTooManyMatches"))
  val BadTooManyMonitoredItems: UaStatusCode = UaStatusCode(UaUInt32(0x80DB0000L), UaString("BadTooManyMonitoredItems"))
  val BadTooManyOperations: UaStatusCode = UaStatusCode(UaUInt32(0x80100000L), UaString("BadTooManyOperations"))
  val BadTooManyPublishRequests: UaStatusCode = UaStatusCode(UaUInt32(0x80780000L), UaString("BadTooManyPublishRequests"))
  val BadTooManySessions: UaStatusCode = UaStatusCode(UaUInt32(0x80560000L), UaString("BadTooManySessions"))
  val BadTooManySubscriptions: UaStatusCode = UaStatusCode(UaUInt32(0x80770000L), UaString("BadTooManySubscriptions"))
  val BadTypeDefinitionInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80630000L), UaString("BadTypeDefinitionInvalid"))
  val BadTypeMismatch: UaStatusCode = UaStatusCode(UaUInt32(0x80740000L), UaString("BadTypeMismatch"))
  val BadUnexpectedError: UaStatusCode = UaStatusCode(UaUInt32(0x80010000L), UaString("BadUnexpectedError"))
  val BadUnknownResponse: UaStatusCode = UaStatusCode(UaUInt32(0x80090000L), UaString("BadUnknownResponse"))
  val BadUserAccessDenied: UaStatusCode = UaStatusCode(UaUInt32(0x801F0000L), UaString("BadUserAccessDenied"))
  val BadUserSignatureInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80570000L), UaString("BadUserSignatureInvalid"))
  val BadViewIdUnknown: UaStatusCode = UaStatusCode(UaUInt32(0x806B0000L), UaString("BadViewIdUnknown"))
  val BadViewParameterMismatch: UaStatusCode = UaStatusCode(UaUInt32(0x80CA0000L), UaString("BadViewParameterMismatch"))
  val BadViewTimestampInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80C90000L), UaString("BadViewTimestampInvalid"))
  val BadViewVersionInvalid: UaStatusCode = UaStatusCode(UaUInt32(0x80CB0000L), UaString("BadViewVersionInvalid"))
  val BadWaitingForInitialData: UaStatusCode = UaStatusCode(UaUInt32(0x80320000L), UaString("BadWaitingForInitialData"))
  val BadWaitingForResponse: UaStatusCode = UaStatusCode(UaUInt32(0x80B20000L), UaString("BadWaitingForResponse"))
  val BadWouldBlock: UaStatusCode = UaStatusCode(UaUInt32(0x80B50000L), UaString("BadWouldBlock"))
  val BadWriteNotSupported: UaStatusCode = UaStatusCode(UaUInt32(0x80730000L), UaString("BadWriteNotSupported"))
  val Uncertain: UaStatusCode = UaStatusCode(UaUInt32(0x40000000L), UaString("Uncertain"))
  val UncertainConfigurationError: UaStatusCode = UaStatusCode(UaUInt32(0x420F0000L), UaString("UncertainConfigurationError"))
  val UncertainDataSubNormal: UaStatusCode = UaStatusCode(UaUInt32(0x40A40000L), UaString("UncertainDataSubNormal"))
  val UncertainDependentValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x40E20000L), UaString("UncertainDependentValueChanged"))
  val UncertainDominantValueChanged: UaStatusCode = UaStatusCode(UaUInt32(0x40DE0000L), UaString("UncertainDominantValueChanged"))
  val UncertainEngineeringUnitsExceeded: UaStatusCode = UaStatusCode(UaUInt32(0x40940000L), UaString("UncertainEngineeringUnitsExceeded"))
  val UncertainInitialValue: UaStatusCode = UaStatusCode(UaUInt32(0x40920000L), UaString("UncertainInitialValue"))
  val UncertainLastUsableValue: UaStatusCode = UaStatusCode(UaUInt32(0x40900000L), UaString("UncertainLastUsableValue"))
  val UncertainNoCommunicationLastUsableValue: UaStatusCode = UaStatusCode(UaUInt32(0x408F0000L), UaString("UncertainNoCommunicationLastUsableValue"))
  val UncertainNotAllNodesAvailable: UaStatusCode = UaStatusCode(UaUInt32(0x40C00000L), UaString("UncertainNotAllNodesAvailable"))
  val UncertainReferenceNotDeleted: UaStatusCode = UaStatusCode(UaUInt32(0x40BC0000L), UaString("UncertainReferenceNotDeleted"))
  val UncertainReferenceOutOfServer: UaStatusCode = UaStatusCode(UaUInt32(0x406C0000L), UaString("UncertainReferenceOutOfServer"))
  val UncertainSensorCalibration: UaStatusCode = UaStatusCode(UaUInt32(0x420A0000L), UaString("UncertainSensorCalibration"))
  val UncertainSensorNotAccurate: UaStatusCode = UaStatusCode(UaUInt32(0x40930000L), UaString("UncertainSensorNotAccurate"))
  val UncertainSimulatedValue: UaStatusCode = UaStatusCode(UaUInt32(0x42090000L), UaString("UncertainSimulatedValue"))
  val UncertainSubNormal: UaStatusCode = UaStatusCode(UaUInt32(0x40950000L), UaString("UncertainSubNormal"))
  val UncertainSubstituteValue: UaStatusCode = UaStatusCode(UaUInt32(0x40910000L), UaString("UncertainSubstituteValue"))
  val UncertainTransducerInManual: UaStatusCode = UaStatusCode(UaUInt32(0x42080000L), UaString("UncertainTransducerInManual"))


  val BuiltIns: Map[UaUInt32, UaStatusCode] = 
    val codes =
      Seq(
        UaStatusCode.Good,
        UaStatusCode.GoodCallAgain,
        UaStatusCode.GoodCascade,
        UaStatusCode.GoodCascadeInitializationAcknowledged,
        UaStatusCode.GoodCascadeInitializationRequest,
        UaStatusCode.GoodCascadeNotInvited,
        UaStatusCode.GoodCascadeNotSelected,
        UaStatusCode.GoodClamped,
        UaStatusCode.GoodCommunicationEvent,
        UaStatusCode.GoodCompletesAsynchronously,
        UaStatusCode.GoodDataIgnored,
        UaStatusCode.GoodDependentValueChanged,
        UaStatusCode.GoodEdited,
        UaStatusCode.GoodEdited_DependentValueChanged,
        UaStatusCode.GoodEdited_DominantValueChanged,
        UaStatusCode.GoodEdited_DominantValueChanged_DependentValueChanged,
        UaStatusCode.GoodEntryInserted,
        UaStatusCode.GoodEntryReplaced,
        UaStatusCode.GoodFaultStateActive,
        UaStatusCode.GoodInitiateFaultState,
        UaStatusCode.GoodLocalOverride,
        UaStatusCode.GoodMoreData,
        UaStatusCode.GoodNoData,
        UaStatusCode.GoodNonCriticalTimeout,
        UaStatusCode.GoodOverload,
        UaStatusCode.GoodPostActionFailed,
        UaStatusCode.GoodResultsMayBeIncomplete,
        UaStatusCode.GoodRetransmissionQueueNotSupported,
        UaStatusCode.GoodShutdownEvent,
        UaStatusCode.GoodSubscriptionTransferred,
        UaStatusCode.Bad,
        UaStatusCode.BadAggregateConfigurationRejected,
        UaStatusCode.BadAggregateInvalidInputs,
        UaStatusCode.BadAggregateListMismatch,
        UaStatusCode.BadAggregateNotSupported,
        UaStatusCode.BadAlreadyExists,
        UaStatusCode.BadApplicationSignatureInvalid,
        UaStatusCode.BadArgumentsMissing,
        UaStatusCode.BadAttributeIdInvalid,
        UaStatusCode.BadBoundNotFound,
        UaStatusCode.BadBoundNotSupported,
        UaStatusCode.BadBrowseDirectionInvalid,
        UaStatusCode.BadBrowseNameDuplicated,
        UaStatusCode.BadBrowseNameInvalid,
        UaStatusCode.BadCertificateChainIncomplete,
        UaStatusCode.BadCertificateHostNameInvalid,
        UaStatusCode.BadCertificateInvalid,
        UaStatusCode.BadCertificateIssuerRevocationUnknown,
        UaStatusCode.BadCertificateIssuerRevoked,
        UaStatusCode.BadCertificateIssuerTimeInvalid,
        UaStatusCode.BadCertificateIssuerUseNotAllowed,
        UaStatusCode.BadCertificatePolicyCheckFailed,
        UaStatusCode.BadCertificateRevocationUnknown,
        UaStatusCode.BadCertificateRevoked,
        UaStatusCode.BadCertificateTimeInvalid,
        UaStatusCode.BadCertificateUntrusted,
        UaStatusCode.BadCertificateUriInvalid,
        UaStatusCode.BadCertificateUseNotAllowed,
        UaStatusCode.BadCommunicationError,
        UaStatusCode.BadConditionAlreadyDisabled,
        UaStatusCode.BadConditionAlreadyEnabled,
        UaStatusCode.BadConditionAlreadyShelved,
        UaStatusCode.BadConditionBranchAlreadyAcked,
        UaStatusCode.BadConditionBranchAlreadyConfirmed,
        UaStatusCode.BadConditionDisabled,
        UaStatusCode.BadConditionNotShelved,
        UaStatusCode.BadConfigurationError,
        UaStatusCode.BadConnectionClosed,
        UaStatusCode.BadConnectionRejected,
        UaStatusCode.BadContentFilterInvalid,
        UaStatusCode.BadContinuationPointInvalid,
        UaStatusCode.BadDataEncodingInvalid,
        UaStatusCode.BadDataEncodingUnsupported,
        UaStatusCode.BadDataLost,
        UaStatusCode.BadDataTypeIdUnknown,
        UaStatusCode.BadDataUnavailable,
        UaStatusCode.BadDeadbandFilterInvalid,
        UaStatusCode.BadDecodingError,
        UaStatusCode.BadDependentValueChanged,
        UaStatusCode.BadDeviceFailure,
        UaStatusCode.BadDialogNotActive,
        UaStatusCode.BadDialogResponseInvalid,
        UaStatusCode.BadDisconnect,
        UaStatusCode.BadDiscoveryUrlMissing,
        UaStatusCode.BadDominantValueChanged,
        UaStatusCode.BadDuplicateReferenceNotAllowed,
        UaStatusCode.BadEdited_OutOfRange,
        UaStatusCode.BadEdited_OutOfRange_DominantValueChanged,
        UaStatusCode.BadEdited_OutOfRange_DominantValueChanged_DependentValueChanged,
        UaStatusCode.BadEncodingError,
        UaStatusCode.BadEncodingLimitsExceeded,
        UaStatusCode.BadEndOfStream,
        UaStatusCode.BadEntryExists,
        UaStatusCode.BadEventFilterInvalid,
        UaStatusCode.BadEventIdUnknown,
        UaStatusCode.BadEventNotAcknowledgeable,
        UaStatusCode.BadExpectedStreamToBlock,
        UaStatusCode.BadFilterElementInvalid,
        UaStatusCode.BadFilterLiteralInvalid,
        UaStatusCode.BadFilterNotAllowed,
        UaStatusCode.BadFilterOperandCountMismatch,
        UaStatusCode.BadFilterOperandInvalid,
        UaStatusCode.BadFilterOperatorInvalid,
        UaStatusCode.BadFilterOperatorUnsupported,
        UaStatusCode.BadHistoryOperationInvalid,
        UaStatusCode.BadHistoryOperationUnsupported,
        UaStatusCode.BadIdentityChangeNotSupported,
        UaStatusCode.BadIdentityTokenInvalid,
        UaStatusCode.BadIdentityTokenRejected,
        UaStatusCode.BadIndexRangeInvalid,
        UaStatusCode.BadIndexRangeNoData,
        UaStatusCode.BadInitialValue_OutOfRange,
        UaStatusCode.BadInsufficientClientProfile,
        UaStatusCode.BadInternalError,
        UaStatusCode.BadInvalidArgument,
        UaStatusCode.BadInvalidSelfReference,
        UaStatusCode.BadInvalidState,
        UaStatusCode.BadInvalidTimestamp,
        UaStatusCode.BadInvalidTimestampArgument,
        UaStatusCode.BadLicenseExpired,
        UaStatusCode.BadLicenseLimitsExceeded,
        UaStatusCode.BadLicenseNotAvailable,
        UaStatusCode.BadMaxAgeInvalid,
        UaStatusCode.BadMaxConnectionsReached,
        UaStatusCode.BadMessageNotAvailable,
        UaStatusCode.BadMethodInvalid,
        UaStatusCode.BadMonitoredItemFilterInvalid,
        UaStatusCode.BadMonitoredItemFilterUnsupported,
        UaStatusCode.BadMonitoredItemIdInvalid,
        UaStatusCode.BadMonitoringModeInvalid,
        UaStatusCode.BadNoCommunication,
        UaStatusCode.BadNoContinuationPoints,
        UaStatusCode.BadNoData,
        UaStatusCode.BadNoDataAvailable,
        UaStatusCode.BadNoDeleteRights,
        UaStatusCode.BadNoEntryExists,
        UaStatusCode.BadNoMatch,
        UaStatusCode.BadNoSubscription,
        UaStatusCode.BadNoValidCertificates,
        UaStatusCode.BadNodeAttributesInvalid,
        UaStatusCode.BadNodeClassInvalid,
        UaStatusCode.BadNodeIdExists,
        UaStatusCode.BadNodeIdInvalid,
        UaStatusCode.BadNodeIdRejected,
        UaStatusCode.BadNodeIdUnknown,
        UaStatusCode.BadNodeNotInView,
        UaStatusCode.BadNonceInvalid,
        UaStatusCode.BadNotConnected,
        UaStatusCode.BadNotExecutable,
        UaStatusCode.BadNotFound,
        UaStatusCode.BadNotImplemented,
        UaStatusCode.BadNotReadable,
        UaStatusCode.BadNotSupported,
        UaStatusCode.BadNotTypeDefinition,
        UaStatusCode.BadNotWritable,
        UaStatusCode.BadNothingToDo,
        UaStatusCode.BadNumericOverflow,
        UaStatusCode.BadObjectDeleted,
        UaStatusCode.BadOperationAbandoned,
        UaStatusCode.BadOutOfMemory,
        UaStatusCode.BadOutOfRange,
        UaStatusCode.BadOutOfRange_DominantValueChanged,
        UaStatusCode.BadOutOfRange_DominantValueChanged_DependentValueChanged,
        UaStatusCode.BadOutOfService,
        UaStatusCode.BadParentNodeIdInvalid,
        UaStatusCode.BadProtocolVersionUnsupported,
        UaStatusCode.BadQueryTooComplex,
        UaStatusCode.BadReferenceLocalOnly,
        UaStatusCode.BadReferenceNotAllowed,
        UaStatusCode.BadReferenceTypeIdInvalid,
        UaStatusCode.BadRefreshInProgress,
        UaStatusCode.BadRequestCancelledByClient,
        UaStatusCode.BadRequestCancelledByRequest,
        UaStatusCode.BadRequestHeaderInvalid,
        UaStatusCode.BadRequestInterrupted,
        UaStatusCode.BadRequestNotAllowed,
        UaStatusCode.BadRequestNotComplete,
        UaStatusCode.BadRequestTimeout,
        UaStatusCode.BadRequestTooLarge,
        UaStatusCode.BadRequestTypeInvalid,
        UaStatusCode.BadResourceUnavailable,
        UaStatusCode.BadResponseTooLarge,
        UaStatusCode.BadSecureChannelClosed,
        UaStatusCode.BadSecureChannelIdInvalid,
        UaStatusCode.BadSecureChannelTokenUnknown,
        UaStatusCode.BadSecurityChecksFailed,
        UaStatusCode.BadSecurityModeInsufficient,
        UaStatusCode.BadSecurityModeRejected,
        UaStatusCode.BadSecurityPolicyRejected,
        UaStatusCode.BadSempahoreFileMissing,
        UaStatusCode.BadSensorFailure,
        UaStatusCode.BadSequenceNumberInvalid,
        UaStatusCode.BadSequenceNumberUnknown,
        UaStatusCode.BadServerHalted,
        UaStatusCode.BadServerIndexInvalid,
        UaStatusCode.BadServerNameMissing,
        UaStatusCode.BadServerNotConnected,
        UaStatusCode.BadServerUriInvalid,
        UaStatusCode.BadServiceUnsupported,
        UaStatusCode.BadSessionClosed,
        UaStatusCode.BadSessionIdInvalid,
        UaStatusCode.BadSessionNotActivated,
        UaStatusCode.BadShelvingTimeOutOfRange,
        UaStatusCode.BadShutdown,
        UaStatusCode.BadSourceNodeIdInvalid,
        UaStatusCode.BadStateNotActive,
        UaStatusCode.BadStructureMissing,
        UaStatusCode.BadSubscriptionIdInvalid,
        UaStatusCode.BadSyntaxError,
        UaStatusCode.BadTargetNodeIdInvalid,
        UaStatusCode.BadTcpEndpointUrlInvalid,
        UaStatusCode.BadTcpInternalError,
        UaStatusCode.BadTcpMessageTooLarge,
        UaStatusCode.BadTcpMessageTypeInvalid,
        UaStatusCode.BadTcpNotEnoughResources,
        UaStatusCode.BadTcpSecureChannelUnknown,
        UaStatusCode.BadTcpServerTooBusy,
        UaStatusCode.BadTicketInvalid,
        UaStatusCode.BadTicketRequired,
        UaStatusCode.BadTimeout,
        UaStatusCode.BadTimestampNotSupported,
        UaStatusCode.BadTimestampsToReturnInvalid,
        UaStatusCode.BadTooManyArguments,
        UaStatusCode.BadTooManyMatches,
        UaStatusCode.BadTooManyMonitoredItems,
        UaStatusCode.BadTooManyOperations,
        UaStatusCode.BadTooManyPublishRequests,
        UaStatusCode.BadTooManySessions,
        UaStatusCode.BadTooManySubscriptions,
        UaStatusCode.BadTypeDefinitionInvalid,
        UaStatusCode.BadTypeMismatch,
        UaStatusCode.BadUnexpectedError,
        UaStatusCode.BadUnknownResponse,
        UaStatusCode.BadUserAccessDenied,
        UaStatusCode.BadUserSignatureInvalid,
        UaStatusCode.BadViewIdUnknown,
        UaStatusCode.BadViewParameterMismatch,
        UaStatusCode.BadViewTimestampInvalid,
        UaStatusCode.BadViewVersionInvalid,
        UaStatusCode.BadWaitingForInitialData,
        UaStatusCode.BadWaitingForResponse,
        UaStatusCode.BadWouldBlock,
        UaStatusCode.BadWriteNotSupported,
        UaStatusCode.Uncertain,
        UaStatusCode.UncertainConfigurationError,
        UaStatusCode.UncertainDataSubNormal,
        UaStatusCode.UncertainDependentValueChanged,
        UaStatusCode.UncertainDominantValueChanged,
        UaStatusCode.UncertainEngineeringUnitsExceeded,
        UaStatusCode.UncertainInitialValue,
        UaStatusCode.UncertainLastUsableValue,
        UaStatusCode.UncertainNoCommunicationLastUsableValue,
        UaStatusCode.UncertainNotAllNodesAvailable,
        UaStatusCode.UncertainReferenceNotDeleted,
        UaStatusCode.UncertainReferenceOutOfServer,
        UaStatusCode.UncertainSensorCalibration,
        UaStatusCode.UncertainSensorNotAccurate,
        UaStatusCode.UncertainSimulatedValue,
        UaStatusCode.UncertainSubNormal,
        UaStatusCode.UncertainSubstituteValue,
        UaStatusCode.UncertainTransducerInManual)

    codes.map(status => status.code -> status).toMap
