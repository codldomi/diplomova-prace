package cz.modemtec.scopcua.util.binarydecoder.impl

import cz.modemtec.scopcua.data.UaDataType.{GeneralEnumeration, GeneralStructure}
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.value.number.*
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.util.binarydecoder.UaBinaryDecoder
import cz.modemtec.scopcua.util.binarydecoder.impl.util.BinaryReadBuffer
import cz.modemtec.scopcua.util.binarydecoder.impl.util.BinaryReadBuffer.releasable
import cz.modemtec.scopcua.util.valuecreator.impl.UaDefaultValueCreator

import scala.util.{Failure, Success, Using}


/** Decoder for the OPC UA Binary DataEncoding.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.1/ Part 6, OPC UA Binary Data Encoding]]
  */
class UaStdBinaryDecoder(val typeOf: PartialFunction[UaId, UaDataType]) extends UaBinaryDecoder:

  private val creator = UaDefaultValueCreator(typeOf)
  private val statusCodes = UaStatusCode.BuiltIns

  /** Manages buffer's lifecycle (from creating to releasing) and runs the code.
    *
    * @param encoded Sequence of bytes.
    * @param function Code run on buffer.
    * @tparam A DataType of the decoded value.
    * @return Decoded value.
    */
  private def usingBuffer[A <: UaValue](encoded: Seq[UaByte])(function: BinaryReadBuffer => A): A = 
    val result = Using(BinaryReadBuffer(statusCodes)): buffer => 
      buffer.writeBytes(encoded)
      function(buffer)
    
    result match 
      case Failure(exception) => throw exception
      case Success(value)     => value
    

  override def decodeBoolean(encoded: Seq[UaByte]): UaBoolean = usingBuffer(encoded): buffer =>
    buffer.readBoolean()
  

  override def decodeByte(encoded: Seq[UaByte]): UaByte = usingBuffer(encoded): buffer =>
    buffer.readByte()
  

  override def decodeSByte(encoded: Seq[UaByte]): UaSByte = usingBuffer(encoded): buffer =>
    buffer.readSByte()
  

  override def decodeInt16(encoded: Seq[UaByte]): UaInt16 = usingBuffer(encoded): buffer =>
    buffer.readInt16()
  

  override def decodeUInt16(encoded: Seq[UaByte]): UaUInt16 = usingBuffer(encoded): buffer =>
    buffer.readUInt16()
  

  override def decodeInt32(encoded: Seq[UaByte]): UaInt32 = usingBuffer(encoded): buffer =>
    buffer.readInt32()
  

  override def decodeUInt32(encoded: Seq[UaByte]): UaUInt32 = usingBuffer(encoded): buffer =>
    buffer.readUInt32()
  

  override def decodeInt64(encoded: Seq[UaByte]): UaInt64 = usingBuffer(encoded): buffer =>
    buffer.readInt64()
  

  override def decodeUInt64(encoded: Seq[UaByte]): UaUInt64 = usingBuffer(encoded): buffer =>
    buffer.readUInt64()
  

  override def decodeFloat(encoded: Seq[UaByte]): UaFloat = usingBuffer(encoded): buffer =>
    buffer.readFloat()
  

  override def decodeDouble(encoded: Seq[UaByte]): UaDouble = usingBuffer(encoded): buffer =>
    buffer.readDouble()
  

  override def decodeString(encoded: Seq[UaByte]): UaString = usingBuffer(encoded): buffer =>
    buffer.readString()
  

  override def decodeXmlElement(encoded: Seq[UaByte]): UaXmlElement = usingBuffer(encoded): buffer =>
    buffer.readXmlElement()
  

  override def decodeByteString(encoded: Seq[UaByte]): UaByteString = usingBuffer(encoded): buffer =>
    buffer.readByteString()
  

  override def decodeGuid(encoded: Seq[UaByte]): UaGuid = usingBuffer(encoded): buffer =>
    buffer.readGuid()
  

  override def decodeNodeId(encoded: Seq[UaByte]): UaNodeId = usingBuffer(encoded): buffer =>
    buffer.readNodeId()
  

  override def decodeExpandedNodeId(encoded: Seq[UaByte]): UaExpandedNodeId = usingBuffer(encoded): buffer =>
    buffer.readExpandedNodeId()
  

  override def decodeQualifiedName(encoded: Seq[UaByte]): UaQualifiedName = usingBuffer(encoded): buffer =>
    buffer.readQualifiedName()
  

  override def decodeLocalizedText(encoded: Seq[UaByte]): UaLocalizedText = usingBuffer(encoded): buffer =>
    buffer.readLocalizedText()
  

  override def decodeDataValue(encoded: Seq[UaByte]): UaDataValue = usingBuffer(encoded): buffer =>
    buffer.readDataValue()
  

  override def decodeDiagnosticInfo(encoded: Seq[UaByte]): UaDiagnosticInfo = usingBuffer(encoded): buffer =>
    buffer.readDiagnosticInfo()
  

  override def decodeDateTime(encoded: Seq[UaByte]): UaDateTime = usingBuffer(encoded): buffer =>
    buffer.readDateTime()
  

  override def decodeStatusCode(encoded: Seq[UaByte]): UaStatusCode = usingBuffer(encoded): buffer =>
    buffer.readStatusCode()
  

  override def decodeVariant(encoded: Seq[UaByte]): UaVariant = usingBuffer(encoded): buffer =>
    buffer.readVariant()
  

  override def decodeExtensionObject(encoded: Seq[UaByte]): UaExtensionObject = usingBuffer(encoded): buffer =>
    buffer.readExtensionObject()
  

  override def decodeStructure(encoded: Seq[UaByte], id: UaId): UaStructure = usingBuffer(encoded): buffer =>
    val dataType = typeOf(id) match 
      case t: UaDataType.GeneralStructure => t
      case _ => throw new IllegalArgumentException("The selected DataType is not a structure.")
    
    decodeGeneralStructure(buffer, dataType)
  

  override def decodeEnumeration(encoded: Seq[UaByte], id: UaId): UaEnumeration = usingBuffer(encoded): buffer =>
    val dataType = typeOf(id) match 
      case e: UaDataType.GeneralEnumeration => e
      case _ => throw new IllegalArgumentException("The selected DataType is not an enumeration.")
    
    decodeGeneralEnumeration(buffer, dataType)
  

  override def decode(encoded: Seq[UaByte], typeId: UaId): UaValue = usingBuffer(encoded): buffer =>
    decodeValue(buffer, typeOf(typeId))
  

  private def decodeValue(buffer: BinaryReadBuffer, dataType: UaDataType): UaValue = dataType match 
    case UaDataType.Boolean               => buffer.readBoolean()
    case UaDataType.SByte                 => buffer.readSByte()
    case UaDataType.Byte                  => buffer.readByte()
    case UaDataType.Int16                 => buffer.readInt16()
    case UaDataType.UInt16                => buffer.readUInt16()
    case UaDataType.Int32                 => buffer.readInt32()
    case UaDataType.UInt32                => buffer.readUInt32()
    case UaDataType.Int64                 => buffer.readInt64()
    case UaDataType.UInt64                => buffer.readUInt64()
    case UaDataType.Float                 => buffer.readFloat()
    case UaDataType.Double                => buffer.readDouble()
    case UaDataType.String                => buffer.readString()
    case UaDataType.DateTime              => buffer.readDateTime()
    case UaDataType.Guid                  => buffer.readGuid()
    case UaDataType.ByteString            => buffer.readByteString()
    case UaDataType.XmlElement            => buffer.readXmlElement()
    case UaDataType.NodeId                => buffer.readNodeId()
    case UaDataType.ExpandedNodeId        => buffer.readExpandedNodeId()
    case UaDataType.StatusCode            => buffer.readStatusCode()
    case UaDataType.QualifiedName         => buffer.readQualifiedName()
    case UaDataType.LocalizedText         => buffer.readLocalizedText()
    case UaDataType.Structure             => buffer.readExtensionObject()
    case UaDataType.DataValue             => buffer.readDataValue()
    case UaDataType.BaseDataType          => buffer.readVariant()
    case UaDataType.DiagnosticInfo        => buffer.readDiagnosticInfo()
    case t: GeneralEnumeration            => decodeGeneralEnumeration(buffer, t)
    case t: GeneralStructure              => decodeGeneralStructure(buffer, t)

    case _ => throw new IllegalArgumentException("Unsupported data type.")
  
  
  private def decodeGeneralStructure(buffer: BinaryReadBuffer, dataType: GeneralStructure): UaStructure = 

    def createBits(mask: Int, dataType: UaDataType.GeneralStructure): Seq[Boolean] =
      var value = mask
      dataType
        .fields
        .foldLeft(Seq.empty[Boolean]): (bits, field) =>
          var bit = true
          if field.isOptional == UaBoolean(true) then
            if value % 2 == 0 then
              bit = false
            value = value >>> 1
          bits :+ bit

    def decodeArray(buffer: BinaryReadBuffer, dataType: UaDataType): UaArray =
      val length = buffer.readInt32()

      if length.value <= 0 then
        UaArray()
      else
        val values = for (_ <- 0 until length.value) yield decodeValue(buffer, dataType)
        UaArray(values.toVector)

    val hasOptionalFields =
      dataType
        .fields
        .exists(_.isOptional == UaBoolean(true))

    val mask = if hasOptionalFields then buffer.readByte().value.toInt else 0

    val bits = createBits(mask, dataType)
    val fieldDescs = dataType.fields.zip(bits)

    val fields = fieldDescs.foldLeft(Map.empty[UaString, UaValue]):
      case (result, (desc, isPresent)) =>
        val fieldType = typeOf(desc.typeId)
        val value = (isPresent, desc.isArray) match 
          case (true, true)  => decodeArray(buffer, fieldType) 
          case (true, false) => decodeValue(buffer, fieldType) 
          case _             => creator.create(fieldType)
            
        result + (desc.name -> value)

    UaStructure(fields)

  
  private def decodeGeneralEnumeration(buffer: BinaryReadBuffer, dataType: GeneralEnumeration): UaEnumeration =
    val value = buffer.readInt32()
    val result = dataType.values.find: (_, current) =>
      current == value

    val name = result match
      case Some(found) => found._1
      case None        => UaString()

    UaEnumeration(value, name)


