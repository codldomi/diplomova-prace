package cz.modemtec.scopcua.data.value.number

import cz.modemtec.scopcua.data.value.UaValue


trait UaNumber extends UaValue:

  def asBigDecimal: BigDecimal


