package cz.modemtec.scopcua.data.value

/** A structure that contains an application specific data type that may not be recognized by the receiver.
  *
  * An ExtensionObject is a container for any Structured DataTypes which cannot be encoded as one of the other built-in
  * data types. The ExtensionObject contains a complex value serialized as a sequence of bytes or as an XML element.
  * It also contains an identifier which indicates what data it contains and how it is encoded.
  *
  * Structured DataTypes are represented in a Server address space as sub-types of the Structure DataType. The
  * DataEncodings available for any given Structured DataTypes are represented as a DataTypeEncoding Object in the
  * Server AddressSpace. The NodeId for the DataTypeEncoding Object is the identifier stored in the ExtensionObject.
  * OPC 10000-3 describes how DataTypeEncoding Nodes are related to other Nodes of the AddressSpace.
  *
  * Server implementers should use namespace qualified numeric NodeIds for any DataTypeEncoding Objects they define.
  * This will minimize the overhead introduced by packing Structured DataType values into an ExtensionObject.
  *
  * ExtensionObjects and Variants allow unlimited nesting which could result in stack overflow errors even if the
  * message size is less than the maximum allowed. Decoders shall support at least 100 nesting levels. Decoders shall
  * report an error if the number of nesting levels exceeds what it supports.
  */
sealed trait UaExtensionObject extends UaValue {

  def encodingId: UaNodeId

}

/** ExtensionObject holding a value encoding as array of bytes.
  */
case class UaByteExtensionObject(encodingId: UaNodeId, body: UaByteString) extends UaExtensionObject

/** ExtensionObject holding a value encoding as XmlElement.
  */
case class UaXmlExtensionObject(encodingId: UaNodeId, body: UaXmlElement) extends UaExtensionObject