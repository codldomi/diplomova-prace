package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel

import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation


case class NodeSetModel(
  name: String,
  uris: Seq[String],
  aliases: Seq[AliasModel],
  dataTypes: Seq[DataTypeModel],
  variables: Seq[VariableModel],
  methods: Seq[MethodModel],
  objects: Seq[ObjectModel],
  objectTypes: Seq[ObjectTypeModel],
  variableTypes: Seq[VariableTypeModel]
)


object NodeSetModel:
  
  def empty: NodeSetModel =
    NodeSetModel(
      name          = "",
      uris          = Seq(OpcUaFoundation.value), 
      aliases       = Seq.empty,
      dataTypes     = Seq.empty,
      variables     = Seq.empty,
      methods       = Seq.empty, 
      objects       = Seq.empty,
      objectTypes   = Seq.empty,
      variableTypes = Seq.empty)
