package cz.modemtec.scopcua.data.value

import scala.annotation.tailrec

/** A representation of OPC UA array. It can be empty, one-dimensional or multi-dimensional array of the selected type.
  *
  * @param values A sequence of value with given index.
  */
case class UaArray(values: Vector[UaValue]) extends UaValue:

  //  todo: Constraints
  //   1. all non-UaArray elements are instances of the same type
  //   2. a multi-dimensional array

  def isEmpty: Boolean = values.isEmpty
  def nonEmpty: Boolean = values.nonEmpty
  def length: Int = values.length
  def apply(index: Int): UaValue = values(index)
  def tail: UaArray = UaArray(values.tail)
  def head: UaValue = values.head
  def isMultiDimensional: Boolean = dimensions.length > 1
  
  def dimensions: Vector[Int] =
    
    @tailrec
    def dimensions(acc: Vector[Int], value: UaValue): Vector[Int] = 
      value match 
        case array: UaArray if array.length > 0 => dimensions(acc :+ array.length, array.head)
        case _ => acc
    
    dimensions(Vector.empty, this)
  

  def flatten: UaArray =
    
    def flatten(acc: Vector[UaValue], value: UaValue): Vector[UaValue] = 
      value match 
        case UaArray(values) => values.foldLeft(acc)(flatten)
        case _ => value +: acc
    
    val flattenValues = flatten(Vector.empty, this).reverse
    copy(values = flattenValues)


object UaArray:

  def apply(values: UaValue*): UaArray = UaArray(values.toVector)

  def ofDim(dimensions: Int*)(values: UaValue*): UaArray =

    @tailrec
    def ofDim(acc: UaArray, dimensions: Vector[Int]): UaArray =
      if dimensions.isEmpty then
        return acc
      
      val dim = dimensions.head
      val groupedValues =
        acc
          .values
          .grouped(dim)
          .map(group => new UaArray(group))
          .toVector

      ofDim(new UaArray(groupedValues), dimensions.tail)
    
    if dimensions.isEmpty then
      UaArray(values.toVector)
    else 
      ofDim(UaArray(values.toVector), dimensions.tail.toVector.reverse)



