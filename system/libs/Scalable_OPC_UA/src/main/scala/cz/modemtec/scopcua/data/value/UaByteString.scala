package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaByte

/** A sequence of octets.
  */
case class UaByteString(bytes: Vector[UaByte] = Vector.empty) extends UaValue:

  def length: Int = bytes.length
  def isEmpty: Boolean = bytes.isEmpty
  def nonEmpty: Boolean = bytes.nonEmpty


object UaByteString:
  def apply(bytes: UaByte*): UaByteString = UaByteString(bytes.toVector)