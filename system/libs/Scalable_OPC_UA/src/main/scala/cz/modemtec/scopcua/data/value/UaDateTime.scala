package cz.modemtec.scopcua.data.value

/** An instance in time. A DateTime value is an 64-bit signed integer which represents the number of 100 nanosecond
  * intervals since January 1, 1601 (UTC).
  *
  * @param value Scala 64-bit signed integer.
  */
case class UaDateTime(value: Long) extends UaValue