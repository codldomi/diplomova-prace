package cz.modemtec.scopcua.data.value.number

/** An IEEE single precision (32 bit) floating point value.
  *
  * @param value Scala representation.
  */
case class UaFloat(value: Float) extends UaNumber:

  override def asBigDecimal: BigDecimal = BigDecimal(value)

