package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class FieldModel(
  name: String,
  value: Option[Int],
  dataType: String,
  isOptional: Boolean,
  valueRank: Int,
  arrayDimensions: Array[Int]
)
