package cz.modemtec.scopcua.util.nodesetextractor

import cz.modemtec.scopcua.data.node.UaNodeSet

import scala.xml.Node


/** A parser for NodeSet containing nodes - typically from the one namespace. So its URI is the same as ModelURI parameter.
  */
trait UaNodeSetExtractor:

  /** Extract NodeSet data from XML node.
    *
    * @param nodeset NodeSet in XML encoding.
    * @return Extracted NodeSet.
    */
  def extract(nodeset: Node): UaNodeSet

