package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

/** A NodeId that allows the namespace URI to be specified instead of an index.
  *
  * @param namespaceUri The URI of the namespace. If this parameter is specified then the namespace index is ignored.
  *                     OPC 10000-12 describes discovery mechanism that can be used to resolve URIs into URLs.
  *
  * @param serverIndex  Index that identifies the Server that contains the TargetNode. This Server may be the local
  *                     Server or a remote Server. This index is the index of that Server in the local Server’s Server
  *                     table. The index of the local Server in the Server table is always 0. All remote Servers have
  *                     indexes greater than 0. The Server table is contained in the Server Object in the AddressSpace
  *                     (see OPC 10000-3 and OPC 10000-5). The Client may read the Server table Variable to access the
  *                     description of the target Server.
  *
  * @param nodeId       The inner NodeId which is expanded by namespaceUri and serverIndex.
  */
case class UaExpandedNodeId(namespaceUri: UaString, nodeId: UaNodeId, serverIndex: UaUInt32) extends UaValue
