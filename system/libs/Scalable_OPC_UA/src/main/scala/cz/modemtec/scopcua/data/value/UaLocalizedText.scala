package cz.modemtec.scopcua.data.value

/** Human readable text with an optional locale identifier.
  *
  * @param locale The identifier for the locale (e.g. “en-US”).
  * @param text The localized text.
  */
case class UaLocalizedText(text: UaString, locale: UaString) extends UaValue
