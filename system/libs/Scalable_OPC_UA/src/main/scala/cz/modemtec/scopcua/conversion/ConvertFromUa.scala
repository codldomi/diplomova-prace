package cz.modemtec.scopcua.conversion

import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.data.value.number.integer.*

import java.sql.Timestamp
import java.util.{Date, TimeZone}
import java.text.SimpleDateFormat


object ConvertFromUa:

  object ToAll extends ToAll

  object ToBigInt extends ToBigInt
  object ToLong extends ToLong
  object ToInt extends ToInt
  object ToShort extends ToShort
  object ToByte extends ToByte
  object ToBoolean extends ToBoolean
  object ToString extends ToString
  object ToFloat extends ToFloat
  object ToDouble extends ToDouble
  object ToTimestamp extends ToTimestamp

  
  trait ToAll
    extends ToBigInt
      with ToLong
      with ToInt
      with ToShort
      with ToByte
      with ToBoolean
      with ToString
      with ToFloat
      with ToDouble
      with ToTimestamp

  
  trait ToBigInt:
    given bigIntFromUaUInt64: Conversion[UaUInt64, BigInt] = (opc: UaUInt64) => opc.value


  trait ToLong:
    given longFromUaByte: Conversion[UaByte, Long] = (opc: UaByte) => opc.value
    given longFromUaSByte: Conversion[UaSByte, Long] = (opc: UaSByte) => opc.value
    given longFromUaInt16: Conversion[UaInt16, Long] = (opc: UaInt16) => opc.value
    given longFromUaUInt16: Conversion[UaUInt16, Long] = (opc: UaUInt16) => opc.value
    given longFromUaInt32: Conversion[UaInt32, Long] = (opc: UaInt32) => opc.value
    given longFromUaUInt32: Conversion[UaUInt32, Long] = (opc: UaUInt32) => opc.value
    given longFromUaInt64: Conversion[UaInt64, Long] = (opc: UaInt64) => opc.value
    given longFromUaDateTime: Conversion[UaDateTime, Long] = (opc: UaDateTime) => opc.value


  trait ToInt:
    given intFromUaByte: Conversion[UaByte, Int] = (opc: UaByte) => opc.value
    given intFromUaSByte: Conversion[UaSByte, Int] = (opc: UaSByte) => opc.value
    given intFromUaInt16: Conversion[UaInt16, Int] = (opc: UaInt16) => opc.value
    given intFromUaUInt16: Conversion[UaUInt16, Int] = (opc: UaUInt16) => opc.value
    given intFromUaInt32: Conversion[UaInt32, Int] = (opc: UaInt32) => opc.value


  trait ToShort:
    given shortFromUaByte: Conversion[UaByte, Short] = (opc: UaByte) => opc.value
    given shortFromUaSByte: Conversion[UaSByte, Short] = (opc: UaSByte) => opc.value
    given shortFromUaInt16: Conversion[UaInt16, Short] = (opc: UaInt16) => opc.value


  trait ToByte:
    given byteFromUaSByte: Conversion[UaSByte, Byte] = (opc: UaSByte) => opc.value


  trait ToBoolean:
    given booleanFromUaBoolean: Conversion[UaBoolean, Boolean] = (opc: UaBoolean) => opc.value


  trait ToFloat:
    given floatFromUaFloat: Conversion[UaFloat, Float] = (opc: UaFloat) => opc.value


  trait ToDouble:
    given doubleFromUaDouble: Conversion[UaDouble, Double] = (opc: UaDouble) => opc.value


  trait ToString:
    given stringFromUaString: Conversion[UaString, String] = (opc: UaString) => opc.value
    given stringFromXmlElement: Conversion[UaXmlElement, String] = (opc: UaXmlElement) => opc.value
    given stringFromUaDateTime: Conversion[UaDateTime, String] = (opc: UaDateTime) =>
      import ToTimestamp.timestampFromUaDateTime

      val unix = timestampFromUaDateTime(opc)
      val date = new Date(unix.getTime)

      val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
      df.setTimeZone(TimeZone.getTimeZone("UTC"))

      df.format(date)

    given stringFromUaGuid: Conversion[UaGuid, String] = (guid: UaGuid) =>
      def hexa(bytes: Seq[UaByte], from: Int, to: Int): String =
        Seq
          .range(from, to)
          .map: index =>
            val byte = bytes(index)
            f"${byte.value}%02X"
          .mkString

      val data1 = f"${guid.data1.value}%08X"
      val data2 = f"${guid.data2.value}%04X"
      val data3 = f"${guid.data3.value}%04X"
      val data4 = hexa(guid.data4, 0, 2)
      val data5 = hexa(guid.data4, 2, 8)

      s"$data1-$data2-$data3-$data4-$data5"


  trait ToTimestamp :
    given timestampFromUaDateTime: Conversion[UaDateTime, Timestamp] = (dateTime: UaDateTime) =>
      val usec = 10L
      val msec = usec * 1000L
      val sec = msec * 1000L
      val unixEpoch = 11644473600L * sec

      val unix = (dateTime.value - unixEpoch) / sec

      Timestamp(unix * 1000)

