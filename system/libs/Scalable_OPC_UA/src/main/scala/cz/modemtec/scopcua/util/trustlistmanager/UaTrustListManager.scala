package cz.modemtec.scopcua.util.trustlistmanager

import org.eclipse.milo.opcua.stack.core.security.TrustListManager


/** A manager for OPC UA certificate trust list.
  */
trait UaTrustListManager extends TrustListManager
