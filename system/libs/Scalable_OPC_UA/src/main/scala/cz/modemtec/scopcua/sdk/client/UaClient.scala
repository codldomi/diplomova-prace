package cz.modemtec.scopcua.sdk.client

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.service.UaReadValue
import cz.modemtec.scopcua.data.value.number.UaDouble
import cz.modemtec.scopcua.data.value.{UaDateTime, UaString, UaValue}
import cz.modemtec.scopcua.data.value.number.integer.{UaUInt16, UaUInt32}

import scala.concurrent.Future

/** OPC UA client's interface.
  */
trait UaClient:

  /** Reads the current value.
    *
    * @note The data type is read from the server.
    * @param id Variable's identifier.
    * @return The value and its source time.
    */
  def readValue(id: UaId): Future[UaReadValue]

  /** Reads the current value.
    *
    * @note The data type is provided by user.
    * @param typeId Variable's data type.
    * @param id Variable's identifier.
    * @return The value and its source time.
    */
  def readValue(id: UaId, typeId: UaId): Future[UaReadValue]

  /** Writes the value.
    *
    * @note The data type is read from the server.
    * @param id Variable's identifier.
    * @param value The value.
    */
  def writeValue(id: UaId, value: UaValue): Future[Unit]

  /** Writes the value.
    *
    * @note The data type is provided by user.
    * @param typeId Variable's data type.
    * @param id Variable's identifier.
    * @param value The value.
    */
  def writeValue(id: UaId, value: UaValue, typeId: UaId): Future[Unit]

  /** Writes the value, but allows null value.
    *
    * @note The data type is read from the server.
    * @param id Variable's identifier.
    * @param value The value or None - if the value is Null.
    */
  def writeValue(id: UaId, value: Option[UaValue]): Future[Unit]

  /** Writes the value, but allows null value.
    *
    * @note The data type is provider by user.
    * @param id Variable's identifier.
    * @param typeId The data type of the value.
    * @param value The value or None - if the value is Null.
    */
  def writeValue(id: UaId, value: Option[UaValue], typeId: UaId): Future[Unit]

  /** Calls the method on the object.
    *
    * @param objectId Object's identifier.
    * @param name Method's name.
    * @param args Input Arguments.
    * @return Output Arguments.
    */
  def callMethod(objectId: UaId, name: UaString, args: Map[UaString, UaValue]): Future[Map[UaString, UaValue]]

  /** Reads history data. Values are server one by one in callback. The whole stream is stopped when resulting Future is done.
    *
    * @note The data type is provided by user.
    * @param id          Variable's identifier.
    * @param from        Lower bound of time interval.
    * @param to          Upper bound of time interval.
    * @param typeId      The data type of the value.
    * @param callback    Function with returned value
    */
  def readHistory(id: UaId, from: UaDateTime, to: UaDateTime, typeId: UaId)(callback: UaReadValue => _): Future[Unit]

  /** Reads history data. Values are server one by one in callback. The whole stream is stopped when resulting Future is done.
    *
    * @note The data type is read from the server.
    * @param id          Variable's identifier.
    * @param from        Lower bound of time interval.
    * @param to          Upper bound of time interval.
    * @param callback    Function with returned value
    */
  def readHistory(id: UaId, from: UaDateTime, to: UaDateTime)(callback: UaReadValue => _): Future[Unit]

  /** Reads the NamespaceArray.
    *
    * @return NamespaceIndexes mapped to NamespaceUris.
    */
  def readNamespaces(): Future[Map[UaUInt16, UaString]]

  /** Creates Subscription.
    *
    * @param publishingInterval PublishingInterval in milliseconds.
    * @return Subscription's identifier.
    */
  def createSubscription(publishingInterval: UaDouble): Future[UaUInt32]

  /** Creates Monitored Item on Variable's value within the selected Subscription.
    *
    * @note The data type is provided by user.
    * @param subId      Subscription's identifier.
    * @param variableId Variable's identifier.
    * @param callback Function (new value, monitored item ID, subscription ID) => _
    * @return Monitored Item's identifier.
    */
  def createMonitoredItem(subId: UaUInt32, variableId: UaId, typeId: UaId)(callback: (UaReadValue, UaUInt32, UaUInt32) => _): Future[UaUInt32]

  /** Creates Monitored Item on Variable's value within the selected Subscription.
    *
    * @note The data type is read from the server.
    * @param subId Subscription's identifier.
    * @param variableId Variable's identifier.
    * @param callback Function (new value, monitored item ID, subscription ID) => _
    * @return Monitored Item's identifier.
    */
  def createMonitoredItem(subId: UaUInt32, variableId: UaId)(callback: (UaReadValue, UaUInt32, UaUInt32) => _): Future[UaUInt32]

  /** Deletes Monitored Item by its identifier and Subscription's identifier.
    *
    * @param subId Subscription's identifier.
    * @param itemId Monitored Item's identifier.
    */
  def deleteMonitoredItem(subId: UaUInt32, itemId: UaUInt32): Future[Unit]

  /** Deletes Subscription (and its Monitored Items) by identifier.
    *
    * @param id Subscription's identifier.
    */
  def deleteSubscription(id: UaUInt32): Future[Unit]

  /** Reads the current DataType's identifier.
    *
    * @param id Variable's identifier.
    * @return DataType's identifier.
    */
  def readDataTypeId(id: UaId): Future[UaId]

  /** Connects to a server.
    */
  def connect(): Future[Unit]

  /** Disconnects from a server.
    */
  def disconnect(): Future[Unit]

