package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class VariableModel(
  id: String,
  parentId: Option[String],
  browseName: String,
  displayName: String,
  description: Option[String],
  references: Seq[ReferenceModel],
  typeId: String,
  valueRank: Int,
  arrayDimensions: Array[Int],
  args: Seq[ArgumentModel],
  symbolicName: Option[String]
) extends NodeModel
