package cz.modemtec.scopcua.util.trustlistmanager.impl

import com.google.common.collect.ImmutableList

import cz.modemtec.scopcua.util.trustlistmanager.UaTrustListManager

import org.eclipse.milo.opcua.stack.core.security.DefaultTrustListManager
import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString

import java.io.File
import java.security.cert.{X509CRL, X509Certificate}
import java.util


/** Wrapper for Eclipse Milo default Trust List.
  *
  * @param baseDir Directory for Public Key Infrastructure.
  */
class UaMiloTrustListManager(baseDir: File) extends UaTrustListManager with AutoCloseable:

  private val underlying = new DefaultTrustListManager(baseDir)

  override def close(): Unit =
    underlying.close()

  override def getIssuerCrls: ImmutableList[X509CRL] =
    underlying.getIssuerCrls

  override def getTrustedCrls: ImmutableList[X509CRL] =
    underlying.getTrustedCrls

  override def getIssuerCertificates: ImmutableList[X509Certificate] =
    underlying.getIssuerCertificates

  override def getTrustedCertificates: ImmutableList[X509Certificate] =
    underlying.getTrustedCertificates

  override def getRejectedCertificates: ImmutableList[X509Certificate] =
    underlying.getRejectedCertificates

  override def setIssuerCrls(issuerCrls: util.List[X509CRL]): Unit =
    underlying.setIssuerCrls(issuerCrls)

  override def setTrustedCrls(trustedCrls: util.List[X509CRL]): Unit =
    underlying.setTrustedCrls(trustedCrls)

  override def setIssuerCertificates(issuerCertificates: util.List[X509Certificate]): Unit =
    underlying.setIssuerCertificates(issuerCertificates)

  override def setTrustedCertificates(trustedCertificates: util.List[X509Certificate]): Unit =
    underlying.setTrustedCertificates(trustedCertificates)

  override def addIssuerCertificate(certificate: X509Certificate): Unit =
    underlying.addIssuerCertificate(certificate)

  override def addTrustedCertificate(certificate: X509Certificate): Unit =
    underlying.addTrustedCertificate(certificate)

  override def addRejectedCertificate(certificate: X509Certificate): Unit =
    underlying.addRejectedCertificate(certificate)

  override def removeIssuerCertificate(thumbprint: ByteString): Boolean =
    underlying.removeIssuerCertificate(thumbprint)

  override def removeTrustedCertificate(thumbprint: ByteString): Boolean =
    underlying.removeTrustedCertificate(thumbprint)

  override def removeRejectedCertificate(thumbprint: ByteString): Boolean =
    underlying.removeRejectedCertificate(thumbprint)


