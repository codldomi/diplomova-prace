package cz.modemtec.scopcua.data.value

/** An XML element.
  *
  * @param value Scala representation.
  */
case class UaXmlElement(value: String = "") extends UaValue:

  def isEmpty: Boolean = value.isEmpty
  def nonEmpty: Boolean = !isEmpty

