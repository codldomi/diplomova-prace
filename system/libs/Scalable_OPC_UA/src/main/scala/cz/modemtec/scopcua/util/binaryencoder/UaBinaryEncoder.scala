package cz.modemtec.scopcua.util.binaryencoder

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.data.value.{UaBoolean, UaByteString, UaDataValue, UaDateTime, UaDiagnosticInfo, UaEnumeration, UaExpandedNodeId, UaExtensionObject, UaGuid, UaLocalizedText, UaNodeId, UaQualifiedName, UaStatusCode, UaString, UaStructure, UaValue, UaVariant, UaXmlElement}
import cz.modemtec.scopcua.data.value.number.integer.{UaByte, UaInt16, UaInt32, UaInt64, UaSByte, UaUInt16, UaUInt32, UaUInt64}


/** Encoder to binary data encoding from the value.
  */
trait UaBinaryEncoder:

  /** Encodes Value into binary format.
    *
    * @param value Selected value.
    * @param typeId DataType of the value.
    * @return Sequence of bytes.
    */
  def encode(value: UaValue, typeId: UaId): Vector[UaByte]

  /** Encodes Boolean into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeBoolean(value: UaBoolean): Vector[UaByte]

  /** Encodes Byte into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeByte(value: UaByte): Vector[UaByte]

  /** Encodes SByte into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeSByte(value: UaSByte): Vector[UaByte]

  /** Encodes Int16 into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeInt16(value: UaInt16): Vector[UaByte]

  /** Encodes UInt16 into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeUInt16(value: UaUInt16): Vector[UaByte]

  /** Encodes Int32 into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeInt32(value: UaInt32): Vector[UaByte]

  /** Encodes UInt32 into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeUInt32(value: UaUInt32): Vector[UaByte]

  /** Encodes Int64 into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeInt64(value: UaInt64): Vector[UaByte]

  /** Encodes UInt64 into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeUInt64(value: UaUInt64): Vector[UaByte]

  /** Encodes Float into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeFloat(value: UaFloat): Vector[UaByte]

  /** Encodes Double into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeDouble(value: UaDouble): Vector[UaByte]

  /** Encodes String into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeString(value: UaString): Vector[UaByte]

  /** Encodes XmlElement into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeXmlElement(value: UaXmlElement): Vector[UaByte]

  /** Encodes ByteString into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeByteString(value: UaByteString): Vector[UaByte]

  /** Encodes Guid into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeGuid(value: UaGuid): Vector[UaByte]

  /** Encodes NodeId into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeNodeId(value: UaNodeId): Vector[UaByte]

  /** Encodes ExpandedNodeId into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeExpandedNodeId(value: UaExpandedNodeId): Vector[UaByte]

  /** Encodes QualifiedName into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeQualifiedName(value: UaQualifiedName): Vector[UaByte]

  /** Encodes LocalizedText into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeLocalizedText(value: UaLocalizedText): Vector[UaByte]

  /** Encodes DataValue into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeDataValue(value: UaDataValue): Vector[UaByte]

  /** Encodes DiagnosticInfo into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeDiagnosticInfo(value: UaDiagnosticInfo): Vector[UaByte]

  /** Encodes DateTime into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeDateTime(value: UaDateTime): Vector[UaByte]

  /** Encodes StatusCode into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeStatusCode(value: UaStatusCode): Vector[UaByte]

  /** Encodes Variant into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeVariant(value: UaVariant): Vector[UaByte]

  /** Encodes ExtensionObject into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeExtensionObject(value: UaExtensionObject): Vector[UaByte]

  /** Encodes user defined Structure into binary format.
    *
    * @param typeId DataType of the value.
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeStructure(value: UaStructure, typeId: UaId): Vector[UaByte]

  /** Encodes Enumeration into binary format.
    *
    * @param value Selected value.
    * @return Sequence of bytes.
    */
  def encodeEnumeration(value: UaEnumeration): Vector[UaByte]

