package cz.modemtec.scopcua.util.valuecreator.impl

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.*
import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.util.valuecreator.UaValueCreator

import scala.util.chaining.scalaUtilChainingOps


/** Creates default value based on OPC UA standard.
  *
  * @param typeOf Function returning data type for its identifier.
  */
class UaDefaultValueCreator(val typeOf: PartialFunction[UaId, UaDataType]) extends UaValueCreator:

  def create(typeId: UaId): UaValue = create(typeOf(typeId))

  def create(dataType: UaDataType): UaValue = 

    def byte: UaByte = 0.toShort
    def sbyte: UaSByte = 0.toByte
    def uint16: UaUInt16 = 0
    def int16: UaInt16 = 0.toShort
    def uint32: UaUInt32 = 0
    def int32: UaInt32 = 0
    def uint64: UaUInt64 = 0
    def int64: UaInt64 = 0
    def float: UaFloat = 0.0f
    def double: UaDouble = 0.0
    def boolean: UaBoolean = false
    def byteString: UaByteString = UaByteString()

    def dataValue: UaDataValue =
      UaDataValue(
        value             = None,
        status            = None,
        sourceTimestamp   = None,
        sourcePicoSeconds = None,
        serverTimestamp   = None,
        serverPicoSeconds = None)

    def dateTime: UaDateTime = UaDateTime(0)

    def diagnosticInfo: UaDiagnosticInfo =
      UaDiagnosticInfo(
        symbolicId          = None,
        namespaceUri        = None,
        localizedText       = None,
        locale              = None,
        additionalInfo      = None,
        innerStatusCode     = None,
        innerDiagnosticInfo = None)

    def expandedNodeId: UaExpandedNodeId =
      UaExpandedNodeId(
        namespaceUri = "",
        nodeId       = UaNumericNodeId(0, 0),
        serverIndex  = 0)

    def extensionObject: UaExtensionObject =
      UaByteExtensionObject(
        encodingId = UaNumericNodeId(UaUInt16(0), UaUInt32(0)),
        body       = UaByteString())

    def guid: UaGuid = UaGuid()

    def localizedText: UaLocalizedText =
      UaLocalizedText(
        text   = "",
        locale = "")

    def nodeId: UaNodeId =
      UaNumericNodeId(
        namespaceIndex = 0,
        identifier     = 0)

    def qualifiedName: UaQualifiedName =
      UaQualifiedName(
        name           = "",
        namespaceIndex = 0)

    def statusCode: UaStatusCode = UaStatusCode(0, "Good")

    def string: UaString = ""

    def variant: UaVariant =
      UaVariant(
        id    = UaBuiltInId.Null,
        value = None)

    def xmlElement: UaXmlElement = UaXmlElement()

    def array: UaArray = UaArray()

    def enumeration(dataType: UaDataType.GeneralEnumeration): UaEnumeration = 
      val first = dataType.values.head
      UaEnumeration(first._2, first._1)

    def structure(dataType: UaDataType.GeneralStructure): UaStructure =
      dataType
        .fields
        .foldLeft(Map.empty[UaString, UaValue]): (result, field) => 
          val value = create(typeOf(field.typeId))
          val fieldValue = if field.isArray then array else value
          result + (field.name -> fieldValue)
        .pipe(UaStructure.apply)

    dataType match 
      case UaDataType.Boolean               => boolean
      case UaDataType.SByte                 => sbyte
      case UaDataType.Byte                  => byte
      case UaDataType.Int16                 => int16
      case UaDataType.UInt16                => uint16
      case UaDataType.Int32                 => int32
      case UaDataType.UInt32                => uint32
      case UaDataType.Int64                 => int64
      case UaDataType.UInt64                => uint64
      case UaDataType.Float                 => float
      case UaDataType.Double                => double
      case UaDataType.String                => string
      case UaDataType.DateTime              => dateTime
      case UaDataType.Guid                  => guid
      case UaDataType.ByteString            => byteString
      case UaDataType.XmlElement            => xmlElement
      case UaDataType.NodeId                => nodeId
      case UaDataType.ExpandedNodeId        => expandedNodeId
      case UaDataType.StatusCode            => statusCode
      case UaDataType.QualifiedName         => qualifiedName
      case UaDataType.LocalizedText         => localizedText
      case UaDataType.DataValue             => dataValue
      case UaDataType.BaseDataType          => variant
      case UaDataType.DiagnosticInfo        => diagnosticInfo
      case UaDataType.Structure             => extensionObject

      case t: UaDataType.GeneralEnumeration => enumeration(t)
      case t: UaDataType.GeneralStructure   => structure(t)

      case _ => throw new IllegalArgumentException("Unsupported data type.")
