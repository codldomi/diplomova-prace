package cz.modemtec.scopcua.util.binaryencoder.impl

import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.data.value.number.integer.{UaInt16, UaInt32, UaInt64, UaSByte}
import cz.modemtec.scopcua.data.value.number.integer.{UaByte, UaUInt16, UaUInt32, UaUInt64}
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.util.binaryencoder.UaBinaryEncoder
import cz.modemtec.scopcua.util.binaryencoder.impl.util.BinaryWriteBuffer
import cz.modemtec.scopcua.conversion.ConvertToUa.FromInt.given Conversion[?, ?]
import cz.modemtec.scopcua.conversion.ConvertFromUa.ToString.stringFromUaString
import cz.modemtec.scopcua.data.UaDataType.GeneralStructure
import cz.modemtec.scopcua.data.{UaDataType, UaId}

import scala.util.{Failure, Success, Using}


/** Encoder for the OPC UA Binary DataEncoding.
  *
  * @see [[https://reference.opcfoundation.org/v104/Core/docs/Part6/5.2.1/ Part 6, OPC UA Binary Data Encoding]]
  */
class UaStdBinaryEncoder(val typeOf: PartialFunction[UaId, UaDataType]) extends UaBinaryEncoder:

  /** Manages buffer's lifecycle (from creating to releasing) and runs the code.
    *
    * @param function Code run on buffer.
    * @tparam A DataType of the encoded value.
    * @return Sequence of bytes.
    */
  private def usingBuffer[A <: UaValue](function: BinaryWriteBuffer => Unit): Vector[UaByte] = 
    val result = Using(BinaryWriteBuffer()): buffer => 
      function(buffer)
      buffer.readBytes()
      
    result match 
      case Failure(exception) => throw exception
      case Success(bytes)     => bytes
    

  override def encodeBoolean(value: UaBoolean): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeBoolean(value)
  

  override def encodeByte(value: UaByte): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeByte(value)
  

  override def encodeSByte(value: UaSByte): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeSByte(value)
  

  override def encodeUInt16(value: UaUInt16): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeUInt16(value)
  

  override def encodeInt16(value: UaInt16): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeInt16(value)
  

  override def encodeUInt32(value: UaUInt32): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeUInt32(value)
  

  override def encodeInt32(value: UaInt32): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeInt32(value)
  

  override def encodeUInt64(value: UaUInt64): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeUInt64(value)
  

  override def encodeInt64(value: UaInt64): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeInt64(value)
  

  override def encodeFloat(value: UaFloat): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeFloat(value)
  

  override def encodeDouble(value: UaDouble): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeDouble(value)
  

  override def encodeString(value: UaString): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeString(value)
  

  override def encodeXmlElement(value: UaXmlElement): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeXmlElement(value)
  

  override def encodeByteString(value: UaByteString): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeByteString(value)
  

  override def encodeGuid(value: UaGuid): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeGuid(value)
  

  override def encodeNodeId(value: UaNodeId): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeNodeId(value)
  

  override def encodeExpandedNodeId(value: UaExpandedNodeId): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeExpandedNodeId(value)
  

  override def encodeQualifiedName(value: UaQualifiedName): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeQualifiedName(value)
  

  override def encodeLocalizedText(value: UaLocalizedText): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeLocalizedText(value)
  

  override def encodeDataValue(value: UaDataValue): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeDataValue(value)
  

  override def encodeDiagnosticInfo(value: UaDiagnosticInfo): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeDiagnosticInfo(value)
  

  override def encodeDateTime(value: UaDateTime): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeDateTime(value)
  

  override def encodeStatusCode(value: UaStatusCode): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeStatusCode(value)
  

  override def encodeVariant(value: UaVariant): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeVariant(value)
  

  override def encodeExtensionObject(value: UaExtensionObject): Vector[UaByte] = usingBuffer: buffer =>
    buffer.writeExtensionObject(value)
  

  override def encodeEnumeration(value: UaEnumeration): Vector[UaByte] = usingBuffer: buffer =>
    encodeGeneralEnumeration(buffer, value)
  

  override def encodeStructure(value: UaStructure, typeId: UaId): Vector[UaByte] = usingBuffer: buffer =>
    encodeGeneralStructure(buffer, value, typeId)
  

  override def encode(value: UaValue, typeId: UaId): Vector[UaByte] = usingBuffer: buffer =>
    encodeValue(buffer, value, typeId)
  

  private def encodeValue(buffer: BinaryWriteBuffer, value: UaValue, typeId: UaId): Unit = value match 
    case x: UaBoolean         => buffer.writeBoolean(x)
    case x: UaSByte           => buffer.writeSByte(x)
    case x: UaByte            => buffer.writeByte(x)
    case x: UaInt16           => buffer.writeInt16(x)
    case x: UaUInt16          => buffer.writeUInt16(x)
    case x: UaInt32           => buffer.writeInt32(x)
    case x: UaUInt32          => buffer.writeUInt32(x)
    case x: UaInt64           => buffer.writeInt64(x)
    case x: UaUInt64          => buffer.writeUInt64(x)
    case x: UaFloat           => buffer.writeFloat(x)
    case x: UaDouble          => buffer.writeDouble(x)
    case x: UaString          => buffer.writeString(x)
    case x: UaDateTime        => buffer.writeDateTime(x)
    case x: UaGuid            => buffer.writeGuid(x)
    case x: UaByteString      => buffer.writeByteString(x)
    case x: UaXmlElement      => buffer.writeXmlElement(x)
    case x: UaNodeId          => buffer.writeNodeId(x)
    case x: UaExpandedNodeId  => buffer.writeExpandedNodeId(x)
    case x: UaStatusCode      => buffer.writeStatusCode(x)
    case x: UaQualifiedName   => buffer.writeQualifiedName(x)
    case x: UaLocalizedText   => buffer.writeLocalizedText(x)
    case x: UaExtensionObject => buffer.writeExtensionObject(x)
    case x: UaDataValue       => buffer.writeDataValue(x)
    case x: UaVariant         => buffer.writeVariant(x)
    case x: UaDiagnosticInfo  => buffer.writeDiagnosticInfo(x)

    case x: UaArray           => encodeArray(buffer, x, typeId)

    case x: UaEnumeration     => encodeGeneralEnumeration(buffer, x)
    case x: UaStructure       => encodeGeneralStructure(buffer, x, typeId)

    case _ => throw new IllegalArgumentException("Unsupported data type.")
  

  private def encodeArray(buffer: BinaryWriteBuffer, array: UaArray, typeId: UaId): Unit = 
    val elements = array.flatten

    if elements.isEmpty then 
      buffer.writeInt32(-1)
    else 
      buffer.writeInt32(elements.length)
      elements.values.foreach(value => encodeValue(buffer, value, typeId))
    

  private def encodeGeneralStructure(buffer: BinaryWriteBuffer, structure: UaStructure, typeId: UaId): Unit = 
    val definition = typeOf(typeId) match 
      case struct: GeneralStructure => struct
      case _ => throw new IllegalArgumentException("The selected type is not a structured one.")
    
    if definition.hasOptionalFields then 
      val mask = 
        definition
          .fields
          .foldLeft(0): (result, field) =>
            if field.isOptional.value then
              (result << 1) | 1 
            else 
              result 

      buffer.writeUInt32(mask)
    
    definition
      .fields
      .foreach: field =>
        val value = structure.get(field.name)
        encodeValue(buffer, value, field.typeId)
        

  private def encodeGeneralEnumeration(buffer: BinaryWriteBuffer, enumeration: UaEnumeration): Unit = buffer.writeInt32(enumeration.value)

