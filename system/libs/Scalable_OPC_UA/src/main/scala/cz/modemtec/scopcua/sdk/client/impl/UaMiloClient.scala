package cz.modemtec.scopcua.sdk.client.impl

import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.config.{UaClientConfig, UaSecurityPolicy}
import cz.modemtec.scopcua.data.service.UaReadValue
import cz.modemtec.scopcua.data.value.number.UaDouble
import cz.modemtec.scopcua.data.value.{UaDateTime, UaStatusCode, UaString, UaValue}
import cz.modemtec.scopcua.sdk.client.UaClient
import cz.modemtec.scopcua.sdk.client.impl.util.ValueConverter
import cz.modemtec.scopcua.data.value.number.integer.{UaUInt16, UaUInt32}
import cz.modemtec.scopcua.exception.UaException
import cz.modemtec.scopcua.sdk.client.impl.UaMiloClient.guard
import cz.modemtec.scopcua.util.binarydecoder.impl.UaStdBinaryDecoder
import cz.modemtec.scopcua.util.binaryencoder.impl.UaStdBinaryEncoder

import org.eclipse.milo.opcua.sdk.client.OpcUaClient
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfigBuilder
import org.eclipse.milo.opcua.sdk.client.api.identity.{AnonymousProvider, UsernameProvider}
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem.ValueConsumer
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.{UaMonitoredItem, UaSubscription}
import org.eclipse.milo.opcua.stack.client.security.DefaultClientCertificateValidator
import org.eclipse.milo.opcua.stack.core.AttributeId
import org.eclipse.milo.opcua.stack.core.UaException as MiloException
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy
import org.eclipse.milo.opcua.stack.core.types.builtin.{ByteString, DataValue, LocalizedText, QualifiedName, Variant}
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint
import org.eclipse.milo.opcua.stack.core.types.enumerated.{MonitoringMode, TimestampsToReturn}
import org.eclipse.milo.opcua.stack.core.types.structured.{EndpointDescription, HistoryData, HistoryReadValueId, MonitoredItemCreateRequest, MonitoringParameters, ReadRawModifiedDetails, ReadValueId}

import java.util
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.FunctionConverters.{enrichAsJavaFunction, enrichAsJavaPredicate}
import scala.jdk.CollectionConverters.*
import scala.jdk.FutureConverters.*
import scala.util.{Failure, Success}


/** Encapsulates Milo's implementation of OPC UA. All public methods are thread-safe.
  *
  * @see [[https://github.com/eclipse/milo Eclipse Milo, GitHub]]
  * @param client Milo's client.
  * @param converter An utility for converting between Milo types and Scalable OPC UA types.
  * @param context Context for asynchronous code.
  */
class UaMiloClient private (val client: OpcUaClient, val converter: ValueConverter)(using val context: ExecutionContext) extends UaClient:

  override def connect(): Future[Unit] = guard: 
    client.connect().asScala.map(_ => ())

  override def disconnect(): Future[Unit] = guard: 
    client.disconnect().asScala.map(_ => ())


  override def readHistory(id: UaId, from: UaDateTime, to: UaDateTime)(callback: UaReadValue => _): Future[Unit] =
    for
      typeId <- readDataTypeId(id)
      result <- readHistory(id, from, to, typeId)(callback)
    yield result


  override def readHistory(id: UaId, from: UaDateTime, to: UaDateTime, typeId: UaId)(callback: UaReadValue => _): Future[Unit] = guard:

    case class Bunch(values: Vector[UaReadValue], continuationPoint: ByteString)

    def readHistory(continuationPoint: ByteString): Future[Bunch] =
      val details =
        new ReadRawModifiedDetails(
          false,                                  // isReadModified
          converter.uaDateTimeToDateTime(from),   // startTime
          converter.uaDateTimeToDateTime(to),     // endTime
          uint(0),                                // numValuesPerNode
          true)                                   // returnBounds (if true: in case there isn't any value, the value is NULL with time the same as bound)

      val nodeId =
        new HistoryReadValueId(
          converter.uaIdToNodeId(id),             // nodeId
          null,                                   // indexRange
          QualifiedName.NULL_VALUE,               // dataEncoding
          continuationPoint)                      // continuationPoint

      val nodesToRead = new util.ArrayList[HistoryReadValueId]()
      nodesToRead.add(nodeId)

      val response =
        client
          .historyRead(details, TimestampsToReturn.Both, false, nodesToRead)
          .asScala

      response.map: response =>
        if response.getResults == null || response.getResults.isEmpty then
          Bunch(Vector.empty, ByteString.NULL_VALUE)
        else
          val result = response.getResults.head

          if result.getStatusCode.isBad then
            throw new MiloException(result.getStatusCode)

          val continuationPoint = result.getContinuationPoint
          val history =
            result
              .getHistoryData
              .decode(client.getStaticSerializationContext)
              .asInstanceOf[HistoryData]
              .getDataValues
              .map: dataValue =>
                val value = converter.variantToUaValue(dataValue.getValue, typeId)
                val sourceTime = converter.dateTimeToUaDateTime(dataValue.getSourceTime)
                UaReadValue(value, sourceTime)
              .filter(_.value.isDefined)
              .toVector

          Bunch(history, continuationPoint)


    def nextData(point: ByteString): Future[Unit] =
      val bunch = readHistory(point)

      bunch.flatMap: bunch =>
        bunch.values.foreach(callback)
        if (bunch.continuationPoint.isNull)
          Future.unit
        else
          nextData(bunch.continuationPoint)


    nextData(ByteString.NULL_VALUE)


  override def readDataTypeId(id: UaId): Future[UaId] = guard:
    val nodeId = converter.uaIdToNodeId(id)
    client
      .getAddressSpace
      .getVariableNodeAsync(nodeId)
      .asScala
      .map: variable =>
        val typeNodeId = variable.getDataType
        converter.nodeIdToUaId(typeNodeId)


  override def readNamespaces(): Future[Map[UaUInt16, UaString]] = guard:
    client
      .readNamespaceTableAsync()
      .asScala
      .map: table =>
        table
          .toArray
          .zipWithIndex
          .foldLeft(Map.empty[UaUInt16, UaString]): (result, namespace) =>
            val uri = UaString(namespace._1)
            val index = UaUInt16(namespace._2)
            result + (index -> uri)


  override def callMethod(objectId: UaId, name: UaString, args: Map[UaString, UaValue]): Future[Map[UaString, UaValue]] = guard:
    val objectNodeId = converter.uaIdToNodeId(objectId)

    for
      node   <- client.getAddressSpace.getObjectNodeAsync(objectNodeId).asScala
      method <- node.getMethodAsync(name.value).asScala
      result <- {
        val input = method.getInputArguments
        val output = method.getOutputArguments

        val invalues = input.map: arg =>
          val typeId = converter.nodeIdToUaId(arg.getDataType)
          val value = args(UaString(arg.getName))
          converter.uaValueToVariant(value, typeId)

        method
          .callAsync(invalues)
          .asScala
          .map: outvalues =>
            outvalues
              .zip(output)
              .map: (variant, arg) =>
                val typeId = converter.nodeIdToUaId(arg.getDataType)
                val name = arg.getName
                val value = converter.variantToUaValue(variant, typeId).get
                (UaString(name), value)
              .toMap
      }
    yield result


  override def createSubscription(publishingInterval: UaDouble): Future[UaUInt32] = guard:
    client
      .getSubscriptionManager
      .createSubscription(publishingInterval.value)
      .asScala
      .map: subscription =>
        val value = subscription.getSubscriptionId.longValue()
        UaUInt32(value)


  override def deleteSubscription(id: UaUInt32): Future[Unit] = guard:
    val subId = uint(id.value)
    client
      .getSubscriptionManager
      .deleteSubscription(subId)
      .asScala
      .map(_ => ())


  override def deleteMonitoredItem(subId: UaUInt32, itemId: UaUInt32): Future[Unit] = guard:
    val items = List(uint(itemId.value))
    client
      .deleteMonitoredItems(uint(subId.value), items.asJava)
      .asScala
      .map: response =>
        response
          .getResults
          .foreach(statusCode => if statusCode.isBad then throw new MiloException(statusCode))


  def createMonitoredItem(subId: UaUInt32, variableId: UaId)(callback: (UaReadValue, UaUInt32, UaUInt32) => _): Future[UaUInt32] =
    for
      typeId <- readDataTypeId(variableId)
      result <- createMonitoredItem(subId, variableId, typeId)(callback)
    yield result


  override def createMonitoredItem(subId: UaUInt32, variableId: UaId, typeId: UaId)(callback: (UaReadValue, UaUInt32, UaUInt32) => _): Future[UaUInt32] = guard:
    val subscription =
      client
        .getSubscriptionManager
        .getSubscriptions
        .asScala
        .find(subs => subs.getSubscriptionId == uint(subId.value))
        .get

    val varNodeId = converter.uaIdToNodeId(variableId)

    val readValueId =
      new ReadValueId(
        varNodeId,                  // nodeId
        AttributeId.Value.uid(),    // attributeId
        null,                       // indexRange
        QualifiedName.NULL_VALUE)   // dataEncoding

    val clientHandle = subscription.nextClientHandle()

    val parameters =
      new MonitoringParameters(
        clientHandle,               // clientHandle
        -1,                         // samplingInterval (default: use subscription sampling interval)
        null,                       // filter
        uint(10),                   // queueSize
        true)                       // discardOldest

    val request =
      new MonitoredItemCreateRequest(
        readValueId,                // itemToMonitor
        MonitoringMode.Reporting,   // monitoringMode
        parameters)                 // requestedParameters

    val items = List(request)

    val consumer = new ValueConsumer {
      override def onValueArrived(item: UaMonitoredItem, dataValue: DataValue): Unit = {
        val monId = UaUInt32(item.getMonitoredItemId.longValue())
        val value = converter.variantToUaValue(dataValue.getValue, typeId)
        val sourceTime = converter.dateTimeToUaDateTime(dataValue.getSourceTime)
        callback(UaReadValue(value, sourceTime), monId, subId)
      }
    }

    val onItemCreated = new UaSubscription.ItemCreationCallback:
      override def onItemCreated(item: UaMonitoredItem, index: Int): Unit = item.setValueConsumer(consumer)

    subscription
        .createMonitoredItems(TimestampsToReturn.Both, items.asJava, onItemCreated)
        .asScala
        .map: result =>
          val id =
            result
              .get(0)
              .getMonitoredItemId
              .longValue()
          UaUInt32(id)


  override def readValue(id: UaId): Future[UaReadValue] =
    for
      typeId <- readDataTypeId(id)
      value  <- readValue(id, typeId)
    yield value


  override def readValue(id: UaId, typeId: UaId): Future[UaReadValue] = guard:
    val nodeId = converter.uaIdToNodeId(id)
    client
      .readValue(0.0, TimestampsToReturn.Both, nodeId)
      .asScala
      .map: dataValue =>
        val value = converter.variantToUaValue(dataValue.getValue, typeId)
        val sourceTime = converter.dateTimeToUaDateTime(dataValue.getSourceTime) match
          case Some(value) => if value.value == 0 then None else Some(value)   // just in case, that server returns default 0
          case None        => None
        UaReadValue(value, sourceTime)


  override def writeValue(id: UaId, value: Option[UaValue]): Future[Unit] =
    for
      typeId <- readDataTypeId(id)
      _      <- writeValue(id, value, typeId)
    yield ()


  override def writeValue(id: UaId, value: Option[UaValue], typeId: UaId): Future[Unit] = guard:
    value match
      case Some(value) => writeValue(id, value, typeId)
      case None =>
        val nodeId = converter.uaIdToNodeId(id)
        val variant = new Variant(null)
        client
          .writeValue(nodeId, new DataValue(variant))
          .asScala
          .map(statusCode => if (statusCode.isBad) throw new MiloException(statusCode))


  override def writeValue(id: UaId, value: UaValue, typeId: UaId): Future[Unit] = guard:
    val nodeId = converter.uaIdToNodeId(id)

    val variant = converter.uaValueToVariant(value, typeId)
    val dataValue = new DataValue(variant)

    client
      .writeValue(nodeId, dataValue)
      .asScala
      .map(statusCode => if (statusCode.isBad) throw new MiloException(statusCode))


  override def writeValue(id: UaId, value: UaValue): Future[Unit] =
    for
      typeId <- readDataTypeId(id)
      _      <- writeValue(id, value, typeId)
    yield ()



/** An factory object for creating clients.
  *
  * @note At this time, the factory creates clients using an OPC UA BinaryEncoding. The reason is that each OPC UA
  *       server supports at least binary encoding.
  */
object UaMiloClient:

  def guard[A](code: => Future[A])(using context: ExecutionContext): Future[A] =
    code.transform:
      case Failure(exception) => exception match

        case ex: MiloException =>
          val value = UaUInt32(ex.getStatusCode.getValue)
          val status = UaStatusCode.BuiltIns.get(value) match
            case Some(found) => found
            case None        => UaStatusCode(value, UaString())
          Failure(UaException(status, ex.getMessage, ex))

        case default => Failure(default)

      case Success(value) => Success(value)

  /** Transforms Eclipse Milo exception to Scalable OPC UA exception.
    *
    * @param code The body of method.
    * @tparam A Return value.
    * @return Result of the method.
    */
  def syncGuard[A](code: => A): A =
    try code
    catch
      case ex: MiloException =>
        val value = UaUInt32(ex.getStatusCode.getValue)
        val status = UaStatusCode.BuiltIns.get(value) match
          case Some(found) => found
          case None        => UaStatusCode(value, UaString())

        throw UaException(status, ex.getMessage, ex)


  /** Creates OPC UA client based on the configuration. Created client supports communication with OPC UA Binary
    * Encoding. Supported Status Codes are the built-in ones.
    *
    * @param config  A configuration instance.
    * @param typeOf  Function returning data type for its identifier.
    * @param context Context for asynchronous code.
    * @return Created OPC UA client.
    */
  def from(config: UaClientConfig, typeOf: PartialFunction[UaId, UaDataType])(using context: ExecutionContext): UaMiloClient = syncGuard:
    val build = (builder: OpcUaClientConfigBuilder) =>
      val identity = config.auth match
        case Some(auth) => new UsernameProvider(auth.username, auth.password)
        case None       => new AnonymousProvider

      builder.setIdentityProvider(identity)

      config.encryption match
        case None => // nothing
        case Some(enc) =>
          val validator = new DefaultClientCertificateValidator(enc.trustList)
          builder
            .setKeyPair(enc.keyPair)
            .setCertificate(enc.certificate)
            .setCertificateChain(enc.chain.toArray)
            .setCertificateValidator(validator)

      builder
        .setRequestTimeout(uint(config.timeout))
        .setApplicationName(LocalizedText.english(config.name))
        .setApplicationUri(config.uri)
        .build()

    val selectEndpoint = (endpoints: util.List[EndpointDescription]) =>
      val predicate = (endpoint: EndpointDescription) =>
        val policy = config.policy match
          case UaSecurityPolicy.None => SecurityPolicy.None
          case UaSecurityPolicy.Basic256 => SecurityPolicy.Basic256
          case UaSecurityPolicy.Basic256Sha256 => SecurityPolicy.Basic256Sha256
          case UaSecurityPolicy.Aes128Sha256RsaOaep => SecurityPolicy.Aes128_Sha256_RsaOaep
          case UaSecurityPolicy.Aes256Sha256RsaPss => SecurityPolicy.Aes256_Sha256_RsaPss
        policy.getUri == endpoint.getSecurityPolicyUri

      endpoints
        .stream()
        .filter(predicate.asJavaPredicate)
        .findFirst()

    val encoder = UaStdBinaryEncoder(typeOf)
    val decoder = UaStdBinaryDecoder(typeOf)

    val client = OpcUaClient.create(config.url, selectEndpoint.asJavaFunction, build.asJavaFunction)
    val converter = new ValueConverter(client, decoder, encoder, typeOf)

    UaMiloClient(client, converter)
          