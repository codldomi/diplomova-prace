package cz.modemtec.scopcua.util.jsonschemagenerator.impl

import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.util.jsonschemagenerator.UaJsonSchemaGenerator
import cz.modemtec.scopcua.util.jsonschemagenerator.impl.util.*
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32
import cz.modemtec.scopcua.data.node.{UaArgument, UaMethodNode, UaVariableNode}

import play.api.libs.json.{JsNumber, JsObject, JsString, JsValue, Json}

import scala.util.chaining.scalaUtilChainingOps


/** Implements ModemTec OPC UA JSON format.
  */
class UaModemTecJsonSchemaGenerator(val typeOf: PartialFunction[UaId, UaDataType]) extends UaJsonSchemaGenerator:

  private val schema = "https://json-schema.org/draft/2020-12/schema"


  override def generate(id: UaId): JsValue =
    val dataType = typeOf(id)
    val jsId = identifier(dataType)
    val title = dataType.name.value
    val property = propertyOf(dataType)
    val model = JsonSchema(schema, jsId, title, property)
    jsonOf(model)


  override def identifier(dataType: UaDataType): String =
    val id = dataType.id
    val name = dataType.name
    var ns = id.namespaceUri.value
    ns = if (ns.reverse.head == '/') ns else ns + '/'

    s"$ns${name.value}"


  override def identifier(id: UaId): String =
    val uri =
      val value = id.namespaceUri.value
      if value.endsWith("/") then value else value + "/"
    val num = id.index.value
    uri + num


  override def identifier(id: UaId, path: String): String =
    val editedUri =
      val value = id.namespaceUri.value
      if value.endsWith("/") then value else value + "/"
    val editedPath = if path.startsWith("/") then path.substring(1) else path
    editedUri + editedPath


  override def generate(node: UaVariableNode, path: String): JsValue =
    val dataType = typeOf(node.typeId)
    val value = ref(dataType)
    val property = maybeArrayProperty(value, node.arrayDimensions)
    val id = identifier(node.id, path)
    val model = JsonSchema(schema, id, "VariableValue", property)
    jsonOf(model)


  override def generate(node: UaMethodNode, path: String): (Option[JsValue], Option[JsValue]) =

    def objectOf(arguments: Vector[UaArgument]): JsonObjectProperty =
      val properties =
        arguments
          .map: arg =>
            val dataType = typeOf(arg.typeId)
            val value = ref(dataType)
            arg.name.value -> maybeArrayProperty(value, arg.arrayDimensions)
          .toMap

      val required = arguments.map(_.name.value).toSet
      JsonObjectProperty(properties, required)

    val id = identifier(node.id, path)

    val input =
      if node.input.isEmpty then
        None
      else
        val property = objectOf(node.input)
        val model = JsonSchema(schema, id + "/Input", "MethodInput", property)
        val json = jsonOf(model)
        Some(json)

    val output =
      if node.output.isEmpty then
        None
      else
        val property = objectOf(node.output)
        val model = JsonSchema(schema, id + "/Output", "MethodOutput", property)
        val json = jsonOf(model)
        Some(json)

    (input, output)


  private def ref(dataType: UaDataType): JsonRefProperty = JsonRefProperty(ref = identifier(dataType))


  private def jsonOf(schema: JsonSchema): JsObject =

    def jsonOf(property: JsonProperty): JsObject = property match
      case JsonObjectProperty(properties, required) =>
        val jsProperties =
          properties
            .map: (name, definition) =>
              (name, jsonOf(definition))
            .toSeq
            .pipe(JsObject)
        Json.obj("type" -> "object", "properties" -> jsProperties, "required" -> required)

      case JsonAnyProperty => Json.obj()

      case JsonNumberProperty(enumvals) =>
        val jsType = Json.obj("type" -> "number")
        val jsEnumvals =
          if enumvals.isEmpty then
            Json.obj()
          else
            enumvals
              .map(BigDecimal.apply)
              .map(JsNumber.apply)
              .pipe(vals => Json.obj("enum" -> vals))
        jsType ++ jsEnumvals

      case JsonStringProperty(format, enumvals) =>
        val jsFormat = format.map(pattern => Json.obj("format" -> pattern)).getOrElse(Json.obj())
        val jsEnumvals =
          if enumvals.isEmpty then
            Json.obj()
          else
            enumvals
              .map(JsString.apply)
              .pipe(vals => Json.obj("enum" -> vals))
        jsFormat ++ jsEnumvals ++ Json.obj("type" -> "string")

      case JsonBooleanProperty => Json.obj("type" -> "boolean")

      case JsonArrayProperty(items, minItems, maxItems) =>
        val min = minItems.map(value => Json.obj("minItems" -> value)).getOrElse(Json.obj())
        val max = maxItems.map(value => Json.obj("maxItems" -> value)).getOrElse(Json.obj())
        val basic = Json.obj("type" -> "array", "items" -> jsonOf(items))
        basic ++ min ++ max

      case JsonAnyOfProperty(types) =>
        val jsTypes = types.map(jsonOf)
        Json.obj("anyOf" -> jsTypes)

      case JsonRecursiveProperty => Json.obj("$ref" -> "#")

      case JsonRefProperty(ref) => Json.obj("$ref" -> ref)


    val core =
      Json.obj(
        "$schema" -> schema.schema,
        "$id"     -> schema.id,
        "title"   -> schema.title)

    val property = jsonOf(schema.property)

    core ++ property

  /** Generates ArrayProperty if the value is an array, otherwise returns property of the value.
    */
  private def maybeArrayProperty(value: JsonProperty, arrayDimensions: Vector[UaUInt32]): JsonProperty = 

    if arrayDimensions.isEmpty then return value

    val length = arrayDimensions.head.value
    val min = if length == 0 then None else Some(length.toInt)
    val max = min

    JsonArrayProperty(
      maxItems = max,
      minItems = min,
      items    = maybeArrayProperty(value, arrayDimensions.tail))


  private def propertyOf(dataType: UaDataType): JsonProperty = 

    def propertyOf(dataType: UaDataType): JsonProperty = dataType match 
      case UaDataType.Boolean => JsonBooleanProperty

      case UaDataType.SByte | UaDataType.Byte | UaDataType.Int16 | UaDataType.UInt16 | UaDataType.Int32 | UaDataType.UInt32 |
           UaDataType.Int64 | UaDataType.UInt64 | UaDataType.Float | UaDataType.Double | UaDataType.Number | UaDataType.Integer |
           UaDataType.UInteger => JsonNumberProperty()

      case UaDataType.String | UaDataType.Guid | UaDataType.ByteString | UaDataType.XmlElement => JsonStringProperty()

      case UaDataType.DateTime => JsonStringProperty(format = Some("date-time"))

      case UaDataType.StatusCode =>
        val properties =
          Map(
            "code"   -> ref(UaDataType.UInt32),
            "symbol" -> ref(UaDataType.String))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.QualifiedName =>
        val properties =
          Map(
            "uri"  -> ref(UaDataType.String),
            "name" -> ref(UaDataType.String))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.LocalizedText =>
        val properties =
          Map(
            "locale" -> ref(UaDataType.String),
            "text"   -> ref(UaDataType.String))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.DataValue =>
        val properties =
          Map(
            "value"             -> ref(UaDataType.BaseDataType),
            "status"            -> ref(UaDataType.StatusCode),
            "serverTimestamp"   -> ref(UaDataType.DateTime),
            "serverPicoSeconds" -> ref(UaDataType.UInt16),
            "sourceTimestamp"   -> ref(UaDataType.DateTime),
            "sourcePicoSeconds" -> ref(UaDataType.UInt16))
        val required = Set.empty[String]
        JsonObjectProperty(properties, required)

      case UaDataType.DiagnosticInfo =>
        val properties =
          Map(
            "symbolicId"          -> ref(UaDataType.Int32),
            "namespaceUri"        -> ref(UaDataType.Int32),
            "localizedText"       -> ref(UaDataType.Int32),
            "additionalInfo"      -> ref(UaDataType.String),
            "locale"              -> ref(UaDataType.Int32),
            "innerStatusCode"     -> ref(UaDataType.StatusCode),
            "innerDiagnosticInfo" -> JsonRecursiveProperty)
        val required = Set.empty[String]
        JsonObjectProperty(properties, required)

      case _: UaDataType.GeneralSimple | UaDataType.Image | UaDataType.Decimal => JsonObjectProperty(Map.empty, Set.empty)

      case UaDataType.GeneralEnumeration(_, _, _, _, values) =>
        val sorted =
          values
            .toVector
            .sortBy(_._2.value)

        val properties =
          Map(
            "value" -> JsonNumberProperty(enumvals = sorted.map(_._2.value)),
            "name"  -> JsonStringProperty(enumvals = sorted.map(_._1.value)))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.Enumeration =>
        val properties =
          Map(
            "value" -> ref(UaDataType.Int32),
            "name" -> ref(UaDataType.String))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.Structure =>
        val properties =
          Map(
            "body"       -> ref(UaDataType.String),
            "enType"     -> JsonStringProperty(enumvals = Vector("BYTE", "XML")),
            "encodingId" -> ref(UaDataType.NodeId))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.BaseDataType =>
        val properties =
          Map(
            "varType" ->
              JsonStringProperty(enumvals = Vector(
                "NULL",
                "BOOLEAN",
                "SBYTE",
                "BYTE",
                "INT16",
                "UINT16",
                "INT32",
                "UINT32",
                "INT64",
                "UINT64",
                "FLOAT",
                "DOUBLE",
                "STRING",
                "DATETIME",
                "GUID",
                "BYTESTRING",
                "XMLELEMENT",
                "NODEID",
                "EXPANDEDNODEID",
                "STATUSCODE",
                "QUALIFIEDNAME",
                "LOCALIZEDTEXT",
                "EXTENSIONOBJECT",
                "DATAVALUE",
                "VARIANT",
                "DIAGNOSTICINFO")),
            "value" -> JsonAnyProperty)
        val required = Set("varType")
        JsonObjectProperty(properties, required)

      case UaDataType.NodeId =>
        val properties =
          Map(
            "idType" -> JsonStringProperty(enumvals = Vector("NUMERIC", "GUID", "OPAQUE", "STRING")),
            "uri"    -> ref(UaDataType.String),
            "id"     -> JsonAnyOfProperty(types = Set(ref(UaDataType.UInt32), ref(UaDataType.String), ref(UaDataType.ByteString), ref(UaDataType.Guid))))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case UaDataType.ExpandedNodeId =>
        val properties =
          Map(
            "id"          -> ref(UaDataType.NodeId),
            "serverIndex" -> ref(UaDataType.UInt32))
        val required = properties.keySet
        JsonObjectProperty(properties, required)

      case s: UaDataType.GeneralStructure =>
        val properties = 
          s
            .fields
            .map: field =>
              val dt = typeOf(field.typeId)
              val property = 
                if identifier(s) == identifier(dt) then 
                  JsonRecursiveProperty
                else 
                  ref(dt)
              (field.name.value, maybeArrayProperty(property, field.arrayDimensions))
            .toMap

        val required =
          s.fields
            .filter(_.isOptional.value == false)
            .map(_.name.value)
            .toSet
        JsonObjectProperty(properties, required)

    propertyOf(dataType)


