package cz.modemtec.scopcua.data.service

import cz.modemtec.scopcua.data.value.{UaDateTime, UaValue}


case class UaReadValue(value: Option[UaValue], sourceTime: Option[UaDateTime])
