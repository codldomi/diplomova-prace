package cz.modemtec.scopcua.data.value.number.integer

/** An signed integer value.
  */
sealed trait UaSInteger extends UaInteger

/** An integer value between −128 and 127 inclusive.
  *
  * @param value Scala representation.
  */
case class UaSByte(value: Byte) extends UaSInteger:

  private val max = 127
  private val min = -128

  require(min <= value && value <= max, "UaSByte is an integer value between −128 and 127 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)


/** An integer value between −32 768 and 32 767 inclusive.
  *
  * @param value Scala representation.
  */
case class UaInt16(value: Short) extends UaSInteger:

  private val max = 32767
  private val min = -32768

  require(min <= value && value <= max, "UaInt16 is an integer value between −32 768 and 32 767 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)


/** An integer value between −2 147 483 648 and 2 147 483 647 inclusive.
  *
  * @param value Scala representation.
  */
case class UaInt32(value: Int) extends UaSInteger:

  private val max = 2147483647
  private val min = -2147483648

  require(min <= value && value <= max, "UaInt32 is an integer value between −2 147 483 648 and 2 147 483 647 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)


/** An integer value between −9 223 372 036 854 775 808 and 9 223 372 036 854 775 807 inclusive.
  *
  * @param value Scala representation.
  */
case class UaInt64(value: Long) extends UaSInteger:

  private val max = 9223372036854775807L
  private val min = -9223372036854775808L

  require(min <= value && value <= max,  "UaInt64 is an integer value between −9 223 372 036 854 775 808 and 9 223 372 036 854 775 807 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)


