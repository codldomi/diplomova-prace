package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaUInt16

/** A data value with an associated status code and timestamps.
  *
  * @param value             The data value. If the StatusCode indicates an error then the value is to be ignored and
  *                          the Server shall set it to null.
  *
  * @param status            The StatusCode that defines with the Server’s ability to access/provide the value.
  *
  * @param sourceTimestamp   The source timestamp for the value.
  *
  * @param sourcePicoSeconds Specifies the number of 10 picoseconds (1,0 e-11 seconds) intervals which shall be added
  *                          to the sourceTimestamp.
  *
  * @param serverTimestamp   The Server timestamp for the value.
  *
  * @param serverPicoSeconds Specifies the number of 10 picoseconds (1,0 e-11 seconds) intervals which shall be added
  *                          to the serverTimestamp.
  *
  * @note The Picoseconds fields store the difference between a high-resolution timestamp with a resolution of 10
  *       picoseconds and the Timestamp field value which only has a 100 ns resolution. The Picoseconds fields shall
  *       contain values less than 10 000. The decoder shall treat values greater than or equal to 10 000 as the
  *       value ‘9999’.
  */
case class UaDataValue(
  value: Option[UaVariant],
  status: Option[UaStatusCode],
  sourceTimestamp: Option[UaDateTime],
  sourcePicoSeconds: Option[UaUInt16],
  serverTimestamp: Option[UaDateTime],
  serverPicoSeconds: Option[UaUInt16]
) extends UaValue:

  private def check(optional: Option[UaUInt16]): Unit =
    optional match 
      case Some(UaUInt16(value)) => require(value < 10000, "The Picoseconds fields shall contain values less than 10 000.")
      case None                  => // nothing
  
  check(sourcePicoSeconds)
  check(serverPicoSeconds)


