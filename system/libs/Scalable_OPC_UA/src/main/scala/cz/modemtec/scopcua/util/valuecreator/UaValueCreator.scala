package cz.modemtec.scopcua.util.valuecreator

import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.value.UaValue

/** Creates value of the data type.
  */
trait UaValueCreator:

  /** Creates value for the data type with the given ID.
    *
    * @param typeId Selected data type ID.
    * @return Created value.
    */
  def create(typeId: UaId): UaValue

  /** Creates value for the data type.
    *
    * @param dataType Selected DataType.
    * @return Created value.
    */
  def create(dataType: UaDataType): UaValue

