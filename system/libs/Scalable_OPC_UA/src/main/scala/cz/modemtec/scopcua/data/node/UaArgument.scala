package cz.modemtec.scopcua.data.node

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.value.number.integer.{UaInt32, UaUInt32}
import cz.modemtec.scopcua.data.value.{UaBoolean, UaString}


case class UaArgument(
  name: UaString,
  isInput: UaBoolean,
  typeId: UaId,
  valueRank: UaInt32,
  arrayDimensions: Vector[UaUInt32],
  ord: UaInt32
)
