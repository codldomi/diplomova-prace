package cz.modemtec.scopcua.util.jsonschemagenerator.impl.util


case class JsonSchema(
  schema: String,
  id: String,
  title: String,
  property: JsonProperty
)