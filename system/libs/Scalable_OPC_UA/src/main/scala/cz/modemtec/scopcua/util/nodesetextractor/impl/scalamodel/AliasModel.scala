package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class AliasModel(
  alias: String,
  value: String
)
