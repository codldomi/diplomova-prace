package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class DefinitionModel(
  name: String,
  fields: Seq[FieldModel]
)