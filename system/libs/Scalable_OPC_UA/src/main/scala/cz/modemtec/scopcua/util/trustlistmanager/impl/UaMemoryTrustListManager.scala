package cz.modemtec.scopcua.util.trustlistmanager.impl

import com.google.common.collect.{ImmutableList, Sets}

import cz.modemtec.scopcua.util.trustlistmanager.UaTrustListManager

import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString
import org.eclipse.milo.opcua.stack.core.util.CertificateUtil

import java.security.cert.{X509CRL, X509Certificate}
import java.util


/** Trust List Manager holding all certificates in memory.
  *
  * @param maxRejected Maximum of rejected certificates.
  */
class UaMemoryTrustListManager(maxRejected: Int = 10) extends UaTrustListManager with AutoCloseable:

  private val issuerCertificates = Sets.newConcurrentHashSet[X509Certificate]
  private val issuerCrls = Sets.newConcurrentHashSet[X509CRL]

  private val trustedCertificates = Sets.newConcurrentHashSet[X509Certificate]
  private val trustedCrls = Sets.newConcurrentHashSet[X509CRL]

  private val rejectedCertificates = Sets.newConcurrentHashSet[X509Certificate]

  override def close(): Unit = 
    issuerCertificates.clear()
    issuerCrls.clear()
    trustedCertificates.clear()
    trustedCrls.clear()
    rejectedCertificates.clear()

  override def getIssuerCrls: ImmutableList[X509CRL] = ImmutableList.copyOf(issuerCrls)

  override def getTrustedCrls: ImmutableList[X509CRL] = ImmutableList.copyOf(trustedCrls)

  override def getIssuerCertificates: ImmutableList[X509Certificate] = ImmutableList.copyOf(issuerCertificates)

  override def getTrustedCertificates: ImmutableList[X509Certificate] = ImmutableList.copyOf(trustedCertificates)

  override def getRejectedCertificates: ImmutableList[X509Certificate] = ImmutableList.copyOf(rejectedCertificates)

  private def replaceCertificates(newOnes: util.List[X509Certificate], crts: util.Set[X509Certificate]): Unit = 
    crts.clear()
    crts.addAll(newOnes)

  private def replaceCrls(newOnes: util.List[X509CRL], crls: util.Set[X509CRL]): Unit = 
    crls.clear()
    crls.addAll(newOnes)
  
  override def setIssuerCrls(issuerCrls: util.List[X509CRL]): Unit = this.synchronized:
    replaceCrls(issuerCrls, this.issuerCrls)
  
  override def setTrustedCrls(trustedCrls: util.List[X509CRL]): Unit = this.synchronized:
    replaceCrls(trustedCrls, this.trustedCrls)
  
  override def setIssuerCertificates(issuerCertificates: util.List[X509Certificate]): Unit = this.synchronized:
    replaceCertificates(issuerCertificates, this.issuerCertificates)

  override def setTrustedCertificates(trustedCertificates: util.List[X509Certificate]): Unit = this.synchronized:
    replaceCertificates(trustedCertificates, this.trustedCertificates)
  
  override def addIssuerCertificate(certificate: X509Certificate): Unit = issuerCertificates.add(certificate)
  
  override def addTrustedCertificate(certificate: X509Certificate): Unit = trustedCertificates.add(certificate)
  
  override def addRejectedCertificate(certificate: X509Certificate): Unit =
    if rejectedCertificates.size() < maxRejected then
      rejectedCertificates.add(certificate)
  
  private def removeCertificate(thumbprint: ByteString, certificates: util.Set[X509Certificate]): Boolean = 
    val optCrt =
      certificates
        .stream()
        .filter(crt => CertificateUtil.thumbprint(crt).equals(thumbprint))
        .findFirst()

    optCrt.ifPresent(crt => certificates.remove(crt))
    optCrt.isPresent
  
  override def removeIssuerCertificate(thumbprint: ByteString): Boolean = removeCertificate(thumbprint, issuerCertificates)
  
  override def removeTrustedCertificate(thumbprint: ByteString): Boolean = removeCertificate(thumbprint, trustedCertificates)
  
  override def removeRejectedCertificate(thumbprint: ByteString): Boolean = removeCertificate(thumbprint, rejectedCertificates)

