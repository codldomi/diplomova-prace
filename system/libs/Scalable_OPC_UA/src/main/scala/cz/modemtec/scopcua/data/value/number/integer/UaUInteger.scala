package cz.modemtec.scopcua.data.value.number.integer

/** An unsigned integer value.
  */
sealed trait UaUInteger extends UaInteger

/** An integer value between 0 and 255 inclusive.
  *
  * @param value Scala representation.
  */
case class UaByte(value: Short) extends UaUInteger:

  private val max = 255
  private val min = 0

  require(min <= value && value <= max, "UaByte is an integer value between 0 and 255 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)
  

/** An integer value between 0 and 65 535 inclusive.
  *
  * @param value Scala representation.
  */
case class UaUInt16(value: Int) extends UaUInteger:

  private val max = 65535
  private val min = 0

  require(min <= value && value <= max, "UaUInt16 an integer value between 0 and 65 535 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)
  

/** An integer value between 0 and 4 294 967 295 inclusive.
  *
  * @param value Scala representation.
  */
case class UaUInt32(value: Long) extends UaUInteger:

  private val max = 4294967295L
  private val min = 0

  require(min <= value && value <= max, "UaUInt32 is an integer value between 0 and 4 294 967 295 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)
  

/** An integer value between 0 and 18 446 744 073 709 551 615 inclusive.
  *
  * @param value Scala representation.
  */
case class UaUInt64(value: BigInt) extends UaUInteger:

  private val max = BigInt("18446744073709551615")
  private val min = 0

  require(min <= value && value <= max, "UaUInt64 is an integer value between 0 and 18 446 744 073 709 551 615 inclusive.")

  override def asBigDecimal: BigDecimal = BigDecimal(value)

