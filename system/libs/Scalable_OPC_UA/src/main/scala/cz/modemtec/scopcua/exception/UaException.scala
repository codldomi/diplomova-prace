package cz.modemtec.scopcua.exception

import cz.modemtec.scopcua.data.value.UaStatusCode


class UaException(
  val status: UaStatusCode,
  val message: String,
  val cause: Throwable
) extends Exception(message, cause)


object UaException:
  def unapply(exception: UaException): Option[(UaStatusCode, String, Throwable)] = Some((exception.status, exception.message, exception.cause))
