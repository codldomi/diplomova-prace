package cz.modemtec.scopcua.data

import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.data.UaDataType.GeneralStructure.Field
import cz.modemtec.scopcua.data.value.{UaBoolean, UaString}
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation


sealed trait UaDataType extends UaIdentifiable:
  def isAbstract: UaBoolean
  def name: UaString
  def superId: Option[UaId]


object UaDataType:

  val BuiltIns: Map[UaId, UaDataType] =
    val dataTypes = Seq(
      Boolean, SByte, Byte, Int16, UInt16, Int32, UInt32, Int64, UInt64, Float, Double, String, DateTime, Guid, ByteString,
      XmlElement, NodeId, ExpandedNodeId, StatusCode, QualifiedName, LocalizedText, Structure, DataValue, BaseDataType,
      DiagnosticInfo, Number, Integer, UInteger, Enumeration, Image, Decimal)

    dataTypes
      .map(dataType => dataType.id -> dataType)
      .toMap


  case object Boolean extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(1, OpcUaFoundation)
    override def name: UaString = "Boolean"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object SByte extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(2, OpcUaFoundation)
    override def name: UaString = "SByte"
    override def superId: Option[UaId] = Some(UaId(27, OpcUaFoundation))


  case object Byte extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(3, OpcUaFoundation)
    override def name: UaString = "Byte"
    override def superId: Option[UaId] = Some(UaId(28, OpcUaFoundation))

  case object Int16 extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(4, OpcUaFoundation)
    override def name: UaString = "Int16"
    override def superId: Option[UaId] = Some(UaId(27, OpcUaFoundation))


  case object UInt16 extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(5, OpcUaFoundation)
    override def name: UaString = "UInt16"
    override def superId: Option[UaId] = Some(UaId(28, OpcUaFoundation))


  case object Int32 extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(6, OpcUaFoundation)
    override def name: UaString = "Int32"
    override def superId: Option[UaId] = Some(UaId(27, OpcUaFoundation))


  case object UInt32 extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(7, OpcUaFoundation)
    override def name: UaString = "UInt32"
    override def superId: Option[UaId] = Some(UaId(28, OpcUaFoundation))


  case object Int64 extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(8, OpcUaFoundation)
    override def name: UaString = "Int64"
    override def superId: Option[UaId] = Some(UaId(27, OpcUaFoundation))


  case object UInt64 extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(9, OpcUaFoundation)
    override def name: UaString = "UInt64"
    override def superId: Option[UaId] = Some(UaId(28, OpcUaFoundation))


  case object Float extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(10, OpcUaFoundation)
    override def name: UaString = "Float"
    override def superId: Option[UaId] = Some(UaId(26, OpcUaFoundation))


  case object Double extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(11, OpcUaFoundation)
    override def name: UaString = "Double"
    override def superId: Option[UaId] = Some(UaId(26, OpcUaFoundation))


  case object String extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(12, OpcUaFoundation)
    override def name: UaString = "String"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object DateTime extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(13, OpcUaFoundation)
    override def name: UaString = "DateTime"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object Guid extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(14, OpcUaFoundation)
    override def name: UaString = "Guid"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object ByteString extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(15, OpcUaFoundation)
    override def name: UaString = "ByteString"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object XmlElement extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(16, OpcUaFoundation)
    override def name: UaString = "XmlElement"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object NodeId extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(17, OpcUaFoundation)
    override def name: UaString = "NodeId"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object ExpandedNodeId extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(18, OpcUaFoundation)
    override def name: UaString = "ExpandedNodeId"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object StatusCode extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(19, OpcUaFoundation)
    override def name: UaString = "StatusCode"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object QualifiedName extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(20, OpcUaFoundation)
    override def name: UaString = "QualifiedName"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object LocalizedText extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(21, OpcUaFoundation)
    override def name: UaString = "LocalizedText"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object Structure extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(22, OpcUaFoundation)
    override def name: UaString = "Structure"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object DataValue extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(23, OpcUaFoundation)
    override def name: UaString = "DataValue"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object BaseDataType extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(24, OpcUaFoundation)
    override def name: UaString = "BaseDataType"
    override def superId: Option[UaId] = None


  case object DiagnosticInfo extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(25, OpcUaFoundation)
    override def name: UaString = "DiagnosticInfo"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object Number extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(26, OpcUaFoundation)
    override def name: UaString = "Number"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object Integer extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(27, OpcUaFoundation)
    override def name: UaString = "Integer"
    override def superId: Option[UaId] = Some(UaId(26, OpcUaFoundation))


  case object UInteger extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(28, OpcUaFoundation)
    override def name: UaString = "UInteger"
    override def superId: Option[UaId] = Some(UaId(27, OpcUaFoundation))


  case object Enumeration extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(29, OpcUaFoundation)
    override def name: UaString = "Enumeration"
    override def superId: Option[UaId] = Some(UaId(24, OpcUaFoundation))


  case object Image extends UaDataType:
    override def isAbstract: UaBoolean = true
    override def id: UaId = UaId(30, OpcUaFoundation)
    override def name: UaString = "Image"
    override def superId: Option[UaId] = Some(UaId(15, OpcUaFoundation))


  case object Decimal extends UaDataType:
    override def isAbstract: UaBoolean = false
    override def id: UaId = UaId(50, OpcUaFoundation)
    override def name: UaString = "Decimal"
    override def superId: Option[UaId] = Some(UaId(26, OpcUaFoundation))


  case class GeneralSimple(
    isAbstract: UaBoolean,
    name: UaString,
    id: UaId,
    superId: Option[UaId]
  ) extends UaDataType


  case class GeneralEnumeration(
    isAbstract: UaBoolean,
    name: UaString,
    id: UaId,
    superId: Option[UaId],
    values: Map[UaString, UaInt32]
  ) extends UaDataType


  object GeneralEnumeration:
    def apply(
      isAbstract: UaBoolean,
      name: UaString,
      id: UaId,
      superId: Option[UaId],
      values: (UaString, UaInt32)*
    ): GeneralEnumeration = GeneralEnumeration(isAbstract, name, id, superId, values.toMap)


  case class GeneralStructure(
    isAbstract: UaBoolean,
    name: UaString,
    id: UaId,
    superId: Option[UaId],
    fields: Vector[Field]
  ) extends UaDataType:
    def hasOptionalFields: Boolean = fields.exists(_.isOptional.value)


  object GeneralStructure:
    def apply(
      isAbstract: UaBoolean,
      name: UaString,
      id: UaId,
      superId: Option[UaId],
      fields: Field*
    ): GeneralStructure = GeneralStructure(isAbstract, name, id, superId, fields.toVector)

    /**
      * @param ord Order of field in structure. Starts by 0.
      */
    case class Field(
      name: UaString,
      typeId: UaId,
      valueRank: UaInt32,
      arrayDimensions: Vector[UaUInt32],
      isOptional: UaBoolean,
      ord: UaInt32
    ):
      def isArray: Boolean = valueRank.value >= 0
