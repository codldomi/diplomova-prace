package cz.modemtec.scopcua.util.jsonvalidator

import cz.modemtec.scopcua.data.UaId
import play.api.libs.json.JsValue

/** Validator for values encoded as JSON.
  */
trait UaJsonValidator:

  /** Checks whether JSON value is valid against data type definition.
    *
    * @param value Encoded value.
    * @param typeId Data type's identifier.
    * @return True, if the value has correct format.
    */
  def validate(value: JsValue, typeId: UaId): Boolean

