package cz.modemtec.scopcua.conversion

import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.data.value.number.integer.*

import java.sql.Timestamp
import java.util.TimeZone
import java.text.SimpleDateFormat


object ConvertToUa:

  object FromAll extends FromAll

  object FromBigInt extends FromBigInt
  object FromLong extends FromLong
  object FromInt extends FromInt
  object FromShort extends FromShort
  object FromByte extends FromByte
  object FromBoolean extends FromBoolean
  object FromString extends FromString
  object FromFloat extends FromFloat
  object FromDouble extends FromDouble
  object FromTimestamp extends FromTimestamp
  object FromPair extends FromPair

  trait FromAll
    extends FromBigInt
      with FromLong
      with FromInt
      with FromShort
      with FromByte
      with FromBoolean
      with FromString
      with FromFloat
      with FromDouble
      with FromTimestamp
      with FromPair

  trait FromPair:
    given pairToEnumPair: Conversion[(String, Int), (UaString, UaInt32)] = (value: (String, Int)) =>
      val first = UaString(value._1)
      val second = UaInt32(value._2)
      (first, second)

    given pairToStructPair: Conversion[(String, UaValue), (UaString, UaValue)] = (field: (String, UaValue)) =>
      val name = UaString(field._1)
      (name, field._2)

    given pairToNamespacePair: Conversion[(Int, String), (UaUInt16, UaString)] = (pair: (Int, String)) =>
      val index = UaUInt16(pair._1)
      val uri = UaString(pair._2)
      (index, uri)


  trait FromBigInt:
    given bigIntToUaUInt64: Conversion[BigInt, UaUInt64] = (value: BigInt) => UaUInt64(value)


  trait FromLong:
    given longToUaUInt32: Conversion[Long, UaUInt32] = (value: Long) => UaUInt32(value)
    given longToUaInt64: Conversion[Long, UaInt64] = (value: Long) => UaInt64(value)
    given longToUaUInt64: Conversion[Long, UaUInt64] = (value: Long) => UaUInt64(value)
    given longToUaDateTime: Conversion[Long, UaDateTime] = (value: Long) => UaDateTime(value)


  trait FromInt:
    given intToUaSByte: Conversion[Int, UaSByte] = (value: Int) => UaSByte(value.toByte)
    given intToUaByte: Conversion[Int, UaByte] = (value: Int) => UaByte(value.toShort)
    given intToUaUInt16: Conversion[Int, UaUInt16] = (value: Int) => UaUInt16(value)
    given intToUaInt16: Conversion[Int, UaInt16] = (value: Int) => UaInt16(value.toShort)
    given intToUaInt32: Conversion[Int, UaInt32] = (value: Int) => UaInt32(value)
    given intToUaUInt32: Conversion[Int, UaUInt32] = (value: Int) => UaUInt32(value)
    given intToUaInt64: Conversion[Int, UaInt64] = (value: Int) => UaInt64(value)
    given intToUaUInt64: Conversion[Int, UaUInt64] = (value: Int) => UaUInt64(value)


  trait FromShort:
    given shortToUaByte: Conversion[Short, UaByte] = (value: Short) => UaByte(value)
    given shortToUaInt16: Conversion[Short, UaInt16] = (value: Short) => UaInt16(value)
    given shortToUaUInt16: Conversion[Short, UaUInt16] = (value: Short) => UaUInt16(value)
    given shortToUaInt32: Conversion[Short, UaInt32] = (value: Short) => UaInt32(value)
    given shortToUaUInt32: Conversion[Short, UaUInt32] = (value: Short) => UaUInt32(value)
    given shortToUaInt64: Conversion[Short, UaInt64] = (value: Short) => UaInt64(value)
    given shortToUaUInt64: Conversion[Short, UaUInt64] = (value: Short) => UaUInt64(BigInt(value))


  trait FromByte:
    given byteToUaSByte: Conversion[Byte, UaSByte] = (value: Byte) => UaSByte(value)


  trait FromBoolean:
    given booleanToUaBoolean: Conversion[Boolean, UaBoolean] = (value: Boolean) => UaBoolean(value)


  trait FromFloat:
    given floatToUaFloat: Conversion[Float, UaFloat] = (value: Float) => UaFloat(value)


  trait FromDouble:
    given doubleToUaDouble: Conversion[Double, UaDouble] = (value: Double) => UaDouble(value)


  trait FromString:
    given stringToUaDateTime: Conversion[String, UaDateTime] = (value: String) =>
      val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
      df.setTimeZone(TimeZone.getTimeZone("UTC"))
      val date = df.parse(value)
      FromTimestamp.timestampToUaDateTime(Timestamp(date.getTime))

    given stringToUaString: Conversion[String, UaString] = (value: String) => UaString(value)
    given stringToUaXmlElement: Conversion[String, UaXmlElement] = (value: String) => UaXmlElement(value)

    given stringToUaNumericNodeId: Conversion[String, UaNumericNodeId] = (value: String) =>
      val nonZeroNsId = "ns=([0-9]+);i=([0-9]+)".r
      val zeroNsId = "i=([0-9]+)".r

      val (i, ns) = nonZeroNsId.findFirstMatchIn(value) match
        case Some(result) => (result.group(2).toLong, result.group(1).toInt)
        case None         => (zeroNsId.findFirstMatchIn(value).get.group(1).toLong, 0)

      UaNumericNodeId(UaUInt16(ns), UaUInt32(i))

    given stringToUaGuid: Conversion[String, UaGuid] = (value: String) =>
      def hexaToBytes(str: String): Vector[UaByte] =
        str
          .grouped(2)
          .map: hexa =>
            val value = Integer.parseInt(hexa, 16)
            UaByte(value.toShort)
          .toVector

      def hexaToUInt16(hexa: String): UaUInt16 =
        val value = Integer.parseInt(hexa, 16)
        UaUInt16(value)

      def hexaToUInt32(hexa: String): UaUInt32 =
        val value = BigInt(hexa, 16)
        UaUInt32(value.longValue)

      val hexa1 = value.substring(0, 8)
      val hexa2 = value.substring(9, 13) //  value[ 8] == '-'
      val hexa3 = value.substring(14, 18) //  value[13] == '-'
      val hexa4 = value.substring(19, 23) //  value[18] == '-'
      val hexa5 = value.substring(24, 36) //  value[23] == '-'

      val data1 = hexaToUInt32(hexa1)
      val data2 = hexaToUInt16(hexa2)
      val data3 = hexaToUInt16(hexa3)
      val data4 = hexaToBytes(hexa4 + hexa5)

      UaGuid(data1, data2, data3, data4)


  trait FromTimestamp:
    given timestampToUaDateTime: Conversion[Timestamp, UaDateTime] = (timestamp: Timestamp) =>
      val usec = 10L
      val msec = usec * 1000L
      val sec = msec * 1000L
      val unixEpoch = 11644473600L * sec

      val unix = timestamp.getTime / 1000

      UaDateTime((unix * sec) + unixEpoch)
