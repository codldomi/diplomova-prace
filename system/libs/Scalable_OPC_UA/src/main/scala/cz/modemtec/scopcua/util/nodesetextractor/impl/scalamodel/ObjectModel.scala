package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class ObjectModel(
  id: String,
  parentId: Option[String],
  browseName: String,
  displayName: String,
  description: Option[String],
  references: Seq[ReferenceModel],
  symbolicName: Option[String]
) extends NodeModel
