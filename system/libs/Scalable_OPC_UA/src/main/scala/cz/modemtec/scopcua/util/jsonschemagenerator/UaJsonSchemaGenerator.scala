package cz.modemtec.scopcua.util.jsonschemagenerator

import cz.modemtec.scopcua.data.node.{UaMethodNode, UaVariableNode}
import cz.modemtec.scopcua.data.{UaDataType, UaId}

import play.api.libs.json.JsValue


/** Generator for generating JSON Schema
  */
trait UaJsonSchemaGenerator:

  /** Generates JSON Schema for the data type.
    *
    * @param dataType Selected data type.
    * @return JSON Schema.
    */
  def generate(dataType: UaDataType): JsValue = generate(dataType.id)

  /** Generates JSON Schema for the data type.
    *
    * @param id     Selected data type's identifier.
    * @return JSON Schema.
    */
  def generate(id: UaId): JsValue

  /** Generates the "id" attribute of JSON Schema from OPC UA data type.
    *
    * @param dataType Selected data type.
    * @return Data type URI identifier.
    */
  def identifier(dataType: UaDataType): String

  /** Generates the "id" attribute of JSON Schema from OPC UA identifier.
    *
    * @param id Selected OPC UA identifier.
    * @return Node URI identifier.
    */
  def identifier(id: UaId): String

  /** Generates the "id" attribute of JSON Schema from OPC UA identifier and path.
    *
    * @param id Selected OPC UA identifier.
    * @return Node URI identifier.
    */
  def identifier(id: UaId, path: String): String

  /** Generates JSON Schema for Variable's value.
    *
    * @param node Variable node.
    * @param path Relative path to node from the root (/).
    * @return Value's JSON Schema.
    */
  def generate(node: UaVariableNode, path: String): JsValue

  /** Generates JSON Schema for Method's input and output arguments. Input/Output is generated as JSON Object.
    *
    * @param node Method node.
    * @param path Relative path to node from the root (/).
    * @return Tuple containing JSON Schemas for (Input, Output). If there are no input or output arguments, the result is None.
    */
  def generate(node: UaMethodNode, path: String): (Option[JsValue], Option[JsValue])

