package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.{UaByte, UaUInt16, UaUInt32}

/** A 16-byte value that can be used as a globally unique identifier.
  */
case class UaGuid(
  data1: UaUInt32 = UaUInt32(0),
  data2: UaUInt16 = UaUInt16(0),
  data3: UaUInt16 = UaUInt16(0),
  data4: Vector[UaByte] = Vector.fill(8)(UaByte(0))
) extends UaValue:

  require(data4.length == 8, "UaGuid::data4 is a 8-byte value.")
