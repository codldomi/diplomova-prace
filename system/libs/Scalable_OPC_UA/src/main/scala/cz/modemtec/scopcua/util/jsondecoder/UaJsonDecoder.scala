package cz.modemtec.scopcua.util.jsondecoder

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.value.UaValue

import play.api.libs.json.JsValue


/** A Decoder from JSON format to Value.
  */
trait UaJsonDecoder:

  /** Decodes JSON to Value.
    *
    * @param encoded JSON.
    * @param typeId Datatype id.
    * @return Decoded Value.
    */
  def decode(encoded: JsValue, typeId: UaId): UaValue

  /** Decodes JSON to Value, but allows null value.
    *
    * @param encoded JSON.
    * @param typeId  Datatype id.
    * @return Decoded optional Value.
    */
  def decodeOption(encoded: JsValue, typeId: UaId): Option[UaValue]

