package cz.modemtec.scopcua.util.jsonschemagenerator.impl.util


sealed trait JsonProperty

case class JsonObjectProperty(
  properties: Map[String, JsonProperty],
  required: Set[String]
) extends JsonProperty

case class JsonNumberProperty(
  enumvals: Vector[Int] = Vector.empty
) extends JsonProperty

case class JsonStringProperty(
  format: Option[String] = None,
  enumvals: Vector[String] = Vector.empty
)  extends JsonProperty

case object JsonBooleanProperty extends JsonProperty

case class JsonArrayProperty(
  items: JsonProperty,
  minItems: Option[Int] = None,
  maxItems: Option[Int] = None
) extends JsonProperty

case class JsonAnyOfProperty(types: Set[JsonProperty] = Set.empty) extends JsonProperty

case object JsonAnyProperty extends JsonProperty

case object JsonRecursiveProperty extends JsonProperty

case class JsonRefProperty(
  ref: String
) extends JsonProperty