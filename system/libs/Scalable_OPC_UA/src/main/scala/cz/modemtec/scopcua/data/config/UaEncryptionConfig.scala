package cz.modemtec.scopcua.data.config

import cz.modemtec.scopcua.util.trustlistmanager.UaTrustListManager

import java.security.KeyPair
import java.security.cert.X509Certificate


case class UaEncryptionConfig(
  keyPair: KeyPair,
  certificate: X509Certificate,
  chain: Vector[X509Certificate],
  trustList: UaTrustListManager
)
