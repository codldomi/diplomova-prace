package cz.modemtec.scopcua.util.nodesetextractor.impl

import com.ctc.wstx.stax.{WstxInputFactory, WstxOutputFactory}
import com.fasterxml.jackson.dataformat.xml.{XmlFactory, XmlMapper}

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation
import cz.modemtec.scopcua.data.node.*
import cz.modemtec.scopcua.data.value.UaString
import cz.modemtec.scopcua.data.value.number.integer.{UaInt32, UaUInt32}
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.util.nodesetextractor.UaNodeSetExtractor
import cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel.*
import cz.modemtec.scopcua.util.nodesetextractor.impl.javamodel.*

import javax.xml.stream.XMLInputFactory

import scala.jdk.CollectionConverters.*
import scala.xml.Node as XmlNode


object UaNodeSet1v04Extractor extends UaNodeSet1v04Extractor


/** Provides NodeSetExtractor for the OPC UA v1.04.
  */
trait UaNodeSet1v04Extractor extends UaNodeSetExtractor:
  
  override def extract(nodeset: XmlNode): UaNodeSet =
    var model = deserialize(nodeset)

    given uris: Map[Int, String] =
      model
        .uris
        .zipWithIndex
        .map(_.swap)
        .toMap

    given aliases: Map[String, String] =
      model
        .aliases
        .map(alias => alias.alias -> alias.value)
        .toMap

    model = supplyImplicit(model)
    model = optimize(model)

    val dataTypes =
      model
        .dataTypes
        .map(toUaDataType)
        .map(dataType => dataType.id -> dataType)
        .toMap

    val nodes = toNodes(model.objects, model.variables, model.methods)

    UaNodeSet(modelUri = model.name, uris.values.toSet, dataTypes, nodes)

  /** The extensions providing implicit String parsing into UaId and removing index from the UaQualifiedName.
    */
  extension (self: String)
    private def asUaId(using aliases: Map[String, String], uris: Map[Int, String]): UaId =
      val text = if aliases.isDefinedAt(self) then aliases(self) else self
      val nodeId = stringToUaNumericNodeId(text)
      val uri = uris(nodeId.namespaceIndex.value)
      UaId(nodeId.identifier, uri)

  extension (self: String)
    private def withoutIndex: String = "[0-9]+:(.*)".r.findFirstMatchIn(self) match
      case Some(result) => result.group(1)
      case None => self

  /** The extension providing implicit Option[String] parsing into UaId.
    */
  extension(self: Option[String])
    private def asUaId(using aliases: Map[String, String], uris: Map[Int, String]): Option[UaId] = self.map(_.asUaId)


  /** Supplies implicit values to the model.
    *
    * @note Assumes no custom non-hierarchical references.
    * @param model The model with implicits.
    * @param aliases Aliases mapping.
    * @param uris URIs mapping.
    * @return Model without missing information.
    */
  private def supplyImplicit(model: NodeSetModel)(using aliases: Map[String, String], uris: Map[Int, String]): NodeSetModel =

    def isHierarchical(typeId: UaId): Boolean =
      val id: Int => UaId = UaId(_, OpcUaFoundation)
      // todo: HARD CODED References - When OPC UA References is implemented, create code more general.
      val nonHierarchical =
        Set(
          id(32),    // NonHierarchicalReferences
          id(51),    // FromState
          id(41),    // GeneratesEvent
          id(17604), // HasAddIn
          id(53),    // HasCause
          id(39),    // HasDescription
          id(54),    // HasEffect
          id(38),    // HasEncoding
          id(17603), // HasInterface
          id(37),    // HasModellingRule
          id(40),    // HasTypeDefinition
          id(52))    // ToState

      !nonHierarchical.contains(typeId)

    def parentId(node: NodeModel): Option[String] = node.parentId match
      case Some(parentId) => Some(parentId)
      case None =>
        node
          .references
          .find: ref =>
            val typeId = ref.refType.asUaId
            !ref.isForward && isHierarchical(typeId)
          .map(ref => ref.to)

    val objects = model.objects.map(node => node.copy(parentId = parentId(node)))
    val methods = model.methods.map(node => node.copy(parentId = parentId(node)))
    val variables = model.variables.map(node => node.copy(parentId = parentId(node)))

    model.copy(
      objects = objects,
      methods = methods,
      variables = variables)

  /** Removes nodes not containing in the Object Tree.
    *
    * @note Assumes Object, Variable or Method aren't attached to a foreign ObjectType. But they can be under ObjectType
    *       from the same namespace. It's allowed everything in Objects (ObjectsFolder) subtree.
    * @param model The model with unnecessary nodes.
    * @param aliases Aliases mapping.
    * @param uris URIs mapping.
    * @return Optimized model.
    */
  private def optimize(model: NodeSetModel)(using aliases: Map[String, String], uris: Map[Int, String]): NodeSetModel =

    def subtree(root: UaId, nodes: Iterable[NodeModel]): Set[UaId] =
      val children =
        nodes
          .filter(_.parentId.asUaId.contains(root))
          .map(_.id.asUaId)
          .foldLeft(Set.empty[UaId]): (accumulator, child) =>
            accumulator ++ subtree(child, nodes)

      children + root

    val nodes = model.variables ++ model.methods ++ model.objects

    val types = model.objectTypes ++ model.variableTypes ++ model.dataTypes
    val dictionaries = 
      model
        .variables
        .filter: node =>
          node.browseName.contains("TypeDictionary") ||
          node.symbolicName.contains("BinarySchema") ||
          node.symbolicName.contains("XmlSchema")
      
    val withoutParents = nodes.filter(node => node.parentId.isEmpty && node.id.asUaId != UaId(84, OpcUaFoundation))

    val roots = types ++ dictionaries ++ withoutParents

    val forbidden =
      roots
        .map(_.id.asUaId)
        .toSet
        .foldLeft(Set.empty[UaId]): (acc, id) =>
          acc ++ subtree(id, nodes)

    val objects = model.objects.filter(node => !forbidden.contains(node.id.asUaId))
    val variables = model.variables.filter(node => !forbidden.contains(node.id.asUaId))
    val methods = model.methods.filter(node => !forbidden.contains(node.id.asUaId))

    model.copy(objects = objects, variables = variables, methods = methods)


  private def toNodes(objects: Seq[ObjectModel], variables: Seq[VariableModel], methods: Seq[MethodModel])(using aliases: Map[String, String], uris: Map[Int, String]): Map[UaId, UaNode] =
    val argumentId = UaId(296, OpcUaFoundation)

    val objectNodes =
      objects
        .map: model =>
          UaObjectNode(
            id          = model.id.asUaId,
            parentId    = model.parentId.asUaId,
            browseName  = model.browseName.withoutIndex,
            displayName = model.displayName,
            description = model.description.map(stringToUaString))
        .map(node => node.id -> node)
        .toMap

    val variableNodes =
      variables
        .filter(_.typeId.asUaId != argumentId)
        .map: model =>
          UaVariableNode(
            id              = model.id.asUaId,
            parentId        = model.parentId.asUaId,
            browseName      = model.browseName.withoutIndex,
            displayName     = model.displayName,
            description     = model.description.map(stringToUaString),
            typeId          = model.typeId.asUaId,
            valueRank       = model.valueRank,
            arrayDimensions = model.arrayDimensions.toVector.map(intToUaUInt32))
        .map(node => node.id -> node)
        .toMap

    val arguments =
      variables
        .filter(_.typeId.asUaId == argumentId)
        .map: model =>
          val isInput = model.browseName.withoutIndex == "InputArguments"
          val methodId = model.parentId.asUaId.get
          val args =
            model
              .args
              .zipWithIndex
              .map: (arg, index) =>
                UaArgument(
                  name            = arg.name,
                  isInput         = isInput,
                  typeId          = arg.typeId.asUaId,
                  valueRank       = arg.valueRank,
                  arrayDimensions = arg.arrayDimensions.toVector.map(intToUaUInt32),
                  ord             = index)
          (methodId, isInput) -> args
        .toMap

    val methodNodes =
      methods
        .map: model =>
          val id = model.id.asUaId

          val input = arguments.getOrElse((id, true), Nil)
          val output = arguments.getOrElse((id, false), Nil)

          UaMethodNode(
            id          = id,
            parentId    = model.parentId.asUaId,
            browseName  = model.browseName.withoutIndex,
            displayName = model.displayName,
            description = model.description.map(stringToUaString),
            input       = input.toVector,
            output      = output.toVector)

        .map(node => node.id -> node)
        .toMap

    objectNodes ++ methodNodes ++ variableNodes


  private def toUaDataType(model: DataTypeModel)(using aliases: Map[String, String], uris: Map[Int, String]): UaDataType =
    val HasSubtypeId = UaId(45, OpcUaFoundation)
    val id = model.id.asUaId

    val builtIn = UaDataType.BuiltIns.get(id)
    if builtIn.isDefined then return builtIn.get

    val name = model.browseName.withoutIndex
    val isAbstract = model.isAbstract

    def toSimple(superId: UaId): UaDataType.GeneralSimple = UaDataType.GeneralSimple(isAbstract, name, id, Some(superId))

    def toEnumeration: UaDataType.GeneralEnumeration =
      val superId = UaDataType.Enumeration.id
      val fields =
        model
          .definition
          .get
          .fields
          .zipWithIndex
          .map: (field, ord) =>
            val name = field.name
            val value = field.value.getOrElse(ord)
            UaString(name) -> UaInt32(value)

      UaDataType.GeneralEnumeration(isAbstract, name, id, Some(superId), fields: _*)

    def toStructure: UaDataType.GeneralStructure =
      val superId = UaDataType.Structure.id
      val fields =
        model
          .definition
          .get
          .fields
          .zipWithIndex
          .map: (field, ord) =>
            val name = field.name
            val typeId = field.dataType.asUaId
            val isOptional = field.isOptional
            val valueRank = field.valueRank
            val arrayDimensions =
              field
                .arrayDimensions
                .map(value => UaUInt32(value.toLong))
                .toVector
            UaDataType.GeneralStructure.Field(name, typeId, valueRank, arrayDimensions, isOptional, ord)

      UaDataType.GeneralStructure(isAbstract, name, id, Some(superId), fields*)

    val superId =
      model
        .references
        .find(ref => !ref.isForward && ref.refType.asUaId == HasSubtypeId)
        .map(_.to.asUaId)
        .get

    if superId == UaDataType.Structure.id then
      toStructure
    else if superId == UaDataType.Enumeration.id then
      toEnumeration
    else
      toSimple(superId)


  private def deserialize(nodeset: XmlNode): NodeSetModel =
    val mapper = new XmlMapper()

    def asArray(text: String): Array[Int] =
      text
        .split(",")
        .filter(_ != "")
        .map(_.toInt)

    def asReferences(refs: java.util.List[Reference]): Seq[ReferenceModel] =
      refs
        .asScala
        .toSeq
        .map: ref =>
          ReferenceModel(
            refType   = ref.getReferenceType,
            isForward = ref.isForward,
            to        = ref.getTo)

    def asFields(fields: java.util.List[DataTypeField]): Seq[FieldModel] =
      fields
        .asScala
        .toSeq
        .map: field =>
          FieldModel(
            name            = field.getName,
            value           = if field.getValue == -1 then None else Some(field.getValue),
            dataType        = field.getDataType,
            isOptional      = field.isOptional,
            valueRank       = field.getValueRank,
            arrayDimensions = asArray(field.getArrayDimensions))

    def asDefinition(obj: DataTypeDefinition): DefinitionModel =
      DefinitionModel(
        name = obj.getName,
        fields = asFields(obj.getField))

    def asValue(node: Option[XmlNode]): Seq[ArgumentModel] = node match
      case Some(variable) =>
        val nodes = (variable \\ "Argument") ++ (variable \\ "uax:Argument")
        nodes.map: value =>
          val xml =
            value
              .toString
              .replace("<uax:", "<")
              .replace("</uax:", "</")
          val obj = mapper.readValue(xml, classOf[Argument])
          ArgumentModel(
            name            = obj.getName,
            typeId          = obj.getDataType.getIdentifier,
            valueRank       = obj.getValueRank,
            arrayDimensions = asArray(obj.getArrayDimensions))

      case None => Seq.empty

    def asObject(node: XmlNode): ObjectModel =
      val obj = mapper.readValue(node.toString, classOf[UAObject])
      ObjectModel(
        id           = obj.getNodeId,
        parentId     = Option(obj.getParentNodeId),
        browseName   = obj.getBrowseName,
        displayName  = obj.getDisplayName,
        description  = Option(obj.getDescription),
        references   = asReferences(obj.getReferences),
        symbolicName = Option(obj.getSymbolicName))

    def asObjectType(node: XmlNode): ObjectTypeModel =
      val obj = mapper.readValue(node.toString, classOf[UAObjectType])
      ObjectTypeModel(
        id           = obj.getNodeId,
        browseName   = obj.getBrowseName,
        displayName  = obj.getDisplayName,
        description  = Option(obj.getDescription),
        references   = asReferences(obj.getReferences),
        symbolicName = Option(obj.getSymbolicName))

    def asVariable(node: XmlNode): VariableModel =
      val obj = mapper.readValue(node.toString, classOf[UAVariable])
      VariableModel(
        id              = obj.getNodeId,
        parentId        = Option(obj.getParentNodeId),
        browseName      = obj.getBrowseName,
        displayName     = obj.getDisplayName,
        description     = Option(obj.getDescription),
        references      = asReferences(obj.getReferences),
        typeId          = obj.getDataType,
        valueRank       = obj.getValueRank,
        arrayDimensions = asArray(obj.getArrayDimensions),
        args            = asValue((node \ "Value").headOption),
        symbolicName    = Option(obj.getSymbolicName))

    def asVariableType(node: XmlNode): VariableTypeModel =
      val obj = mapper.readValue(node.toString, classOf[UAVariableType])
      VariableTypeModel(
        id           = obj.getNodeId,
        browseName   = obj.getBrowseName,
        displayName  = obj.getDisplayName,
        description  = Option(obj.getDescription),
        references   = asReferences(obj.getReferences),
        symbolicName = Option(obj.getSymbolicName))

    def asMethod(node: XmlNode): MethodModel =
      val obj = mapper.readValue(node.toString, classOf[UAMethod])
      MethodModel(
        id           = obj.getNodeId,
        parentId     = Option(obj.getParentNodeId),
        browseName   = obj.getBrowseName,
        displayName  = obj.getDisplayName,
        description  = Option(obj.getDescription),
        references   = asReferences(obj.getReferences),
        symbolicName = Option(obj.getSymbolicName))

    def asDataType(node: XmlNode): DataTypeModel =
      val obj = mapper.readValue(node.toString, classOf[UADataType])
      DataTypeModel(
        id           = obj.getNodeId,
        browseName   = obj.getBrowseName,
        displayName  = obj.getDisplayName,
        description  = Option(obj.getDescription),
        references   = asReferences(obj.getReferences),
        definition   = Option(obj.getDefinition).map(asDefinition),
        isAbstract   = obj.getAbstract,
        symbolicName = Option(obj.getSymbolicName))

    nodeset.nonEmptyChildren.foldLeft(NodeSetModel.empty): (acc, node) =>
      node.label match
        case "UAVariable"     => acc.copy(variables = acc.variables :+ asVariable(node))
        case "UAVariableType" => acc.copy(variableTypes = acc.variableTypes :+ asVariableType(node))
        case "UAObject"       => acc.copy(objects = acc.objects :+ asObject(node))
        case "UAObjectType"   => acc.copy(objectTypes = acc.objectTypes :+ asObjectType(node))
        case "UAMethod"       => acc.copy(methods = acc.methods :+ asMethod(node))
        case "UADataType"     => acc.copy(dataTypes = acc.dataTypes :+ asDataType(node))
        case "NamespaceUris" =>
          val children = node \ "Uri"
          val uris = children.map: child =>
            val obj = mapper.readValue(child.toString, classOf[Uri])
            obj.getUri
          acc.copy(uris = acc.uris ++ uris)

        case "Models" =>
          val child = (node \ "Model").head
          val model = mapper.readValue(child.toString, classOf[Model])
          acc.copy(name = model.getModelUri)

        case "Aliases" =>
          val children = node \ "Alias"
          val aliases = children.map: child =>
            val obj = mapper.readValue(child.toString, classOf[Alias])
            AliasModel(
              alias = obj.getAlias,
              value = obj.getValue)
          acc.copy(aliases = acc.aliases ++ aliases)

        case _ => acc

