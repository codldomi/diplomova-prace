package cz.modemtec.scopcua.util.jsondecoder.impl

import cz.modemtec.scopcua.data.{UaDataType, UaId, value}
import cz.modemtec.scopcua.data.value.number.integer.*
import cz.modemtec.scopcua.util.jsondecoder.UaJsonDecoder
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.value.UaVariant.UaBuiltInId
import cz.modemtec.scopcua.data.value.number.{UaDouble, UaFloat}
import cz.modemtec.scopcua.util.valuecreator.impl.UaDefaultValueCreator

import io.netty.buffer.UnpooledByteBufAllocator
import play.api.libs.json.*

import java.util.Base64


/** Implements ModemTec OPC UA JSON format.
  */
class UaModemTecJsonDecoder(
  val typeOf: PartialFunction[UaId, UaDataType],
  val indexOf: PartialFunction[UaString, UaUInt16]
) extends UaJsonDecoder:

  private val creator = UaDefaultValueCreator(typeOf)

  
  override def decode(encoded: JsValue, typeId: UaId): UaValue = extract(encoded, typeId)


  override def decodeOption(encoded: JsValue, typeId: UaId): Option[UaValue] = 
    encoded match 
      case JsNull => None
      case other  => Some(extract(other, typeId))
  

  private def extract(json: JsValue, typeId: UaId): UaValue = 

    def extractOne(json: JsValue, typeId: UaId): UaValue = typeOf(typeId) match 
      case UaDataType.Boolean => extractBoolean(json)
      case UaDataType.SByte => extractSByte(json)
      case UaDataType.Byte => extractByte(json)
      case UaDataType.Int16 => extractInt16(json)
      case UaDataType.UInt16 => extractUInt16(json)
      case UaDataType.Int32 => extractInt32(json)
      case UaDataType.UInt32 => extractUInt32(json)
      case UaDataType.Int64 => extractInt64(json)
      case UaDataType.UInt64 => extractUInt64(json)
      case UaDataType.Float => extractFloat(json)
      case UaDataType.Double => extractDouble(json)
      case UaDataType.String => extractString(json)
      case UaDataType.DateTime => extractDateTime(json)
      case UaDataType.Guid => extractGuid(json)
      case UaDataType.ByteString => extractByteString(json)
      case UaDataType.XmlElement => extractXmlElement(json)
      case UaDataType.NodeId => extractNodeId(json)
      case UaDataType.ExpandedNodeId => extractExpandedNodeId(json)
      case UaDataType.StatusCode => extractStatusCode(json)
      case UaDataType.QualifiedName => extractQualifiedName(json)
      case UaDataType.LocalizedText => extractLocalizedText(json)
      case UaDataType.Structure => extractExtensionObject(json)
      case UaDataType.DataValue => extractDataValue(json)
      case UaDataType.DiagnosticInfo => extractDiagnosticInfo(json)
      case UaDataType.Enumeration => extractEnumeration(json)
      case UaDataType.BaseDataType => extractVariant(json)
      case _: UaDataType.GeneralEnumeration => extractEnumeration(json)
      case s: UaDataType.GeneralStructure => extractStructure(json, s)

      case _ => throw new IllegalArgumentException("Unsupported data type.")
    

    def extractStructure(json: JsValue, struct: UaDataType.GeneralStructure): UaStructure = 
      val fields = struct.fields.map(field => field.name.value -> field.typeId).toMap

      val obj = json match 
        case JsObject(obj) => obj
        case _ => throw new IllegalArgumentException("The given JSON is not a structured type.")
      

      val values = obj.map: (name, js) =>
        val typeId = fields(name)
        val value = extract(js, typeId)
        UaString(name) -> value
      
      UaStructure(values.toMap)

    def extractByteString(json: JsValue): UaByteString = 
      val signedBytes = Base64.getDecoder.decode(json.as[String])
      val buffer = UnpooledByteBufAllocator.DEFAULT.buffer()

      val unsignedBytes =
        try 
          buffer.writeBytes(signedBytes)
          for _ <- 0 until signedBytes.length yield buffer.readUnsignedByte()
        finally 
          buffer.release()

      UaByteString(unsignedBytes.map(UaByte.apply).toVector)

    def extractVariant(json: JsValue): UaVariant = 
      val varType = (json \ "varType").as[String]

      if varType == "NULL" then return UaVariant()

      val (id, dataType) = varType match 
        case "BOOLEAN" => (UaBuiltInId.Boolean, UaDataType.Boolean)
        case "SBYTE" => (UaBuiltInId.SByte, UaDataType.SByte)
        case "BYTE" => (UaBuiltInId.Byte, UaDataType.Byte)
        case "INT16" => (UaBuiltInId.Int16, UaDataType.Int16)
        case "UINT16" => (UaBuiltInId.UInt16, UaDataType.UInt16)
        case "INT32" => (UaBuiltInId.Int32, UaDataType.Int32)
        case "UINT32" => (UaBuiltInId.UInt32, UaDataType.UInt32)
        case "INT64" => (UaBuiltInId.Int64, UaDataType.Int64)
        case "UINT64" => (UaBuiltInId.UInt64, UaDataType.UInt64)
        case "FLOAT" => (UaBuiltInId.Float, UaDataType.Float)
        case "DOUBLE" => (UaBuiltInId.Double, UaDataType.Double)
        case "STRING" => (UaBuiltInId.String, UaDataType.String)
        case "DATETIME" => (UaBuiltInId.DateTime, UaDataType.DateTime)
        case "GUID" => (UaBuiltInId.Guid, UaDataType.Guid)
        case "BYTESTRING" => (UaBuiltInId.ByteString, UaDataType.ByteString)
        case "XMLELEMENT" => (UaBuiltInId.XmlElement, UaDataType.XmlElement)
        case "NODEID" => (UaBuiltInId.NodeId, UaDataType.NodeId)
        case "EXPANDEDNODEID" => (UaBuiltInId.ExpandedNodeId, UaDataType.ExpandedNodeId)
        case "STATUSCODE" => (UaBuiltInId.StatusCode, UaDataType.StatusCode)
        case "QUALIFIEDNAME" => (UaBuiltInId.QualifiedName, UaDataType.QualifiedName)
        case "LOCALIZEDTEXT" => (UaBuiltInId.LocalizedText, UaDataType.LocalizedText)
        case "EXTENSIONOBJECT" => (UaBuiltInId.ExtensionObject, UaDataType.Structure)
        case "DATAVALUE" => (UaBuiltInId.DataValue, UaDataType.DataValue)
        case "VARIANT" => (UaBuiltInId.Variant, UaDataType.BaseDataType)
        case "DIAGNOSTICINFO" => (UaBuiltInId.DiagnosticInfo, UaDataType.DiagnosticInfo)

        case _ => throw new IllegalArgumentException("Unsupported enum value.")
      

      val value = extract((json \ "value").get, dataType.id)
      UaVariant(id, Some(value))

    def extractDataValue(json: JsValue): UaDataValue = 
      val optionStatus = (json \ "status").getOrElse(JsNull)
      val optionSourcePicoSeconds = (json \ "sourcePicoSeconds").getOrElse(JsNull)
      val optionServerPicoSeconds = (json \ "serverPicoSeconds").getOrElse(JsNull)
      val optionSourceTimestamp = (json \ "sourceTimestamp").getOrElse(JsNull)
      val optionServerTimestamp = (json \ "serverTimestamp").getOrElse(JsNull)
      val optionValue = (json \ "value").getOrElse(JsNull)

      val status = optionStatus match 
        case JsNull => None
        case js => Some(extractStatusCode(js))
      
      val sourcePicoSeconds = optionSourcePicoSeconds match 
        case JsNull => None
        case js => Some(extractUInt16(js))
      
      val serverPicoSeconds = optionServerPicoSeconds match 
        case JsNull => None
        case js => Some(extractUInt16(js))
      
      val sourceTimestamp = optionSourceTimestamp match 
        case JsNull => None
        case js => Some(extractDateTime(js))
      
      val serverTimestamp = optionServerTimestamp match 
        case JsNull => None
        case js => Some(extractDateTime(js))
      
      val value = optionValue match 
        case JsNull => None
        case js => Some(extractVariant(js))
      

      UaDataValue(value, status, sourceTimestamp, sourcePicoSeconds, serverTimestamp, serverPicoSeconds)

    def extractDiagnosticInfo(json: JsValue): UaDiagnosticInfo = 
      val optionSymbolicId = (json \ "symbolicId").getOrElse(JsNull)
      val optionNamespaceUri = (json \ "namespaceUri").getOrElse(JsNull)
      val optionLocalizedText = (json \ "localizedText").getOrElse(JsNull)
      val optionLocale = (json \ "locale").getOrElse(JsNull)
      val optionAdditionalInfo = (json \ "additionalInfo").getOrElse(JsNull)
      val optionInnerStatusCode = (json \ "innerStatusCode").getOrElse(JsNull)
      val optionInnerDiagnosticInfo = (json \ "innerDiagnosticInfo").getOrElse(JsNull)
      
      val symbolicId = optionSymbolicId match 
        case JsNull => None
        case js => Some(extractInt32(js))
      
      val namespaceUri = optionNamespaceUri match 
        case JsNull => None
        case js => Some(extractInt32(js))
      
      val localizedText = optionLocalizedText match 
        case JsNull => None
        case js => Some(extractInt32(js))
      
      val locale = optionLocale match 
        case JsNull => None
        case js => Some(extractInt32(js))
      
      val additionalInfo = optionAdditionalInfo match 
        case JsNull => None
        case js => Some(extractString(js))
      
      val innerStatusCode = optionInnerStatusCode match 
        case JsNull => None
        case js => Some(extractStatusCode(js))
      
      val innerDiagnosticInfo = optionInnerDiagnosticInfo match 
        case JsNull => None
        case js => Some(extractDiagnosticInfo(js))
      
      UaDiagnosticInfo(symbolicId, namespaceUri, localizedText, locale, additionalInfo, innerStatusCode, innerDiagnosticInfo)

    def extractDateTime(json: JsValue): UaDateTime = 
      val str = json.as[String]
      stringToUaDateTime(str)

    def extractEnumeration(json: JsValue): UaEnumeration = 
      val value = extractInt32((json \ "value").get)
      val name = extractString((json \ "name").get)
      UaEnumeration(value, name)

    def extractExpandedNodeId(json: JsValue): UaExpandedNodeId = 
      val namespaceUri = extractString((json \ "id" \ "uri").get)
      val id = extractNodeId((json \ "id").get)
      val serverIndex = extractUInt32((json \ "serverIndex").get)
      UaExpandedNodeId(namespaceUri, id, serverIndex)

    def extractExtensionObject(json: JsValue): UaExtensionObject = 
      val enType = (json \ "enType").as[String]
      enType match 
        case "BYTE" => extractByteExtensionObject(json)
        case "XML" => extractXmlExtensionObject(json)
        case _ => throw new IllegalArgumentException("Not supported datatype.")
      

    def extractByteExtensionObject(json: JsValue): UaByteExtensionObject = 
      val encodingId = extractNodeId((json \ "encodingId").get)
      val body = extractByteString((json \ "body").get)
      UaByteExtensionObject(encodingId, body)

    def extractXmlExtensionObject(json: JsValue): UaXmlExtensionObject = 
      val encodingId = extractNodeId((json \ "encodingId").get)
      val body = extractXmlElement((json \ "body").get)
      UaXmlExtensionObject(encodingId, body)

    def extractLocalizedText(json: JsValue): UaLocalizedText = 
      val text = extractString((json \ "text").get)
      val locale = extractString((json \ "locale").get)
      UaLocalizedText(text, locale)

    def extractNodeId(json: JsValue): UaNodeId = 
      val idType = (json \ "idType").as[String]
      idType match 
        case "NUMERIC" => extractNumericNodeId(json)
        case "GUID" => extractGuidNodeId(json)
        case "OPAQUE" => extractOpaqueNodeId(json)
        case "STRING" => extractStringNodeId(json)
        case _ => throw new IllegalArgumentException("Not supported datatype.")
      

    def extractNumericNodeId(json: JsValue): UaNumericNodeId = 
      val namespaceUri = extractString((json \ "uri").get)
      val namespaceIndex = indexOf(namespaceUri)
      val identifier = extractUInt32((json \ "id").get)
      UaNumericNodeId(namespaceIndex, identifier)

    def extractGuidNodeId(json: JsValue): UaGuidNodeId = 
      val namespaceUri = extractString((json \ "uri").get)
      val namespaceIndex = indexOf(namespaceUri)
      val identifier = extractGuid((json \ "id").get)
      UaGuidNodeId(namespaceIndex, identifier)

    def extractOpaqueNodeId(json: JsValue): UaOpaqueNodeId = 
      val namespaceUri = extractString((json \ "uri").get)
      val namespaceIndex = indexOf(namespaceUri)
      val identifier = extractByteString((json \ "id").get)
      UaOpaqueNodeId(namespaceIndex, identifier)

    def extractStringNodeId(json: JsValue): UaStringNodeId = 
      val namespaceUri = extractString((json \ "uri").get)
      val namespaceIndex = indexOf(namespaceUri)
      val identifier = extractString((json \ "id").get)
      UaStringNodeId(namespaceIndex, identifier)

    def extractStatusCode(json: JsValue): UaStatusCode = 
      val code = extractUInt32((json \ "code").get)
      val name = extractString((json \ "symbol").get)
      value.UaStatusCode(code, name)

    def extractQualifiedName(json: JsValue): UaQualifiedName = 
      val name = extractString((json \ "name").get)
      val namespaceUri = extractString((json \ "uri").get)
      val namespaceIndex = indexOf(namespaceUri)
      UaQualifiedName(name, namespaceIndex)

    def extractGuid(json: JsValue): UaGuid = json.as[String]
    def extractBoolean(json: JsValue): UaBoolean = json.as[Boolean]
    def extractXmlElement(json: JsValue): UaXmlElement = json.as[String]
    def extractString(json: JsValue): UaString = json.as[String]
    def extractDouble(json: JsValue): UaDouble = json.as[Double]
    def extractFloat(json: JsValue): UaFloat = json.as[Float]
    def extractByte(json: JsValue): UaByte = json.as[Short]
    def extractSByte(json: JsValue): UaSByte = json.as[Byte]
    def extractUInt16(json: JsValue): UaUInt16 = json.as[Int]
    def extractInt16(json: JsValue): UaInt16 = json.as[Short]
    def extractUInt32(json: JsValue): UaUInt32 = json.as[Long]
    def extractInt32(json: JsValue): UaInt32 = json.as[Int]
    def extractUInt64(json: JsValue): UaUInt64 = json.as[BigInt]
    def extractInt64(json: JsValue): UaInt64 = json.as[Long]
    
    json match 
      case JsNull       => creator.create(typeId)
      case JsArray(arr) =>
        val elements = arr.map(j => extract(j, typeId))
        UaArray(elements.toVector)
      case j => extractOne(j, typeId)

