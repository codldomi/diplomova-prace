package cz.modemtec.scopcua.data.value.number

/** An IEEE double precision (64 bit) floating point value.
  *
  * @param value Scala representation.
  */
case class UaDouble(value: Double) extends UaNumber:

  override def asBigDecimal: BigDecimal = BigDecimal(value)
