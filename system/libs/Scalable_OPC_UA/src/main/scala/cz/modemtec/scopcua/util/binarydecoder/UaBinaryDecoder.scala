package cz.modemtec.scopcua.util.binarydecoder

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.value.number.*
import cz.modemtec.scopcua.data.value.*
import cz.modemtec.scopcua.data.value.number.integer.*


/** Decoder from binary data encoding to the value.
  */
trait UaBinaryDecoder:

  /** Decodes bytes with an encoded Value.
    *
    * @param encoded Sequence of bytes.
    * @param typeId Datatype identifier.
    * @return Decoded Value.
    */
  def decode(encoded: Seq[UaByte], typeId: UaId): UaValue

  /** Decodes bytes with an encoded Boolean.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Boolean.
    */
  def decodeBoolean(encoded: Seq[UaByte]): UaBoolean

  /** Decodes bytes with an encoded Byte.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Byte.
    */
  def decodeByte(encoded: Seq[UaByte]): UaByte

  /** Decodes bytes with an encoded SByte.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded SByte.
    */
  def decodeSByte(encoded: Seq[UaByte]): UaSByte

  /** Decodes bytes with an encoded Int16.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Int16.
    */
  def decodeInt16(encoded: Seq[UaByte]): UaInt16

  /** Decodes bytes with an encoded UInt16.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded UInt16.
    */
  def decodeUInt16(encoded: Seq[UaByte]): UaUInt16

  /** Decodes bytes with an encoded Int32.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Int32.
    */
  def decodeInt32(encoded: Seq[UaByte]): UaInt32

  /** Decodes bytes with an encoded UInt32.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded UInt32.
    */
  def decodeUInt32(encoded: Seq[UaByte]): UaUInt32

  /** Decodes bytes with an encoded Int64.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Int64.
    */
  def decodeInt64(encoded: Seq[UaByte]): UaInt64

  /** Decodes bytes with an encoded UInt64.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded UInt64.
    */
  def decodeUInt64(encoded: Seq[UaByte]): UaUInt64

  /** Decodes bytes with an encoded Float.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Float.
    */
  def decodeFloat(encoded: Seq[UaByte]): UaFloat

  /** Decodes bytes with an encoded Double.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Double.
    */
  def decodeDouble(encoded: Seq[UaByte]): UaDouble

  /** Decodes bytes with an encoded String.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded String.
    */
  def decodeString(encoded: Seq[UaByte]): UaString

  /** Decodes bytes with an encoded XmlElement.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded XmlElement.
    */
  def decodeXmlElement(encoded: Seq[UaByte]): UaXmlElement

  /** Decodes bytes with an encoded ByteString.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded ByteString.
    */
  def decodeByteString(encoded: Seq[UaByte]): UaByteString

  /** Decodes bytes with an encoded Guid.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Guid.
    */
  def decodeGuid(encoded: Seq[UaByte]): UaGuid

  /** Decodes bytes with an encoded NodeId.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded NodeId.
    */
  def decodeNodeId(encoded: Seq[UaByte]): UaNodeId

  /** Decodes bytes with an encoded ExpandedNodeId.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded ExpandedNodeId.
    */
  def decodeExpandedNodeId(encoded: Seq[UaByte]): UaExpandedNodeId

  /** Decodes bytes with an encoded QualifiedName.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded QualifiedName.
    */
  def decodeQualifiedName(encoded: Seq[UaByte]): UaQualifiedName

  /** Decodes bytes with an encoded LocalizedText.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded LocalizedText.
    */
  def decodeLocalizedText(encoded: Seq[UaByte]): UaLocalizedText

  /** Decodes bytes with an encoded DataValue.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded DataValue.
    */
  def decodeDataValue(encoded: Seq[UaByte]): UaDataValue

  /** Decodes bytes with an encoded DiagnosticInfo.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded DiagnosticInfo.
    */
  def decodeDiagnosticInfo(encoded: Seq[UaByte]): UaDiagnosticInfo

  /** Decodes bytes with an encoded DateTime.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded DateTime.
    */
  def decodeDateTime(encoded: Seq[UaByte]): UaDateTime

  /** Decodes bytes with an encoded StatusCode.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded StatusCode.
    */
  def decodeStatusCode(encoded: Seq[UaByte]): UaStatusCode

  /** Decodes bytes with an encoded Variant.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Variant.
    */
  def decodeVariant(encoded: Seq[UaByte]): UaVariant

  /** Decodes bytes with an encoded ExtensionObject.
    *
    * @param encoded Sequence of bytes.
    * @return Decoded Extension.
    */
  def decodeExtensionObject(encoded: Seq[UaByte]): UaExtensionObject

  /** Decodes bytes with an encoded user defined Structure.
    *
    * @param encoded Sequence of bytes.
    * @param id DataType of the value.
    * @return Decoded user defined Structure.
    */
  def decodeStructure(encoded: Seq[UaByte], id: UaId): UaStructure

  /** Decodes bytes with an encoded user defined Enumeration.
    *
    * @param encoded Sequence of bytes.
    * @param id DataType of the value.
    * @return Decoded user defined Enumeration.
    */
  def decodeEnumeration(encoded: Seq[UaByte], id: UaId): UaEnumeration


