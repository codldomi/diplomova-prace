package cz.modemtec.scopcua.data.value

import cz.modemtec.scopcua.data.value.number.integer.UaUInt16

/** A name qualified by a namespace.
  *
  * @param name           The text portion of the QualifiedName restricted to 512 characters.
  *
  * @param namespaceIndex Index that identifies the namespace that defines the name. This index is the index of that
  *                       namespace in the local Server’s NamespaceArray.The Client may read the NamespaceArray Variable
  *                       to access the string value of the namespace.
  */
case class UaQualifiedName(name: UaString, namespaceIndex: UaUInt16) extends UaValue:

  require(name.length <= 512, "The text portion of the QualifiedName is restricted to 512 characters.")

