package cz.modemtec.scopcua.data.node

import cz.modemtec.scopcua.data.{UaId, UaIdentifiable}
import cz.modemtec.scopcua.data.value.number.integer.{UaInt32, UaUInt32}
import cz.modemtec.scopcua.data.value.UaString


sealed trait UaNode extends UaIdentifiable:
  def parentId: Option[UaId]
  def browseName: UaString
  def displayName: UaString
  def description: Option[UaString]


case class UaObjectNode(
  id: UaId,
  parentId: Option[UaId],
  browseName: UaString,
  displayName: UaString,
  description: Option[UaString],
) extends UaNode


case class UaVariableNode(
  id: UaId,
  parentId: Option[UaId],
  browseName: UaString,
  displayName: UaString,
  description: Option[UaString],
  typeId: UaId,
  valueRank: UaInt32,
  arrayDimensions: Vector[UaUInt32]
) extends UaNode


case class UaMethodNode(
  id: UaId,
  parentId: Option[UaId],
  browseName: UaString,
  displayName: UaString,
  description: Option[UaString],
  input: Vector[UaArgument],
  output: Vector[UaArgument]
) extends UaNode