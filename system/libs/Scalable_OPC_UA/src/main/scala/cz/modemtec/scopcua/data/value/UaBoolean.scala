package cz.modemtec.scopcua.data.value

/** A two-state logical value (true or false).
  *
  * @param value Scala representation.
  */
case class UaBoolean(value: Boolean) extends UaValue
