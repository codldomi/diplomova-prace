package cz.modemtec.scopcua.data.value

import UaVariant.UaBuiltInId

/** A Variant is a union of all built-in data types including an ExtensionObject. Variants can also contain arrays of
  * any of these built-in types. Variants are used to store any value or parameter with a data type of BaseDataType or
  * one of its subtypes.
  *
  * Variants can be empty. An empty Variant is described as having a null value and should be treated like a null column
  * in a SQL database. A null value in a Variant may not be the same as a null value for data types that support nulls
  * such as Strings. Some DevelopmentPlatforms may not be able to preserve the distinction between a null for a DataType
  * and a null for a Variant, therefore, applications shall not rely on this distinction. This requirement also means
  * that if an Attribute supports the writing of a null value it shall also support writing of an empty Variant and
  * vice versa.
  *
  * Variants can contain arrays of Variants but they cannot directly contain another Variant.
  *
  * DiagnosticInfo types only have meaning when returned in a response message with an associated StatusCode and table
  * of strings. As a result, Variants cannot contain instances of DiagnosticInfo.
  *
  * Values of Attributes are always returned in instances of DataValues. Therefore, the DataType of an Attribute cannot
  * be a DataValue. Variants can contain DataValue when used in other contexts such as Method Arguments or PubSub
  * Messages. The Variant in a DataValue cannot, directly or indirectly, contain another DataValue.
  *
  * Variables with a DataType of BaseDataType are mapped to a Variant, however, the ValueRank and ArrayDimensions
  * Attributes place restrictions on what is allowed in the Variant. For example, if the ValueRank is Scalar then the
  * Variant may only contain scalar values.
  *
  * ExtensionObjects and Variants allow unlimited nesting which could result in stack overflow errors even if the
  * message size is less than the maximum allowed. Decoders shall support at least 100 nesting levels. Decoders shall
  * report an error if the number of nesting levels exceeds what it supports.
  *
  * @param id Built-in DataType's identifier.
  * @param value The value corresponding to its DataType's identifier.
  */
case class UaVariant(
  id: UaBuiltInId = UaBuiltInId.Null,
  value: Option[UaValue] = None
) extends UaValue:

  // todo : implement constraints checking (more in description above)

  def nonArray: Boolean = !isArray

  def isArray: Boolean = 
    value match 
      case Some(value) => value.isInstanceOf[UaArray]
      case None        => false
  


object UaVariant:

  sealed trait UaBuiltInId

  object UaBuiltInId:
    case object Null            extends UaBuiltInId
    case object Boolean         extends UaBuiltInId
    case object SByte           extends UaBuiltInId
    case object Byte            extends UaBuiltInId
    case object Int16           extends UaBuiltInId
    case object UInt16          extends UaBuiltInId
    case object Int32           extends UaBuiltInId
    case object UInt32          extends UaBuiltInId
    case object Int64           extends UaBuiltInId
    case object UInt64          extends UaBuiltInId
    case object Float           extends UaBuiltInId
    case object Double          extends UaBuiltInId
    case object String          extends UaBuiltInId
    case object DateTime        extends UaBuiltInId
    case object Guid            extends UaBuiltInId
    case object ByteString      extends UaBuiltInId
    case object XmlElement      extends UaBuiltInId
    case object NodeId          extends UaBuiltInId
    case object ExpandedNodeId  extends UaBuiltInId
    case object StatusCode      extends UaBuiltInId
    case object QualifiedName   extends UaBuiltInId
    case object LocalizedText   extends UaBuiltInId
    case object ExtensionObject extends UaBuiltInId
    case object DataValue       extends UaBuiltInId
    case object Variant         extends UaBuiltInId
    case object DiagnosticInfo  extends UaBuiltInId
  
    



