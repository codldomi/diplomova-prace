package cz.modemtec.scopcua.util.nodesetextractor.impl.scalamodel


case class ReferenceModel(
  refType: String,
  isForward: Boolean,
  to: String
)
