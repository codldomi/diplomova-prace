package cz.modemtec.scopcua.data.config


case class UaClientConfig(
  name: String,
  uri: String,
  url: String,
  timeout: Int,
  encryption: Option[UaEncryptionConfig],
  auth: Option[UaAuthConfig],
  policy: UaSecurityPolicy,
)
