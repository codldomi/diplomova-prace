
#include <gtest/gtest.h>
#include <open62541/types.h>
#include "test_server/dataset/record.h"

using namespace test_server::dataset;

namespace test_project {

TEST(RecordTests, At) {

  const void *value;

  std::string first = "first";
  std::string second = "second";
  std::string third = "third";
  std::string cz = "cz";

  const UA_DataType *type = &UA_TYPES[UA_TYPES_LOCALIZEDTEXT];
  const UA_LocalizedText data[] = {
      UA_LOCALIZEDTEXT(cz.data(), first.data()),
      UA_LOCALIZEDTEXT(cz.data(), second.data()),
      UA_LOCALIZEDTEXT(cz.data(), third.data())
  };

  Record record = Record(type, data, sizeof(data), "LocalizedTexts");

  for (int index = 0; index < record.Size(); index++) {
    value = record.At(index);
    EXPECT_EQ(&data[index], value);
  }
}

}