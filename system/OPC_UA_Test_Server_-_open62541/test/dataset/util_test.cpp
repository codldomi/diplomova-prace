
#include <gtest/gtest.h>
#include <open62541/types.h>
#include "test_server/dataset/util.h"

using namespace test_server::dataset;

namespace test_project {

TEST(UtilTests, EncodeBinary) {

  const UA_String strings[] = {UA_STRING_STATIC("string"), UA_STRING_STATIC(""), UA_STRING_STATIC("helloWorld")};
  std::vector<Record> records = {
    Record(&UA_TYPES[UA_TYPES_STRING], strings, 3, "Strings")
  };

  std::vector<BinaryRecord> expected = {
      BinaryRecord(
          "Strings",
          {{6, 0, 0, 0, 115, 116, 114, 105, 110, 103},
           {255, 255, 255, 255},
           {10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100}})
  };

  std::vector<BinaryRecord> result = EncodeBinary(records);

  EXPECT_EQ(result, expected);
}

TEST(UtilTests, GenerateScalaVariables) {

  std::vector<BinaryRecord> records = {
      BinaryRecord(
          "Strings",
          {{6, 0, 0, 0, 115, 116, 114, 105, 110, 103},
           {255, 255, 255, 255},
           {10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100}})
  };

  std::string expected =
      "val Strings: Seq[Seq[UaByte]] =\n"
      "  Seq(\n"
      "    Seq(6, 0, 0, 0, 115, 116, 114, 105, 110, 103),\n"
      "    Seq(255, 255, 255, 255),\n"
      "    Seq(10, 0, 0, 0, 104, 101, 108, 108, 111, 87, 111, 114, 108, 100))\n\n";

  std::string result = GenerateScalaVariables(records);

  EXPECT_EQ(result, expected);
}

}