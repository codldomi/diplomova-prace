## OPC UA Test Server

Program umožňující generování testovacích dat a spuštění testovacího OPC UA serveru.

verze 0.1.0

---

### Jazyk

- [C++ 17](https://en.cppreference.com/w/cpp/17)
- konvence dle [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)

### Použité knihovny

- [Google Test](https://github.com/google/googletest)
- [open62541](http://www.open62541.org/)
- [OPC UA Nodes Extension](http://gitlab.intranet.modemtec.cz/libraries/pc/opcua_nodes_extension)

### Funkcionality

- podpora OPC UA verze 1.04
- testovací server (pouze anonymní přihlášení bez šifrování)
- testovací data
