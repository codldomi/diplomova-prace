#include <iostream>

#include "test_server/server/iec_server.h"
#include "test_server/dataset/util.h"


using namespace std;
using namespace test_server;


void RunServer() {

  auto server = IecServer::Create();
  server.Run();
}


void Generate() {
  auto records = dataset::kRecords;
  auto encoded_records = dataset::EncodeBinary(records);

  auto scala = dataset::GenerateScalaVariables(encoded_records);
  std::cout << scala << std::endl;
}


int main() {

  //Generate();
  RunServer();
  return 0;
}
