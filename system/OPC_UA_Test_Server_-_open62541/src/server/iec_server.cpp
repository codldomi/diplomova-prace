
#include "test_server/server/iec_server.h"

#include "test_server/nodeset/iec7-3_cs/types_iec7_3_generated.h"
#include "test_server/nodeset/iec7-3_cs/ns_iec7_3_reduced.h"
#include "test_server/nodeset/iec7-4_cs/ns_iec7_4_reduced.h"
#include "test_server/nodeset/mtpdm1_cs/namespace_mtpdm1_generated.h"
#include "test_server/nodeset/mtpdm1_cs/mtpdm1_nodeids.h"

#include <open62541/server_config_default.h>

#include <open62541/plugin/historydata/history_data_backend_memory.h>
#include <open62541/plugin/historydata/history_data_gathering_default.h>
#include <open62541/plugin/historydata/history_database_default.h>
#include <open62541/plugin/historydatabase.h>

#include <nex_macros.h>
#include <nex_utility.h>
#include <stdexcept>


namespace test_server {

void IecServer::InitNamespaces(UA_Server * server) {
  UA_StatusCode retval = UA_STATUSCODE_GOOD;
  UA_NodeId root = UA_NODEID_NUMERIC(0, UA_NS0ID_ROOTFOLDER);

  retval = ns_iec7_3_reduced(server);
  NEX_CHECK_GOTO(retval, exit)

  retval = ns_iec7_4_reduced(server);
  NEX_CHECK_GOTO(retval, exit)

  retval = namespace_mtpdm1_generated(server);
  NEX_CHECK_GOTO(retval, exit)

  retval = NEX_Duplicates_remove(server, &root);

exit:
  UA_NodeId_clear(&root);

  if (UA_StatusCode_isBad(retval)){
    throw std::logic_error("Error during namespaces initialization.");
  }
}


void IecServer::InitHistoryData(UA_Server *server) {
  UA_StatusCode retval = UA_STATUSCODE_GOOD;
  //  ../SPDC1/Beh/q
  UA_NodeId id = UA_NODEID_NUMERIC(4, 6058);

  UA_ServerConfig *config = UA_Server_getConfig(server);

  UA_HistoryDataGathering gathering = UA_HistoryDataGathering_Default(1);
  config->historyDatabase = UA_HistoryDatabase_default(gathering);

  /* Now we define the settings for our node */
  UA_HistorizingNodeIdSettings setting;

  /* There is a memory based database plugin. We will use that. We just
     * reserve space for 3 nodes with 10 values each. This will NOT automatically grow
     * but will store data as a circular buffer of size 10. The 11th value will be
     * stored replacing the oldest one and the process will continue like that. */
  setting.historizingBackend = UA_HistoryDataBackend_Memory(3, 20);

  /* We want the server to serve a maximum of 100 values per request. This
   * value depend on the plattform you are running the server. A big server
   * can serve more values, smaller ones less. */
  setting.maxHistoryDataResponseSize = 2;

  /* If we have a sensor which do not report updates
   * and need to be polled we change the setting like that.
   * The polling interval in ms.
   *
  setting.pollingInterval = 100;
   *
   * Set the update strategie to polling.
   *
  setting.historizingUpdateStrategy = UA_HISTORIZINGUPDATESTRATEGY_POLL;
   */

  /* If you want to insert the values to the database yourself, we can set the user strategy here.
   * This is useful if you for example want a value stored, if a defined delta is reached.
   * Then you should use a local monitored item with a fuzziness and store the value in the callback.
   *
  setting.historizingUpdateStrategy = UA_HISTORIZINGUPDATESTRATEGY_USER;
   */

  /* We want the values stored in the database, when the nodes value is
   * set. */
  setting.historizingUpdateStrategy = UA_HISTORIZINGUPDATESTRATEGY_VALUESET;

  /* At the end we register the node for gathering data in the database. */
  retval = gathering.registerNodeId(server, gathering.context, &id, setting);;

  UA_Quality qualities[] = {
      {
        .validity = UA_VALIDITYKIND_GOOD,
        .detailQual =
        {
          .overflow = UA_FALSE,
          .outOfRange = UA_FALSE,
          .badReference = UA_FALSE,
          .oscillatory = UA_FALSE,
          .failure = UA_FALSE,
          .oldData = UA_FALSE,
          .inconsistent = UA_FALSE,
          .inaccurate = UA_FALSE
        },
        .source = UA_SOURCEKIND_PROCESS,
        .test = UA_FALSE,
        .operatorBlocked = UA_FALSE
      },
      {
        .validity = UA_VALIDITYKIND_INVALID,
        .detailQual =
        {
          .overflow = UA_TRUE,
          .outOfRange = UA_TRUE,
          .badReference = UA_TRUE,
          .oscillatory = UA_TRUE,
          .failure = UA_FALSE,
          .oldData = UA_FALSE,
          .inconsistent = UA_FALSE,
          .inaccurate = UA_FALSE
        },
        .source = UA_SOURCEKIND_PROCESS,
        .test = UA_TRUE,
        .operatorBlocked = UA_TRUE
      },
      {
        .validity = UA_VALIDITYKIND_GOOD,
        .detailQual =
        {
            .overflow = UA_FALSE,
            .outOfRange = UA_FALSE,
            .badReference = UA_FALSE,
            .oscillatory = UA_FALSE,
            .failure = UA_FALSE,
            .oldData = UA_FALSE,
            .inconsistent = UA_FALSE,
            .inaccurate = UA_FALSE
        },
        .source = UA_SOURCEKIND_PROCESS,
        .test = UA_FALSE,
        .operatorBlocked = UA_FALSE
      },
  };

  retval = UA_Server_writeHistorizing(server, id, true);
  NEX_CHECK_GOTO(retval, exit)

  for (auto quality : qualities) {
    UA_Variant value;
    UA_Variant_setScalar(&value, &quality, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);

    retval = UA_Server_writeValue(server, id, value);
    NEX_CHECK_BREAK(retval)
  }

exit:
  if (UA_StatusCode_isBad(retval)) {
    throw std::logic_error("Error during history data initialization.");
  }
}


void IecServer::InitValues(UA_Server * server) {
  UA_StatusCode retval = UA_STATUSCODE_GOOD;

  UA_Quality quality = {
    .validity = UA_VALIDITYKIND_INVALID,
    .detailQual = {
      .overflow = UA_TRUE,
      .outOfRange = UA_FALSE,
      .badReference = UA_TRUE,
      .oscillatory = UA_FALSE,
      .failure = UA_TRUE,
      .oldData = UA_FALSE,
      .inconsistent = UA_TRUE,
      .inaccurate = UA_FALSE
      },
      .source = UA_SOURCEKIND_PROCESS,
      .test = UA_FALSE,
      .operatorBlocked = UA_TRUE
  };

  UA_BehaviourModeKind behaviour_mode_kind = UA_BEHAVIOURMODEKIND_TEST;

  retval = NEX_Server_writeScalarValue(server, UA_NODEID_NUMERIC(4, 6001), &quality, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
  NEX_CHECK_GOTO(retval, exit)

  retval = NEX_Server_writeScalarValue(server, UA_NODEID_NUMERIC(4, 6002), &behaviour_mode_kind, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);

exit:
  if (UA_StatusCode_isBad(retval)) {
    throw std::logic_error("Error during values initialization.");
  }
}


void IecServer::IntegerSequence(UA_Server *server, void *data) {
  static int number = 0;
  UA_NodeId node = UA_NODEID_NUMERIC(4, 6057);

  UA_StatusCode retval = NEX_Server_writeScalarValue(server, node, &number, &UA_TYPES[UA_TYPES_INT32]);

  number++;
  number = number % 5;

  if (UA_StatusCode_isBad(retval)) {
    throw std::logic_error("Error during subscription initialization.");
  }
}


void IecServer::InitRepeatedJobs(UA_Server *server) {
  UA_StatusCode retval = UA_Server_addRepeatedCallback(server, IntegerSequence, nullptr, 500, nullptr);

  if (UA_StatusCode_isBad(retval)) {
    throw std::logic_error("Error during subscription initialization.");
  }
}


void IecServer::InitMethods(UA_Server *server) {
  UA_StatusCode retval = UA_STATUSCODE_GOOD;
  UA_UInt32 identifier;

  size_t count = 1;
  NEX_Nodes_BindMethod methods[] = {
    {
      .identifier = UA_MTPDM1ID_PDM1_CONF_CHANGEPASS,
      .methodCallback = ChangePassCallback
    }
  };

  const UA_String uri = UA_STRING_STATIC("http://www.modemtec.cz/PD/");
  size_t index;
  retval = UA_Server_getNamespaceByName(server, uri, &index);
  NEX_CHECK_GOTO(retval, exit)

  retval = NEX_Nodes_bindMethod(server, index, methods, count, &identifier);

exit:
  if (UA_StatusCode_isBad(retval)) {
    throw std::logic_error("Error during methods initialization.");
  }
}


IecServer::~IecServer() {
  if (server_ != nullptr) {
    UA_Server_delete(server_);
  }
}


void IecServer::Run() {
  is_running_ = true;
  UA_StatusCode retval = UA_Server_run(server_, &is_running_);

  if (UA_StatusCode_isBad(retval)){
    throw std::logic_error("Error during server running.");
  }
}


IecServer::IecServer(UA_Server *server) :
server_(server),
is_running_(false)
{}


const UA_DataTypeArray kModemTecTypes = {
  .next = nullptr,
  .typesSize = UA_TYPES_MTPDM1_COUNT,
  .types = UA_TYPES_MTPDM1
};

const UA_DataTypeArray kIec74Types = {
  .next = &kModemTecTypes,
  .typesSize = UA_TYPES_IEC7_4_COUNT,
  .types = UA_TYPES_IEC7_4
};

const UA_DataTypeArray kIec73Types = {
  .next = &kIec74Types,
  .typesSize = UA_TYPES_IEC7_3_COUNT,
  .types = UA_TYPES_IEC7_3
};

const UA_DataTypeArray IecServer::kCustomTypes = kIec73Types;


IecServer IecServer::Create() {
  auto server = UA_Server_new();
  auto config = UA_Server_getConfig(server);

  UA_ServerConfig_setDefault(config);
  config->customDataTypes = &kCustomTypes;
  config->allowEmptyVariables = UA_RULEHANDLING_ACCEPT;

  InitNamespaces(server);
  InitValues(server);
  InitMethods(server);
  InitRepeatedJobs(server);
  InitHistoryData(server);

  return IecServer(server);
}


UA_StatusCode IecServer::ChangePassCallback(UA_Server *server, const UA_NodeId *session_id, void *session_context,
                                            const UA_NodeId *method_id, void *method_context, const UA_NodeId *object_id,
                                            void *object_context, size_t input_size, const UA_Variant *input,
                                            size_t output_size,UA_Variant *output) {
  UA_StatusCode retval = UA_STATUSCODE_GOOD;
  UA_MtRequestResult result = UA_MTREQUESTRESULT_OK;

  if (input_size != 2) {
    result = UA_MTREQUESTRESULT_INVALIDARGUMENT;
    retval = NEX_Argument_setValue(output, 0, &result, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
    return retval;
  }

  //  reference values
  UA_String ref_username = UA_STRING_STATIC("username");
  UA_String ref_password = UA_STRING_STATIC("password");

  auto username = (UA_String *)NEX_Argument_getValue(input, 0);
  auto password = (UA_String *)NEX_Argument_getValue(input, 1);

  //  null values
  if (!username || !password) {
    result = UA_MTREQUESTRESULT_INVALIDARGUMENT;
    retval = NEX_Argument_setValue(output, 0, &result, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
    return retval;
  }

  //  non-null values
  if (!UA_String_equal(username, &ref_username) || !UA_String_equal(password, &ref_password)) {
    result = UA_MTREQUESTRESULT_INVALIDARGUMENT;
    retval = NEX_Argument_setValue(output, 0, &result, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
    return retval;
  }

  retval = NEX_Argument_setValue(output, 0, &result, &UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_MTREQUESTRESULT]);
  return retval;
}

}


