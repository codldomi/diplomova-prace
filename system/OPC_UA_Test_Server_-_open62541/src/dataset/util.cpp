
#include <test_server/dataset/util.h>

#include <nex_macros.h>
#include <stdexcept>
#include <open62541/types_generated_handling.h>

namespace test_server::dataset {

/**
 * Encodes the selected value into OPC UA BinaryEncoding.
 * @param value The selected value.
 * @param data_type Value's data type.
 * @return Vector of bytes.
 */
std::vector<uint8_t> encode(const void *value, const UA_DataType *data_type) {

  UA_StatusCode retval = UA_STATUSCODE_GOOD;
  std::vector<uint8_t> result;

  UA_ByteString encoded;
  UA_ByteString_init(&encoded);

  retval = UA_encodeBinary(value, data_type, &encoded);
  NEX_CHECK_GOTO(retval, exit)

  for (size_t i = 0; i < encoded.length; i++) {
    UA_Byte byte = encoded.data[i];
    result.push_back(byte);
  }

exit:
  UA_ByteString_clear(&encoded);

  if (UA_StatusCode_isBad(retval)) {
    throw std::invalid_argument("Encoding ended with bad status code.");
  }

  return result;
}

std::string BytesToString(const std::vector<uint8_t> &bytes) {

  std::string result = "Vector(";

  for (int i = 0; i < bytes.size(); i++) {
    result += std::to_string(bytes[i]);

    if (i != bytes.size() - 1) {
      result += ", ";
    }
  }

  return result + ")";
}

std::vector<BinaryRecord> EncodeBinary(const std::vector<Record> &records) {

  auto result = std::vector<BinaryRecord>();

  std::vector<uint8_t> bytes;
  const void *value;
  BinaryRecord binary_record;

  for (auto &record : records) {
    binary_record = BinaryRecord(record.Name());

    for (int index = 0; index < record.Size(); index++) {
      value = record.At(index);
      bytes = encode(value, record.DataType());
      binary_record.AddEncodedValue(bytes);
    }

    result.push_back(binary_record);
  }

  return result;
}

std::string GenerateScalaVariables(const std::vector<BinaryRecord> &records) {

  std::vector<uint8_t> bytes;
  std::string result;

  for (const auto &record : records) {

    if (record.EncodedValues().empty()) {
      result += "val " + record.Name() + ": Vector[Vector[UaByte]] = Vector.empty\n\n";
      continue;
    }

    if (record.EncodedValues().size() == 1) {
      result += "val " + record.Name() + ": Vector[Vector[UaByte]] = (" + BytesToString(record.EncodedValues().front()) + ")\n\n";
      continue;
    }

    result += "val " + record.Name() + ": Vector[Vector[UaByte]] =\n  Vector(\n";

    for (int i = 0; i < record.EncodedValues().size(); i++) {

      bytes = record.EncodedValues()[i];
      result += "    " + BytesToString(bytes);

      if (i != record.EncodedValues().size() - 1) {
        result += ",\n";
      }
    }

    result += ")\n\n";
  }

  return result;
}

}