
#include <test_server/dataset/record.h>

#include <stdexcept>

namespace test_server::dataset {

Record::Record(const UA_DataType *data_type, const void *data, int size, std::string name) :
    data_type_(data_type),
    size_(size),
    data_(data),
    name_(std::move(name)) {}

const void *Record::Data() const {
  return data_;
}

int Record::Size() const {
  return size_;
}

const UA_DataType *Record::DataType() const {
  return data_type_;
}

std::string Record::Name() const {
  return name_;
}

const void *Record::At(int index) const {

  if (index < 0 || index >= size_) {
    throw std::out_of_range("An index is out of range.");
  }

  auto bytes = (const UA_Byte *)data_;
  int byte_index = index * data_type_->memSize;

  return &bytes[byte_index];
}

}

