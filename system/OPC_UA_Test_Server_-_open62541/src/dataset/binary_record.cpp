
#include <test_server/dataset/binary_record.h>

namespace test_server::dataset {

std::string BinaryRecord::Name() const {
  return name_;
}

std::vector<std::vector<uint8_t>> BinaryRecord::EncodedValues() const {
  return encoded_values_;
}

BinaryRecord &BinaryRecord::AddEncodedValue(const std::vector<uint8_t> &value) {
  encoded_values_.push_back(value);
  return *this;
}

BinaryRecord::BinaryRecord(std::string name, std::vector<std::vector<uint8_t>> encoded_values):
name_(std::move(name)),
encoded_values_(std::move(encoded_values))
{}

bool BinaryRecord::operator==(const BinaryRecord &other) const {

  if (name_ != other.name_) {
    return false;
  }

  return encoded_values_ == other.encoded_values_;
}

}