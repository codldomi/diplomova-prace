
#ifndef OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_UTIL_H_
#define OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_UTIL_H_

#include <vector>
#include "test_server/dataset/record.h"
#include "test_server/dataset/binary_record.h"

namespace test_server::dataset {

  std::vector<BinaryRecord> EncodeBinary(const std::vector<Record> &records);

  std::string GenerateScalaVariables(const std::vector<BinaryRecord> &records);

}

#endif //OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_UTIL_H_
