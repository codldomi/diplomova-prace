
#ifndef OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_RECORD_H_
#define OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_RECORD_H_

#include <string>
#include <vector>
#include <open62541/types.h>
#include <open62541/types_generated.h>

#include "test_server/dataset/value.h"

namespace test_server::dataset {

class Record {

 private:

  const UA_DataType *data_type_;
  const void *data_;
  const int size_;
  const std::string name_;

 public:

  Record(const UA_DataType *data_type, const void *data, int size, std::string name);

  const void *Data() const;

  int Size() const;

  const void * At(int index) const;

  const UA_DataType *DataType() const;

  std::string Name() const;

};

const std::vector<Record> kRecords = {
    Record(&UA_TYPES[UA_TYPES_BOOLEAN], kBooleans, 3, "Booleans"),
    Record(&UA_TYPES[UA_TYPES_BYTE], kBytes, 3, "Bytes"),
    Record(&UA_TYPES[UA_TYPES_SBYTE], kSBytes, 3, "SBytes"),
    Record(&UA_TYPES[UA_TYPES_INT16], kInt16s, 3, "Int16s"),
    Record(&UA_TYPES[UA_TYPES_UINT16], kUInt16s, 3, "UInt16s"),
    Record(&UA_TYPES[UA_TYPES_INT32], kInt32s, 3, "Int32s"),
    Record(&UA_TYPES[UA_TYPES_UINT32], kUInt32s, 3, "UInt32s"),
    Record(&UA_TYPES[UA_TYPES_INT64], kInt64s, 3, "Int64s"),
    Record(&UA_TYPES[UA_TYPES_UINT64], kUInt64s, 3, "UInt64s"),
    Record(&UA_TYPES[UA_TYPES_FLOAT], kFloats, 3, "Floats"),
    Record(&UA_TYPES[UA_TYPES_DOUBLE], kDoubles, 3, "Doubles"),
    Record(&UA_TYPES[UA_TYPES_DATETIME], kDateTimes, 2, "DateTimes"),
    Record(&UA_TYPES[UA_TYPES_BYTESTRING], kByteStrings, 3, "ByteStrings"),
    Record(&UA_TYPES[UA_TYPES_STRING], kStrings, 3, "Strings"),
    Record(&UA_TYPES[UA_TYPES_GUID], kGuids, 3, "Guids"),
    Record(&UA_TYPES[UA_TYPES_XMLELEMENT], kXmlElements, 3, "XmlElements"),
    Record(&UA_TYPES[UA_TYPES_QUALIFIEDNAME], kQualifiedNames, 3, "QualifiedNames"),
    Record(&UA_TYPES[UA_TYPES_LOCALIZEDTEXT], kLocalizedTexts, 3, "LocalizedTexts"),
    Record(&UA_TYPES[UA_TYPES_NODEID], kNodeIds, 6, "NodeIds"),
    Record(&UA_TYPES[UA_TYPES_DIAGNOSTICINFO], kDiagnosticInfos, 3, "DiagnosticInfos"),
    Record(&UA_TYPES[UA_TYPES_EXPANDEDNODEID], kExpandedNodeIds, 3, "ExpandedNodeIds"),
    Record(&UA_TYPES[UA_TYPES_EXTENSIONOBJECT], kExtensionObjects, 3, "ExtensionObjects"),
    Record(&UA_TYPES[UA_TYPES_VARIANT], kVariants, 3, "Variants"),
    Record(&UA_TYPES[UA_TYPES_DATAVALUE], kDataValues, 3, "DataValues")
};

}

#endif //OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_RECORD_H_
