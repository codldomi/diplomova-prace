
#ifndef OPC_UA_TEST_SERVER_H
#define OPC_UA_TEST_SERVER_H

#include <open62541/types.h>

#define STR UA_STRING_STATIC

namespace test_server::dataset {

// helper variables
namespace {

//  Byte in OPC UA is unsigned 8-bit integer (0..255 inclusive)
uint8_t kBytes1[] = {115, 116, 114, 105, 110, 103, '\0'};
uint8_t kBytes2[] = {104, 101, 108, 108, 111, 87, 111, 114, 108, 100, '\0'};
uint8_t kBytes3[] = {'\0'};
uint8_t kBytes4[] = {0, 0, 0, 0, '\0'};
char lowest[] = "lowest";
char secondHighest[] = "secondHighest";
char maximum[] = "maximum";
char text[] = "text";
char locale[] = "locale";
char empty[] = "";
char string[] = "string";
char kGuid1[] = "C496578A-0DFE-4B8F-870A-745238C6AEAE";
char kGuid2[] = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
char kGuid3[] = "00000000-0000-0000-0000-000000000000";
UA_Int32 varValue1 = -2147483648;
UA_Double varValue2[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
UA_UInt32 dimensions[] = {3, 3};

UA_DiagnosticInfo kInnerDiagnosticInfo ={
    .hasSymbolicId = false,
    .hasNamespaceUri = true,
    .hasLocalizedText = true,
    .hasLocale = false,
    .hasAdditionalInfo = false,
    .hasInnerStatusCode = false,
    .hasInnerDiagnosticInfo = false,
    .symbolicId = 0,
    .namespaceUri = 2147483647,
    .localizedText = -1,
    .locale = 0,
    .additionalInfo = UA_STRING_NULL,
    .innerStatusCode = UA_STATUSCODE_GOOD,
    .innerDiagnosticInfo = nullptr,
};

}

const UA_Boolean kBooleans[] = {true, false};

const UA_Byte kBytes[] = {0, 10, 255};

const UA_SByte kSBytes[] = {-128, 0, 127};

const UA_UInt16 kUInt16s[] = {0, 65'535, 32'767};

const UA_Int16 kInt16s[] = {-32'768, 0, 32'767};

const UA_UInt32 kUInt32s[] = {0, 4'294'967'295, 2'147'483'647};

const UA_Int32 kInt32s[] = {-2'147'483'648, 0, 2'147'483'647};

const UA_UInt64 kUInt64s[] = {0, 18'446'744'073'709'551'615ul, 9'223'372'036'854'775'807ul};

const UA_Int64 kInt64s[] = {-9'223'372'036'854'775'807, 0, 9'223'372'036'854'775'807};

const UA_Float kFloats[] = {1.17549e-38f, 0, 3.40282e38f};

const UA_Double kDoubles[] = {2.3e-308, 0, 1.7e308};

const UA_String kStrings[] = {STR("string"), STR(""), STR("helloWorld")};

const UA_DateTime kDateTimes[] = {0, 9'223'372'036'850'000'000};

const UA_ByteString kByteStrings[] = {
    UA_BYTESTRING((char *)kBytes2),
    UA_BYTESTRING((char *)kBytes3),
    UA_BYTESTRING(nullptr)
};

const UA_Guid kGuids[] = {
    UA_GUID(kGuid1),
    UA_GUID(kGuid2),
    UA_GUID(kGuid3)
};

const UA_XmlElement kXmlElements[] = {STR("string"), STR(""), STR("helloWorld")};

const UA_QualifiedName kQualifiedNames[] = {
    UA_QUALIFIEDNAME(0, lowest),
    UA_QUALIFIEDNAME(32767, secondHighest),
    UA_QUALIFIEDNAME(65535, maximum)
};

const UA_LocalizedText kLocalizedTexts[] = {
    UA_LOCALIZEDTEXT(locale, lowest),
    UA_LOCALIZEDTEXT(text, secondHighest),
    UA_LOCALIZEDTEXT(empty, empty)
};

const UA_NodeId kNodeIds[] = {
    UA_NODEID_NUMERIC(0, 255),
    UA_NODEID_NUMERIC(255, 65535),
    UA_NODEID_NUMERIC(32767, 2147483647),
    UA_NODEID_STRING(32767, string),
    UA_NODEID_GUID(65407, UA_GUID(kGuid1)),
    UA_NODEID_BYTESTRING(65535, (char *)kBytes1)
};

const UA_DiagnosticInfo kDiagnosticInfos[] = {
    {
      .hasSymbolicId = false,
      .hasNamespaceUri = false,
      .hasLocalizedText = false,
      .hasLocale = false,
      .hasAdditionalInfo = false,
      .hasInnerStatusCode = false,
      .hasInnerDiagnosticInfo = false,
      .symbolicId = 0,
      .namespaceUri = 0,
      .localizedText = 0,
      .locale = 0,
      .additionalInfo = UA_STRING_NULL,
      .innerStatusCode = UA_STATUSCODE_GOOD,
      .innerDiagnosticInfo = nullptr,
    },
    {
      .hasSymbolicId = false,
      .hasNamespaceUri = true,
      .hasLocalizedText = false,
      .hasLocale = true,
      .hasAdditionalInfo = true,
      .hasInnerStatusCode = true,
      .hasInnerDiagnosticInfo = false,
      .symbolicId = 0,
      .namespaceUri = 150,
      .localizedText = 0,
      .locale = 1000,
      .additionalInfo = STR(empty),
      .innerStatusCode = UA_STATUSCODE_GOOD,
      .innerDiagnosticInfo = nullptr,
    },
    {
      .hasSymbolicId = false,
      .hasNamespaceUri = false,
      .hasLocalizedText = true,
      .hasLocale = false,
      .hasAdditionalInfo = true,
      .hasInnerStatusCode = false,
      .hasInnerDiagnosticInfo = true,
      .symbolicId = 0,
      .namespaceUri = 0,
      .localizedText = 20,
      .locale = 0,
      .additionalInfo = STR("string"),
      .innerStatusCode = UA_STATUSCODE_GOOD,
      .innerDiagnosticInfo = &kInnerDiagnosticInfo,
      }
};

const UA_ExpandedNodeId kExpandedNodeIds[] = {
    {
      .nodeId = UA_NODEID_NUMERIC(0, 4294967295L),
      .namespaceUri = STR("http://opcfoundation.org/UA/"),
      .serverIndex = 0,
    },
    {
      .nodeId = UA_NODEID_STRING(0, string),
      .namespaceUri = STR("http://opcfoundation.org/UA/"),
      .serverIndex = 4294967295L
    },
    {
      .nodeId = UA_NODEID_NUMERIC(0, 65535),
      .namespaceUri = STR("http://opcfoundation.org/UA/"),
      .serverIndex = 2147483647
    }
};

const UA_ExtensionObject kExtensionObjects[] = {
    {
      .encoding = UA_EXTENSIONOBJECT_ENCODED_BYTESTRING,
      .content = {
          .encoded = {
              .typeId = UA_TYPES[UA_TYPES_INT32].typeId,
              .body = {
                  .length = 4,
                  .data = (UA_Byte*)kBytes4
              }
          }
      }
    },
    {
      .encoding = UA_EXTENSIONOBJECT_ENCODED_NOBODY,
      .content = {
          .encoded = {
              .typeId = UA_NODEID_NULL,
              .body = UA_BYTESTRING_NULL
          }
      }
    },
    {
      .encoding = UA_EXTENSIONOBJECT_ENCODED_XML,
      .content = {
          .encoded = {
              .typeId = UA_TYPES[UA_TYPES_STRING].typeId,
              .body = STR("element")
          }
      }
    }
};

const UA_Variant kVariants[] = {
    {
      .type = &UA_TYPES[UA_TYPES_INT32],
      .storageType = UA_VARIANT_DATA_NODELETE,
      .arrayLength = 0,
      .data = &varValue1,
      .arrayDimensionsSize = 0,
      .arrayDimensions = nullptr
    },
    {
      .type = nullptr,
      .storageType = UA_VARIANT_DATA_NODELETE,
      .arrayLength = 0,
      .data = nullptr,
      .arrayDimensionsSize = 0,
      .arrayDimensions = nullptr
    },
    {
      .type = &UA_TYPES[UA_TYPES_DOUBLE],
      .storageType = UA_VARIANT_DATA_NODELETE,
      .arrayLength = 9,
      .data = &varValue2,
      .arrayDimensionsSize = 2,
      .arrayDimensions = dimensions
    }
};

const UA_DataValue kDataValues[] = {
    {
      .value = {
          .type = nullptr,
          .storageType = UA_VARIANT_DATA_NODELETE,
          .arrayLength = 0,
          .data = nullptr,
          .arrayDimensionsSize = 0,
          .arrayDimensions = nullptr
      },
      .sourceTimestamp = 9223372036850000000L,
      .serverTimestamp = 0,
      .sourcePicoseconds = 6500,
      .serverPicoseconds = 0,
      .status = UA_STATUSCODE_GOOD,
      .hasValue = false,
      .hasStatus = true,
      .hasSourceTimestamp = true,
      .hasServerTimestamp = false,
      .hasSourcePicoseconds = true,
      .hasServerPicoseconds = false
    },
    {
      .value = {
          .type = &UA_TYPES[UA_TYPES_INT32],
          .storageType = UA_VARIANT_DATA_NODELETE,
          .arrayLength = 0,
          .data = &varValue1,
          .arrayDimensionsSize = 0,
          .arrayDimensions = nullptr
      },
      .sourceTimestamp = 0,
      .serverTimestamp = 9223372036850000000L,
      .sourcePicoseconds = 0,
      .serverPicoseconds = 1000,
      .status = UA_STATUSCODE_GOOD,
      .hasValue = true,
      .hasStatus = true,
      .hasSourceTimestamp = true,
      .hasServerTimestamp = true,
      .hasSourcePicoseconds = false,
      .hasServerPicoseconds = true
    },
    {
      .value = {
          .type = nullptr,
          .storageType = UA_VARIANT_DATA_NODELETE,
          .arrayLength = 0,
          .data = nullptr,
          .arrayDimensionsSize = 0,
          .arrayDimensions = nullptr
      },
      .sourceTimestamp = 0,
      .serverTimestamp = 0,
      .sourcePicoseconds = 0,
      .serverPicoseconds = 0,
      .status = UA_STATUSCODE_GOOD,
      .hasValue = false,
      .hasStatus = false,
      .hasSourceTimestamp = false,
      .hasServerTimestamp = false,
      .hasSourcePicoseconds = false,
      .hasServerPicoseconds = false
    }
};

}

#endif //OPC_UA_TEST_SERVER_H
