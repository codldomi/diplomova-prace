
#ifndef OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_BINARY_RECORD_H_
#define OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_BINARY_RECORD_H_

#include <string>
#include <vector>

namespace test_server::dataset {

class BinaryRecord {

 private:

  std::string name_;
  std::vector<std::vector<uint8_t>> encoded_values_;

 public:

  BinaryRecord() = default;

  BinaryRecord(const BinaryRecord &other) = default;

  explicit BinaryRecord(std::string name, std::vector<std::vector<uint8_t>> encoded_values = {});

  std::string Name() const;

  std::vector<std::vector<uint8_t>> EncodedValues() const;

  BinaryRecord &AddEncodedValue(const std::vector<uint8_t> &value);

  bool operator==(const BinaryRecord &other) const;

};

}

#endif //OPC_UA_TEST_SERVER_INCLUDE_TEST_SERVER_DATASET_BINARY_RECORD_H_
