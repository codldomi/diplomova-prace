
#ifndef TEST_SERVER_IEC_SERVER_H
#define TEST_SERVER_IEC_SERVER_H

#include <open62541/server.h>


namespace test_server {

class IecServer {

 private:

  UA_Server *server_;
  UA_Boolean is_running_;

  static const UA_DataTypeArray kCustomTypes;

  static UA_StatusCode ChangePassCallback(UA_Server *server, const UA_NodeId *session_id, void *session_context,
                                          const UA_NodeId *method_id, void *method_context, const UA_NodeId *object_id,
                                          void *object_context, size_t input_size, const UA_Variant *input,
                                          size_t output_size,UA_Variant *output);


  explicit IecServer(UA_Server *server);

  static void InitValues(UA_Server *server);

  static void InitNamespaces(UA_Server *server);

  static void InitMethods(UA_Server *server);

  static void InitRepeatedJobs(UA_Server *server);

  /**
   * Integer sequence from 0 to 4 inclusive. The value is being changed every 500 ms.
   */
  static void IntegerSequence(UA_Server *server, void *data);

  static void InitHistoryData(UA_Server *server);

 public:

  ~IecServer();

  void Run();

  static IecServer Create();

};

}


#endif //TEST_SERVER_IEC_SERVER_H
