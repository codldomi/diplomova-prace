/* Generated from ModemTec.PDM1.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py * on host martin-dev4 by user martin at 2021-11-29 10:24:00 */

#ifndef TYPES_MTPDM1_GENERATED_H_
#define TYPES_MTPDM1_GENERATED_H_

#ifdef UA_ENABLE_AMALGAMATION
#include "open62541.h"
#else
#include <open62541/types.h>
#include <open62541/types_generated.h>

#endif

_UA_BEGIN_DECLS


/**
 * Every type is assigned an index in an array containing the type descriptions.
 * These descriptions are used during type handling (copying, deletion,
 * binary encoding, ...). */
#define UA_TYPES_MTPDM1_COUNT 4
extern UA_EXPORT const UA_DataType UA_TYPES_MTPDM1[UA_TYPES_MTPDM1_COUNT];

/**
 * MtGridFreq
 * ^^^^^^^^^^
 */
typedef enum {
    UA_MTGRIDFREQ_50HZ = 0,
    UA_MTGRIDFREQ_60HZ = 1,
    __UA_MTGRIDFREQ_FORCE32BIT = 0x7fffffff
} UA_MtGridFreq;
UA_STATIC_ASSERT(sizeof(UA_MtGridFreq) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_MTPDM1_MTGRIDFREQ 0

/**
 * MtLogLevel
 * ^^^^^^^^^^
 */
typedef enum {
    UA_MTLOGLEVEL_EMERGENCY = 0,
    UA_MTLOGLEVEL_ALERT = 1,
    UA_MTLOGLEVEL_CRITICAL = 2,
    UA_MTLOGLEVEL_ERROR = 3,
    UA_MTLOGLEVEL_WARNING = 4,
    UA_MTLOGLEVEL_NOTICE = 5,
    UA_MTLOGLEVEL_INFO = 6,
    UA_MTLOGLEVEL_DEBUG = 7,
    __UA_MTLOGLEVEL_FORCE32BIT = 0x7fffffff
} UA_MtLogLevel;
UA_STATIC_ASSERT(sizeof(UA_MtLogLevel) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_MTPDM1_MTLOGLEVEL 1

/**
 * MtMeasureStatus
 * ^^^^^^^^^^^^^^^
 */
typedef enum {
    UA_MTMEASURESTATUS_OK = 0,
    UA_MTMEASURESTATUS_BUSY = 1,
    UA_MTMEASURESTATUS_STORING = 2,
    UA_MTMEASURESTATUS_ERROR = 3,
    UA_MTMEASURESTATUS_TIMEOUT = 4,
    UA_MTMEASURESTATUS_SIGNALLOW = 5,
    UA_MTMEASURESTATUS_SIGNALOVERFOW = 6,
    UA_MTMEASURESTATUS_UNKNOWN = 7,
    __UA_MTMEASURESTATUS_FORCE32BIT = 0x7fffffff
} UA_MtMeasureStatus;
UA_STATIC_ASSERT(sizeof(UA_MtMeasureStatus) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_MTPDM1_MTMEASURESTATUS 2

/**
 * MtRequestResult
 * ^^^^^^^^^^^^^^^
 */
typedef enum {
    UA_MTREQUESTRESULT_OK = 0,
    UA_MTREQUESTRESULT_ERROR = 1,
    UA_MTREQUESTRESULT_BUSY = 2,
    UA_MTREQUESTRESULT_INVALIDARGUMENT = 3,
    UA_MTREQUESTRESULT_TIMEOUT = 4,
    UA_MTREQUESTRESULT_ACCESS = 5,
    __UA_MTREQUESTRESULT_FORCE32BIT = 0x7fffffff
} UA_MtRequestResult;
UA_STATIC_ASSERT(sizeof(UA_MtRequestResult) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_MTPDM1_MTREQUESTRESULT 3


_UA_END_DECLS

#endif /* TYPES_MTPDM1_GENERATED_H_ */
