/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#ifndef NS_IEC7_4_REDUCED_H_
#define NS_IEC7_4_REDUCED_H_


#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include <open62541/server.h>
#endif



_UA_BEGIN_DECLS

extern UA_StatusCode ns_iec7_4_reduced(UA_Server *server);

_UA_END_DECLS

#endif /* NS_IEC7_4_REDUCED_H_ */
