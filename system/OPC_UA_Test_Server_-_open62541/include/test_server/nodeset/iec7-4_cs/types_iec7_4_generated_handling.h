/* Generated from Opc.Ua.IEC61850-7-4.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:23:37 */

#ifndef TYPES_IEC7_4_GENERATED_HANDLING_H_
#define TYPES_IEC7_4_GENERATED_HANDLING_H_

#include "types_iec7_4_generated.h"

_UA_BEGIN_DECLS

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wmissing-field-initializers"
# pragma GCC diagnostic ignored "-Wmissing-braces"
#endif


/* BehaviourModeKind */
static UA_INLINE void
UA_BehaviourModeKind_init(UA_BehaviourModeKind *p) {
    memset(p, 0, sizeof(UA_BehaviourModeKind));
}

static UA_INLINE UA_BehaviourModeKind *
UA_BehaviourModeKind_new(void) {
    return (UA_BehaviourModeKind*)UA_new(&UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
}

static UA_INLINE UA_StatusCode
UA_BehaviourModeKind_copy(const UA_BehaviourModeKind *src, UA_BehaviourModeKind *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
}

UA_DEPRECATED static UA_INLINE void
UA_BehaviourModeKind_deleteMembers(UA_BehaviourModeKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
}

static UA_INLINE void
UA_BehaviourModeKind_clear(UA_BehaviourModeKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
}

static UA_INLINE void
UA_BehaviourModeKind_delete(UA_BehaviourModeKind *p) {
    UA_delete(p, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_BEHAVIOURMODEKIND]);
}

/* HealthKind */
static UA_INLINE void
UA_HealthKind_init(UA_HealthKind *p) {
    memset(p, 0, sizeof(UA_HealthKind));
}

static UA_INLINE UA_HealthKind *
UA_HealthKind_new(void) {
    return (UA_HealthKind*)UA_new(&UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND]);
}

static UA_INLINE UA_StatusCode
UA_HealthKind_copy(const UA_HealthKind *src, UA_HealthKind *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND]);
}

UA_DEPRECATED static UA_INLINE void
UA_HealthKind_deleteMembers(UA_HealthKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND]);
}

static UA_INLINE void
UA_HealthKind_clear(UA_HealthKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND]);
}

static UA_INLINE void
UA_HealthKind_delete(UA_HealthKind *p) {
    UA_delete(p, &UA_TYPES_IEC7_4[UA_TYPES_IEC7_4_HEALTHKIND]);
}

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic pop
#endif

_UA_END_DECLS

#endif /* TYPES_IEC7_4_GENERATED_HANDLING_H_ */
