/* Generated from Opc.Ua.IEC61850-7-3.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py * on host martin-dev4 by user martin at 2021-11-29 10:23:27 */

#ifndef TYPES_IEC7_3_GENERATED_H_
#define TYPES_IEC7_3_GENERATED_H_

#ifdef UA_ENABLE_AMALGAMATION
#include "open62541.h"
#else
#include <open62541/types.h>
#include <open62541/types_generated.h>

#endif

_UA_BEGIN_DECLS


/**
 * Every type is assigned an index in an array containing the type descriptions.
 * These descriptions are used during type handling (copying, deletion,
 * binary encoding, ...). */
#define UA_TYPES_IEC7_3_COUNT 8
extern UA_EXPORT const UA_DataType UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_COUNT];

/**
 * ValidityKind
 * ^^^^^^^^^^^^
 */
typedef enum {
    UA_VALIDITYKIND_GOOD = 0,
    UA_VALIDITYKIND_INVALID = 1,
    UA_VALIDITYKIND_RESERVED = 2,
    UA_VALIDITYKIND_QUESTIONABLE = 3,
    __UA_VALIDITYKIND_FORCE32BIT = 0x7fffffff
} UA_ValidityKind;
UA_STATIC_ASSERT(sizeof(UA_ValidityKind) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_IEC7_3_VALIDITYKIND 0

/**
 * SourceKind
 * ^^^^^^^^^^
 */
typedef enum {
    UA_SOURCEKIND_PROCESS = 0,
    UA_SOURCEKIND_SUBSTITUTED = 1,
    __UA_SOURCEKIND_FORCE32BIT = 0x7fffffff
} UA_SourceKind;
UA_STATIC_ASSERT(sizeof(UA_SourceKind) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_IEC7_3_SOURCEKIND 1

/**
 * SIUnitKind
 * ^^^^^^^^^^
 */
typedef enum {
    UA_SIUNITKIND_ = 1,
    UA_SIUNITKIND_METER = 2,
    UA_SIUNITKIND_KILOGRAM = 3,
    UA_SIUNITKIND_SECOND = 4,
    UA_SIUNITKIND_AMPER = 5,
    UA_SIUNITKIND_KELVIN = 6,
    UA_SIUNITKIND_MOLE = 7,
    UA_SIUNITKIND_CANDELA = 8,
    UA_SIUNITKIND_DEGREE = 9,
    UA_SIUNITKIND_RADIAN = 10,
    UA_SIUNITKIND_STERADIAN = 11,
    UA_SIUNITKIND_GRAY = 21,
    UA_SIUNITKIND_BECQUEREL = 22,
    UA_SIUNITKIND_DEGREE_OF_CELSIUS = 23,
    UA_SIUNITKIND_SIEVERT = 24,
    UA_SIUNITKIND_FARAD = 25,
    UA_SIUNITKIND_COULOMB = 26,
    UA_SIUNITKIND_SIEMENS = 27,
    UA_SIUNITKIND_HENRY = 28,
    UA_SIUNITKIND_VOLT = 29,
    UA_SIUNITKIND_OHM = 30,
    UA_SIUNITKIND_JOULE = 31,
    UA_SIUNITKIND_NEWTON = 32,
    UA_SIUNITKIND_HERTZ = 33,
    UA_SIUNITKIND_LUX = 34,
    UA_SIUNITKIND_LUMEN = 35,
    UA_SIUNITKIND_WEBER = 36,
    UA_SIUNITKIND_TESLA = 37,
    UA_SIUNITKIND_WATT = 38,
    UA_SIUNITKIND_PASCAL = 39,
    UA_SIUNITKIND_SQUARE_METRE = 41,
    UA_SIUNITKIND_CUBIC_METRE = 42,
    UA_SIUNITKIND_METER_PER_SECOND = 43,
    UA_SIUNITKIND_METRE_PER_SECOND_SQUARED = 44,
    UA_SIUNITKIND_CUBIC_METRE_PER_SECOND = 45,
    UA_SIUNITKIND_METRE_PER_CUBIC_METRE = 46,
    UA_SIUNITKIND_M = 47,
    UA_SIUNITKIND_KILOGRAM_PER_CUBIC_METRE = 48,
    UA_SIUNITKIND_SQUARE_METRE_PER_SECOND = 49,
    UA_SIUNITKIND_WATT_PER_METRE_K = 50,
    UA_SIUNITKIND_JOULE_PER_KELVIN = 51,
    UA_SIUNITKIND_PPM = 52,
    UA_SIUNITKIND_ONE_PER_SECOND = 53,
    UA_SIUNITKIND_RADIAN_PER_SECOND = 54,
    UA_SIUNITKIND_WATT_PER_SQUARE_METRE = 55,
    UA_SIUNITKIND_JOUL_PER_SQUARE_METRE = 56,
    UA_SIUNITKIND_SIEMENS_PER_METRE = 57,
    UA_SIUNITKIND_KELVIN_PER_SECOND = 58,
    UA_SIUNITKIND_PASCAL_PER_SECOND = 59,
    UA_SIUNITKIND_JOUL_PER_KILOGRAM_K = 60,
    UA_SIUNITKIND_VA = 61,
    UA_SIUNITKIND_WATTS = 62,
    UA_SIUNITKIND_VAR = 63,
    UA_SIUNITKIND_PHI = 64,
    UA_SIUNITKIND_COS_PHI = 65,
    UA_SIUNITKIND_VS = 66,
    UA_SIUNITKIND_V2 = 67,
    UA_SIUNITKIND_AS = 68,
    UA_SIUNITKIND_A2 = 69,
    UA_SIUNITKIND_A2T = 70,
    UA_SIUNITKIND_VAH = 71,
    UA_SIUNITKIND_WH = 72,
    UA_SIUNITKIND_VARH = 73,
    UA_SIUNITKIND_VOLT_PER_HERTZ = 74,
    UA_SIUNITKIND_HERTZ_PER_SECOND = 75,
    UA_SIUNITKIND_CHAR = 76,
    UA_SIUNITKIND_CHAR_PER_SECOND = 77,
    UA_SIUNITKIND_KGM2 = 78,
    UA_SIUNITKIND_DB = 79,
    UA_SIUNITKIND_J_PER_WH = 80,
    UA_SIUNITKIND_WATT_PER_SECOND = 81,
    UA_SIUNITKIND_L_PER_S = 82,
    UA_SIUNITKIND_DBM = 83,
    UA_SIUNITKIND_HOUR = 84,
    UA_SIUNITKIND_MINUTE = 85,
    __UA_SIUNITKIND_FORCE32BIT = 0x7fffffff
} UA_SIUnitKind;
UA_STATIC_ASSERT(sizeof(UA_SIUnitKind) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_IEC7_3_SIUNITKIND 2

/**
 * MultiplierKind
 * ^^^^^^^^^^^^^^
 */
typedef enum {
    UA_MULTIPLIERKIND_YOCTO = -24,
    UA_MULTIPLIERKIND_ZEPTO = -21,
    UA_MULTIPLIERKIND_ATTO = -18,
    UA_MULTIPLIERKIND_FEMTO = -15,
    UA_MULTIPLIERKIND_PICO = -12,
    UA_MULTIPLIERKIND_NANO = -9,
    UA_MULTIPLIERKIND_MICRO = -6,
    UA_MULTIPLIERKIND_MILI = -3,
    UA_MULTIPLIERKIND_CENTI = -2,
    UA_MULTIPLIERKIND_DECI = -1,
    UA_MULTIPLIERKIND_ = 0,
    UA_MULTIPLIERKIND_DECA = 1,
    UA_MULTIPLIERKIND_HECTO = 2,
    UA_MULTIPLIERKIND_KILO = 3,
    UA_MULTIPLIERKIND_MEGA = 6,
    UA_MULTIPLIERKIND_GIGA = 9,
    UA_MULTIPLIERKIND_TERA = 12,
    UA_MULTIPLIERKIND_PETA = 15,
    UA_MULTIPLIERKIND_EXA = 18,
    UA_MULTIPLIERKIND_ZETTA = 21,
    UA_MULTIPLIERKIND_YOTTA = 24,
    __UA_MULTIPLIERKIND_FORCE32BIT = 0x7fffffff
} UA_MultiplierKind;
UA_STATIC_ASSERT(sizeof(UA_MultiplierKind) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_TYPES_IEC7_3_MULTIPLIERKIND 3

/**
 * DetailQual
 * ^^^^^^^^^^
 */
typedef struct {
    UA_Boolean overflow;
    UA_Boolean outOfRange;
    UA_Boolean badReference;
    UA_Boolean oscillatory;
    UA_Boolean failure;
    UA_Boolean oldData;
    UA_Boolean inconsistent;
    UA_Boolean inaccurate;
} UA_DetailQual;

#define UA_TYPES_IEC7_3_DETAILQUAL 4

/**
 * Quality
 * ^^^^^^^
 */
typedef struct {
    UA_ValidityKind validity;
    UA_DetailQual detailQual;
    UA_SourceKind source;
    UA_Boolean test;
    UA_Boolean operatorBlocked;
} UA_Quality;

#define UA_TYPES_IEC7_3_QUALITY 5

/**
 * AnalogueValue
 * ^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 i;
    UA_Float f;
} UA_AnalogueValue;

#define UA_TYPES_IEC7_3_ANALOGUEVALUE 6

/**
 * Unit
 * ^^^^
 */
typedef struct {
    UA_SIUnitKind sIUnit;
    UA_MultiplierKind multiplier;
} UA_Unit;

#define UA_TYPES_IEC7_3_UNIT 7


_UA_END_DECLS

#endif /* TYPES_IEC7_3_GENERATED_H_ */
