/* Generated from Opc.Ua.IEC61850-7-3.NodeSet2.bsd with script /home/martin/Develop/MtDev/tools/opcua_reduced_iec_nodeset_generator/open62541/tools/generate_datatypes.py
 * on host martin-dev4 by user martin at 2021-11-29 10:23:27 */

#ifndef TYPES_IEC7_3_GENERATED_HANDLING_H_
#define TYPES_IEC7_3_GENERATED_HANDLING_H_

#include "types_iec7_3_generated.h"

_UA_BEGIN_DECLS

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wmissing-field-initializers"
# pragma GCC diagnostic ignored "-Wmissing-braces"
#endif


/* ValidityKind */
static UA_INLINE void
UA_ValidityKind_init(UA_ValidityKind *p) {
    memset(p, 0, sizeof(UA_ValidityKind));
}

static UA_INLINE UA_ValidityKind *
UA_ValidityKind_new(void) {
    return (UA_ValidityKind*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND]);
}

static UA_INLINE UA_StatusCode
UA_ValidityKind_copy(const UA_ValidityKind *src, UA_ValidityKind *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND]);
}

UA_DEPRECATED static UA_INLINE void
UA_ValidityKind_deleteMembers(UA_ValidityKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND]);
}

static UA_INLINE void
UA_ValidityKind_clear(UA_ValidityKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND]);
}

static UA_INLINE void
UA_ValidityKind_delete(UA_ValidityKind *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_VALIDITYKIND]);
}

/* SourceKind */
static UA_INLINE void
UA_SourceKind_init(UA_SourceKind *p) {
    memset(p, 0, sizeof(UA_SourceKind));
}

static UA_INLINE UA_SourceKind *
UA_SourceKind_new(void) {
    return (UA_SourceKind*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND]);
}

static UA_INLINE UA_StatusCode
UA_SourceKind_copy(const UA_SourceKind *src, UA_SourceKind *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND]);
}

UA_DEPRECATED static UA_INLINE void
UA_SourceKind_deleteMembers(UA_SourceKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND]);
}

static UA_INLINE void
UA_SourceKind_clear(UA_SourceKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND]);
}

static UA_INLINE void
UA_SourceKind_delete(UA_SourceKind *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SOURCEKIND]);
}

/* SIUnitKind */
static UA_INLINE void
UA_SIUnitKind_init(UA_SIUnitKind *p) {
    memset(p, 0, sizeof(UA_SIUnitKind));
}

static UA_INLINE UA_SIUnitKind *
UA_SIUnitKind_new(void) {
    return (UA_SIUnitKind*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND]);
}

static UA_INLINE UA_StatusCode
UA_SIUnitKind_copy(const UA_SIUnitKind *src, UA_SIUnitKind *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND]);
}

UA_DEPRECATED static UA_INLINE void
UA_SIUnitKind_deleteMembers(UA_SIUnitKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND]);
}

static UA_INLINE void
UA_SIUnitKind_clear(UA_SIUnitKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND]);
}

static UA_INLINE void
UA_SIUnitKind_delete(UA_SIUnitKind *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_SIUNITKIND]);
}

/* MultiplierKind */
static UA_INLINE void
UA_MultiplierKind_init(UA_MultiplierKind *p) {
    memset(p, 0, sizeof(UA_MultiplierKind));
}

static UA_INLINE UA_MultiplierKind *
UA_MultiplierKind_new(void) {
    return (UA_MultiplierKind*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND]);
}

static UA_INLINE UA_StatusCode
UA_MultiplierKind_copy(const UA_MultiplierKind *src, UA_MultiplierKind *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND]);
}

UA_DEPRECATED static UA_INLINE void
UA_MultiplierKind_deleteMembers(UA_MultiplierKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND]);
}

static UA_INLINE void
UA_MultiplierKind_clear(UA_MultiplierKind *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND]);
}

static UA_INLINE void
UA_MultiplierKind_delete(UA_MultiplierKind *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_MULTIPLIERKIND]);
}

/* DetailQual */
static UA_INLINE void
UA_DetailQual_init(UA_DetailQual *p) {
    memset(p, 0, sizeof(UA_DetailQual));
}

static UA_INLINE UA_DetailQual *
UA_DetailQual_new(void) {
    return (UA_DetailQual*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL]);
}

static UA_INLINE UA_StatusCode
UA_DetailQual_copy(const UA_DetailQual *src, UA_DetailQual *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL]);
}

UA_DEPRECATED static UA_INLINE void
UA_DetailQual_deleteMembers(UA_DetailQual *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL]);
}

static UA_INLINE void
UA_DetailQual_clear(UA_DetailQual *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL]);
}

static UA_INLINE void
UA_DetailQual_delete(UA_DetailQual *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_DETAILQUAL]);
}

/* Quality */
static UA_INLINE void
UA_Quality_init(UA_Quality *p) {
    memset(p, 0, sizeof(UA_Quality));
}

static UA_INLINE UA_Quality *
UA_Quality_new(void) {
    return (UA_Quality*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
}

static UA_INLINE UA_StatusCode
UA_Quality_copy(const UA_Quality *src, UA_Quality *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
}

UA_DEPRECATED static UA_INLINE void
UA_Quality_deleteMembers(UA_Quality *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
}

static UA_INLINE void
UA_Quality_clear(UA_Quality *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
}

static UA_INLINE void
UA_Quality_delete(UA_Quality *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_QUALITY]);
}

/* AnalogueValue */
static UA_INLINE void
UA_AnalogueValue_init(UA_AnalogueValue *p) {
    memset(p, 0, sizeof(UA_AnalogueValue));
}

static UA_INLINE UA_AnalogueValue *
UA_AnalogueValue_new(void) {
    return (UA_AnalogueValue*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE]);
}

static UA_INLINE UA_StatusCode
UA_AnalogueValue_copy(const UA_AnalogueValue *src, UA_AnalogueValue *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE]);
}

UA_DEPRECATED static UA_INLINE void
UA_AnalogueValue_deleteMembers(UA_AnalogueValue *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE]);
}

static UA_INLINE void
UA_AnalogueValue_clear(UA_AnalogueValue *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE]);
}

static UA_INLINE void
UA_AnalogueValue_delete(UA_AnalogueValue *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_ANALOGUEVALUE]);
}

/* Unit */
static UA_INLINE void
UA_Unit_init(UA_Unit *p) {
    memset(p, 0, sizeof(UA_Unit));
}

static UA_INLINE UA_Unit *
UA_Unit_new(void) {
    return (UA_Unit*)UA_new(&UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT]);
}

static UA_INLINE UA_StatusCode
UA_Unit_copy(const UA_Unit *src, UA_Unit *dst) {
    return UA_copy(src, dst, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT]);
}

UA_DEPRECATED static UA_INLINE void
UA_Unit_deleteMembers(UA_Unit *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT]);
}

static UA_INLINE void
UA_Unit_clear(UA_Unit *p) {
    UA_clear(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT]);
}

static UA_INLINE void
UA_Unit_delete(UA_Unit *p) {
    UA_delete(p, &UA_TYPES_IEC7_3[UA_TYPES_IEC7_3_UNIT]);
}

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic pop
#endif

_UA_END_DECLS

#endif /* TYPES_IEC7_3_GENERATED_HANDLING_H_ */
