package cz.modemtec.adapter.api.util

import cz.modemtec.adapter.api.data.{CertData, ReadResult}

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import play.api.libs.json.*

import java.security.cert.{CertificateFactory, X509Certificate}
import java.sql.Timestamp
import java.time.Instant


class AdapterApiFormatTest extends AnyFunSuite with Matchers with AdapterApiFormat:

  val timestamp: Timestamp = Timestamp.from(Instant.ofEpochSecond(1680637259))

  test("ReadResult Format - Good ones"):
    val objects =
      Vector(
        ReadResult(
          value = Json.obj("value" -> JsNumber(3), "name" -> JsString("test")),
          time  = None),
        ReadResult(
          value = JsNull,
          time  = None),
        ReadResult(
          value = Json.obj("value" -> JsNumber(3), "name" -> JsString("test")),
          time  = Some(timestamp)),
        ReadResult(
          value = JsNull,
          time  = Some(timestamp)))

    val json =
      Vector(
        """{ "value" : { "value" : 3, "name" : "test" } }""",
        """{ }""",
        """{ "value" : { "value" : 3, "name" : "test" }, "time" : "2023-04-04T19:40:59Z" }""",
        """{ "time" : "2023-04-04T19:40:59Z" }""")

    val expectedObjects = objects
    val expectedJson =
      Vector(
        Json.obj(
          "value" -> Json.obj("value" -> JsNumber(3), "name" -> JsString("test"))),
        Json.obj(),
        Json.obj(
          "value" -> Json.obj("value" -> JsNumber(3), "name" -> JsString("test")),
          "time"  -> JsString("2023-04-04T19:40:59Z")),
        Json.obj(
          "time"  -> JsString("2023-04-04T19:40:59Z")))

    val resultJson = objects.map(Json.toJson[ReadResult])
    val resultObjects = json.map(Json.parse).map(js => Json.fromJson[ReadResult](js).get)

    resultJson shouldBe expectedJson
    resultObjects shouldBe expectedObjects
  

  test("CertData encode to JSON, decode from JSON and compare."):
    val factory = CertificateFactory.getInstance("X.509")
    val stream = getClass.getClassLoader.getResourceAsStream("certs/milo_demo_server.crt")
    val certificate = factory.generateCertificate(stream).asInstanceOf[X509Certificate]

    val data = CertData(guid = "SomeGUID", cert = certificate)

    val encoded = Json.toJson(data)
    val decoded = encoded.as[CertData]

    decoded.guid shouldBe data.guid

    val expected = certificate.getEncoded
    val result = decoded.cert.getEncoded

    result should equal (expected)
  
