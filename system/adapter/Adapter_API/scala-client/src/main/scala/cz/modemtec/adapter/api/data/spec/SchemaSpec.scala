package cz.modemtec.adapter.api.data.spec

import play.api.libs.json.JsValue

/** Object describes specification of data type.
  *
  * @param uri          Unique URI (uniqueness is guaranteed within the resource specification).
  * @param name         Data type name.
  * @param schema       JSON Schema for the data type. Schema's URI is the same as attribute uri.
  * @param dependencies Set of used URIs by this data type.
  */
case class SchemaSpec(
  uri: String,
  name: String,
  schema: JsValue,
  dependencies: Set[String]
)