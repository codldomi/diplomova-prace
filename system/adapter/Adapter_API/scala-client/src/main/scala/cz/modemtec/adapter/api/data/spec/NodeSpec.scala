package cz.modemtec.adapter.api.data.spec

/** Common attributes for Node specification.
  */
sealed trait NodeSpec:
  def path: String
  def description: String
  def name: String

/** Specification for Variable node.
  *
  * @param path        Absolute path to the node.
  * @param description Description of the node.
  * @param name        Name of the node. The last item in path.
  * @param schema      URI of value JSON schema.
  */
case class VariableSpec(
  path: String,
  description: String,
  name: String,
  schema: String
) extends NodeSpec

/** Specification for Method node.
  *
  * @param path        Absolute path to the node.
  * @param description Description of the node.
  * @param name        Name of the node. The last item in path.
  * @param input       URI of input JSON schema.
  * @param output      URI of output JSON schema.
  */
case class MethodSpec(
  path: String,
  description: String,
  name: String,
  input: Option[String],
  output: Option[String]
) extends NodeSpec
