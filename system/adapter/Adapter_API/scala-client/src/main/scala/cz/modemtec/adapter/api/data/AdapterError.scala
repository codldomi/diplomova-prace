package cz.modemtec.adapter.api.data

import play.api.http.Status

/** Object contains information about an error.
  *
  * @param uri     Error's URI.
  * @param name    Error's name.
  * @param code    Error's code, typically HTTP status.
  * @param message Detailed description of the error.
  */
case class AdapterError(
  uri: String,
  name: String,
  code: Int,
  message: String
) extends Exception(s"AdapterError $uri with message: $message"):

  def asText: String = s"{ uri = $uri, name = $name, message = $message, code = $code }"


/** Factory for general AdapterError and predefined errors.
  */
object AdapterError:

  /** Certificate with the given GUID is not found.
    */
  def CertNotFound(message: String): AdapterError =
    AdapterError(
      name    = "CertNotFound",
      code    = Status.NOT_FOUND,
      message = message)

  /** Resource with the given GUID is not found.
    */
  def ResourceNotFound(message: String): AdapterError =
    AdapterError(
      name    = "ResourceNotFound",
      code    = Status.NOT_FOUND,
      message = message)

  /** Node of the given resource and with the given path is not found.
    */
  def NodeNotFound(message: String): AdapterError =
    AdapterError(
      name    = "NodeNotFound",
      code    = Status.NOT_FOUND,
      message = message)

  /** Given resource can't be connected.
    */
  def ResourceNotConnected(message: String): AdapterError =
    AdapterError(
      name    = "ResourceNotConnected",
      code    = Status.SERVICE_UNAVAILABLE,
      message = message)

  /** Executed operation is not supported, e.g. reading value from the method node.
    */
  def UnsupportedOperation(message: String): AdapterError =
    AdapterError(
      name    = "UnsupportedOperation",
      code    = Status.METHOD_NOT_ALLOWED,
      message = message)

  /** Error had occurred during execution operation.
    */
  def ErrorDuringOperation(message: String): AdapterError =
    AdapterError(
      name    = "ErrorDuringOperation",
      code    = Status.INTERNAL_SERVER_ERROR,
      message = message)

  /** Bad format or value of the given input data for the given operation.
    */
  def BadInputData(message: String): AdapterError =
    AdapterError(
      name    = "BadInputData",
      code    = Status.BAD_REQUEST,
      message = message)

  /** Unknown error state, e.g. conversion from exception to AdapterError is not implemented, so the message is description
    * of the exception.
    */
  def Unknown(message: String): AdapterError =
    AdapterError(
      name    = "Unknown",
      code    = Status.INTERNAL_SERVER_ERROR,
      message = message)

  /** Short-cut creating AdapterError with ModemTec based URI.
    */
  def apply(name: String, code: Int, message: String): AdapterError =
    AdapterError(
      uri     = s"http://modemtec.cz/adapter-api/errors/$name",
      name    = name,
      code    = code,
      message = message)
