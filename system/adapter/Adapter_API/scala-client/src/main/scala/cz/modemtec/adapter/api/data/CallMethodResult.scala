package cz.modemtec.adapter.api.data

import play.api.libs.json.JsValue

/** Object contains successful result of CallMethod operation - the method output.
  *
  * @param output Output method arguments.
  */
case class CallMethodResult(
  output: JsValue
)
