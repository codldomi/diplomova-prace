package cz.modemtec.adapter.api.data

import play.api.libs.json.JsValue

/** Object contains input data for Write operation - data for writing the value.
  *
  * @param value New value.
  */
case class WriteRequest(
  value: JsValue
)
