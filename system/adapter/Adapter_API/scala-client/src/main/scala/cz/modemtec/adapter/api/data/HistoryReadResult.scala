package cz.modemtec.adapter.api.data

/** Object contains successful result of HistoryRead operation.
  *
  * @param values Read values.
  */
case class HistoryReadResult(
  values: Vector[ReadResult]
)

object HistoryReadResult:
  def empty: HistoryReadResult = HistoryReadResult(values = Vector.empty)