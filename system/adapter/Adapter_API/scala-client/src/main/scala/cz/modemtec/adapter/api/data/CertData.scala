package cz.modemtec.adapter.api.data

import java.security.cert.X509Certificate

/** Object contains data for PKI certificate.
  *
  * @param guid Globally unique identifier.
  * @param cert Certificate.
  */
case class CertData(
  guid: String,
  cert: X509Certificate
)
  
