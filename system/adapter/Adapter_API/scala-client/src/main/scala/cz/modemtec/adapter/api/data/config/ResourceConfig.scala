package cz.modemtec.adapter.api.data.config

/** Object contains necessary data to create connection with a resource.
  *
  * @param guid    Resource's identifier.
  * @param connstr Connection string to resource - it can be whatever, e.g. URL for OPC UA.
  * @param policy  Chosen policy - message encryption.
  * @param auth    User authentication. If None, the user is anonymous.
  */
case class ResourceConfig(
  guid: String,
  connstr: String,
  policy: Option[SecurityPolicy],
  auth: Option[AuthConfig]
)
