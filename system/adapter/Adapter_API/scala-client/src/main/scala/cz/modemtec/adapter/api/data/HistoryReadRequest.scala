package cz.modemtec.adapter.api.data

import java.sql.Timestamp

/** Object contains input data for operation HistoryRead - a filter to data selection from history.
  *
  * @param from Time, where to start reading history. Inclusive.
  * @param to   Time, where to end reading history. Inclusive.
  */
case class HistoryReadRequest(
  from: Timestamp,
  to: Timestamp
)
