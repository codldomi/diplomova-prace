package cz.modemtec.adapter.api.util

import cz.modemtec.adapter.api.data.*
import cz.modemtec.adapter.api.data.config.*
import cz.modemtec.adapter.api.data.spec.*
import cz.modemtec.adapter.api.util.PkiUtil.{createX509Cert, asBase64}

import play.api.libs.json.*

import java.io.ByteArrayInputStream
import java.security.cert.{CertificateFactory, X509Certificate}
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.{Date, TimeZone, Base64}
import java.util.Spliterator.OfInt

import scala.util.{Failure, Success, Try}


object AdapterApiFormat extends AdapterApiFormat

/** Formats for JSON serialization.
  */
trait AdapterApiFormat:

  given formatAdapterError: Format[AdapterError] = Json.format[AdapterError]

  given formatTimestamp: Format[Timestamp] = new Format[Timestamp]:

    override def writes(timestamp: Timestamp): JsValue =
      val date = new Date(timestamp.getTime)

      val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
      df.setTimeZone(TimeZone.getTimeZone("UTC"))

      JsString(df.format(date))

    override def reads(json: JsValue): JsResult[Timestamp] =
      val result = Try:
        val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        df.setTimeZone(TimeZone.getTimeZone("UTC"))

        val dateString = json.as[String]
        val date = df.parse(dateString)

        new Timestamp(date.getTime)

      result match
        case Failure(exception) => JsError(exception.getMessage)
        case Success(value) => JsSuccess(value)


  //  Security

  given formatSecurityPolicy: Format[SecurityPolicy] = new Format[SecurityPolicy]:

    override def writes(policy: SecurityPolicy): JsValue = policy match
      case SecurityPolicy.None                => JsString("None")
      case SecurityPolicy.Basic256            => JsString("Basic256")
      case SecurityPolicy.Basic256Sha256      => JsString("Basic256Sha256")
      case SecurityPolicy.Aes128Sha256RsaOaep => JsString("Aes128Sha256RsaOaep")
      case SecurityPolicy.Aes256Sha256RsaPss  => JsString("Aes256Sha256RsaPss")

    override def reads(json: JsValue): JsResult[SecurityPolicy] = json match
      case JsString(value) if value == "None"                => JsSuccess(SecurityPolicy.None)
      case JsString(value) if value == "Basic256"            => JsSuccess(SecurityPolicy.Basic256)
      case JsString(value) if value == "Basic256Sha256"      => JsSuccess(SecurityPolicy.Basic256Sha256)
      case JsString(value) if value == "Aes128Sha256RsaOaep" => JsSuccess(SecurityPolicy.Aes128Sha256RsaOaep)
      case JsString(value) if value == "Aes256Sha256RsaPss"  => JsSuccess(SecurityPolicy.Aes256Sha256RsaPss)
      case _ => JsError("Unknown string for SecurityPolicy JSON representation.")


  given formatAuthConfig: Format[AuthConfig] = Json.format[AuthConfig]

  //  PKI

  given formatCertData: Format[CertData] = new Format[CertData]:

    override def reads(json: JsValue): JsResult[CertData] = json match
      case JsObject(fields) =>
        val result = for
          guid <- fields
            .get("guid")
            .flatMap: json =>
              json
              .validate[String]
              .asOpt
          cert <- fields
            .get("cert")
            .flatMap:
              case JsString(base64) =>
                val res = Try:
                  createX509Cert(base64)
                res.toOption
              case _ => None
        yield CertData(guid, cert)

        result match
          case Some(value) => JsSuccess(value)
          case None        => JsError("CertData has bad JSON encoding.")

      case _ => JsError("CertData should be JSON object.")

    override def writes(obj: CertData): JsValue =
      Json.obj("guid" -> obj.guid, "cert" -> obj.cert.asBase64)


  //  Read Operation

  given formatReadResult: Format[ReadResult] = new Format[ReadResult]:

    override def writes(obj: ReadResult): JsValue =
      val time = obj.time match
        case Some(timestamp) => Json.obj("time" -> timestamp)
        case None => Json.obj()

      val value = obj.value match
        case JsNull => Json.obj()
        case some   => Json.obj("value" -> some)

      time ++ value

    override def reads(json: JsValue): JsResult[ReadResult] = json match
      case JsObject(fields) =>
        val empty = ReadResult(value = JsNull, time = None)
        val result = fields.foldLeft(empty):
          case (result, ("value", jsValue      )) => result.copy(value = jsValue)
          case (result, ("time",  str: JsString)) => Json.fromJson[Timestamp](str) match
            case JsSuccess(timestamp, _) => result.copy(time = Some(timestamp))
            case _ => result.copy(time = null)
          case (result, _                       ) => result

        if result.time == null then
          JsError("Wrong format of Timestamp during deserialization.")
        else
          JsSuccess(result)

      case _ => JsError("ReadResult should be JSON object.")


  //  Write Operation

  given formatWriteResult: Format[WriteResult] = new Format[WriteResult]:

    override def writes(o: WriteResult): JsValue = o.value match
      case JsNull => Json.obj()
      case _      => Json.obj("value" -> o.value)

    override def reads(json: JsValue): JsResult[WriteResult] = json match
      case JsObject(fields) =>
        fields
          .find: (name, _) =>
            name == "value"
          .map: field =>
            JsSuccess(WriteResult(field._2))
          .getOrElse(JsSuccess(WriteResult(JsNull)))

      case _ => JsError("WriteResult should be JSON object.")


  given formatWriteRequest: Format[WriteRequest] = new Format[WriteRequest]:

    override def writes(o: WriteRequest): JsValue = o.value match
      case JsNull => Json.obj()
      case _      => Json.obj("value" -> o.value)

    override def reads(json: JsValue): JsResult[WriteRequest] = json match
      case JsObject(fields) =>
        fields
          .find: (name, _) =>
            name == "value"
          .map: field =>
            JsSuccess(WriteRequest(field._2))
          .getOrElse(JsSuccess(WriteRequest(JsNull)))

      case _ => JsError("WriteRequest should be JSON object.")


  //  Call Method Operation

  given formatCallMethodResult: Format[CallMethodResult] = new Format[CallMethodResult]:

    override def writes(o: CallMethodResult): JsValue = o.output match
      case JsNull => Json.obj()
      case _      => Json.obj("output" -> o.output)

    override def reads(json: JsValue): JsResult[CallMethodResult] = json match
      case JsObject(fields) =>
        fields
          .find: (name, _) =>
            name == "output"
          .map: field =>
            JsSuccess(CallMethodResult(field._2))
          .getOrElse(JsSuccess(CallMethodResult(JsNull)))

      case _ => JsError("CallMethodResult should be JSON object.")


  given formatCallMethodRequest: Format[CallMethodRequest] = new Format[CallMethodRequest]:

    override def writes(o: CallMethodRequest): JsValue = o.input match
      case JsNull => Json.obj()
      case _      => Json.obj("input" -> o.input)

    override def reads(json: JsValue): JsResult[CallMethodRequest] = json match
      case JsObject(fields) =>
        fields
          .find: (name, _) =>
            name == "input"
          .map: field =>
            JsSuccess(CallMethodRequest(field._2))
          .getOrElse(JsSuccess(CallMethodRequest(JsNull)))

      case _ => JsError("CallMethodRequest should be JSON object.")


  //  Adapter Information

  given formatAdapterInfo: Format[AdapterInfo] = Json.format[AdapterInfo]

  //  Resource Configuration

  given formatResourceConfig: Format[ResourceConfig] = Json.format[ResourceConfig]

  //  History Operation

  given formatHistoryReadRequest: Format[HistoryReadRequest] = Json.format[HistoryReadRequest]

  given formatHistoryReadResult: Format[HistoryReadResult] = Json.format[HistoryReadResult]

  //  Resource Specification

  given formatSchemaSpec: Format[SchemaSpec] = Json.format[SchemaSpec]

  given formatVariableSpec: Format[VariableSpec] = Json.format[VariableSpec]

  given formatMethodSpec: Format[MethodSpec] = Json.format[MethodSpec]

  given formatNodeSpec: Format[NodeSpec] = new Format[NodeSpec]:

    override def reads(json: JsValue): JsResult[NodeSpec] = (json \ "$type").as[String] match
      case "VARIABLE" => Json.fromJson[VariableSpec](json)
      case "METHOD"   => Json.fromJson[MethodSpec](json)
      case _ => JsError("Unsupported NodeSpec type.")

    override def writes(o: NodeSpec): JsValue = o match
      case n: VariableSpec =>
        val typeId = Json.obj("$type" -> "VARIABLE")
        val core = Json.toJson(n).asInstanceOf[JsObject]
        typeId ++ core

      case n: MethodSpec =>
        val typeId = Json.obj("$type" -> "METHOD")
        val core = Json.toJson(n).asInstanceOf[JsObject]
        typeId ++ core


  private def createMap[A](values: Iterable[JsResult[(String, A)]]): JsResult[Map[String, A]] =
    val error = values.find:
      case _: JsError => true
      case _          => false

    if error.isDefined then
      error.get.asInstanceOf[JsError]
    else
      JsSuccess(values.map(_.get).toMap)


  given formatMapNodeSpec: Format[Map[String, NodeSpec]] = new Format[Map[String, NodeSpec]]:

    override def reads(json: JsValue): JsResult[Map[String, NodeSpec]] = json match
      case JsArray(values) =>
        val results = values.map: value =>
          Json
            .fromJson[NodeSpec](value)
            .map: spec =>
              (spec.path, spec)
        createMap(results)
      case _ => JsError("Value is mapped as an array.")

    override def writes(o: Map[String, NodeSpec]): JsValue =
      val values = o.values.map(value => Json.toJson(value)).toVector
      JsArray(values)


  given formatMapSchemaSpec: Format[Map[String, SchemaSpec]] = new Format[Map[String, SchemaSpec]]:

    override def reads(json: JsValue): JsResult[Map[String, SchemaSpec]] = json match
      case JsArray(values) =>
        val results = values.map: value =>
          Json
            .fromJson[SchemaSpec](value)
            .map: spec =>
              (spec.uri, spec)
        createMap(results)

      case _ => JsError("Value is mapped as an array.")

    override def writes(o: Map[String, SchemaSpec]): JsValue =
      val values = o.values.map(value => Json.toJson(value)).toVector
      JsArray(values)


  given formatResourceSpec: Format[ResourceSpec] = Json.format[ResourceSpec]

