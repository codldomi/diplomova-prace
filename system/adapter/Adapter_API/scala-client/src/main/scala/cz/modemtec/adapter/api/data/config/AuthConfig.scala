package cz.modemtec.adapter.api.data.config

/** Object contains necessary data to authenticate the user.
  *
  * @param username User's name or alias.
  * @param password Password.
  */
case class AuthConfig(
  username: String,
  password: String
)
