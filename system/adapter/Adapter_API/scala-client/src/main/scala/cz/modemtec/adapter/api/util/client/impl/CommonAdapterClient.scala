package cz.modemtec.adapter.api.util.client.impl

import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{JsonFraming, Source}
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.stream.scaladsl.*
import org.apache.pekko.http.scaladsl.model.ws.*

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.data.spec.ResourceSpec
import cz.modemtec.adapter.api.data.{AdapterError, AdapterInfo, CallMethodRequest, CallMethodResult, CertData, HistoryReadRequest, ReadResult, WriteRequest, WriteResult}
import cz.modemtec.adapter.api.util.{AdapterApiFormat, MonadTransformer}
import cz.modemtec.adapter.api.util.MonadTransformer.*
import cz.modemtec.adapter.api.util.client.AdapterClient

import play.api.http.Status
import play.api.libs.json.*
import play.api.libs.ws.*

import scala.util.{Try, Success, Failure}
import scala.concurrent.{ExecutionContext, Future, Promise}


class CommonAdapterClient(
  val webservice: WSClient,
  val port: Int,
  val address: String
)(using val executionContext: ExecutionContext, val system: ActorSystem) extends AdapterClient with AdapterApiFormat:

  protected val httpPrefix: String = s"http://$address:$port"
  protected val webSocketPrefix: String = s"ws://$address:$port"


  private def parseError(response: WSRequest#Response): AdapterError =
    val result = for
      json   <- Try(response.json)
      parsed <- json.validate(formatAdapterError) match
        case JsSuccess(value, _) => Success(value)
        case JsError(_) => Failure(new IllegalArgumentException("AdapterError has bad format."))
    yield parsed

    result.getOrElse(AdapterError.Unknown(s"Unknown adapter error. Original message: ${response.body}"))


  protected def parseUnitResult(response: WSRequest#Response): Either[AdapterError, Unit] =
    if Status.isSuccessful(response.status) then
      Right(())
    else
      Left(parseError(response))


  protected def parseResult[A](response: WSRequest#Response)(using fjs: Format[A]): Either[AdapterError, A] =
    if (Status.isSuccessful(response.status))
      Right(response.json.as[A])
    else
      Left(parseError(response))


  override def readResourceSpecSignature(guid: String): FutureEither[AdapterError, String] =
    webservice
      .url(httpPrefix + "/resource/" + guid + "/spec/signature")
      .get()
      .map: response =>
        parseResult[String](response)
      .asFutureEither


  override def subscribe[Mat](guid: String, path: String, sink: Sink[ReadResult, Mat]): FutureEither[AdapterError, Promise[Option[Message]]] =
    val deserialize = Flow.fromFunction[Message, ReadResult]: message =>
      val text = message.asTextMessage.getStrictText
      val json = Json.parse(text)
      Json.fromJson[ReadResult](json).get

    val deserializedSink = deserialize.to(sink)
    val flow = Flow.fromSinkAndSourceMat(deserializedSink, Source.maybe[Message])(Keep.right)

    val (upgradeResponse, promise): (Future[WebSocketUpgradeResponse], Promise[Option[Message]]) = Http().singleWebSocketRequest(
      WebSocketRequest(webSocketPrefix + "/node/subscribe/" + guid + path),
      flow)

    upgradeResponse
      .flatMap: upgrade =>
        if upgrade.response.status == StatusCodes.SwitchingProtocols then
          Future.successful(Right(promise))
        else
          val entity = upgrade.response.entity
          val message = Unmarshal(entity).to[String]
          message.map: string =>
            val error = Json.parse(string).validate(formatAdapterError) match
              case JsSuccess(value, _) => value
              case JsError(_) => AdapterError.Unknown("Unknown adapter error.")
            Left(error)
      .asFutureEither


  override def readValue(guid: String, path: String): FutureEither[AdapterError, ReadResult] =
    webservice
      .url(httpPrefix + "/node/" + guid + path)
      .get()
      .map: response =>
        parseResult[ReadResult](response)
      .asFutureEither


  override def writeValue(guid: String, path: String, request: WriteRequest): FutureEither[AdapterError, WriteResult] =
    val json = Json.toJson(request)
    webservice
      .url(httpPrefix + "/node/" + guid + path)
      .put(json)
      .map: response =>
        parseResult[WriteResult](response)
      .asFutureEither


  override def callMethod(guid: String, path: String, request: CallMethodRequest): FutureEither[AdapterError, CallMethodResult] =
    val json = Json.toJson(request)
    webservice
      .url(httpPrefix + "/node/" + guid + path)
      .post(json)
      .map: response =>
        parseResult[CallMethodResult](response)
      .asFutureEither


  override def historyRead(guid: String, path: String, request: HistoryReadRequest): FutureEither[AdapterError, Source[ReadResult, _]] =
    val json = Json.toJson(request)
    webservice
      .url(httpPrefix + "/node/history/" + guid + path)
      .withMethod("POST")
      .withBody(json)
      .stream()
      .map: response =>
        if Status.isSuccessful(response.status) then
          val source =
            response
              .bodyAsSource
              .via(JsonFraming.objectScanner(Int.MaxValue))
              .map: bytes =>
                val text = bytes.utf8String
                Json.parse(text).as[ReadResult]
          Right[AdapterError, Source[ReadResult, _]](source)
        else
          Left(parseError(response))
      .asFutureEither



  override def readResourceSpec(guid: String): FutureEither[AdapterError, ResourceSpec] =
    webservice
      .url(httpPrefix + "/resource/" + guid + "/spec")
      .get()
      .map: response =>
        parseResult[ResourceSpec](response)
      .asFutureEither


  override def createOrUpdateResource(request: ResourceConfig): FutureEither[AdapterError, ResourceConfig] =
    val json = Json.toJson(request)
    webservice
      .url(httpPrefix + "/resource/" + request.guid)
      .put(json)
      .map: response =>
        parseResult[ResourceConfig](response)
      .asFutureEither


  override def readResource(guid: String): MonadTransformer.FutureEither[AdapterError, ResourceConfig] =
    webservice
      .url(httpPrefix + "/resource/" + guid)
      .get()
      .map: response =>
        parseResult[ResourceConfig](response)
      .asFutureEither


  override def deleteResource(guid: String): FutureEither[AdapterError, Unit] =
    webservice
      .url(httpPrefix + "/resource/" + guid)
      .delete()
      .map(parseUnitResult)
      .asFutureEither


  override def readAllResource(): FutureEither[AdapterError, Vector[ResourceConfig]] =
    webservice
      .url(httpPrefix + "/resource/all")
      .get()
      .map: response =>
        parseResult[Vector[ResourceConfig]](response)
      .asFutureEither


  override def health(): Future[Boolean] =
    webservice
      .url(httpPrefix + "/about/health")
      .get()
      .map: response =>
        Status.isSuccessful(response.status)


  override def info(): FutureEither[AdapterError, AdapterInfo] =
    webservice
      .url(httpPrefix + "/about/info")
      .get()
      .map: response =>
        parseResult[AdapterInfo](response)
      .asFutureEither


  override def createOrUpdateTrustedCert(data: CertData): FutureEither[AdapterError, CertData] =
    val json = Json.toJson(data)
    webservice
      .url(httpPrefix + "/pki/trusted/" + data.guid)
      .put(json)
      .map: response =>
        parseResult[CertData](response)
      .asFutureEither


  override def readTrustedCert(guid: String): FutureEither[AdapterError, CertData] =
    webservice
      .url(httpPrefix + "/pki/trusted/" + guid)
      .get()
      .map: response =>
        parseResult[CertData](response)
      .asFutureEither


  override def deleteTrustedCert(guid: String): FutureEither[AdapterError, Unit] =
    webservice
      .url(httpPrefix + "/pki/trusted/" + guid)
      .delete()
      .map(parseUnitResult)
      .asFutureEither


  override def readTrustedCertGuids(): FutureEither[AdapterError, Vector[String]] =
    webservice
      .url(httpPrefix + "/pki/all/trusted/guid")
      .get()
      .map: response =>
        parseResult[Vector[String]](response)
      .asFutureEither

