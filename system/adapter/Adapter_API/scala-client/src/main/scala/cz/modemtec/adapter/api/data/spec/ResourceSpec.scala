package cz.modemtec.adapter.api.data.spec

/** Object contains specification of resource: data types schemas and nodes description.
  *
  * @param name        Specification name.
  * @param signature   Unique string for the given type of specification, which can be shared between more resources
  *                    (uniqueness is guaranteed only within the adapter).
  * @param description Specification description.
  * @param nodes       Description of nodes.
  * @param schemas     Description of used data types.
  */
case class ResourceSpec(
  name: String,
  signature: String,
  description: String,
  nodes: Map[String, NodeSpec],
  schemas: Map[String, SchemaSpec]
)
