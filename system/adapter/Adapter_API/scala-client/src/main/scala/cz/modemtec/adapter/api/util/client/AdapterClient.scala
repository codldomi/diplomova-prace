package cz.modemtec.adapter.api.util.client

import org.apache.pekko.http.scaladsl.model.ws.Message
import org.apache.pekko.stream.scaladsl.{Sink, Source}

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.data.spec.ResourceSpec
import cz.modemtec.adapter.api.data.{AdapterError, AdapterInfo, CallMethodRequest, CallMethodResult, CertData, HistoryReadRequest, ReadResult, WriteRequest, WriteResult}
import cz.modemtec.adapter.api.util.MonadTransformer.FutureEither

import scala.concurrent.{Future, Promise}

/** Client interface to common adapter.
  */
trait AdapterClient:

  /** Reads variable's value.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @return Read value.
    */
  def readValue(guid: String, path: String): FutureEither[AdapterError, ReadResult]

  /** Writes variable's value.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @param request Request data.
    * @return Written value.
    */
  def writeValue(guid: String, path: String, request: WriteRequest): FutureEither[AdapterError, WriteResult]

  /** Calls method with input.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @param request Method input.
    * @return Method output.
    */
  def callMethod(guid: String, path: String, request: CallMethodRequest): FutureEither[AdapterError, CallMethodResult]

  /** Reads variable's history values.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @param request Request data.
    * @return Stream of history values.
    */
  def historyRead(guid: String, path: String, request: HistoryReadRequest): FutureEither[AdapterError, Source[ReadResult, _]]

  /** Subscribes variable's value.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @param sink Stream's sink.
    * @tparam Mat Materialized type.
    * @return Promise used as cancellation switch - call {{{promise.success(None)}}} in order to cancel subscription.
    */
  def subscribe[Mat](guid: String, path: String, sink: Sink[ReadResult, Mat]): FutureEither[AdapterError, Promise[Option[Message]]]

  /** Reads specification for the resource.
    *
    * @param guid Resource identifier.
    * @return Resource's specification.
    */
  def readResourceSpec(guid: String): FutureEither[AdapterError, ResourceSpec]

  /** Reads only signature for resource's specification.
    *
    * @param guid Resource identifier.
    * @return Signature of resource's specification.
    */
  def readResourceSpecSignature(guid: String): FutureEither[AdapterError, String]

  /** Creates or modifies resource by its configuration.
    *
    * @param request Resource configuration.
    * @return Used configuration.
    */
  def createOrUpdateResource(request: ResourceConfig): FutureEither[AdapterError, ResourceConfig]

  /** Reads stored resource.
    *
    * @param guid Resource identifier.
    * @return Configuration.
    */
  def readResource(guid: String): FutureEither[AdapterError, ResourceConfig]

  /** Deletes resource.
    *
    * @param guid Resource identifier.
    */
  def deleteResource(guid: String): FutureEither[AdapterError, Unit]

  /** Reads all stored resources.
    *
    * @return Vector of ResourceConfigs.
    */
  def readAllResource(): FutureEither[AdapterError, Vector[ResourceConfig]]

  /** Creates or modifies trusted certificate.
    *
    * @param data Certificate with metadata.
    */
  def createOrUpdateTrustedCert(data: CertData): FutureEither[AdapterError, CertData]

  /** Reads trusted certificate by its GUID.
    *
    * @param guid Certificate identifier.
    * @return Certificate with metadata.
    */
  def readTrustedCert(guid: String): FutureEither[AdapterError, CertData]

  /** Deletes certificate from the trusted list.
    *
    * @param guid Certificate identifier.
    */
  def deleteTrustedCert(guid: String): FutureEither[AdapterError, Unit]

  /** Reads all GUIDs corresponding to trusted certificates.
    *
    * @return Vector of certificates GUIDs.
    */
  def readTrustedCertGuids(): FutureEither[AdapterError, Vector[String]]

  /** Checks whether the adapter is available.
    *
    * @return True, if adapter is available.
    */
  def health(): Future[Boolean]

  /** Reads info about the adapter.
    *
    * @return Adapter info.
    */
  def info(): FutureEither[AdapterError, AdapterInfo]

