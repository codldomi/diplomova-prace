package cz.modemtec.adapter.api.util

import java.io.ByteArrayInputStream
import java.security.cert.{CertificateFactory, X509Certificate}
import java.util.Base64


object PkiUtil extends PkiUtil


trait PkiUtil:

  extension (self: X509Certificate)
    def asBase64: String =
      val bytes = self.getEncoded
      Base64.getEncoder.encodeToString(bytes)
      
  def createX509Cert(base64: String): X509Certificate =
    val bytes = Base64.getDecoder.decode(base64)
    val certFactory = CertificateFactory.getInstance("X.509")
    val in = new ByteArrayInputStream(bytes)
    certFactory.generateCertificate(in).asInstanceOf[X509Certificate]    