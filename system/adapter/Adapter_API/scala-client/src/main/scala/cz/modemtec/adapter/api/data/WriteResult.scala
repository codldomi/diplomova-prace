package cz.modemtec.adapter.api.data

import play.api.libs.json.JsValue

/** Object contains successful result of Write operation - the written data.
  *
  * @param value Written value.
  */
case class WriteResult(
  value: JsValue
)
