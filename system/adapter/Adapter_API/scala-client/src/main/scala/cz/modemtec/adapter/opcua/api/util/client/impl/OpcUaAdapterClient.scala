package cz.modemtec.adapter.opcua.api.util.client.impl

import org.apache.pekko.actor.ActorSystem

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.util.MonadTransformer.*
import cz.modemtec.adapter.api.util.client.impl.CommonAdapterClient

import cz.modemtec.adapter.opcua.api.data.DeleteNamespaceRequest
import cz.modemtec.adapter.opcua.api.util.OpcUaApiFormat

import play.api.libs.ws.*
import play.api.libs.json.*

import scala.concurrent.ExecutionContext
import scala.xml.Node


class OpcUaAdapterClient(
  override val webservice: WSClient,
  override val port: Int,
  override val address: String
)(using override val executionContext: ExecutionContext, override val system: ActorSystem)
  extends CommonAdapterClient(webservice, port, address)
    with OpcUaApiFormat:

  def createOrUpdateNamespace(nodeset: Node): FutureEither[AdapterError, Unit] =
    webservice
      .url(httpPrefix + "/nodeset")
      .put(nodeset)
      .map(parseUnitResult)
      .asFutureEither

  def readNamespaces(): FutureEither[AdapterError, Vector[String]] =
    webservice
      .url(httpPrefix + "/nodeset/all")
      .get()
      .map: response =>
        parseResult[Vector[String]](response)
      .asFutureEither


  def deleteNamespace(uri: String): FutureEither[AdapterError, Unit] =
    val json = Json.toJson(DeleteNamespaceRequest(uri))

    webservice
      .url(httpPrefix + "/delete/nodeset")
      .post(json)
      .map(parseUnitResult)
      .asFutureEither
