package cz.modemtec.adapter.api.data

import play.api.libs.json.JsValue
import java.sql.Timestamp

/** Object contains data about value in time. If attribute time is None, the time of value creation is unknown.
  *
  * @param value Value in the given time.
  * @param time  The time.
  */
case class ReadResult(
  value: JsValue,
  time: Option[Timestamp]
)
