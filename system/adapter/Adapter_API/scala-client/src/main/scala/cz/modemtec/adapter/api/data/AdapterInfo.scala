package cz.modemtec.adapter.api.data

/** Object contains general information about the adapter. It's useful for adapter registration or getting the
  * transformed protocol.
  *
  * @param protocol        String used for protocol part in URL, e.g. opc.tcp, http.
  * @param name            Name of the adapter, e.g. OPC UA Adapter.
  * @param adapterVersion  Version of adapter, e.g. 0.2.0. It hasn't to be the same as apiVersion attribute.
  * @param apiVersion      Version of implemented Adapter API, e.g. 0.1.0.
  */
case class AdapterInfo(
  protocol: String,
  name: String,
  adapterVersion: String,
  apiVersion: String
)
