package cz.modemtec.adapter.opcua.api.data


case class DeleteNamespaceRequest(uri: String)
