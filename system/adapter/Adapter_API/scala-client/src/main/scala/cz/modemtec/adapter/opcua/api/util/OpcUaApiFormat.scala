package cz.modemtec.adapter.opcua.api.util

import cz.modemtec.adapter.opcua.api.data.DeleteNamespaceRequest

import play.api.libs.json.*


object OpcUaApiFormat extends OpcUaApiFormat

/** Formats for JSON serialization.
  */
trait OpcUaApiFormat:

  given formatDeleteNamespaceReq: Format[DeleteNamespaceRequest] = Json.format[DeleteNamespaceRequest]
