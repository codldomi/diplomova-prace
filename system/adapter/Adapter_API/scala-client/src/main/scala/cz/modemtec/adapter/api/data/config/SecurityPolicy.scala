package cz.modemtec.adapter.api.data.config

/** Object refers to enumeration type. It represents chosen encryption.
  */
sealed trait SecurityPolicy


object SecurityPolicy:
  case object None                extends SecurityPolicy
  case object Basic256            extends SecurityPolicy
  case object Basic256Sha256      extends SecurityPolicy
  case object Aes128Sha256RsaOaep extends SecurityPolicy
  case object Aes256Sha256RsaPss  extends SecurityPolicy
