package cz.modemtec.adapter.api.util

import cats.data.EitherT

import scala.concurrent.{ExecutionContext, Future}
import scala.util.chaining.scalaUtilChainingOps


object MonadTransformer extends MonadTransformer


trait MonadTransformer:

  /** Monad transformer for `Future[Either[L, R]]` from the Cats library.
    *
    * @tparam L The type of the value on the left (typically some error).
    * @tparam R The type of the value on the right (typically some value).
    */
  type FutureEither[L, R] = EitherT[Future, L, R]

  /** Contains methods transforming `Future[Either[L, R]]` to `FutureEither[L, R]` transformer.
    */
  extension [L, R](self: Future[Either[L, R]])

    /** Converts `Future[Either[L, R]]` to `FutureEither[L, R]` transformer.
      *
      * @return The resulting `Future[Either[L, R]]`.
      */
    def asFutureEither: FutureEither[L, R] = EitherT[Future, L, R](self)

  /** Contains additional operations to `FutureEither[L, R]` transformer.
    */
  extension [L, R](self: FutureEither[L, R])

    /** Calls function f(Either[L, R]) without any effect on this instance. Useful e.g. for logging.
      *
      * @return The resulting `Future[Either[L, R]]`.
      */
    def aside(f: Either[L, R] => Unit)(using executor: ExecutionContext): FutureEither[L, R] =
      self.transform: value =>
        f(value)
        value


  object FutureEither:

    /** Creates `Future[Either[L, Unit]]`, where the right value is Unit.
      *
      * @tparam L The type of the value on the left (typically some error).
      * @return
      */
    def unit[L]: FutureEither[L, Unit] = Future.successful(Right(())).asFutureEither

    /** Creates `Future[Either[L, R]]` with the right value.
      *
      * @param value The right value.
      * @tparam L The type of the value on the left (typically some error).
      * @tparam R The type of the value on the right (typically some value).
      * @return
      */
    def right[L, R](value: R): FutureEither[L, R] = Future.successful(Right(value)).asFutureEither

    /** Creates `Future[Either[L, R]]` with the left value.
      *
      * @param value The left value.
      * @tparam L The type of the value on the left (typically some error).
      * @tparam R The type of the value on the right (typically some value).
      * @return
      */
    def left[L, R](value: L): FutureEither[L, R] = Future.successful(Left(value)).asFutureEither

    /** Reduces sequence of `Future[Either[L, R]]` to the one `Future[Either[L, Seq[R]]]`.
      *
      * @param items Sequence of `Future[Either[L, R]]`.
      * @param context Context for the Future.
      * @tparam L The type of the value on the left (typically some error).
      * @tparam R The type of the value on the right (typically some value).
      * @return The reduced `Future[Either[L, R]]`.
      */
    def sequence[L, R](items: Iterable[FutureEither[L, R]])(using context: ExecutionContext): FutureEither[L, Seq[R]] =
      items
        .map(_.value)
        .pipe: futures =>
          Future.sequence(futures)
        .map: eithers =>
          eithers.foldLeft(Right(Seq.empty): Either[L, Seq[R]]): (acc, either) =>
            for
              items <- acc
              item  <- either
            yield items :+ item
        .asFutureEither

