package cz.modemtec.adapter.api.data

import play.api.libs.json.JsValue

/** Object contains input data for operation CallMethod - data representing input arguments of the given method.
  *
  * @param input Input method arguments.
  */
case class CallMethodRequest(
  input: JsValue
)
