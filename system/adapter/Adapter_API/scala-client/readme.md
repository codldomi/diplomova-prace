## Scala Client for Adapter API 

Version 0.1.0

The library implements client for Adapter API v0.1.0 and for OPC UA Adapter v0.1.0.

---

### Language

- [Scala](https://www.scala-lang.org/) 3.3.1
- conventions as [Scala Style Guide](https://docs.scala-lang.org/style/)
- building tool [sbt](https://www.scala-sbt.org/) 1.9.6
- JDK 17 [Eclipse Temurin by Adoptium](https://adoptium.net/temurin/releases/)

### Development Environment

- [IntelliJ IDEA](https://www.jetbrains.com/idea/) (Community or Ultimate) with [plugin Scala](https://www.jetbrains.com/help/idea/discover-intellij-idea-for-scala.html)

### Publishing

- in terminal ```sbt publish``` creates files for publishing to [Maven repository](https://maven.apache.org/index.html)
  specified in ```build.sbt```
- in terminal ```sbt publishLocal``` creates files for publishing to local [Maven repository](https://maven.apache.org/index.html)
  often located in ```/home/<user>/.m2/``` (more info [here](https://www.baeldung.com/maven-local-repository))
