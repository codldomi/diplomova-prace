//----------------------------------------------------------------------------------------------------------------------
//- BUILD
//  tutorials and tips about Scala libraries: https://docs.scala-lang.org/overviews/contributors/index.html

scalaVersion := "3.3.1"

//  authentication for Sonatype Nexus Repository Manager
credentials := Seq(Credentials(Path.userHome / ".sbt" / ".credentials"))

val pekkoVersion = "1.0.1"
val pekkoHttpVersion = "1.0.0"

libraryDependencies ++= Seq(
  //  BASIC
  "org.scalactic" %% "scalactic" % "3.2.14",

  //  WS CLIENT
  "org.playframework" %% "play-json" % "3.0.1",
  "org.playframework" %% "play-ws" % "3.0.0",

  //  CATS
  "org.typelevel" %% "cats-core" % "2.9.0",

  "org.apache.pekko" %% "pekko-actor-typed" % pekkoVersion,
  "org.apache.pekko" %% "pekko-stream" % pekkoVersion,
  "org.apache.pekko" %% "pekko-http" % pekkoHttpVersion,

  //  LOGGER
  "ch.qos.logback" % "logback-classic" % "1.4.7" % "test",

  //  TEST
  "org.scalatest" %% "scalatest" % "3.2.14" % "test"
)

//----------------------------------------------------------------------------------------------------------------------
//- PUBLISH

version := "0.1.0"

//  used as artifactId
name := "adapter-api"

//  used as groupId
organization := "cz.modemtec"

description := "A library providing Adapter API client-side."

versionScheme := Some("early-semver")

publishMavenStyle := true

licenses := Seq("MIT License" -> url("http://www.opensource.org/licenses/mit-license.php"))

homepage := Some(url("http://gitlab.intranet.modemtec.cz/products/server/pdds/adapter-api"))
scmInfo :=
  Some(
    ScmInfo(
      url("http://gitlab.intranet.modemtec.cz/products/server/pdds/adapter-api"),
      "git@gitlab.intranet.modemtec.cz:products/server/pdds/adapter-api.git"))

developers := List(
  Developer(id="dominik.codl", name="Dominik Codl", email="dominik.codl@modemtec.cz", url=url("https://modemtec.cz/cs/"))
)


//----------------------------------------------------------------------------------------------------------------------
