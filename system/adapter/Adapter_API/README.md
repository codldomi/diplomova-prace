## Adapter API

version 0.1.0

---

The project target is to describe Adapter API. It provides documentation in the Czech language presenting motivation and design principles behind the API and the API definition. Also, there are libraries implementing clients to the corresponding version of API.

### Textual Description

The API is defined in text form located in ```/docs``` folder. The documentation uses MarkDown format and the MkDocs tool to create a static site - the command in the terminal is the following:

```shell
$ mkdocs build
```

Then the generated static webpage shows up in the ```/site``` folder. Theme and other configurations are placed in ```mkdocs.yml``` file. For more information about MkDocs check [the offical site](https://www.mkdocs.org/).

### Machine-Readable Definition

There is an OpenAPI file ```/schemas/openapi.yml``` formally describing HTTP operations and data structures - WebSocket endpoint is specified only textually, because OpenAPI doesn't natively support WebSocket, although WebSocket starts connection via HTTP handshake (to learn more, see [Wikipedia](https://en.wikipedia.org/wiki/WebSocket)). The OpenAPI Specification is under Swagger, which offers tools and materials, [click here](https://swagger.io/specification/), e.g. editor helping write the YML file correctly.

### Default Client

A communication with Adapter API behaves as any other client-server chatting. In the project folder, there are several default implementations of the client. They can be used to test the server side or to execute operations. Futhermore it contains definitions for classes representing request/result objects. Feel free to implement your own client or add a new one. Client projects are located in folders ```/[programming language]-client```, e.g. ```/scala-client```. The list of client libraries follows:

- Scala