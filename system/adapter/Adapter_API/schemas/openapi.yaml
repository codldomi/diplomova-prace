openapi: 3.0.3

info:
  title: Adapter API
  description: Common API for adapters.
  version: 1.0.0

externalDocs:
  description: Project description.
  url: http://gitlab.intranet.modemtec.cz/products/server/pdds/adapter-api

paths:

  /about/info:
    get:
      summary: Reads information about adapter.
      operationId: AboutInfo
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterInfo"

  /about/health:
    get:
      summary: Checks whether service is available.
      operationId: AboutHealth
      responses:

        "200":
          description: Succesful operation.

  /node/{guid}/{nodepath}:
    get:
      summary: Reads actual value of variable.
      operationId: NodeRead
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
        - name: nodepath
          in: path
          description: Path to the node in the object tree.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ReadResult"

        "404":
          description: Path to node is not valid or server does't exist. It's specified with error message.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "405":
          description: Executed operation is not supported, e.g. selected node is not a variable.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during actions with resource.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "503":
          description: Resource can't be connected.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

    put:
      summary: Writes value to variable.
      operationId: NodeWrite
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
        - name: nodepath
          in: path
          description: Path to the node in the object tree.
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/WriteRequest"
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/WriteResult"
        "400":
          description: Value has different data type than targeted variable.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "404":
          description: Path to node is not valid or server does't exist. It's specified with error message.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "405":
          description: Executed operation is not supported, e.g. selected node is not a variable.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during actions with resource.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "503":
          description: Resource can't be connected.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

    post:
      summary: Calls method on node.
      operationId: NodeCallMethod
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
        - name: nodepath
          in: path
          description: Path to the node in the object tree.
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/CallMethodRequest"
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/CallMethodResult"
        "400":
          description: Input data types and order don't satified targeted method arguments.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "404":
          description: Path to node is not valid or server does't exist. It's specified with error message.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "405":
          description: Executed operation is not supported, e.g. selected node is not a method.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during actions with resource.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "503":
          description: Resource can't be connected.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /node/history/{guid}/{nodepath}:
    post:
      summary: Reads historic values from closed time interval [from, to].
      operationId: NodeHistoryRead
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
        - name: nodepath
          in: path
          description: Path to the node in the object tree.
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/HistoryReadRequest"
        required: true
      responses:

        "200":
          description: Succesful operation. Returns stream/chunked response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HistoryReadResult"

        "404":
          description: Path to node is not valid or server does't exist. It's specified with error message.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "405":
          description: Executed operation is not supported, e.g. selected node is not a variable.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during actions with resource.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "503":
          description: Resource can't be connected.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /resource/all:
    get:
      summary: Gets all stored resource configs.
      operationId: ReadAllResourceConfigs
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ResourceConfig"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /resource/{guid}:
    get:
      summary: Reads config for the resource with the given GUID.
      operationId: ReadResourceConfig
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ResourceConfig"

        "404":
          description: Resource doesn't exist.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

    put:
      summary: Updates or creates resource config.
      operationId: CreateOrUpdateResourceConfig
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ResourceConfig"

        "400":
          description: Bad data in resource config.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

    delete:
      summary: Deletes resource by the given GUID.
      operationId: DeleteResourceConfig
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Succesful operation.

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /resource/{guid}/spec/signature:
    get:
      summary: Reads signature attribute of the specification of resource with the given GUID.
      operationId: ReadResourceSpecSignature
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: string

        "404":
          description: Resource doesn't exist.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "503":
          description: Resource can't be connected.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /resource/{guid}/spec:
    get:
      summary: Reads specification of resource with the given GUID.
      operationId: ReadResourceSpec
      parameters:
        - name: guid
          in: path
          description: Resource globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ResourceSpec"

        "404":
          description: Resource doesn't exist.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "503":
          description: Resource can't be connected.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /pki/trusted/{guid}:
    get:
      summary: Reads certificate with its metadata.
      operationId: ReadCertData
      parameters:
        - name: guid
          in: path
          description: Certificate globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/CertData"

        "404":
          description: Certificate doesn't exist.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

    put:
      summary:  Creates or updates certificate with its metadata.
      operationId: CreateOrUpdateCertData
      parameters:
        - name: guid
          in: path
          description: Certificate globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Succesful operation.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/CertData"

        "400":
          description: Bad data in certificate or metadata.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

    delete:
      summary: Deletes certificate.
      operationId: DeleteCertData
      parameters:
        - name: guid
          in: path
          description: Certificate globally unique identifier.
          required: true
          schema:
            type: string
      responses:

        "200":
          description: Succesful operation.

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

  /pki/all/trusted/guid:
    get:
      summary: Reads all trusted certificates GUIDs.
      operationId: ReadAllTrustedGuid
      responses:

        "200":
          description: Successful operation.
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string

        "500":
          description: Error during operation occurred.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/AdapterError"

components:
  schemas:
    ResourceConfig:
      type: object
      properties:
        guid:
          type: string
        connstr:
          type: string
        policy:
          $ref: "#/components/schemas/SecurityPolicy"
        auth:
          $ref: "#/components/schemas/AuthConfig"
      required: [ guid, connstr ]

    AuthConfig:
      type: object
      properties:
        username:
          type: string
        password:
          type: string
      required: [ username, password ]

    SecurityPolicy:
      type: string
      enum: [ None, Basic256, Basic256Sha256, Aes128Sha256RsaOaep, Aes256Sha256RsaPss ]

    ReadResult:
      type: object
      properties:
        value: {}
        time:
          type: string
          format: date-time

    WriteRequest:
      type: object
      properties:
        value: {}

    WriteResult:
      type: object
      properties:
        value: {}

    CallMethodRequest:
      type: object
      properties: {}

    CallMethodResult:
      type: object
      properties: {}

    HistoryReadRequest:
      type: object
      properties:
        from:
          type: string
          format: date-time
        to:
          type: string
          format: date-time
      required: [ from ]

    HistoryReadResult:
      type: object
      properties:
        values:
          type: array
          items:
            $ref: "#/components/schemas/ReadResult"

    ResourceSpec:
      type: object
      properties:
        name:
          type: string
        signature:
          type: string
        description:
          type: string
        nodes:
          type: object
          additionalProperties:
            type: object
            properties:
              path:
                type: string
              nodespec:
                oneOf:
                  - $ref: "#/components/schemas/VariableSpec"
                  - $ref: "#/components/schemas/MethodSpec"
        schemas:
          type: object
          additionalProperties:
            type: object
            properties:
              uri:
                type: string
              schemaspec:
                $ref: "#/components/schemas/SchemaSpec"
      required: [ name, signature, description, nodes, schemas ]

    NodeSpec:
      type: object
      properties:
        path:
          type: string
        description:
          type: string
        name:
          type: string
      required: [ path, description, name ]

    VariableSpec:
      allOf:
        - $ref: "#/components/schemas/NodeSpec"
        - type: object
          properties:
            schema: {}
          required: [ schema ]

    MethodSpec:
      allOf:
        - $ref: "#/components/schemas/NodeSpec"
        - type: object
          properties:
            input: {}
            output: {}

    SchemaSpec:
      type: object
      properties:
        uri:
          type: string
        name:
          type: string
        dependencies:
          type: array
          items:
            type: string
        schema: {}
      required: [ uri, name, dependencies, schema ]

    AdapterInfo:
      type: object
      properties:
        name:
          type: string
        protocol:
          type: string
        adapterVersion:
          type: string
        apiVersion:
          type: string
      required: [ protocol, apiVersion, adapterVersion, name ]

    CertData:
      type: object
      properties:
        guid:
          type: string
        cert:
          type: string
      required: [ guid, cert ]

    AdapterError:
      type: object
      properties:
        uri:
          type: string
        code:
          type: number
        name:
          type: string
        message:
          type: string
      required: [ uri, code, name, message ]
