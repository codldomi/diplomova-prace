# Návrh

V této části je popsán návrh Adapter API. Jedná se o textový popis rozhraní adaptéru. V projektu se nachází i adresář ```/schemas```, kde lze dohledat různá schémata ve strojově čitelných formátech, např. ```/schemas/openapi.yaml``` obsahuje popis HTTP části API ve formátu OpenAPI v3.0.0.

Adaptér spravuje zdroje v daném protokolu a Adapter API poskytuje jednotné rozhraní pro všechny adaptéry. Úlohou adaptéru je udržovat připojení nebo naopak jejich zánik, poskytnout operace pro zisk dat ze zdrojů či jejich zápis. Adapter API se snaží o vytvoření co nejuniverzálnějšího rozhraní. Čerpá z požadavků, které jsou obvykle kladeny na sběr dat z IoT zařízení. API však lze zobecnit na jakýkoliv server nebo síť.

## Verzování

Existují dvě základní strategie pro verzování API využívající URL pro určení endpointu, což jsou například protokoly vystavěné nad HTTP či SOAP. První možností jak verzovat je vložit číslo verze do společné části URL všech endpointů. Druhou možností je poskytnutí endpointu, který nabízí informace o daném serveru, např. verzi API, název, popis.

Adapter API používá druhou strategii. HTTP endpoint s cestou ```{URL serveru}/about/info``` při volání metody GET vrací strukturu *AdapterInfo*, která mimo jiné obsahuje číslo verze Adapter API a číslo verze samotného adaptéru. Lze tak dostat komplexní informaci o verzování a případně v budoucnu mít možnost rozšíření struktury o další data.

## Specifikace zdroje

Zdroj (*Resource*) v řeči adaptéru představuje cokoliv, co může vlastnit endpointy, nad kterými lze volat operace. Typickým zdrojem je například server, ale může se jednat i o síť. Z toho důvodu objekt *ResourceConfig* obsahuje i velmi obecný atribut *connstr* (dlouze rozepsáno *connection string*), kde lze uložit kupříkladu URL serveru či pouze název portu, je-li zdrojem sériový port.

Zdroj popisují metadata inspirované protokolem OPC UA, jenž jsou nezavislé na použité technologii pro implementaci Adapter API. Data s popisem zdrojem představuje objekt *ResourceSpec*. Základním stavebním kamenem je uzel (Node), který představuje bod pro komunikaci. Uzlem může být metoda (Method) nebo proměnná (Variable). V OPC UA existuje dále také i objekt. Nicméně se nejedná o koncový bod API, na kterém by bylo možné volat nějakou metodu nebo spravovat proměnnou. Tudíž je od objektů opuštěno. Přesto ale lze objektově modelovat a to díky atributu cesta, který každý uzel vlastní a který v rámci specifikace zdroje je unikátní. Navíc lze mít cesty, aniž by se za nimi schovával nějaký objektový model - tedy jako identifikátor nebo klasická cesta v rámci URL. Uzly popisují objekty *MethodSpec* a *VariableSpec*, jenž dědí od *NodeSpec*.

Operace nad uzly jsou rovněž inspirované OPC UA. Pro uzel *Variable* lze použít čtení (*Read*), zápis hodnoty (*Write*), vyčtení historie hodnoty (*HistoryRead*) nebo odběr nových hodnot (*Subscribe*). Nad uzlem *Method* je možné volat metodu (*CallMethod*). Datové typy proměnných nebo vstupní/výstupní argumenty metod jsou popsány pomocí JSON Schema. To je obsaženo v objektu *SchemaSpec*. Identifikací schéma je jeho URI.

![Model specifikace zdroje](img/resource_spec.jpg)

Obrázek výše zobrazuje UML diagram tříd specifikace zdroje. Bližším informace k atributům se nacházejí v části *Objekty*.

### Příklad

Nechť *P* je libovolný protokol, který implementuje server vytvořený uživatelem a uživatel chce funkcionality serveru namapovat na rozhraní adaptéru. Budiž to server na měřiči teploty, který poskytuje teplotu ve stupních celsia (*temperature*) a metodu "kalibruj" (*calibrate*). Aby to nebyl úplně triviální příklad, endpoint *temperature* poskytuje pole 5 posledních hodnot typu Int a *calibrate* má dva vstupní argumenty *number* a *otherNumber* s typy taktéž Int.

Dané dva endpointy můžou být namodelovány jako dva uzly:

1. *Variable* *temparature* s cestou ```/temperature``` a se schéma identifikovaný pomocí URI ```http:/www.measure.com/temperature```;

2. *Method* *calibrate* s cestou ```/tools/calibrate``` a se schéma identifikovaným pomocí URI ```http:/www.measure.com/tools/calibrate/Input```.

Daná schémata vypadají následovně:

- Datový typ Teplota.

    ```json
    {
        "$schema" : "https://json-schema.org/draft/2020-12/schema",
        "$id" : "http:/www.measure.com/Temp",
        "title" : "Temp",
        "type" : "number"
    }
    ```

- Schéma proměnné "teplota".

    ```json
    {
        "$schema" : "https://json-schema.org/draft/2020-12/schema",
        "$id" : "http:/www.measure.com/temperature",
        "title" : "VariableValue",
        "type" : "array",
        "items" : {
            "$ref" : "http:/www.measure.com/Temp"
        },
        "minItems" : 0,
        "maxItems" : 5
    }
    ```

- Schéma vstupních argumentů metody "kalibruj".

    ```json
    {
        "$schema" : "https://json-schema.org/draft/2020-12/schema",
        "$id" : "http:/www.measure.com/tools/calibrate/Input",
        "title" : "MethodInput",
        "type" : "object",
        "properties" : {
            "number" : {
                "type" : "number"
            },
            "otherNumber" : {
                "type" : "number"
            }
        },
        "required" : [ "number", "otherNumber" ]
    }
    ```

Za pozornost stojí, že vstupní argumenty metody jsou zde modelovány jako *JSON Object* a že proměnná není skalární. Vstupní argumenty jako objekt jsou užitečné pro zachování názvů argumentů. Jinými slovy hodnoty argumentů nejsou dosazovány "naslepo", přesněji v pořadí, které nám říká dokumentace k danému protokolu *P*, ale podle jejich názvů. Proměnná, která není skalární, může mít jedno schéma, nebo mít dvě schémata, kde jedno je schéma pro skálární datový typ a druhé pro proměnnou, která obsahuje pole hodnot tohoto typu.

Specifikace zdroje může být využita ve vícero situacích. Jmenovitě například generování tříd/datových typů podle JSON Schéma nebo řešení uživatelského vstupu ve frontendu, případně zobrazení datových struktur uživateli. Další text bude zameřen na bližší popis operací.

## Operace

Současné API adaptéru poskytuje dva protokoly pro komunikaci - HTTP a WebSockets. HTTP pro restful část a WebSockets pro streamování dat.

### HTTP

Na následujících řádcích je popsána část API využívající protokol HTTP. Nejdříve pár poznámek společných pro všechny operace.

1. Cesty zdrojů v HTTP (nikoliv zdrojů v řeči Adapter API) jsou zapsány relativně vůči výsledné URL, která však může mít různou adresu v závislosti, kde se nachází adaptér.

1. Atribut *guid* použitý v cestách je shodný s atributem *guid* v *ResourceConfig* objektu. Představuje globální unikátní identifikátor zdroje. Atribut *nodepath* představuje libovolnou cestu k uzlu, která vznikla během transformace protokolu zdrojů do Adapter API. Některé adaptéry mohou mít neměnnou množinu cest k uzlům, ale existují i adaptéry, které mají množinu cest závislou od aktuálně dostupných zdrojů. Např. OPC UA Adapter mapuje cesty podle informačních modelů konkrétních OPC UA serverů, ke kterým vlastní klienty.

1. Všechny úspěšně provedené operace vrací HTTP kód 200 (OK). Přístup k ostatním návratovým kódům je rozvinut v části "Návratové kódy".

1. Veškerá data v tělech HTTP zpráv jsou ve formátu JSON. Bližší informace lze nahlédnout v části "Serializace objektů".

1. Implementace metod *PUT*, *DELETE* musí dodržovat idempodenci. *POST* a *GET* zmíněný požadavek nemá. Což vzhledem k využití *POST* pro operaci *CallMethod* je na místě, jelikož není garantováno, že zavolaná metoda bude vždy vracet stejný výstup na stejný vstup. Obdobně *POST* pro *HistoryRead* se může nalézt v situaci, kdy část historie byla smazána v době mezi dvěma voláním této HTTP metody se stejnými parametry. Oproti tomu *GET* by podle obecně známých doporučení měl být idempodentní. Avšak adaptér typicky spravuje zařízení, kterými jsou různé měřiče, jenž přirozeně mohou v čase měnit hodnotu proměnné.

#### /about/health

- GET: vrátí OK (200), pokud služba je dostupná.

#### /about/info

- GET: vrátí informace o adaptéru, např. verze nebo použitý protokol (*AdapterInfo*).

#### /node/{guid}/{nodepath}

- PUT: zapíše hodnotu v těle zprávy (WriteRequest), pokud je uzlem proměnná, a vrátí zpět hodnotu (*WriteResult*).
- POST: zavolá metodu se vstupními argumenta reprezentovanými jako objekt v těle zprávy (*CallMethodRequest*), pokud je uzlem metoda, a vrátí výsledné návratové hodnoty (*CallMethodResult*).
- GET: vrátí aktuální hodnotu a čas v těle zprávy (*ReadResult*), pokud je uzlem proměnná.

#### /node/history/{guid}/{nodepath}

- POST: vrátí hodnoty s časy (HistoryReadResult) z historie podle parametrů (*HistoryReadRequest*).
- pozn.: návratovou hodnotou ale nemusí být *HistoryReadResult*. Server vrátí chunked response a je na klientovi, zda ji agreguje do *HistoryReadResult* nebo s ní bude zacházet formou streamu.

#### /resource/{guid}

- GET: vrátí konfiguraci (*ResourceConfig*) zdroje pro zvolený server.
- PUT: vytvoří/modifikuje daný zdroj/konfiguraci klienta (*ResourceConfig*).
- DELETE: odstraní zdroj.

#### /resouce/{guid}/spec

- GET: vrátí specifikaci daného zdroje (*ResourceSpec*).

#### /resouce/{guid}/spec/signature

- GET: vrátí atribut *signature* z *ResourceSpec* pro daný zdroj.
- pozn.: operace ```GET /resouce/{guid}/spec``` může být časově nákladná, tudíž se hodí pomocí atributu *signature* ověřit, zda již danou specifikaci nevlastníme.

#### /resource/all

- GET: vrátí všechny uložené konfigurace zdrojů.

#### /pki/trusted/{guid}

- GET: vrátí certifikát s daným GUID (*CertData*).
- PUT: vytvoří/modifikuje certifikát s daným GUID (*CertData*).
- DELETE: smaže certifikát s daným GUID.

#### /pki/all/trusted/guid

- GET: vrátí GUID všech uložených certifikátů,

### WebSocket

WebSocket umožňuje posílání zpráv/streamování oběma směry. Nicméně Adapter API dodržuje client-server komunikaci. Tudíž současná verze podporuje pouze half-closed kanály, kde tečou data od adaptéru k jeho klientovi. Jinými slovy režim Publisher-Subscriber. Jakmile klient, jakožto subscriber, ztratí o odběr dat zájem, ukončí komunikaci. Nutno ještě dodat, že WebSocket pro zahájení komunikace používá HTTP handshake, a tak lze operaci namapovat na klasickou HTTP cestu. Níže přehled podporovaných operací.

#### /node/subscribe/{guid}/{nodepath}

 - představuje operaci *Subscribe*, kdy adaptér v každé zprávě pošle novou hodnotu (*ReadResult*) proměnné, jenž je určena cestou *nodepath*. Na druhé straně cokoliv pošle klient adaptéru, je ignorováno.

## Chybové stavy

Veškeré návratové kódy jsou popsány ve zmíněném dokumentu OpenAPI. Zde jsou rozebrány pouze predefinované chyby, které mohou nastat, u takřka libovolné operace. Návratové kódy jsou pro HTTP část, a tudíž odpovídají obvyklému užití HTTP kódů. WebSocket vrací vždy korektní hodnoty, ale může dojít k chybě při tvorbě spojení, nebo během spojení atp. - v těchto případech se řídí návratové hodnoty protokolem WebSocket.

Nicméně nedostatkem návratových kódů je jejich obecnost. Příkladem budiž "404 Not Found" pro jakoukoliv operaci. Z tohoto statusu nelze zjistit, zda-li nebyl nalezen zdroj, nebo uzel. Adapter API takovéto situace řeší dodatečnou informací (*AdapterError*) v těle odpovědi. Objekt *AdapterError* obsahuje URI, které pomáhá identifikovat chybu napříč systémem, název chyby, zprávu s popisem chyby a kód, typicky HTTP status.

Níže je slíbený rozbor predefinovaných chyb.

### Certifikát nenalezen

Pro kód je použit HTTP status "404 Not Found" a pro URI "http://modemtec.cz/adapter-api/errors/CertNotFound" - chyba je na straně klienta. Certifikát se zadaným GUID nemůže adaptér najít.

### Zdroj nenalezen

Pro kód je použit HTTP status "404 Not Found" a pro URI "http://modemtec.cz/adapter-api/errors/ResourceNotFound" - chyba je na straně klienta. Zdroj se zadaným GUID nemůže adaptér najít.

### Uzel nenalezen

Pro kód je použit HTTP status "404 Not Found" a pro URI "http://modemtec.cz/adapter-api/errors/NodeNotFound" - chyba je na straně klienta. Uzel na daném zdroji a s danou cestou nemůže adaptér najít.

### Nelze se připojit ke zdroji

Pro kód je použit HTTP status "503 Service Unavailable" a pro URI "http://modemtec.cz/adapter-api/errors/ResourceNotConnected" - chyba je obvykle na straně serveru, ale špatnou konfiguraci zdroje mohl zaslat dříve již klient. Jedná se o situaci, kdy se adaptér nemůže připojit ke zdroji.

### Nepodporovaná operace

Pro kód je použit HTTP status "405 Method Not Allowed" a pro URI "http://modemtec.cz/adapter-api/errors/UnsupportedOperation" - chyba je na straně klienta. Uzel nepodporuje danou operaci, např. volá se vyčtení hodnotu na metodě.

### Chyba v průběhu operace

Pro kód je použit HTTP status "500 Internal Server Error" a pro URI "http://modemtec.cz/adapter-api/errors/ErrorDuringOperation" - chyba je na straně serveru. Vznikne v případě chyby v průběhu vykávání operace. Problém blíže specifikuje atribut zpráva.

### Chybná vstupní data

Pro kód je použit HTTP status "400 Bad Request" a pro URI "http://modemtec.cz/adapter-api/errors/BadInputData" - chyba je na straně klienta. Může se jednat o chybný formát dat v těle žádosti nebo chybnou hodnotu na vstupu operace.

### Neznámá chyba

Pro kód je použit HTTP status "500 Internal Server Error" a pro URI "http://modemtec.cz/adapter-api/errors/Unknown" - chyba je obvykle na straně serveru. Typicky se jedná o situaci, že není implementován převod chyby na *AdapterError*. V atributu zpráva se vyskytuje popis převzatý například ze zachycené výjimky.

## Objekty

Následuje definice objektů, které operace využívají. Vzhledem k verzování API pomocí objektu *AdapterInfo* je možné tyto objekty měnit, aniž by se musely měnit názvy nebo URI schémat. Budou-li navíc změny pouze ve smyslu přidání atributů, zachová se zpětná kompatibilita. Tedy objekty nové verze budou použitelné i pro starší verze Adapter API. Tudíž definice jsou minimální možné pro plnohodnotné používání adaptérů.

Pro popis datových typů je použito následující značení:

- *Option[A]* značí hodnotu typu *A*, která může ale nemusí být přítomna;
- *Map[String, B]* značí slovník, kde klíč je typu *String* a hodnota typu *B*;
- *Any* značí libovolnou hodnotu, která musí být přítomna - pro hodnotu, jenž nemusí být přítomna, je použito značení *Option[Any]*;
- *Array[A]* značí pole hodnot typu *A*;
- *Set[A]* značí množinu hodnot typu *A*.

### AdapterInfo

Objekt obsahuje obecné informace o adaptéru. Hodí se k registraci adaptéru nebo zjištění, který protokol transformuje.

| atribut | typ | význam |
| :--- | :--- | :--- |
| protocol | String | řetezec užívaný pro protokol v rámci URL, např. opc.tcp, http |
| name | String | název adaptéru, např. OPC UA Adapter |
| adapterVersion | String | verze adaptéru, např. 0.2.0. Nemusí být shodná s *apiVersion* |
| apiVersion | String | verze Adapter API, které adaptér implementuje |

### ResourceConfig

Objekt obsahuje data potřebná k vytvoření spojení se zdrojem.

| atribut | typ | význam |
| :--- | :--- | :--- |
| guid | String | Globální identifikátor zdroje |
| connstr | String | řetezec obsahující informace k připojení |
| policy | Option[SecurityPolicy] | bezpečnostní politika - šifrování zpráv |
| auth | Option[AuthConfig] | autentizace uživatele - pokud None, uživatel je brán jako anonymní |

### AuthConfig

Objekt obsahuje data potřebná k autentizaci uživatele.

| atribut | typ | význam |
| :--- | :--- | :--- |
| username | String | uživatelské jméno nebo alias |
| password | String | heslo |

### SecurityPolicy

Objekt odpovídá enumeračnímu typu. Vyjadřuje zvolené šifrování.

| enum hodnota |
| :--- |
| None |
| Basic256 |
| Basic256Sha256 |
| Aes128Sha256RsaOaep |
| Aes256Sha256RsaPss |

### ReadResult

Objekt obsahuje data vyčtená v daném čase. Pokud atribut *time* je None, není znám čas vzniku hodnoty. Objekt využívá jak operace *Read*, tak operace *HistoryRead* nebo *Subscribe*.

| atribut | typ | význam |
| :--- | :--- | :--- |
| value | Any | hodnota v daném čase |
| time | Option[DateTime] | čas hodnoty |

### WriteRequest

Objekt obsahuje vstupní data pro operaci *Write*, tedy data pro zápis hodnoty.

| atribut | typ | význam |
| :--- | :--- | :--- |
| value | Any | nová hodnota |

### WriteResult

Objekt obsahuje úspěšný výsledek operace *Write*, tedy data, jenž byla zapsána.

| atribut | typ | význam |
| :--- | :--- | :--- |
| value | Any | zapsaná hodnota |

### CallMethodRequest

Objekt obsahuje vstupní data pro operaci *CallMethod*, tedy data reprezentující vstupní argumenty dané metody.

| atribut | typ | význam |
| :--- | :--- | :--- |
| input | Map[String, Any] | vstupní parametry |

### CallMethodResult

Objekt obsahuje úspěšný výsledek operace *CallMethod*, tedy data reprezentující výstupní argumenty dané metody.

| atribut | typ | význam |
| :--- | :--- | :--- |
| output | Map[String, Any] | výstupní parametry |

### HistoryReadRequest

Objekt obsahuje vstupní data pro operaci *HistoryRead*, tedy filtr na výběr dat z historie.

| atribut | typ | význam |
| :--- | :--- | :--- |
| from | DateTime | čas, včetně, odkud se má začít |
| to | DateTime | čas, včetně, kde se má skončit aktuální hodnoty |

### HistoryReadResult

Objekt obsahuje úspěný výsledek operace *HistoryRead*.

| atribut | typ | význam |
| :--- | :--- | :--- |
| values | Array[ReadResult] | hodnoty v daném období |

### ResourceSpec

Objekt obsahuje specifikaci zdroje: schémata datových typů a popis uzlů.

| atribut | typ | význam |
| :--- | :--- | :--- |
| name | String | název |
| signature | String | unikátní řetězec pro daný typ specifikace, jenž může používat více zdrojů (unikátnost je garantována pouze v rámci daného adaptéru) |
| description | String | popis |
| nodes | Map[String, NodeSpec] | popis uzlů |
| schemas | Map[String, SchemaSpec] | popis použitých datových typů |

### NodeSpec

Objekt představující společné atributy specifikace uzlů. Často se implementuje jako abstraktní třída nebo trait.

| atribut | typ | význam |
| :--- | :--- | :--- |
| path | String | absolutní cesta k uzlu, např. "/a/b/c" |
| description | String | popis uzlu |
| name | String | název uzlu, poslední položka v cestě, např. "c" |

#### VariableSpec

Objekt popisuje specifikaci uzlu typu proměnná. Dědí od *NodeSpec*.

| atribut | typ | význam |
| :--- | :--- | :--- |
| path | String | absolutní cesta k uzlu, např. "/a/b/c" |
| description | String | popis uzlu |
| name | String | název uzlu, poslední položka v cestě, např. "c" |
| schema | String | URI Json Schema pro datový typ proměnné |

#### MethodSpec

Objekt popisuje specifikaci uzlu typu metoda. Dědí od *NodeSpec*.

| atribut | typ | význam |
| :--- | :--- | :--- |
| path | String | absolutní cesta k uzlu, např. "/a/b/c" |
| description | String | popis uzlu |
| name | String | název uzlu, poslední položka v cestě, např. "c" |
| input | Option[String] | URI Json Schema pro vstup |
| output | Option[String] | URI Json Schema pro výstup |

### SchemaSpec

Objekt popisuje specifikaci datového typu.

| atribut | typ | význam |
| :--- | :--- | :--- |
| uri | String | unikátní URI (unikátnost je garantována v rámci specifikace zdroje) |
| name | String | název |
| schema | Any | JSON Schema - URI je shodné s *uri* |
| dependencies | Set[String] | množina URI datových typů, které toto schéma využívá |

### AdapterError

Objekt popisuje výjimku/chybu na začátku, v průběhu či na konci vykonávání operace.

| atribut | typ | význam |
| :--- | :--- | :--- |
| uri | String | unikátní URI (unikátnost je garantována v rámci adaptéru) |
| code | Number | kód chyby, typicky odpovídající HTTP Status Code |
| name | String | název |
| message | String | zpráva/další informace |

### CertData

Objekt popisuje X.509 certifikát a přidružená metadata.

| atribut | typ | význam |
| :--- | :--- | :--- |
| cert | String | X.509 certifikát kódovaný v binárním DER formátu, jenž byl zakodován do Base64 řetězce |
| guid | String | globální identifikátor dat |

## Serializace objektů

Objekty jsou kódovány v JSON formátu. Názvy atributů objektů odpovídají názvům atributů JSON objektů. Pro atributy a jejich hodnoty jsou aplikovány následující pravidla:

- jestliže *Option[A]* má přitomnou hodnotu, pak je kódován pro hodnotu typu *A* - naopak není-li přítomna, atribut není ve výsledném *JSON Object* přítomen;
- *Map[String, B]* je kódován jako *JSON Object* - výjimku tvoří slovníky v objektu *ResouceSpec*, které jsou serializovány jako *JSON Array* s hodnotami kódovanými pro typ *B*;
- *DateTime* je kódován jako *JSON String* s formátem podle normy ISO 8601 s UTC: ```"yyyy-MM-ddTHH:mm:ssZ"```, např. ```"2022-06-27T15:16:00Z"```
- *Array[A]* je kódován jako *JSON Array* s hodnotami kódovanými pro typ *A*;
- *Set[A]* je kódován jako *JSON Array* s hodnotami kódovanými pro typ *A*;
- *Any* je kódováno podle těchto pravidel;
- jinak je použito obvyklé JSON kódování, např. *String* je kódován jako *JSON String*.

U JSON formátu, narozdíl od binárních kódování, nezáleží na pořadí atributů v objektu.

## Bezpečnost

Otázka bezpečnosti je ponechána čistě na implementaci adaptéru. Adaptér nemusí být zabezpečen vůbec, nebo může použit HTTPS protokol či JSON Web Token.
