# Adapter API 

verze 0.1.0

---

Adapter API představuje rozhraní pro komponenty, jenž poskytují transformaci dat a operací z daného protokolu do dat a operací definovaných v Adapter API. Další nedílnou součástí funkcí je správa klientů. 

V nadcházejím textu budeme hovořit o klientech jako o zdrojích (resource), jejichž spojení může být i session-less v závislosti na konkrétním protokolu. A tudíž se hodí správu klientů, nebo-li alokace, vytvoření spojení, zrušení spojení, případná optimalizace klientů v paměti, atd., ponechat na implementaci adaptéru a z pohledu API poskytnout operace pro zdroj. Problematičtější částí se tak stává výsledek. Lze přemýšlet o volbě protokolu, ve kterém mají být operace API, nebo v jakém formátu mají být přenášena data či jak zabezpečené má API být. Úvahy nad zmíněnými otázkami popisuje část věnovaná analýze, po které následuje návrh, kde se API definuje, a na závěr se provede diskuze o výsledku. Konkrétní příklad implementace Adapter API lze kupříkladu vidět v projektu [OPC UA Adapter](http://gitlab.intranet.modemtec.cz/products/server/pdds/opc-ua-adapter).