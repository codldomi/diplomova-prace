# Knihovny

Projekt Adapter API obsahuje také i knihovny s klienty pro Adapter API a základní adaptéry, např. OPC UA Adapter. V současné chvíli se na seznamu nachází pouze klient implementovaný v jazyce Scala.

## Scala

Klient napsaný v jazyce Scala implementuje Adapter API verze 0.1.0. Využívá především projekty Play Framework a Pekko (open-source verze knihovny Akka). Z Play si bere implementaci klienta pro webové služby a podporu JSON formátu. Což umožňuje hladké začlenění knihovny do aplikací postavených nad Play Framework pracující s HTTP. Pekko, kterou rovněž využívá i Play, dodává podporu streamů a protokolu WebSocket.

### Návratové hodnoty a monády

Hlavním rysem implementace je užití funkcionálních prvků, nejčastěji monády. Pomocí monád se ve Scale řeší asynchronní kód (třída *Future[A]*) nebo zpracování chybových stavů (lze pomocí tříd *Either[A, B]*, *Try[A]* nebo *Option[A]*). V případě této knihovny se pro chybové stavy užívá monáda *Either[AdapterError, A]*, kde *A* je návratová hodnota v případě, že nedošlo k chybě. *AdapterError* obsahuje popis chyby (viz. návrh API). Vzhledem k tomu, že operace klienta jsou vyhodnocovány asynchronně, výsledkem může být typ *Future[Either[AdapterError, A]]*. Nicméně takto vnořené monády mohou být obtížnější pro programátora na zápis. Řešením jsou tzv. Monad Transformers, které umožňují přístup přímo k zanořené hodnotě - v tomto případě instanci *A*. Implementaci Monad Transformers nabízí knihovna [Cats](https://typelevel.org/cats/), která krom nich obsahuje i další funkce a třídy užitečné pro funkcionální paradigma. *EitherT[Future, A, B]* implementuje transformace monády *Future[Either[A, B]]*. Lze tak používat i for-comprehension pro sekvenční zápis monád (viz část "Ukázky použití klienta"). Níže ukázka využití v definici typu.

```scala
object MonadTransformer extends MonadTransformer

trait MonadTransformer:

  type FutureEither[L, R] = EitherT[Future, L, R]

  extension [L, R](self: Future[Either[L, R]]) 
    def asFutureEither: FutureEither[L, R] = EitherT[Future, L, R](self)
  
  object FutureEither:

    def unit[L]: FutureEither[L, Unit] = Future.successful(Right(())).asFutureEither

    def right[L, R](value: R): FutureEither[L, R] = Future.successful(Right(value)).asFutureEither

    def left[L, R](value: L): FutureEither[L, R] = Future.successful(Left(value)).asFutureEither

    def sequence[L, R](items: Iterable[FutureEither[L, R]])(using context: ExecutionContext): FutureEither[L, Seq[R]] =
      items
        .map(_.value)
        .pipe: futures =>
          Future.sequence(futures)
        .map: eithers =>
          eithers.foldLeft(Right(Seq.empty): Either[L, Seq[R]]): (acc, either) =>
            for 
              items <- acc
              item  <- either
            yield items :+ item
        .asFutureEither
```

### Ukázky použití klienta

Níže je uvedeno pár příkladů použití knihovny. Části kódu jsou převzaty z testů OPC UA Adapter.

#### Tvorba klienta

K vytvoření klienta je nutný webový klient z Play, port, kde se služba nachází a IP adresa služby. Z nich je vytvořen prefix pro operace v protokolu HTTP a WebSocket. Implicitními parametry jsou ExecutionContext pro správu asynchronního výpočtu a ActorSystem - ten běží na pozadí Play aplikace, nicméně zde je potřeba kvůli streamům a WebSocket.

```scala
class CommonAdapterClient(
  val webservice: WSClient,
  val port: Int,
  val address: String
)(using val executionContext: ExecutionContext, val system: ActorSystem) extends AdapterClient:

  protected val httpPrefix: String = s"http://$address:$port"
  protected val webSocketPrefix: String = s"ws://$address:$port"

  // code ...
```

Například při testovaní adaptéru v rámci aplikace v Play lze inicializovat testy v sadě následovně.

```scala
"Test name" in new WithServer(app = AppWith("test.conf"), port = testPort):
  override def running(): Unit = 
    given actors: ActorSystem = app.actorSystem
    val guid = "Some server identifier"
    val ws = app.injector.instanceOf[WSClient]
    val client = OpcUaAdapterClient(ws, port, "localhost")

    // code ...
```

Což vytvoří aplikaci s danou konfigurací v ```test.conf``` na daném portu. K tomu Play umožní vytvořit klienta pro webovou službu, nad kterým je naprogramován klient pro adaptér.

#### Vyčtení historie (*HistoryRead*)

Operace je definována následovně.

```scala
trait AdapterClient:

  /** Reads variable's history values.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @param request Request data.
    * @return Stream of history values.
    */
  def historyRead(guid: String, path: String, request: HistoryReadRequest): FutureEither[AdapterError, Source[ReadResult, _]]

  // other methods ...
```

Historie je vyčítána postupně a vracena přes tzv. chunked HTTP response. Klient stream dat rozčlení do jednotlivých hodnot, deserealizuje z JSON a poskytne tento stream. Na straně uživatele je poté možné vytvořit plán streamu - co a jak se s ním má provést.

```scala
//  client creation ...

val expected = // expected values ...

val config =
  ResourceConfig(
    guid    = guid,
    connstr = "opc.tcp://localhost:4840/",
    policy  = None,
    auth    = None)

val request =
  HistoryReadRequest(
    from = SqlTimestamp.valueOf("2000-01-01 00:00:00"),
    to   = SqlTimestamp.valueOf("2025-01-01 00:00:00"))

val sink =
  Sink
    .fold[HistoryReadResult, ReadResult](HistoryReadResult.empty): (result, value) =>
      result.copy(values = result.values :+ value)
    
val action = for 
  _      <- client.createOrUpdateResource(config)
  source <- client.historyRead(guid, "/PDM1/SPDC1/Beh/q", request)
yield source

val future =
  action
    .value
    .flatMap:
      case Left(error)   => throw error
      case Right(source) => source.runWith(sink)

val history = awaitFor(future)

val result = history.values.foldLeft(Vector.empty[JsValue]): (values, readResult) =>
  println(readResult)
  values :+ readResult.value

result mustBe expected
```

#### Odběr novinek (*Subscribe*)

Operace *Subscribe* využívá Akka WebSockets. Protokol je obousměrný - vytvoří se stream od klienta k serveru a od serveru ke klientovi. Možným úskálím implementace v Akka je, že komunikace skončí v okamžiku, kdy jeden z těchto streamů je uzavřen. *Subscribe* však streamuje data pouze ze strany serveru, tzv. half-closed WebSockets. Aby se předešlo ukončení odběru, klient vytvoří instanci *Promise*. Stream dat od klienta k serveru je tedy v nekonečném očekávání odeslání hodnoty. Pokud-li uživatel chce *Subscribe* operaci ukončit, úspěšně dokončí *Promise* přes metodu ```promise.success(None)```.

```scala
trait AdapterClient:

  /** Subscribes variable's value.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @param sink Stream's sink.
    * @tparam Mat Materialized type.
    * @return Promise used as cancellation switch - call {{{promise.success(None)}}} in order to cancel subscription.
    */
  def subscribe[Mat](guid: String, path: String, sink: Sink[ReadResult, Mat]): FutureEither[AdapterError, Promise[Option[Message]]]

  // other methods ...
```

V kódu níže lze vidět celý příklad použití streamování a způsob ukončení.

```scala
//  client creation ...

val config:  =
  ResourceConfig(
    guid    = guid,
    connstr = "opc.tcp://localhost:4840/",
    policy  = None,
    auth    = None)

val expected = Vector(JsNumber(0), JsNumber(1), JsNumber(2), JsNumber(3), JsNumber(4))
var values = Vector.empty[ReadResult]

val sink = Sink.foreach[ReadResult]: message =>
  values = values :+ message

val cancellation = for 
  _       <- client.createOrUpdateResource(config)
  promise <- client.subscribe(guid, "/PDM1/SPDC1/OpCnt/stVal", sink)
yield promise

val promise = awaitFor(cancellation.value) match
  case Left(error)  => throw error
  case Right(value) => value

Thread.sleep(10000)

promise.success(None)

values.map(_.value) containsSlice expected mustBe true
```

#### Vyčtení hodnoty (*Read*)

Pouhé vyčtení aktuální hodnoty proměnné představuje metoda *readValue*.

```scala
trait AdapterClient:

  /** Reads variable's value.
    *
    * @param guid Resource identifier.
    * @param path Path to variable.
    * @return Read value.
    */
  def readValue(guid: String, path: String): FutureEither[AdapterError, ReadResult]

  // other methods ...
```

Níže je k náhlednutí kód, který tuto metodu zavolá.

```scala
//  client creation ...

val config: ResourceConfig =
  ResourceConfig(
    guid    = guid,
    connstr = "opc.tcp://localhost:4840/",
    policy  = None,
    auth    = None)

val expected = Json.obj("value" -> JsNumber(3), "name" -> JsString("test")).asRight

val action = for 
  _      <- client.createOrUpdateResource(config)
  result <- client.readValue(guid, "/PDM1/LLN0/Beh/stVal")
yield result.value

val result = awaitFor(action.value)

result mustBe expected
```
