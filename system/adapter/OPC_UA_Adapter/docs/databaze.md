# Databáze

``` mermaid
erDiagram

t_namespace {
    integer id pk
    text uri
}

t_id {
    integer id pk
    integer c_index
    integer t_namespace_id fk
}

t_namespace ||--o{ t_id : id_from_namespace

t_node {
    integer id pk
    text c_name
    integer c_type
    integer t_data_type_id fk
}

t_node |o--|| t_id : has_id
t_node }o--o| t_id : has_type

t_argument {
    integer id pk
    integer t_node_id fk
    integer c_is_input
    integer t_type_id fk
    text c_name
}

t_argument }o--|| t_id : has_type
t_argument }o--|| t_node : of_method

t_reference {
    integer id pk
    integer t_from_id fk
    integer t_to_id fk
}

t_reference }o--|| t_node : from
t_reference }o--|| t_node : to

t_server {
    integer id pk
    text c_url
    text c_alias
}

t_server_node {
    integer id pk
    integer t_server_id fk
    integer t_node_id fk
}

t_server_node }o--|| t_server : from_server
t_server_node }o--|| t_node : of_node

```