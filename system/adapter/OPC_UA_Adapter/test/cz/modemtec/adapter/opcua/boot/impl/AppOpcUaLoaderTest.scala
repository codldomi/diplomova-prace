package cz.modemtec.adapter.opcua.boot.impl

import com.typesafe.config.{Config, ConfigFactory}
import cz.modemtec.scopcua.data.config.{UaClientConfig, UaEncryptionConfig, UaSecurityPolicy}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import play.api.Configuration


class AppOpcUaLoaderTest extends AnyFunSuite with Matchers:

  val ModemTec: String = "http://www.modemtec.cz/PD/"

  def createConfig(file: String): Configuration =
    val underlying = ConfigFactory.load(file)
    Configuration.apply(underlying)


  test("Default configuration"):
    val config = createConfig("config/test_opcua.conf")
    val loader = AppOpcUaLoader(config)

    val expected =
      UaClientConfig(
        policy = UaSecurityPolicy.None,
        uri = "urn:opc:ua:app:test:client",
        url = "",
        name = "OPC UA App Test Client",
        timeout = 5000,
        encryption = None,
        auth = None)

    val result = loader.loadClientConfig()

    result shouldBe expected

