package cz.modemtec.adapter.opcua.repository.impl

import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class NodeRepositoryImplTest extends AnyFunSuite with Matchers:

  extension (self: String) 
    def asVector: Vector[Int] =
      self
        .split(",")
        .filter(_ != "")
        .map(_.toInt).toVector
  
  extension (self: Vector[UaUInt32]) 
    def asString: String = 
      self
        .map(_.value)
        .mkString(",")


  test("IntVector"):
    val inputOnes = Seq(Vector(), Vector(UaUInt32(2), UaUInt32(4)), Vector(UaUInt32(1)))
    val expectedOnes = Seq("", "2,4", "1")

    inputOnes
      .zip(expectedOnes)
      .foreach: (input, expected) =>
        val result = input.asString
        result shouldBe expected
  

  test("StringExtension"):
    val inputOnes = Seq("", "2,4", "1")
    val expectedOnes = Seq(Vector(), Vector(2, 4), Vector(1))

    inputOnes
      .zip(expectedOnes)
      .foreach: (input, expected) =>
        val result = input.asVector
        result shouldBe expected
      
