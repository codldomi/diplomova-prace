package cz.modemtec.adapter.opcua.service.impl

import com.google.inject.AbstractModule
import com.typesafe.config.{Config, ConfigFactory}

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.util.MonadTransformer.*

import cz.modemtec.adapter.opcua.boot.impl.AppOpcUaLoader
import cz.modemtec.adapter.opcua.boot.{InitApp, OpcUaLoader}
import cz.modemtec.adapter.opcua.repository.impl.{DataTypeRepositoryImpl, NamespaceRepositoryImpl, NodeRepositoryImpl, ResourceRepositoryImpl}
import cz.modemtec.adapter.opcua.repository.{DataTypeRepository, NamespaceRepository, NodeRepository, ResourceRepository}
import cz.modemtec.adapter.opcua.service.NodeSetService
import cz.modemtec.adapter.opcua.test.OpcUaConstants
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.datatypemanager.impl.MapDataTypeManager

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.node.{UaArgument, UaMethodNode}
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation

import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers

import play.api.inject.bind
import play.api.{Configuration, Mode}
import play.api.inject.guice.GuiceApplicationBuilder

import scala.concurrent.ExecutionContext
import scala.io.Source
import scala.util.chaining.scalaUtilChainingOps
import scala.xml.XML


class NodeSetServiceImplTest extends AsyncFunSuite with Matchers with OpcUaConstants:

  /** more info: https://stackoverflow.com/questions/62242989/why-my-scala-async-test-never-completes-when-i-do-await-result
    */
  override given executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global


  def createService(): (NodeSetService, NodeRepository, DataTypeRepository) = 
    val tsOpcUa: Config = ConfigFactory.load("config/test_empty_opcua.conf")
    val tsDb: Config = ConfigFactory.load("config/sqlite_db.conf")
    val module: AbstractModule = new AbstractModule:
      override def configure(): Unit = 
        bind(classOf[InitApp]).asEagerSingleton()
        bind(classOf[DataTypeManager]).toInstance(MapDataTypeManager())

    val injector =
      new GuiceApplicationBuilder()
        .configure(Configuration(tsOpcUa))
        .configure(Configuration(tsDb))
        .bindings(module)
        .bindings(bind[OpcUaLoader].to[AppOpcUaLoader])
        .bindings(bind[NodeRepository].to[NodeRepositoryImpl])
        .bindings(bind[NamespaceRepository].to[NamespaceRepositoryImpl])
        .bindings(bind[ResourceRepository].to[ResourceRepositoryImpl])
        .bindings(bind[DataTypeRepository].to[DataTypeRepositoryImpl])
        .bindings(bind[NodeSetService].to[NodeSetServiceImpl])
        .in(Mode.Test)
        .injector()

    val service = injector.instanceOf[NodeSetService]
    val dataTypes = injector.instanceOf[DataTypeRepository]
    val nodes = injector.instanceOf[NodeRepository]

    (service, nodes, dataTypes)

  end createService
  

  test("Init nodes"):
    val nodeSets =
      Vector(
        "opcua/Opc.Ua.IEC61850-7-3.NodeSet2.xml",
        "opcua/Opc.Ua.IEC61850-7-4.NodeSet2.xml",
        "opcua/ModemTec.PDM1.NodeSet2.xml")

    val (service, nodes, _) = createService()

    val creation = nodeSets.foldLeft(FutureEither.unit[AdapterError]): (result, path) =>
      val xml =
        Source
          .fromResource(path)
          .mkString
          .pipe(XML.loadString)

      val nodeSet = service.extract(xml)

      result
        .flatMap: _ =>
          nodeSet match 
            case Right(nodes) => service.create(nodes)
            case Left(error)  => FutureEither.left(error)

    val nodeId = UaId(7001, ModemTec)          // /PDM1/CONF/Calib/setup
    val expected =
      UaMethodNode(
        id = UaId(7001, ModemTec),             // /PDM1/CONF/Calib/setup
        parentId = Some(UaId(5017, ModemTec)), // /PDM1/CONF/Calib
        browseName = "setup",
        displayName = "setup",
        description = None,
        input =
          Vector(
            UaArgument(
              isInput = true,
              name = "channel",
              typeId = UaId(3, OpcUaFoundation), //  UaByte
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 0),
            UaArgument(
              isInput = true,
              name = "attenuator",
              typeId = UaId(3, OpcUaFoundation), //  UaByte
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 1),
            UaArgument(
              isInput = true,
              name = "reference",
              typeId = UaId(7, OpcUaFoundation), // UaUInt32
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 2)),
        output =
          Vector(
            UaArgument(
              isInput = false,
              name = "result",
              typeId = UaId(3002, ModemTec), //  MtRequestResult
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 0)))

    val action = for 
      _      <- creation.value
      result <- nodes.readMethod(nodeId)
    yield result

    action.map: result =>
      result shouldBe expected

