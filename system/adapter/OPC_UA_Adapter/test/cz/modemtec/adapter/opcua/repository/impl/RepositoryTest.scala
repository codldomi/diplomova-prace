package cz.modemtec.adapter.opcua.repository.impl

import com.google.inject.AbstractModule
import com.typesafe.config.{Config, ConfigFactory}

import cz.modemtec.adapter.api.data.config.ResourceConfig

import cz.modemtec.adapter.opcua.boot.{InitApp, OpcUaLoader}
import cz.modemtec.adapter.opcua.boot.impl.AppOpcUaLoader
import cz.modemtec.adapter.opcua.repository.{DataTypeRepository, NamespaceRepository, NodeRepository, ResourceRepository}
import cz.modemtec.adapter.opcua.service.NodeSetService
import cz.modemtec.adapter.opcua.service.impl.NodeSetServiceImpl
import cz.modemtec.adapter.opcua.test.OpcUaConstants
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.datatypemanager.impl.MapDataTypeManager

import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation
import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.node.{UaArgument, UaMethodNode}
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]

import scala.concurrent.ExecutionContext

import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers

import play.api.{Configuration, Mode}
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder


class RepositoryTest extends AsyncFunSuite with Matchers with OpcUaConstants:

  /** more info: https://stackoverflow.com/questions/62242989/why-my-scala-async-test-never-completes-when-i-do-await-result
    */
  override given executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global


  def createRepositories(): (NodeRepository, NamespaceRepository, ResourceRepository, DataTypeRepository) = 
    val tsOpcUa: Config = ConfigFactory.load("config/test_opcua.conf")
    val tsDb: Config = ConfigFactory.load("config/sqlite_db.conf")
    val module: AbstractModule = new AbstractModule:
      override def configure(): Unit = 
        bind(classOf[InitApp]).asEagerSingleton()
        bind(classOf[DataTypeManager]).toInstance(MapDataTypeManager())

    val injector =
      new GuiceApplicationBuilder()
        .configure(Configuration(tsOpcUa))
        .configure(Configuration(tsDb))
        .bindings(module)
        .bindings(bind[OpcUaLoader].to[AppOpcUaLoader])
        .bindings(bind[NodeRepository].to[NodeRepositoryImpl])
        .bindings(bind[NodeSetService].to[NodeSetServiceImpl])
        .bindings(bind[NamespaceRepository].to[NamespaceRepositoryImpl])
        .bindings(bind[ResourceRepository].to[ResourceRepositoryImpl])
        .bindings(bind[DataTypeRepository].to[DataTypeRepositoryImpl])
        .in(Mode.Test)
        .injector()

    val nodes = injector.instanceOf[NodeRepository]
    val namespaces = injector.instanceOf[NamespaceRepository]
    val resources = injector.instanceOf[ResourceRepository]
    val dataTypes = injector.instanceOf[DataTypeRepository]

    (nodes, namespaces, resources, dataTypes)
    
  end createRepositories
    
  
  test("Add namespace and read") {
    val (_, namespaces, _, _) = createRepositories()

    for {
      _      <- namespaces.create("Test Namespace")
      result <- namespaces.readAll()
    } yield result should contain ("Test Namespace")
  }


  test("Read Id by path"):
    val (nodes, _, resources, _) = createRepositories()

    val guid = "Test Server"
    val path = "PDM1/LLN0/Beh/stVal"
    val config =
      ResourceConfig(
        guid    = guid,
        connstr = "opc.tcp://Dell-NTB:4840/",
        policy  = None,
        auth    = None)

    val expected = UaId(6002, ModemTec)

    for
      _      <- resources.create(config)
      _      <- resources.attachNamespaces(guid, Set(ModemTec, IEC_61580_7_3, IEC_61580_7_4, OPCUAFOUNDATION))
      result <- nodes.readNodeId(guid, path)
    yield result shouldBe expected


  test("Read all configs"):
    val (_, _, resources, _) = createRepositories()

    val guid = "Test Server"
    val config =
      ResourceConfig(
        guid    = guid,
        connstr = "opc.tcp://Dell-NTB:4840/",
        policy  = None,
        auth    = None)

    val expected = config

    for
      _ <- resources.create(config)
      result <- resources.readAll()
    yield result.head shouldBe expected


  test("Read Method node"):
    val (nodes, _, _, _) = createRepositories()

    val nodeId = UaId(7001, ModemTec)          // /PDM1/CONF/Calib/setup
    val expected =
      UaMethodNode(
        id = UaId(7001, ModemTec),             // /Root/Objects/PDM1/CONF/Calib/setup
        parentId = Some(UaId(5017, ModemTec)), // /Root/Objects/PDM1/CONF/Calib
        browseName = "setup",
        displayName = "setup",
        description = None,
        input =
          Vector(
            UaArgument(
              isInput = true,
              name = "channel",
              typeId = UaId(3, OpcUaFoundation), //  UaByte
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 0),
            UaArgument(
              isInput = true,
              name = "attenuator",
              typeId = UaId(3, OpcUaFoundation), //  UaByte
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 1),
            UaArgument(
              isInput = true,
              name = "reference",
              typeId = UaId(7, OpcUaFoundation), // UaUInt32
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 2)),
        output =
          Vector(
            UaArgument(
              isInput = false,
              name = "result",
              typeId = UaId(3002, ModemTec),    //  MtRequestResult
              valueRank = -1,
              arrayDimensions = Vector.empty,
              ord = 0)))

    for 
      result <- nodes.readMethod(nodeId)
    yield result shouldBe expected

