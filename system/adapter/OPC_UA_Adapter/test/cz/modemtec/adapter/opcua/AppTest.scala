package cz.modemtec.adapter.opcua

import org.apache.pekko.Done
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.model.ws.Message
import org.apache.pekko.stream.scaladsl.{Sink, Source}

import com.google.inject.AbstractModule
import com.typesafe.config.{Config, ConfigFactory}

import cz.modemtec.adapter.api.data.spec.ResourceSpec
import cz.modemtec.adapter.api.data.{AdapterError, HistoryReadRequest, HistoryReadResult, ReadResult}
import cz.modemtec.adapter.api.data.config.*
import cz.modemtec.adapter.api.util.AdapterApiFormat
import cz.modemtec.adapter.opcua.api.util.client.impl.OpcUaAdapterClient
import cz.modemtec.adapter.api.util.MonadTransformer.*

import cz.modemtec.adapter.opcua.boot.{InitApp, OpcUaLoader, OptimizationTask}
import cz.modemtec.adapter.opcua.boot.impl.AppOpcUaLoader
import cz.modemtec.adapter.opcua.controller.{NodeController, NodeSetController, PkiController, ResourceController, ResourceSpecController}
import cz.modemtec.adapter.opcua.repository.impl.{NamespaceRepositoryImpl, NodeRepositoryImpl, PkiRepositoryImpl, ResourceRepositoryImpl}
import cz.modemtec.adapter.opcua.repository.{NamespaceRepository, NodeRepository, PkiRepository, ResourceRepository}
import cz.modemtec.adapter.opcua.service.{NodeSetService, PkiService, ResourceService, ResourceSpecService}
import cz.modemtec.adapter.opcua.service.impl.{NodeSetServiceImpl, PkiServiceImpl, ResourceServiceImpl, ResourceSpecServiceImpl}
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.datatypemanager.impl.MapDataTypeManager
import cz.modemtec.adapter.opcua.test.OpcUaConstants

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]

import org.scalatestplus.play.PlaySpec

import play.api.{Application, Configuration, Mode}
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsNumber, JsObject, JsString, JsValue, Json}
import play.api.libs.ws.WSClient
import play.api.test.*
import play.api.test.Helpers.*

import cats.syntax.all.*

import java.sql.Timestamp as SqlTimestamp

import scala.concurrent.duration.DurationInt
import scala.xml.Node as XmlNode
import scala.xml.XML
import scala.io.Source as IOSource
import scala.concurrent.{Await, ExecutionContext, Future, Promise}

//  todo: can App test be asynchronous?

class AppTest extends PlaySpec with AdapterApiFormat with OpcUaConstants:

  given executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  
  val testPort: Int = 19001

  /** Await for the results max 5 minutes.
    */
  def awaitFor[A](future: Future[A]): A = Await.result(future, 5.minute)

  def AppWith(opcUaConfig: String): Application =
    val tsOpcUa: Config = ConfigFactory.load(opcUaConfig)
    val tsDb: Config = ConfigFactory.load("config/sqlite_db.conf")
    val tsApp: Config = ConfigFactory.load("config/test_app.conf")
    val module: AbstractModule = new AbstractModule:
      override def configure(): Unit =
        bind(classOf[InitApp]).asEagerSingleton()
        bind(classOf[OptimizationTask]).asEagerSingleton()

        bind(classOf[NodeController]).asEagerSingleton()
        bind(classOf[ResourceController]).asEagerSingleton()
        bind(classOf[ResourceSpecController]).asEagerSingleton()
        bind(classOf[NodeSetController]).asEagerSingleton()
        bind(classOf[PkiController]).asEagerSingleton()

        bind(classOf[DataTypeManager]).toInstance(MapDataTypeManager())

    new GuiceApplicationBuilder()
      .configure(Configuration(tsOpcUa))
      .configure(Configuration(tsDb))
      .configure(Configuration(tsApp))

      .bindings(module)

      .bindings(bind[OpcUaLoader].to[AppOpcUaLoader])

      .bindings(bind[NodeRepository].to[NodeRepositoryImpl])
      .bindings(bind[NamespaceRepository].to[NamespaceRepositoryImpl])
      .bindings(bind[ResourceRepository].to[ResourceRepositoryImpl])
      .bindings(bind[PkiRepository].to[PkiRepositoryImpl])

      .bindings(bind[ResourceService].to[ResourceServiceImpl])
      .bindings(bind[NodeSetService].to[NodeSetServiceImpl])
      .bindings(bind[ResourceSpecService].to[ResourceSpecServiceImpl])
      .bindings(bind[PkiService].to[PkiServiceImpl])

      .appRoutes: app =>
        /*
             todo: how the route file can be used instead of this manual workaround?
        */
        case ("GET", "/node/testServer/PDM1/LLN0/Beh/stVal") =>
          app
            .injector
            .instanceOf(classOf[NodeController])
            .read("testServer", "PDM1/LLN0/Beh/stVal")

        case ("PUT", "/nodeset") =>
          app
            .injector
            .instanceOf(classOf[NodeSetController])
            .createOrUpdate()

        case ("POST", "/delete/nodeset/") =>
          app
            .injector
            .instanceOf(classOf[NodeSetController])
            .delete()

        case ("GET", "/nodeset/all") =>
          app
            .injector
            .instanceOf(classOf[NodeSetController])
            .readAll()

        case ("POST", "/node/history/testServer/PDM1/SPDC1/Beh/q") =>
          app
            .injector
            .instanceOf(classOf[NodeController])
            .historyRead("testServer", "PDM1/SPDC1/Beh/q")

        case ("PUT", "/resource/testServer") =>
          app
            .injector
            .instanceOf(classOf[ResourceController])
            .createOrUpdate("testServer")

        case ("GET", "/resource/testServer/spec") =>
          app
            .injector
            .instanceOf(classOf[ResourceSpecController])
            .spec("testServer")

        case ("GET", "/node/subscribe/testServer/PDM1/SPDC1/OpCnt/stVal") =>
          app
            .injector
            .instanceOf(classOf[NodeController])
            .subscribe("testServer", "/PDM1/SPDC1/OpCnt/stVal")

        case ("GET", "/node/testServer/Server/NamespaceArray") =>
          app
            .injector
            .instanceOf(classOf[NodeController])
            .read("testServer", "/Server/NamespaceArray")

      .in(Mode.Test)
      .build()

  end AppWith


  def loadXml(path: String): XmlNode =
    val text = IOSource.fromResource(path).mkString
    XML.loadString(text)


  def sendNodeSet(client: OpcUaAdapterClient, path: String): FutureEither[AdapterError, Unit] =
    val data = loadXml(path)
    client.createOrUpdateNamespace(data)


  "Read value" in new WithServer(app = AppWith("config/test_opcua.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val guid = "testServer"
      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, port, "localhost")

      val config =
        ResourceConfig(
          guid    = guid,
          connstr = "opc.tcp://localhost:4840/",
          policy  = None,
          auth    = None)

      val expected = Json.obj("value" -> JsNumber(3), "name" -> JsString("test")).asRight

      val action = for
        _      <- client.createOrUpdateResource(config)
        result <- client.readValue(guid, "/PDM1/LLN0/Beh/stVal")
      yield result.value

      val result = awaitFor(action.value)

      result mustBe expected


  "Create NodeSet" in new WithServer(app = AppWith("config/test_empty_opcua.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val guid = "testServer"
      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, port, "localhost")

      val config =
        ResourceConfig(
          guid    = guid,
          connstr = "opc.tcp://localhost:4840/",
          policy  = None,
          auth    = None)

      val expectedValue = Json.obj("value" -> JsNumber(3), "name" -> JsString("test"))
      val expectedNs = Set(ModemTec, IEC_61580_7_3, IEC_61580_7_4, OPCUAFOUNDATION)

      val expected = (expectedValue, expectedNs).asRight

      val action = for
        _      <- sendNodeSet(client, "opcua/Opc.Ua.IEC61850-7-3.NodeSet2.xml")
        _      <- sendNodeSet(client, "opcua/Opc.Ua.IEC61850-7-4.NodeSet2.xml")
        _      <- sendNodeSet(client, "opcua/ModemTec.PDM1.NodeSet2.xml")
        ns     <- client.readNamespaces()
        _      <- client.createOrUpdateResource(config)
        result <- client.readValue(guid, "/PDM1/LLN0/Beh/stVal")
      yield (result.value, ns.toSet)

      val result = awaitFor(action.value)

      result mustBe expected


  "Delete NodeSet" in new WithServer(app = AppWith("config/test_empty_opcua.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, port, "localhost")

      val expected = Vector(IEC_61580_7_3, IEC_61580_7_4, OPCUAFOUNDATION)

      val action = for
        _  <- sendNodeSet(client, "opcua/Opc.Ua.IEC61850-7-3.NodeSet2.xml")
        _  <- sendNodeSet(client, "opcua/Opc.Ua.IEC61850-7-4.NodeSet2.xml")
        _  <- sendNodeSet(client, "opcua/ModemTec.PDM1.NodeSet2.xml")
        _  <- client.deleteNamespace(ModemTec)
        ns <- client.readNamespaces()
      yield ns

      val result = awaitFor(action.value)

      result.isRight mustBe true

      result.foreach: values =>
        values mustBe expected


  "History read" in new WithServer(app = AppWith("config/test_opcua.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val guid = "testServer"
      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, port, "localhost")

      val config =
        ResourceConfig(
          guid    = guid,
          connstr = "opc.tcp://localhost:4840/",
          policy  = None,
          auth    = None)

      val request =
        HistoryReadRequest(
          from = SqlTimestamp.valueOf("2000-01-01 00:00:00"),
          to   = SqlTimestamp.valueOf("2025-01-01 00:00:00"))

      val sink = Sink.fold[HistoryReadResult, ReadResult](HistoryReadResult.empty): (result, value) =>
        result.copy(values = result.values :+ value)

      val expected =
        Vector(
          Json.obj(
            "validity"   -> Json.obj("value" -> 0, "name" -> "good"),
            "detailQual" ->
              Json.obj(
                "overflow"     -> false,
                "outOfRange"   -> false,
                "badReference" -> false,
                "oscillatory"  -> false,
                "failure"      -> false,
                "oldData"      -> false,
                "inconsistent" -> false,
                "inaccurate"   -> false),
            "source" -> Json.obj("value" -> 0, "name" -> "process"),
            "test"   -> false,
            "operatorBlocked" -> false),
          Json.obj(
            "validity"   -> Json.obj("value" -> 1, "name" -> "invalid"),
            "detailQual" ->
              Json.obj(
                "overflow"     -> true,
                "outOfRange"   -> true,
                "badReference" -> true,
                "oscillatory"  -> true,
                "failure"      -> false,
                "oldData"      -> false,
                "inconsistent" -> false,
                "inaccurate"   -> false),
            "source" -> Json.obj("value" -> 0, "name" -> "process"),
            "test"   -> true,
            "operatorBlocked" -> true),
          Json.obj(
            "validity"   -> Json.obj("value" -> 0, "name" -> "good"),
            "detailQual" ->
              Json.obj(
                "overflow"     -> false,
                "outOfRange"   -> false,
                "badReference" -> false,
                "oscillatory"  -> false,
                "failure"      -> false,
                "oldData"      -> false,
                "inconsistent" -> false,
                "inaccurate"   -> false),
            "source" -> Json.obj("value" -> 0, "name" -> "process"),
            "test"   -> false,
            "operatorBlocked" -> false))

      val action = for
        _      <- client.createOrUpdateResource(config)
        source <- client.historyRead(guid, "/PDM1/SPDC1/Beh/q", request)
      yield source

      val future =
        action
          .value
          .flatMap:
            case Left(error)   => throw error
            case Right(source) =>  source.runWith(sink)

      val history = awaitFor(future)

      val result = history.values.foldLeft(Vector.empty[JsValue]): (values, readResult) =>
        println(readResult)
        values :+ readResult.value

      result mustBe expected


  "Subscription" in new WithServer(app = AppWith("config/test_opcua.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val guid = "testServer"
      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, port, "localhost")

      val config =
        ResourceConfig(
          guid    = guid,
          connstr = "opc.tcp://localhost:4840/",
          policy  = None,
          auth    = None)

      val expected = Vector(JsNumber(0), JsNumber(1), JsNumber(2), JsNumber(3), JsNumber(4))
      var values = Vector.empty[ReadResult]

      val sink = Sink.foreach[ReadResult]: message =>
       values = values :+ message


      val cancellation = for
        _       <- client.createOrUpdateResource(config)
        promise <- client.subscribe(guid, "/PDM1/SPDC1/OpCnt/stVal", sink)
      yield promise

      val promise = awaitFor(cancellation.value) match
        case Left(error)  => throw error
        case Right(value) => value


      Thread.sleep(10000)

      promise.success(None)

      Thread.sleep(10000)

      values.map(_.value) containsSlice expected mustBe true


  "Resource doesn't exist, so ResourceSpec can't be created" in new WithServer(app = AppWith("config/test_opcua.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, port, "localhost")

      val action = client.readResourceSpec("testServer")

      val result = awaitFor(action.value)

      result.isLeft mustBe true


  "Read namespace array from secured resource" in new WithServer(app = AppWith("config/test_opcua_with_pki.conf"), port = testPort):
    override def running(): Unit =

      given actors: ActorSystem = app.actorSystem

      val guid = "testServer"
      val ws = app.injector.instanceOf[WSClient]
      val client = OpcUaAdapterClient(ws, testPort, "localhost")

      val config =
        ResourceConfig(
          guid    = guid,
          connstr = "opc.tcp://127.0.0.1:62541/milo",
          policy  = Some(SecurityPolicy.Basic256Sha256),
          auth    = Some(AuthConfig(
            username = "admin",
            password = "password")))

      val expected = Json.arr(
        "http://opcfoundation.org/UA/",
        "urn:eclipse:milo:opcua:server:1566c7d6-2d66-44b5-b395-eb0991840917",
        "urn:eclipse:milo:opcua:server:demo").asRight

      val action = for
        _      <- client.createOrUpdateResource(config)
        result <- client.readValue(guid, "/Server/NamespaceArray")
      yield result.value

      val result = awaitFor(action.value)

      result mustBe expected

