package cz.modemtec.adapter.opcua.service.impl

import com.google.inject.AbstractModule
import com.typesafe.config.{Config, ConfigFactory}

import cz.modemtec.adapter.api.util.MonadTransformer.*
import cz.modemtec.adapter.api.data.{AdapterError, WriteRequest}
import cz.modemtec.adapter.api.data.config.ResourceConfig

import cz.modemtec.adapter.opcua.repository.impl.{DataTypeRepositoryImpl, NamespaceRepositoryImpl, NodeRepositoryImpl, ResourceRepositoryImpl}
import cz.modemtec.adapter.opcua.repository.{DataTypeRepository, NamespaceRepository, NodeRepository, ResourceRepository}
import cz.modemtec.adapter.opcua.service.NodeSetService
import cz.modemtec.adapter.opcua.boot.impl.AppOpcUaLoader
import cz.modemtec.adapter.opcua.boot.{InitApp, OpcUaLoader}
import cz.modemtec.adapter.opcua.service.ResourceService
import cz.modemtec.adapter.opcua.test.NeedsRunningOpcUa
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.datatypemanager.impl.MapDataTypeManager

import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers

import cats.syntax.all.*

import play.api.inject.bind
import play.api.{Configuration, Mode}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsBoolean, JsNumber, JsString, Json}

import scala.concurrent.ExecutionContext
import scala.util.chaining.scalaUtilChainingOps


class ResourceServiceImplTest extends AsyncFunSuite with Matchers:


  /** more info: https://stackoverflow.com/questions/62242989/why-my-scala-async-test-never-completes-when-i-do-await-result
    */
  override given executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global


  def createService(): ResourceService = 
    val tsOpcUa: Config = ConfigFactory.load("config/test_opcua.conf")
    val tsDb: Config = ConfigFactory.load("config/sqlite_db.conf")
    val module: AbstractModule = new AbstractModule:
      override def configure(): Unit = 
        bind(classOf[InitApp]).asEagerSingleton()
        bind(classOf[DataTypeManager]).toInstance(MapDataTypeManager())

    val injector =
      new GuiceApplicationBuilder()
        .configure(Configuration(tsOpcUa))
        .configure(Configuration(tsDb))
        .bindings(module)
        .bindings(bind[OpcUaLoader].to[AppOpcUaLoader])
        .bindings(bind[NodeRepository].to[NodeRepositoryImpl])
        .bindings(bind[NamespaceRepository].to[NamespaceRepositoryImpl])
        .bindings(bind[ResourceRepository].to[ResourceRepositoryImpl])
        .bindings(bind[DataTypeRepository].to[DataTypeRepositoryImpl])
        .bindings(bind[NodeSetService].to[NodeSetServiceImpl])
        .bindings(bind[ResourceService].to[ResourceServiceImpl])
        .in(Mode.Test)
        .injector()

    injector.instanceOf[ResourceService]

  end createService
  

  test("Create resource and read node /PDM1/LLN0/Beh/stVal", NeedsRunningOpcUa):
    val service = createService()
    val guid = "test-server-open62541"
    val path = "PDM1/LLN0/Beh/stVal"

    val expected = Json.obj("value" -> JsNumber(3), "name" -> JsString("test")).asRight

    val config =
      ResourceConfig(
        guid    = guid,
        connstr = "opc.tcp://127.0.0.1:4840",
        policy  = None,
        auth    = None)

    val action = for 
      _      <- service.createOrUpdateResource(config)
      _      <- service.checkResource(guid)
      nodeId <- service.readNodeId(guid, path)
      result <- service.read(guid, nodeId)
      _      <- service.deleteResource(guid)
    yield result.value

    action
      .value
      .map: result =>
        result shouldBe expected
  

  test("Create resource, read from 8 nodes.", NeedsRunningOpcUa):
    val service = createService()
    val guid = "test-server-open62541"

    val paths =
      Vector(
        "PDM1/SPDC1/Beh/stVal",
        "PDM1/SPDC2/Beh/stVal",
        "PDM1/SPDC3/Beh/stVal",
        "PDM1/SPDC4/Beh/stVal",
        "PDM1/SPDC5/Beh/stVal",
        "PDM1/SPDC6/Beh/stVal",
        "PDM1/SPDC7/Beh/stVal",
        "PDM1/SPDC8/Beh/stVal")

    val config =
      ResourceConfig(
        guid = guid,
        connstr = "opc.tcp://127.0.0.1:4840",
        policy = None,
        auth = None)

    val action = for 
      _ <- service.createOrUpdateResource(config)
      _ <- service.checkResource(guid)
      results <-
        paths
          .map: path =>
            for 
              nodeId <- service.readNodeId(guid, path)
              result <- service.read(guid, nodeId)
            yield result
          .pipe: futureEithers =>
            FutureEither.sequence(futureEithers)
      _ <- service.deleteResource(guid)
    yield results

    action
      .value
      .map: results =>
        results.isRight shouldBe true


  test("Test optimization during after reading from node /PDM1/SPDC1/OpCnt/stVal", NeedsRunningOpcUa):
    val service = createService()
    val guid = "test-server-open62541"
    val path = "PDM1/LLN0/Beh/stVal"

    val config =
      ResourceConfig(
        guid    = guid,
        connstr = "opc.tcp://127.0.0.1:4840",
        policy  = None,
        auth    = None)

    val action = for 
      _      <- service.createOrUpdateResource(config)
      _      <- service.checkResource(guid)
      nodeId <- service.readNodeId(guid, path)
      _      <- service.read(guid, nodeId)
      _      <- FutureEither.right(service.optimizeResources())
      _      <- service.deleteResource(guid)
    yield ()

    action
      .value
      .map:
        case Left(_)  => fail()
        case Right(_) => succeed


  test("Test optimization during active subscription on node /PDM1/SPDC1/OpCnt/stVal", NeedsRunningOpcUa):
    val service = createService()
    val guid = "test-server-open62541"
    val path = "/PDM1/SPDC1/OpCnt/stVal"

    val config =
      ResourceConfig(
        guid    = guid,
        connstr = "opc.tcp://127.0.0.1:4840",
        policy  = None,
        auth    = None)

    val subscribe = for 
      _      <- service.createOrUpdateResource(config)
      _      <- service.checkResource(guid)
      nodeId <- service.readNodeId(guid, path)
      subId  <- service.createSubscription(config.guid, nodeId)(_ => ())
    yield subId

    Thread.sleep(5000)
    service.optimizeResources()

    val cancel = for 
      subId <- subscribe
      _     <- FutureEither.right(Thread.sleep(5000))
      _     <- service.deleteSubscription(guid, subId)
      _     <- service.deleteResource(guid)
    yield ()

    cancel
      .value
      .map:
        case Left(_)  => fail()
        case Right(_) => succeed


  test("Create resource, write and read node /PDM1/LLN0/Beh/q", NeedsRunningOpcUa):
    val service = createService()
    val guid = "test-server-open62541"
    val path = "PDM1/LLN0/Beh/q"

    val value =
      Json.obj(
        "validity" -> Json.obj("value" -> JsNumber(0), "name" -> JsString("good")),
        "detailQual" ->
          Json.obj(
            "overflow"     -> JsBoolean(true),
            "outOfRange"   -> JsBoolean(false),
            "badReference" -> JsBoolean(true),
            "oscillatory"  -> JsBoolean(false),
            "failure"      -> JsBoolean(true),
            "oldData"      -> JsBoolean(false),
            "inconsistent" -> JsBoolean(true),
            "inaccurate"   -> JsBoolean(false)),
        "source" -> Json.obj("value" -> JsNumber(0), "name" -> JsString("process")),
        "test" -> JsBoolean(false),
        "operatorBlocked" -> JsBoolean(true))

    val config =
      ResourceConfig(
        guid    = guid,
        connstr = "opc.tcp://127.0.0.1:4840",
        policy  = None,
        auth    = None)

    val request = WriteRequest(value)

    val action = for 
      _      <- service.createOrUpdateResource(config)
      _      <- service.checkResource(guid)
      nodeId <- service.readNodeId(guid, path)
      result <- service.write(config.guid, nodeId, request)
      _      <- service.deleteResource(guid)
    yield result.value

    action
      .value
      .map: result =>
        result shouldBe value

