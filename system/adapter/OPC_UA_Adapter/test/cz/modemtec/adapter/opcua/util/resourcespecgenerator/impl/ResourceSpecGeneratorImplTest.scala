package cz.modemtec.adapter.opcua.util.resourcespecgenerator.impl

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.util.AdapterApiFormat

import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.node.{UaNode, UaNodeSet}
import cz.modemtec.scopcua.util.jsonschemagenerator.impl.UaModemTecJsonSchemaGenerator
import cz.modemtec.scopcua.util.nodesetextractor.impl.UaNodeSet1v04Extractor

import org.scalatest.funsuite.AnyFunSuite
import play.api.libs.json.Json

import scala.xml.XML
import scala.io.Source


class ResourceSpecGeneratorImplTest extends AnyFunSuite with AdapterApiFormat:

  private val generator = ResourceSpecGeneratorImpl(types => UaModemTecJsonSchemaGenerator(types))


  def loadNodeSet(path: String): UaNodeSet =
    val text = Source.fromResource(path).mkString
    val xml = XML.loadString(text)
    UaNodeSet1v04Extractor.extract(xml)


  test("Just show result."):
    val resource =
      ResourceConfig(
        guid    = "TestServer",
        connstr = "address",
        policy  = None,
        auth    = None)

    val paths =
      Vector(
        "opcua/Opc.Ua.NodeSet2.xml",
        "opcua/Opc.Ua.IEC61850-7-3.NodeSet2.xml",
        "opcua/Opc.Ua.IEC61850-7-4.NodeSet2.xml",
        "opcua/ModemTec.PDM1.NodeSet2.xml")

    val (nodes, dataTypes) = paths.foldLeft((Map.empty[UaId, UaNode], Map.empty[UaId, UaDataType])):
      case ((nodes, dataTypes), path) =>
        val nodeset = loadNodeSet(path)
        (nodes ++ nodeset.nodes, dataTypes ++ nodeset.dataTypes)

    val result = generator.generate(resource, nodes, dataTypes)

    val serverSubtree = result.nodes.count { case (path, _) => path.startsWith("/Server") }
    val pdm1Subtree = result.nodes.count { case (path, _) => path.startsWith("/PDM1") }

    println(s"Server subtree size: $serverSubtree")
    println(s"PDM1 subtree size: $pdm1Subtree")

    println(result)

    println(result.schemas.keySet)


  test("Just show result in JSON."):
    val resource =
      ResourceConfig(
        guid = "TestServer",
        connstr = "address",
        policy = None,
        auth = None)

    val paths =
      Vector(
        "opcua/Opc.Ua.NodeSet2.xml",
        "opcua/Opc.Ua.IEC61850-7-3.NodeSet2.xml",
        "opcua/Opc.Ua.IEC61850-7-4.NodeSet2.xml",
        "opcua/ModemTec.PDM1.NodeSet2.xml")

    val (nodes, dataTypes) = paths.foldLeft((Map.empty[UaId, UaNode], Map.empty[UaId, UaDataType])):
      case ((nodes, dataTypes), path) =>
        val nodeset = loadNodeSet(path)
        (nodes ++ nodeset.nodes, dataTypes ++ nodeset.dataTypes)

    val result = generator.generate(resource, nodes, dataTypes)

    val json = Json.toJson(result)
    val text = Json.prettyPrint(json)

    println(text)
