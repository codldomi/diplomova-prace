--
-- File generated with SQLiteStudio v3.4.4 on p� z�� 29 21:08:13 2023
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: t_argument
DROP TABLE IF EXISTS t_argument;
CREATE TABLE IF NOT EXISTS t_argument (c_id INTEGER NOT NULL CONSTRAINT t_argument_pk PRIMARY KEY AUTOINCREMENT, c_is_input INTEGER NOT NULL, c_type_id INTEGER NOT NULL CONSTRAINT t_type_id___fk REFERENCES t_id ON UPDATE CASCADE ON DELETE CASCADE, c_node_id INTEGER NOT NULL CONSTRAINT t_node_id___fk REFERENCES t_node ON UPDATE CASCADE ON DELETE CASCADE, c_ord INTEGER NOT NULL, c_name TEXT NOT NULL, c_value_rank INTEGER NOT NULL, c_array_dimensions TEXT NOT NULL);

-- Table: t_certificate
DROP TABLE IF EXISTS t_certificate;
CREATE TABLE IF NOT EXISTS t_certificate (c_id INTEGER PRIMARY KEY NOT NULL, c_guid TEXT, c_encoded TEXT NOT NULL, c_type INTEGER NOT NULL);

-- Table: t_data_type
DROP TABLE IF EXISTS t_data_type;
CREATE TABLE IF NOT EXISTS t_data_type (c_id INTEGER PRIMARY KEY REFERENCES t_id (c_id) NOT NULL, c_super_id INTEGER REFERENCES t_id (c_id), c_name TEXT NOT NULL, c_is_abstract INTEGER NOT NULL, c_type INTEGER NOT NULL);

-- Table: t_enum_field
DROP TABLE IF EXISTS t_enum_field;
CREATE TABLE IF NOT EXISTS t_enum_field (c_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, c_data_type_id INTEGER REFERENCES t_data_type (c_id) NOT NULL, c_name TEXT NOT NULL, c_value INTEGER NOT NULL);

-- Table: t_id
DROP TABLE IF EXISTS t_id;
CREATE TABLE IF NOT EXISTS t_id (c_id INTEGER NOT NULL CONSTRAINT t_id_pk PRIMARY KEY AUTOINCREMENT, c_index INTEGER NOT NULL, c_namespace_id INTEGER NOT NULL CONSTRAINT t_namespace_id___fk REFERENCES t_namespace (c_id) ON DELETE CASCADE ON UPDATE CASCADE);

-- Table: t_namespace
DROP TABLE IF EXISTS t_namespace;
CREATE TABLE IF NOT EXISTS t_namespace (c_id integer NOT NULL CONSTRAINT t_namespace_pk PRIMARY KEY AUTOINCREMENT, c_uri text NOT NULL);

-- Table: t_node
DROP TABLE IF EXISTS t_node;
CREATE TABLE IF NOT EXISTS t_node (c_id INTEGER NOT NULL CONSTRAINT t_node_pk PRIMARY KEY REFERENCES t_id ON UPDATE CASCADE ON DELETE CASCADE, c_browse_name TEXT NOT NULL, c_type INTEGER NOT NULL, c_data_type_id INTEGER REFERENCES t_id ON UPDATE CASCADE ON DELETE CASCADE, c_parent_id int CONSTRAINT t_node_t_id_id_fk REFERENCES t_id DEFERRABLE INITIALLY DEFERRED, c_display_name TEXT NOT NULL, c_description TEXT, c_value_rank INTEGER, c_array_dimensions TEXT);

-- Table: t_resource
DROP TABLE IF EXISTS t_resource;
CREATE TABLE IF NOT EXISTS t_resource (c_id INTEGER NOT NULL CONSTRAINT t_server_pk PRIMARY KEY AUTOINCREMENT, c_url TEXT NOT NULL, c_guid TEXT NOT NULL UNIQUE, c_security_policy INTEGER, c_username TEXT, c_password TEXT);

-- Table: t_resource_namespace
DROP TABLE IF EXISTS t_resource_namespace;
CREATE TABLE IF NOT EXISTS t_resource_namespace (c_id integer NOT NULL CONSTRAINT t_server_namespace_pk PRIMARY KEY AUTOINCREMENT, c_resource_id INTEGER NOT NULL REFERENCES t_resource (c_id), c_namespace_id INTEGER NOT NULL CONSTRAINT t_server_namespace_t_namespace_id_fk REFERENCES t_namespace DEFERRABLE INITIALLY DEFERRED);

-- Table: t_struct_field
DROP TABLE IF EXISTS t_struct_field;
CREATE TABLE IF NOT EXISTS t_struct_field (c_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, c_name TEXT NOT NULL, c_structure_id INTEGER NOT NULL REFERENCES t_data_type (c_id), c_type_id INTEGER NOT NULL REFERENCES t_data_type (c_id), c_value_rank INTEGER NOT NULL, c_array_dimensions TEXT NOT NULL, c_is_optional INTEGER NOT NULL, c_ord INTEGER NOT NULL);

-- Index: t_argument_id_uindex
DROP INDEX IF EXISTS t_argument_id_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_argument_id_uindex ON t_argument (c_id);

-- Index: t_id_id_uindex
DROP INDEX IF EXISTS t_id_id_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_id_id_uindex ON t_id (c_id);

-- Index: t_namespace_c_uri_uindex
DROP INDEX IF EXISTS t_namespace_c_uri_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_namespace_c_uri_uindex ON t_namespace (c_uri);

-- Index: t_namespace_id_uindex
DROP INDEX IF EXISTS t_namespace_id_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_namespace_id_uindex ON t_namespace (c_id);

-- Index: t_node_id_uindex
DROP INDEX IF EXISTS t_node_id_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_node_id_uindex ON t_node (c_id);

-- Index: t_server_c_alias_uindex
DROP INDEX IF EXISTS t_server_c_alias_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_server_c_alias_uindex ON t_resource (c_guid);

-- Index: t_server_id_uindex
DROP INDEX IF EXISTS t_server_id_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_server_id_uindex ON t_resource (c_id);

-- Index: t_server_namespace_id_uindex
DROP INDEX IF EXISTS t_server_namespace_id_uindex;
CREATE UNIQUE INDEX IF NOT EXISTS t_server_namespace_id_uindex ON t_resource_namespace (c_id);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
