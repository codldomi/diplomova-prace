logLevel := Level.Warn

resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("org.playframework" % "sbt-plugin" % "3.0.0")
//  IDE SPECIFIC
addSbtPlugin("org.jetbrains.scala" % "sbt-ide-settings" % "1.1.1")

addDependencyTreePlugin