## OPC UA Adapter

Version 0.1.0

The application provides a mapping between HTTP methods and OPC UA services. Also, it can be used as a gateway - the app
stores connections to OPC UA servers. Supported information models are loaded as NodeSet2.xml files and provided as 
OpenAPI specifications. The format of exchanged data is JSON.

---

### Language

- [Scala](https://www.scala-lang.org/) 3.3.1
- conventions as [Scala Style Guide](https://docs.scala-lang.org/style/)
- building tool [sbt](https://www.scala-sbt.org/) 1.9.6
- JDK 17 [Eclipse Temurin by Adoptium](https://adoptium.net/temurin/releases/)

### Development Environment

- [IntelliJ IDEA](https://www.jetbrains.com/idea/) (Community or Ultimate) with [plugin Scala](https://www.jetbrains.com/help/idea/discover-intellij-idea-for-scala.html)

### Documentation

- in Czech language
- web pages generated from [Markdown](https://www.markdownguide.org/) with tool [MkDocs](https://www.mkdocs.org/)
- in terminal executing ```mkdocs build``` creates directory ```site``` with documentation

### Run the App

- the App has its inner state (data types or clients in memory), so turn off auto-reloading of Play application
- in terminal ```sbt run``` starts application in development mode
- in terminal ```sbt dist``` cleans, creates packages and script in production mode

  - creates file ```target/universal/opc-ua-adapter-0.1.0.zip``` with all libraries
  - in terminal ```opc-ua-adapter-0.1.0/bin/opc-ua-adapter -Dplay.http.secret.key=ad31779d4ee49d5ad5162bf1429c32e2e9933f3b``` 
  runs application in production mode with the given secret key (for Windows there is file with .bat postfix)

### Dockerize the App

- in terminal ```sbt docker:publishLocal``` creates an image by instructions in ```build.sbt```
- in terminal ```docker run --rm -p 9000:9000 opc-ua-adapter``` runs a docker container

### Used Projects

- [Scalable OPC UA](http://gitlab.intranet.modemtec.cz/products/server/pdds/libs/scalable-opc-ua)
- [Adapter API](http://gitlab.intranet.modemtec.cz/products/server/pdds/adapter/adapter-api)