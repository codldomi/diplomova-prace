package cz.modemtec.adapter.opcua.util.datatypemanager.impl

import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.scopcua.data.{UaDataType, UaId}

import java.util.concurrent.ConcurrentHashMap
import scala.jdk.CollectionConverters.MapHasAsJava


/** A concurrent map as data type manager. The class is thread-safe.
  *
  * @param types Data types to manage.
  */
class MapDataTypeManager private (val types: ConcurrentHashMap[UaId, UaDataType]) extends DataTypeManager:

  override def apply(id: UaId): UaDataType = types.get(id)

  override def add(dataType: UaDataType): Unit = types.put(dataType.id, dataType)

  override def remove(id: UaId): Unit = types.remove(id)

  override def size: Int = types.size()

  override def clear(): Unit = types.clear()

  override def isDefinedAt(id: UaId): Boolean = types.contains(id)

  override def filter(f: UaDataType => Boolean): Vector[UaDataType] = 
    val it = types.elements()
    var dataTypes = Vector.empty[UaDataType]

    while it.hasMoreElements do
      val dataType = it.nextElement()
      if f(dataType) then
        dataTypes = dataTypes :+ dataType

    dataTypes


object MapDataTypeManager:

  def apply(types: Map[UaId, UaDataType] = Map.empty): MapDataTypeManager = 
    val threadSafeMap = new ConcurrentHashMap[UaId, UaDataType](types.asJava)
    new MapDataTypeManager(threadSafeMap)

