package cz.modemtec.adapter.opcua.service

import com.google.inject.*

import cz.modemtec.adapter.api.data.{AdapterError, CertData}
import cz.modemtec.adapter.api.util.MonadTransformer.FutureEither

import cz.modemtec.adapter.opcua.service.impl.PkiServiceImpl

import cz.modemtec.scopcua.util.trustlistmanager.UaTrustListManager

import scala.concurrent.Future


/** The service interface providing managing Public Key Infrastructure for resources.
  */
@ImplementedBy(classOf[PkiServiceImpl])
trait PkiService:

  /*
       ---
       Methods useful for controllers and API. Contain also error handling.
       ---
   */

  /** Creates or updates certificate with its metadata.
    *
    * @param guid Certificate identifier.
    * @param data Certificate and its metadata.
    * @return Certificate with metadata.
    */
  def createOrUpdateTrusted(guid: String, data: CertData): FutureEither[AdapterError, CertData]

  /** Deletes certificate.
    *
    * @param guid Certificate identifier.
    * @return
    */
  def deleteTrusted(guid: String): FutureEither[AdapterError, Unit]

  /** Reads certificate with its metadata.
    *
    * @param guid Certificate identifier.
    * @return Certificate and its metadata.
    */
  def readTrusted(guid: String): FutureEither[AdapterError, CertData]

  /** Reads all trusted certificates GUIDs.
    *
    * @return Vector of certification GUID.
    */
  def readAllTrustedGuids(): FutureEither[AdapterError, Vector[String]]

  /*
       ---
       Methods useful for other app components.
       ---
   */

  /** Gets trust list manager for OPC UA.
    *
    * @return TrustListManager.
    */
  def trustListManager: UaTrustListManager

  /** Deletes all stored certificates.
    */
  def deleteAll(): Future[Unit]
