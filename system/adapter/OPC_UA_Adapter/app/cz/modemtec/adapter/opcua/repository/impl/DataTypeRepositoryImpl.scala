package cz.modemtec.adapter.opcua.repository.impl

import cz.modemtec.adapter.opcua.repository.{DataTypeRepository, DatabaseSchema, QueryExecutor}

import cz.modemtec.scopcua.conversion.ConvertFromUa.ToAll.given Conversion[?, ?]
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.value.UaString
import cz.modemtec.scopcua.data.value.number.integer.{UaInt32, UaUInt32}
import cz.modemtec.scopcua.data.{UaDataType, UaId}

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import javax.inject.*
import scala.concurrent.{ExecutionContext, Future}


@Singleton()
class DataTypeRepositoryImpl @Inject()
  (protected val dbConfigProvider: DatabaseConfigProvider)
  (using val executionContext: ExecutionContext)
  extends DataTypeRepository
    with DatabaseSchema
    with QueryExecutor
    with HasDatabaseConfigProvider[JdbcProfile]:

  import profile.api.*


  extension (self: String)
    private def asVector: Vector[UaUInt32] =
      self
        .split(",")
        .filter(_ != "")
        .map(_.toLong)
        .map(UaUInt32.apply)
        .toVector


  extension (self: Vector[UaUInt32])
    private def asString: String =
      self
        .map(_.value)
        .mkString(",")


  extension (self: Long)
    private def asNodeId: DBIO[UaId] =
      val rows = for
        ident <- Ids if ident.id === self
        ns    <- Namespaces if ns.id === ident.namespaceId
      yield (ident.index, ns.uri)

      rows.result.head.map: (index, uri) =>
        UaId(index, uri)


  extension (self: Option[Long])
    private def asNodeId: DBIO[Option[UaId]] = self match
      case Some(value) => value.asNodeId.map(nodeId => Some(nodeId))
      case None        => DBIO.successful(None)


  private def createStructDataType(dataType: UaDataType.GeneralStructure, ids: Map[UaId, Long]): DBIO[Int] =
    val structId = ids(dataType.id)
    val superId = dataType.superId.map(ids)
    for
      num <- DataTypes += DataTypeRow(structId, superId, dataType.name, dataType.isAbstract, DataTypeType.Struct)
      _ <- {
        val actions =
          dataType
            .fields
            .map: field =>
              val typeId = ids(field.typeId)
              StructFields += StructFieldRow(0, structId, typeId, field.name, field.valueRank, field.arrayDimensions.asString, field.isOptional, field.ord)

        DBIO.sequence(actions)
      }
    yield num


  private def createEnumDataType(dataType: UaDataType.GeneralEnumeration, ids: Map[UaId, Long]): DBIO[Int] =
    val enumId = ids(dataType.id)
    val superId = dataType.superId.map(ids)
    for
      num <- DataTypes += DataTypeRow(enumId, superId, dataType.name, dataType.isAbstract, DataTypeType.Enum)
      _ <- {
        val actions =
          dataType
            .values
            .map: (name, value) =>
              EnumFields += EnumFieldRow(0, enumId, name, value)
        DBIO.sequence(actions)
      }
    yield num


  private def createBuiltInDataType(dataType: UaDataType.GeneralSimple, ids: Map[UaId, Long]): DBIO[Int] =
    val id = ids(dataType.id)
    val superId = dataType.superId.map(ids)
    DataTypes += DataTypeRow(id, superId, dataType.name, dataType.isAbstract, DataTypeType.BuiltIn)


  private def createDataType(dataType: UaDataType, ids: Map[UaId, Long]): DBIO[Int] = dataType match
    case enumeration:   UaDataType.GeneralEnumeration => createEnumDataType(enumeration, ids)
    case struct:        UaDataType.GeneralStructure   => createStructDataType(struct, ids)
    case builtin =>
      val simple = UaDataType.GeneralSimple(builtin.isAbstract, builtin.name, builtin.id, builtin.superId)
      createBuiltInDataType(simple, ids)


  private def readNamespaces(): DBIO[Map[String, Long]] =
    Namespaces
      .result
      .map(namespaces => namespaces.map(namespace => namespace.uri -> namespace.id).toMap)


  private def createIds(namespaces: Map[String, Long], types: Iterable[UaDataType]): DBIO[Map[UaId, Long]] =
    val ids = types.foldLeft(Set.empty[UaId]): (accu, dataType) =>
      val specific = dataType match
        case UaDataType.GeneralStructure(_, _, _, _, fields) => fields.map(_.typeId).toSet
        case _ => Set.empty[UaId]

      val superId = dataType.superId match
        case Some(value) => Set(value)
        case None        => Set.empty[UaId]

      accu ++ specific ++ superId + dataType.id

    val table = Ids returning Ids.map(_.id) into { (row, id) => row.copy(id = id) }
    val rows = ids.map: nodeId =>
      val row = IdRow(0, nodeId.index.toInt, namespaces(nodeId.namespaceUri))
      nodeId -> row

    val actions =
      rows
        .map: (nodeId, row) =>
          (table += row).map(row => nodeId -> row.id)
        .toSeq

    DBIO.sequence(actions).map(pairs => pairs.toMap)


  override def createAll(types: Iterable[UaDataType]): Future[Unit] =
    val insertion = for
      namespaces <- readNamespaces()
      ids <- createIds(namespaces, types)
      _ <- {
        val actions = types.map(dataType => createDataType(dataType, ids))
        DBIO.sequence(actions)
      }
    yield ()

    transaction(insertion)


  override def deleteAll(): Future[Unit] =
    val deletion = for
      _ <- EnumFields.delete
      _ <- StructFields.delete
      _ <- DataTypes.delete
    yield ()

    transaction(deletion)


  private def readSimpleDataType(row: DataTypeRow): DBIO[UaDataType] =

    def toBuiltIn(simple: UaDataType.GeneralSimple): UaDataType = UaDataType.BuiltIns(simple.id)

    for
      nodeId <- row.id.asNodeId
      superNodeId <- row.superId.asNodeId
    yield toBuiltIn(UaDataType.GeneralSimple(row.isAbstract, row.name, nodeId, superNodeId))


  private def readEnumDataType(row: DataTypeRow): DBIO[UaDataType.GeneralEnumeration] = for
    nodeId      <- row.id.asNodeId
    superNodeId <- row.superId.asNodeId
    fields <-
      EnumFields
        .filter(_.dataTypeId === row.id)
        .result
        .map(rows =>
          rows
            .map(field => (UaString(field.name), UaInt32(field.value)))
            .toMap)
  yield UaDataType.GeneralEnumeration(row.isAbstract, row.name, nodeId, superNodeId, fields)


  private def readStructDataType(row: DataTypeRow): DBIO[UaDataType.GeneralStructure] = for
    nodeId      <- row.id.asNodeId
    superNodeId <- row.superId.asNodeId
    rows <-
      StructFields
        .filter(_.structureId === row.id)
        .sortBy(_.ord)
        .result
    fields <- {
      val actions =
        rows
          .map: field =>
            for
              typeId <- field.typeId.asNodeId
            yield UaDataType.GeneralStructure.Field(field.name, typeId, field.valueRank, field.arrayDimensions.asVector, field.isOptional, field.ord)
      DBIO.sequence(actions.toVector)
    }
  yield UaDataType.GeneralStructure(row.isAbstract, row.name, nodeId, superNodeId, fields)


  override def readAll(): Future[Vector[UaDataType]] =
    val selection = for
      rows      <- DataTypes.result
      dataTypes <- {
        val actions = rows.map: row =>
          row.dataTypeType match
            case DataTypeType.Enum    => readEnumDataType(row)
            case DataTypeType.BuiltIn => readSimpleDataType(row)
            case DataTypeType.Struct  => readStructDataType(row)
        DBIO.sequence(actions.toVector)
      }
    yield dataTypes

    transaction(selection)


  override def read(uri: String): Future[Vector[UaDataType]] =
    val selection = for
      rows <-
        Namespaces
          .filter(_.uri === uri)
          .join(Ids).on(_.id === _.namespaceId)
          .join(DataTypes).on { case ((_, ident), dataType) => dataType.id === ident.id }
          .map { case (_, dataType) => dataType }
          .result

      dataTypes <- {
        val actions = rows.map: row =>
          row.dataTypeType match
            case DataTypeType.Enum    => readEnumDataType(row)
            case DataTypeType.BuiltIn => readSimpleDataType(row)
            case DataTypeType.Struct  => readStructDataType(row)
        DBIO.sequence(actions.toVector)
      }

    yield dataTypes

    transaction(selection)


