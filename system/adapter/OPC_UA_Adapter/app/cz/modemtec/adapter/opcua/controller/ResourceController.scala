package cz.modemtec.adapter.opcua.controller

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.util.AdapterApiFormat

import cz.modemtec.adapter.opcua.service.ResourceService
import cz.modemtec.adapter.opcua.util.HttpExtension

import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import com.google.inject.*
import scala.concurrent.ExecutionContext


@Singleton()
class ResourceController @Inject() (
  val controllerComponents: ControllerComponents,
  val resources: ResourceService
)(using val context: ExecutionContext) extends BaseController with AdapterApiFormat with HttpExtension:

  /** Modifies or creates resource config.
    *
    * Responses:
    *
    *   - 200: ResourceConfig - Successful operation.
    *   - 400: AdapterError - Bad data in resource config.
    *   - 500: AdapterError - Error during operation occurred.
    *
    * @param guid Resource globally unique identifier.
    */
  def createOrUpdate(guid: String): Action[ResourceConfig] = Action(parse.json[ResourceConfig]).async: request =>
    resources
      .createOrUpdateResource(request.body)
      .value
      .map(config => config.asHttpResult)

  /** Reads config for the resource with the given GUID.
    *
    * Responses:
    *
    *   - 200: ResourceConfig - Successful operation.
    *   - 404: AdapterError - Resource doesn't exist.
    *   - 500: AdapterError - Error during operation occurred.
    *
    * @param guid Resource globally unique identifier.
    */
  def read(guid: String): Action[AnyContent] = Action.async:
    resources
      .readResource(guid)
      .value
      .map(config => config.asHttpResult)

  /** Deletes resource by the given GUID.
    *
    * Responses:
    *
    *   - 200: Nothing - Successful operation
    *   - 500: AdapterError - Error during operation occurred.
    *
    * @param guid Resource globally unique identifier.
    */
  def delete(guid: String): Action[AnyContent] = Action.async:
    resources
      .deleteResource(guid)
      .value
      .map(_.asEmptyHttpResult)

  /** Reads all stored resource configurations.
    *
    * Responses:
    *
    *   - 200: ResourceConfig - Successful operation.
    *   - 404: AdapterError - Resource doesn't exist.
    *   - 500: AdapterError - Error during operation occurred.
    */
  def readAll: Action[AnyContent] = Action.async:
    resources
      .readAll()
      .value
      .map(configs => configs.asHttpResult)
    