package cz.modemtec.adapter.opcua.boot

import com.google.inject.ImplementedBy
import cz.modemtec.adapter.api.data.CertData
import cz.modemtec.adapter.opcua.boot.impl.AppOpcUaLoader
import cz.modemtec.scopcua.data.config.UaClientConfig

import scala.xml.Node as XmlNode


/** The loader interface providing loading OPC UA data.
  */
@ImplementedBy(classOf[AppOpcUaLoader])
trait OpcUaLoader:

  /** Loads stored OPC UA Client configuration. It's typically the default configuration for the resources.
    *
    * @return OPC UA Client configuration.
    */
  def loadClientConfig(): UaClientConfig

  /** Loads paths to files containing OPC UA information models.
    *
    * @return File paths.
    */
  def loadNodeSets(): Vector[String]

  /** Loads default trusted certificates.
    *
    * @return Trusted certificates with metadata.
    */
  def loadTrustedCerts(): Vector[CertData]

  /** Loads OPC UA NodeSet from the file.
    *
    * @param path File path.
    * @return OPC UA NodeSet in its standard XML format.
    */
  def loadNodeSet(path: String): XmlNode

  /** Loads wantCleanUp variable.
    *
    * @return True, if the database clean up must be performed before starting the application.
    */
  def wantCleanUp: Boolean

