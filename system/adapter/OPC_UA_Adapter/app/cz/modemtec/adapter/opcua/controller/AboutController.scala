package cz.modemtec.adapter.opcua.controller

import cz.modemtec.adapter.api.data.AdapterInfo
import cz.modemtec.adapter.api.util.AdapterApiFormat

import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import play.api.libs.json.Json

import com.google.inject.*
import scala.concurrent.ExecutionContext


@Singleton()
class AboutController @Inject() (
 val controllerComponents: ControllerComponents
)(using val context: ExecutionContext) extends BaseController with AdapterApiFormat:

  /** Reads information about adapter.
    *
    * Responses:
    *
    *   - 200: AdapterInfo - Successful operation.
    */
  def info(): Action[AnyContent] = Action:
    val info =
      AdapterInfo(
        name = "OPC UA Adapter",
        protocol = "opc.tcp",
        adapterVersion = "0.1.0",
        apiVersion = "0.1.0")
    val json = Json.toJson(info)
    Ok(json)
  
  /** Checks whether service is available.
    *
    * Responses:
    *
    *   - 200: Nothing - Successful operation.
    */
  def health(): Action[AnyContent] = Action:
    Ok
  