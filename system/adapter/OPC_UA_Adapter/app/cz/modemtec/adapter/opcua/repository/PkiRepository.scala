package cz.modemtec.adapter.opcua.repository

import com.google.inject.*
import cz.modemtec.adapter.api.data.CertData
import cz.modemtec.adapter.opcua.repository.impl.PkiRepositoryImpl

import java.security.cert.X509Certificate
import scala.concurrent.Future


/** The repository interface providing operations on Public Key Infrastructure.
  */
@ImplementedBy(classOf[PkiRepositoryImpl])
trait PkiRepository:

  /** Reads trusted certificate by its GUID.
    *
    * @param guid The unique certificate's identifier.
    * @return
    */
  def readTrusted(guid: String): Future[CertData]

  /** Creates trusted certificate in the repository.
    *
    * @param data Certificate with metadata.
    */
  def createTrusted(data: CertData): Future[Unit]

  /** Deletes trusted certificate by its GUID.
    *
    * @param guid The unique certificate's identifier.
    */
  def deleteTrusted(guid: String): Future[Unit]

  /** Reads all trusted certificates GUIDs.
    *
    * @return Vector of trusted certificates GUIDs.
    */
  def readAllTrustedGuids(): Future[Vector[String]]

  /** Reads all trusted certificates.
    *
    * @return Vector of certificates with metadata.
    */
  def readAllTrusted(): Future[Vector[CertData]]

  /** Deletes all records about PKI.
    */
  def deleteAll(): Future[Unit]

