package cz.modemtec.adapter.opcua.util.datatypemanager

import cz.modemtec.scopcua.data.{UaDataType, UaId}


/** A manager for OPC UA data types definitions.
  */
trait DataTypeManager extends PartialFunction[UaId, UaDataType]:

  /** Adds data type.
    *
    * @param dataType Data type.
    */
  def add(dataType: UaDataType): Unit

  /** Removes data type by its identifier.
    *
    * @param id Data type identifier.
    */
  def remove(id: UaId): Unit

  /** Gets number of data types.
    */
  def size: Int

  def filter(f: UaDataType => Boolean): Vector[UaDataType]

  def clear(): Unit
  
