package cz.modemtec.adapter.opcua

import com.google.inject.AbstractModule

import cz.modemtec.adapter.opcua.boot.InitApp
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.datatypemanager.impl.MapDataTypeManager
import cz.modemtec.adapter.opcua.boot.OptimizationTask


/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 *
 * Play will automatically use any class called `cz.modemtec.adapter.opcua.Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
class Module extends AbstractModule:

  override def configure(): Unit =
    bind(classOf[InitApp]).asEagerSingleton()
    bind(classOf[DataTypeManager]).toInstance(MapDataTypeManager())
    bind(classOf[OptimizationTask]).asEagerSingleton()

