package cz.modemtec.adapter.opcua.boot.impl

import com.typesafe.config.Config

import cz.modemtec.adapter.api.data.CertData
import cz.modemtec.adapter.api.data.config.SecurityPolicy

import cz.modemtec.adapter.opcua.boot.OpcUaLoader
import cz.modemtec.adapter.opcua.model.{ClientConfig, KeyStoreConfig, SecurityPolicyConverter}

import cz.modemtec.scopcua.data.config.{UaClientConfig, UaEncryptionConfig}
import cz.modemtec.scopcua.util.trustlistmanager.UaTrustListManager
import cz.modemtec.scopcua.util.trustlistmanager.impl.UaMemoryTrustListManager

import org.eclipse.milo.opcua.stack.core.util.CertificateUtil

import play.api.{ConfigLoader, Configuration}

import java.security.{KeyPair, KeyStore, PrivateKey}
import java.security.cert.X509Certificate
import javax.inject.*

import scala.jdk.CollectionConverters.ListHasAsScala
import scala.io.Source
import scala.util.chaining.scalaUtilChainingOps
import scala.util.{Failure, Success, Using}
import scala.xml.XML
import scala.xml.Node as XmlNode


@Singleton()
class AppOpcUaLoader @Inject() (val config: Configuration) extends OpcUaLoader:

  private val prefix = "opcua"

  private case class CertRecord(guid: String, file: String)


  private given ConfigLoader[KeyStoreConfig] with
    def load(root: Config, path: String): KeyStoreConfig =
      val config = root.getConfig(path)
      val instance = config.getString("instance")
      val alias = config.getString("alias")
      val password = config.getString("password")
      val filepath = config.getString("path")

      KeyStoreConfig(alias, instance, password, filepath)


  private given ConfigLoader[ClientConfig] with
    def load(root: Config, path: String): ClientConfig =
      val config = root.getConfig(path)
      val name = config.getString("name")
      val uri = config.getString("uri")
      val timeout = config.getInt("timeout")
      val policy = config.getString("policy") match
        case "None" => SecurityPolicy.None
        case "Basic256" => SecurityPolicy.Basic256
        case "Basic256Sha256" => SecurityPolicy.Basic256Sha256
        case "Aes128Sha256RsaOaep" => SecurityPolicy.Aes128Sha256RsaOaep
        case "Aes256Sha256RsaPss" => SecurityPolicy.Aes256Sha256RsaPss
        case _=> throw IllegalArgumentException("Unknown security policy option for OPC UA clients.")

      ClientConfig(name, uri, timeout, policy)


  private given ConfigLoader[Seq[CertRecord]] with
    def load(root: Config, path: String): Seq[CertRecord] =
      root
        .getConfigList(path)
        .asScala
        .toSeq
        .map: config =>
          val guid = config.getString("guid")
          val file = config.getString("file")
          CertRecord(guid, file)


  override def loadNodeSets(): Vector[String] =
    config
      .get[Seq[String]](s"$prefix.namespaces")
      .toVector


  override def loadNodeSet(path: String): XmlNode =
    Source
      .fromResource(path)
      .mkString
      .pipe(XML.loadString)


  override def loadTrustedCerts(): Vector[CertData] =
    config
      .get[Seq[CertRecord]](s"$prefix.trustList")
      .map: record =>
        val serverCertStream = getClass.getClassLoader.getResourceAsStream(record.file)
        val cert = CertificateUtil.decodeCertificate(serverCertStream)
        CertData(record.guid, cert)
      .toVector


  private def loadEncryption(): Option[UaEncryptionConfig] =
    val defaultTrustList = UaMemoryTrustListManager()
    val keyStoreConfig = config.get[KeyStoreConfig](s"$prefix.keyStore")

    val ks = KeyStore.getInstance(keyStoreConfig.instance)
    val tryConfig = Using(getClass.getClassLoader.getResourceAsStream(keyStoreConfig.path)): is =>
      ks.load(is, keyStoreConfig.password.toCharArray)
      val certificate =
        ks
          .getCertificate(keyStoreConfig.alias)
          .asInstanceOf[X509Certificate]
      val chain =
        ks
          .getCertificateChain(keyStoreConfig.alias)
          .map(_.asInstanceOf[X509Certificate])
          .toVector
      val privateKey = ks.getKey(keyStoreConfig.alias, keyStoreConfig.password.toCharArray)
      val publicKey = certificate.getPublicKey
      val keyPair = new KeyPair(publicKey, privateKey.asInstanceOf[PrivateKey])

      Some(UaEncryptionConfig(keyPair, certificate, chain, defaultTrustList))

    tryConfig match
      case Failure(exception) => throw exception
      case Success(value)     => value


  override def loadClientConfig(): UaClientConfig =
    val default = config.get[ClientConfig](s"$prefix.defaultConfig")
    val wantBeAnonymous = config.get[Boolean](s"$prefix.wantBeAnonymous")

    val defaultPolicy = SecurityPolicyConverter.toUa(default.policy)
    val defaultEncryption = if wantBeAnonymous then None else loadEncryption()

    UaClientConfig(
      name       = default.name,
      uri        = default.uri,
      url        = "",
      timeout    = default.timeout,
      auth       = None,
      policy     = defaultPolicy,
      encryption = defaultEncryption)


  override def wantCleanUp: Boolean = config.get[Boolean](s"$prefix.wantCleanUp")

