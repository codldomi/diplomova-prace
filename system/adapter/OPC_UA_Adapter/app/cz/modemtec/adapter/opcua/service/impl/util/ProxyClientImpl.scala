package cz.modemtec.adapter.opcua.service.impl.util

import cz.modemtec.adapter.api.data.{CallMethodRequest, CallMethodResult, HistoryReadRequest, ReadResult, WriteRequest, WriteResult}

import cz.modemtec.adapter.opcua.repository.ResourceRepository
import cz.modemtec.adapter.opcua.service.impl.util.ProxyClientImpl.{JsonCodec, Behaviour}
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager

import cz.modemtec.scopcua.conversion.ConvertFromUa.ToTimestamp.timestampFromUaDateTime
import cz.modemtec.scopcua.conversion.ConvertToUa.FromTimestamp.timestampToUaDateTime
import cz.modemtec.scopcua.conversion.ConvertFromUa.ToString.stringFromUaString
import cz.modemtec.scopcua.conversion.ConvertToUa.FromDouble.doubleToUaDouble
import cz.modemtec.scopcua.data.config.UaClientConfig
import cz.modemtec.scopcua.data.value.{UaString, UaValue}
import cz.modemtec.scopcua.sdk.client.UaClient
import cz.modemtec.scopcua.sdk.client.impl.UaMiloClient
import cz.modemtec.scopcua.util.jsondecoder.impl.UaModemTecJsonDecoder
import cz.modemtec.scopcua.util.jsonencoder.impl.UaModemTecJsonEncoder
import cz.modemtec.scopcua.data.node.{UaMethodNode, UaVariableNode}
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.value.number.integer.{UaUInt16, UaUInt32}

import play.api.Logging
import play.api.libs.json.{JsObject, JsValue}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

import java.util.concurrent.atomic.AtomicInteger


object ProxyClientImpl:

  case class JsonCodec(
    uriOf: PartialFunction[UaUInt16, UaString],
    indexOf: PartialFunction[UaString, UaUInt16],
    typeOf: PartialFunction[UaId, UaDataType]
  ):

    private val encoder = UaModemTecJsonEncoder(uriOf)
    private val decoder = UaModemTecJsonDecoder(typeOf, indexOf)

    def encode(value: UaValue): JsValue = encoder.encode(value)

    def encode(value: Option[UaValue]): JsValue = encoder.encode(value)

    def decode(encoded: JsValue, typeId: UaId): UaValue = decoder.decode(encoded, typeId)

    def decodeOption(encoded: JsValue, typeId: UaId): Option[UaValue] = decoder.decodeOption(encoded, typeId)

  end JsonCodec


  case class Behaviour(
    client: UaClient,
    codec: JsonCodec,
  )

end ProxyClientImpl


/** OPC UA Client during creating connects server and searches endpoints. This class manages problem cases, e.g.
  * server doesn't exists at time of client creation.
  */
class ProxyClientImpl(
  val types: DataTypeManager,
  val guid: String,
  val config: UaClientConfig,
  val configs: ResourceRepository
)(using val context: ExecutionContext) extends ProxyClient with Logging:

  private val counter: AtomicInteger = new AtomicInteger(0)

  private var behaviour: Option[Behaviour] = None
  private val lock = new Object


  private def notWorking: Boolean = counter.get() == 0
  private def inc(): Unit = counter.incrementAndGet()
  private def dec(): Unit = counter.decrementAndGet()
  private def reset(): Unit = counter.set(0)


  private def createClient(): Future[Unit] =
    logger.debug(s"The OPC UA client for resource with GUID $guid is being created.")

    val action = for
      createdClient <- Future(UaMiloClient.from(config, types))
      _             <- createdClient.connect()
      nsMap         <- createdClient.readNamespaces()
      _             <- configs.detachNamespaces(guid)
      _             <- configs.attachNamespaces(guid, nsMap.values.map(stringFromUaString).toSet)
    yield (nsMap, createdClient)

    action.map: (nsMap, createdClient) =>
      val createdCodec = JsonCodec(nsMap, nsMap.map(_.swap), types)
      val createdBehaviour = Behaviour(createdClient, createdCodec)

      behaviour = Some(createdBehaviour)
      logger.debug(s"The OPC UA client for resource with GUID $guid successfully created.")


  override def check(): Future[Unit] =
    lock.synchronized:
      if behaviour.isDefined then
        Future.unit
      else
        createClient()


  private def withClient[A](code: (UaClient, JsonCodec) => Future[A]): Future[A] =
    inc()

    val action = check().flatMap(_ => code(behaviour.get.client, behaviour.get.codec))
    action.onComplete(_ => dec())

    action


  override def optimize(): Future[Unit] =
    if notWorking then
      lock.synchronized:

        if notWorking && behaviour.isDefined then
          logger.debug(s"The OPC UA client for resource with GUID $guid is being cleaned up.")

          val action = behaviour.get.client.disconnect()
          action.onComplete:
            case Failure(_) => logger.debug("The OPC UA Client for resource with GUID $guid can't be cleaned up.")
            case Success(_) => logger.debug(s"The OPC UA client for resource with GUID $guid is cleaned up.")

          behaviour = None
          action
        else
          Future.unit
    else
      Future.unit


  override def dispose(): Future[Unit] =
    logger.debug(s"The OPC UA client for resource with GUID $guid is being disposed.")
    val client = behaviour.map(_.client)

    lock.synchronized(() => behaviour = None)

    reset()
    client.map(_.disconnect()).getOrElse(Future.unit)


  override def read(node: UaVariableNode): Future[ReadResult] = withClient: (client, codec) =>
    client
      .readValue(node.id, node.typeId)
      .map: result => 
        val encoded = codec.encode(result.value)
        val timestamp = result.sourceTime.map(timestampFromUaDateTime)
        ReadResult(encoded, timestamp)


  override def write(node: UaVariableNode, request: WriteRequest): Future[WriteResult] = withClient: (client, codec) =>
    val value = codec.decode(request.value, node.typeId)

    for 
      _      <- client.writeValue(node.id, value, node.typeId)
      result <- client.readValue(node.id, node.typeId)
    yield WriteResult(codec.encode(result.value))


  override def callMethod(node: UaMethodNode, request: CallMethodRequest): Future[CallMethodResult] = withClient: (client, codec) =>
    val input =
      node
        .input
        .map: arg =>
          val js = request.input \ arg.name
          val value = codec.decode(js.get, arg.typeId)
          (arg.name, value)
        .toMap

    client
      .callMethod(node.parentId.get, node.browseName, input)
      .map: output =>
        val fields =
          node
            .output
            .map: arg =>
              val value = output(arg.name)
              (arg.name.value, codec.encode(value))
        val js = JsObject(fields)
        CallMethodResult(js)


  override def historyRead(node: UaVariableNode, request: HistoryReadRequest)(callback: ReadResult => _): Future[Unit] = withClient: (client, codec) =>
    val from = timestampToUaDateTime(request.from)
    val to = timestampToUaDateTime(request.to)

    client.readHistory(node.id, from, to, node.typeId):
      readValue =>
        val value = codec.encode(readValue.value)
        val time = readValue.sourceTime.map(timestampFromUaDateTime)
        val result = ReadResult(value, time)
        callback(result)


  override def deleteSubscription(subId: UaUInt32): Future[Unit] = withClient: (client, _) =>
    client
      .deleteSubscription(subId)
      .transformWith: _ =>
        dec()
        Future.unit
    

  override def createMonitoredItem(subId: UaUInt32, node: UaVariableNode)(callback: (ReadResult, UaUInt32, UaUInt32) => _): Future[UaUInt32] = withClient: (client, codec) =>
    client
      .createMonitoredItem(subId, node.id, node.typeId): (readValue, monId, subId) =>
        val value = codec.encode(readValue.value)
        val time = readValue.sourceTime.map(timestampFromUaDateTime)
        val result = ReadResult(value, time)
        callback(result, monId, subId)
  

  override def deleteMonitoredItem(subId: UaUInt32, monId: UaUInt32): Future[Unit] = withClient: (client, _) =>
    client.deleteMonitoredItem(subId, monId)


  override def createSubscription(): Future[UaUInt32] = withClient: (client, _) =>
    client
      .createSubscription(1000.0)
      .map: subId =>
        inc()
        subId



