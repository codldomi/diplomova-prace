package cz.modemtec.adapter.opcua.model


case class KeyStoreConfig(
  alias: String,
  instance: String,
  password: String,
  path: String
)
