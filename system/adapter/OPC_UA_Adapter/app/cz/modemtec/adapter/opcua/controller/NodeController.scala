package cz.modemtec.adapter.opcua.controller

import org.apache.pekko.actor.{Actor, ActorSystem, Props}
import org.apache.pekko.stream.Materializer
import org.apache.pekko.stream.scaladsl.Flow

import cz.modemtec.adapter.api.util.MonadTransformer.*
import cz.modemtec.adapter.api.data.{AdapterError, CallMethodRequest, HistoryReadRequest, WriteRequest}
import cz.modemtec.adapter.api.util.AdapterApiFormat

import cz.modemtec.adapter.opcua.service.ResourceService
import cz.modemtec.adapter.opcua.util.HttpExtension

import cz.modemtec.scopcua.data.value.number.integer.UaUInt32
import cz.modemtec.scopcua.data.UaId

import play.api.libs.json.{JsValue, Json}
import play.api.libs.streams.ActorFlow
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents, WebSocket}

import com.google.inject.*

import play.api.Logging

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success, Try}


@Singleton()
class NodeController @Inject() (
  val controllerComponents: ControllerComponents,
  val resources: ResourceService
)(using val context: ExecutionContext, val system: ActorSystem, val mat: Materializer) extends BaseController with AdapterApiFormat with HttpExtension with Logging:

  /** Reads actual value of variable.
    *
    * Responses:
    *
    *   - 200: ReadResult - Successful operation.
    *   - 404: AdapterError - Path to node is not valid or server doesn't exist. It's specified with error message.
    *   - 405: AdapterError - Executed operation is not supported, e.g. selected node is not a variable.
    *   - 500: AdapterError - Error during actions with resource.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    * @param path Path to the node in the object tree.
    */
  def read(guid: String, path: String): Action[AnyContent] = Action.async: request =>
    val action = for
      _      <- resources.checkResource(guid)
      nodeId <- resources.readNodeId(guid, path)
      value  <- resources.read(guid, nodeId)
    yield value

    action
      .aside:
        case Left(error)  => logger.error(s"Read operation end up with AdapterError = ${error.asText}")
        case Right(value) => logger.debug(s"Read operation end up with result $value")
      .value
      .map(value => value.asHttpResult)

  /** Writes value to variable.
    *
    * Responses:
    *
    *   - 200: ReadResult - Successful operation.
    *   - 404: AdapterError - Path to node is not valid or server doesn't exist. It's specified with error message.
    *   - 405: AdapterError - Executed operation is not supported, e.g. selected node is not a variable.
    *   - 500: AdapterError - Error during actions with resource.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    * @param path Path to the node in the object tree.
    */
  def write(guid: String, path: String): Action[WriteRequest] = Action(parse.json[WriteRequest]).async: request =>
    val action = for
      _      <- resources.checkResource(guid)
      nodeId <- resources.readNodeId(guid, path)
      value  <- resources.write(guid, nodeId, request.body)
    yield value

    action
      .aside:
        case Left(error)  => logger.error(s"Write operation end up with AdapterError = ${error.asText}")
        case Right(value) => logger.debug(s"Write operation end up with result $value")
      .value
      .map(value => value.asHttpResult)

  /** Calls method on node.
    *
    * Responses:
    *
    *   - 200: ReadResult - Successful operation.
    *   - 404: AdapterError - Path to node is not valid or server doesn't exist. It's specified with error message.
    *   - 405: AdapterError - Executed operation is not supported, e.g. selected node is not a method.
    *   - 500: AdapterError - Error during actions with resource.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    * @param path Path to the node in the object tree.
    */
  def callMethod(guid: String, path: String): Action[CallMethodRequest] = Action(parse.json[CallMethodRequest]).async: request =>
    val action = for
      _      <- resources.checkResource(guid)
      nodeId <- resources.readNodeId(guid, path)
      output <- resources.callMethod(guid, nodeId, request.body)
    yield output

    action
      .aside:
        case Left(error)  => logger.error(s"CallMethod operation end up with AdapterError = ${error.asText}")
        case Right(value) => logger.debug(s"CallMethod operation end up with result $value")
      .value
      .map(output => output.asHttpResult)

  /** Reads historic values from closed time interval [from, to].
    *
    * Responses:
    *
    *   - 200: ReadResult - Successful operation.
    *   - 404: AdapterError - Path to node is not valid or server doesn't exist. It's specified with error message.
    *   - 405: AdapterError - Executed operation is not supported, e.g. selected node is not a variable.
    *   - 500: AdapterError - Error during actions with resource.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    * @param path Path to the node in the object tree.
    */
  def historyRead(guid: String, path: String): Action[HistoryReadRequest] = Action(parse.json[HistoryReadRequest]).async: request =>
    val action = for
      _      <- resources.checkResource(guid)
      nodeId <- resources.readNodeId(guid, path)
      source <- resources.historyRead(guid, nodeId, request.body)
    yield source

    action
      .aside:
        case Left(error)  => logger.error(s"HistoryRead operation end up with AdapterError = ${error.asText}")
        case Right(value) => logger.debug(s"HistoryRead operation end up with result $value")
      .value
      .map:
        case Left(error)   => error.asHttpResult
        case Right(source) => Ok.chunked(source.map(item => Json.toJson(item)))

  /** Subscribes for data change on the node.
    *
    * Responses:
    *
    *   - 101: ReadResult - Successful creation. Switching to WebSockets protocol.
    *   - 404: AdapterError - Path to node is not valid or server doesn't exist. It's specified with error message.
    *   - 405: AdapterError - Executed operation is not supported, e.g. selected node is not a variable.
    *   - 500: AdapterError - Error during actions with resource.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    * @param path Path to the node in the object tree.
    */
  def subscribe(guid: String, path: String): WebSocket = WebSocket.acceptOrResult[JsValue, JsValue]: request =>

    def createFlow(nodeId: UaId): FutureEither[AdapterError, Flow[JsValue, JsValue, _]] =

      object WebSocketActor:
        def props(subId: UaUInt32): Props = Props(new WebSocketActor(subId))

      class WebSocketActor(subId: UaUInt32) extends Actor:
        def receive: Receive =
          case _ =>

        override def postStop(): Unit =
          logger.debug(s"Subscribe operation finished.")
          val deletion =
            resources
            .deleteSubscription(guid, subId)
            .value
          Await.ready(deletion, 2.minutes)

      end WebSocketActor


      val result = Try:
        ActorFlow.actorRef: out =>
          val creation =
            resources
            .createSubscription(guid, nodeId): data =>
              val json = Json.toJson(data)
              out ! json
            .map(subId => WebSocketActor.props(subId))
            .value

          Await.result(creation, 2.minutes) match //  todo: refactor in order to avoid this Await
            case Left(error) => throw error
            case Right(props) => props

      result match
        case Failure(exception) =>
          val error = AdapterError.ErrorDuringOperation(s"An error occurred during the operation subscribe on node with NodeId $nodeId and resource GUID $guid. Exception message: ${exception.getMessage}")
          FutureEither.left(error)
        case Success(flow) =>
          FutureEither.right(flow)

    end createFlow


    val action = for
      _      <- resources.checkResource(guid)
      nodeId <- resources.readNodeId(guid, path)
      flow   <- createFlow(nodeId)
    yield flow

    action
      .aside:
        case Left(error)  => logger.error(s"Subscribe operation end up with AdapterError = ${error.asText}")
        case Right(value) => logger.debug(s"Subscribe operation started with result $value")
      .value
      .map:
        case Left(error) => Left(error.asHttpResult)
        case Right(flow) => Right(flow)

  end subscribe
