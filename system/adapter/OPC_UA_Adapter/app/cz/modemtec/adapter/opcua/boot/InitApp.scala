package cz.modemtec.adapter.opcua.boot

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.util.MonadTransformer.*
import cz.modemtec.adapter.opcua.service.{NodeSetService, PkiService, ResourceService}

import play.api.Logging
import play.api.inject.ApplicationLifecycle

import javax.inject.*
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.chaining.scalaUtilChainingOps



@Singleton()
class InitApp @Inject() (
  val lifecycle: ApplicationLifecycle,
  val loader: OpcUaLoader,
  val nodeSetService: NodeSetService,
  val resourceService: ResourceService,
  val pkiService: PkiService
)(using val context: ExecutionContext) extends Logging:

  logger.debug("Initializing data.")

  lifecycle.addStopHook: () =>
    resourceService.dispose()

  logger.debug(s"Database should be cleaned up before usage: ${loader.wantCleanUp}")

  
  private def deleting: Future[Unit] = for
    _ <- resourceService.deleteAll()
    _ <- nodeSetService.deleteAll()
    _ <- pkiService.deleteAll()
  yield ()


  private def storeNodeSets: Future[Unit] =
    loader
      .loadNodeSets()
      .foldLeft(FutureEither.unit[AdapterError]): (result, path) =>
        val xml = loader.loadNodeSet(path)
        val nodeSet = nodeSetService.extract(xml)
        result.flatMap:  _ =>
          nodeSet match
            case Left(error)  => FutureEither.left(error)
            case Right(nodes) => nodeSetService.create(nodes)
      .value
      .map(_ => ())

  
  private def storeCertificates: Future[Unit] =
    loader
      .loadTrustedCerts()
      .map: data =>
        pkiService.createOrUpdateTrusted(data.guid, data)
      .pipe: results =>
        FutureEither.sequence(results)
      .value
      .map(_ => ())


  val initSequence: Future[Unit] =
    if loader.wantCleanUp then
      for
        _ <- deleting
        _ <- storeNodeSets
        _ <- storeCertificates
      yield ()
    else
      for
        _ <- nodeSetService.loadDataTypes()
        _ <- resourceService.loadResources()
      yield ()

  Await.result(initSequence, 5.minutes)

  logger.debug("Data are initialized.")


