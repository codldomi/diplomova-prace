package cz.modemtec.adapter.opcua.controller

import cz.modemtec.adapter.api.util.AdapterApiFormat

import cz.modemtec.adapter.opcua.service.{ResourceService, ResourceSpecService}
import cz.modemtec.adapter.opcua.util.HttpExtension

import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import com.google.inject.*
import scala.concurrent.ExecutionContext


@Singleton()
class ResourceSpecController @Inject() (
  val controllerComponents: ControllerComponents,
  val resources: ResourceService,
  val service: ResourceSpecService
)(using val context: ExecutionContext) extends BaseController with AdapterApiFormat with HttpExtension:

  /** Reads specification of resource with the given GUID.
    *
    * Responses:
    *
    *   - 200: ResourceSpec - Successful operation.
    *   - 404: AdapterError - Resource doesn't exist.
    *   - 500: AdapterError - Error during operation occurred.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    */
  def spec(guid: String): Action[AnyContent] = Action.async:
    val action = for
      _    <- resources.checkResource(guid)
      spec <- service.generate(guid)
    yield spec

    action.value.map(spec => spec.asHttpResult)

  /** Reads signature attribute of the specification of resource with the given GUID.
    *
    * Responses:
    *
    *   - 200: String - Successful operation.
    *   - 404: AdapterError - Resource doesn't exist.
    *   - 500: AdapterError - Error during operation occurred.
    *   - 503: AdapterError - Resource can't be connected.
    *
    * @param guid Resource globally unique identifier.
    */
  def signature(guid: String): Action[AnyContent] = Action.async:
    val action = for
      _    <- resources.checkResource(guid)
      sign <- service.signature(guid)
    yield sign

    action.value.map(sign => sign.asHttpResult)

