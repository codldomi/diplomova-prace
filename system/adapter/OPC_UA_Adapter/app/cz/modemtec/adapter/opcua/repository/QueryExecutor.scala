package cz.modemtec.adapter.opcua.repository

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.Future


trait QueryExecutor:
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api.*

  def execute[A](action: DBIO[A]): Future[A] = db.run(action)

  def transaction[A](action: DBIO[A]): Future[A] = db.run(action.transactionally)

