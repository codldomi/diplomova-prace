package cz.modemtec.adapter.opcua.repository

import com.google.inject.*

import cz.modemtec.adapter.opcua.repository.impl.NodeRepositoryImpl
import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.node.{UaMethodNode, UaNode, UaObjectNode, UaVariableNode}

import scala.concurrent.Future


/** The repository interface providing operations on nodes from the OPC UA information model.
  */
@ImplementedBy(classOf[NodeRepositoryImpl])
trait NodeRepository:

  /** Reads node's identifier within the information model of the selected server.
   *
   * @param guid   The unique server's identifier.
   * @param path   The relative path to the node starting in ObjectsFolder node, e.g. /MyObject/MyVariable corresponds
   *               to the absolute path Root/ObjectsFolder/MyObject/MyVariable.
   * @return Node's identifier.
   */
  def readNodeId(guid: String, path: String): Future[UaId]

  /** Reads node of Variable class.
    *
    * @param nodeId Node's identifier.
    * @return The variable node.
    */
  def readVariable(nodeId: UaId): Future[UaVariableNode]

  /** Reads node of Method class.
    *
    * @param nodeId Node's identifier.
    * @return The method node.
    */
  def readMethod(nodeId: UaId): Future[UaMethodNode]

  /** Reads node of Object class.
    *
    * @param nodeId Node's identifier.
    * @return The object node.
    */
  def readObject(nodeId: UaId): Future[UaObjectNode]

  /** Creates and stores nodes.
    *
    * @param nodes Selected nodes.
    */
  def createAll(nodes: Iterable[UaNode]): Future[Unit]

  /** Reads nodes from the selected namespace.
    *
    * @param uri Namespace URI.
    * @return Nodes in the corresponding namespaces.
    */
  def read(uri: String): Future[Vector[UaNode]]

  /** Deletes all nodes.
    */
  def deleteAll(): Future[Unit]
