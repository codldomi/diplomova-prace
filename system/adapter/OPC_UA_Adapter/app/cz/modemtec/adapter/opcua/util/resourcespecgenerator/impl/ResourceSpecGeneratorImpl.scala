package cz.modemtec.adapter.opcua.util.resourcespecgenerator.impl

import cats.syntax.all.*

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.data.spec.{MethodSpec, ResourceSpec, SchemaSpec, VariableSpec}

import cz.modemtec.adapter.opcua.util.resourcespecgenerator.ResourceSpecGenerator

import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaConstants.OpcUaFoundation
import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.node.{UaMethodNode, UaNode, UaVariableNode}
import cz.modemtec.scopcua.util.jsonschemagenerator.UaJsonSchemaGenerator

import play.api.libs.json.{JsObject, JsValue}

import java.security.MessageDigest


class ResourceSpecGeneratorImpl(
  createSchemaGenerator: PartialFunction[UaId, UaDataType] => UaJsonSchemaGenerator
) extends ResourceSpecGenerator:

  /** Title of the API is hash of namespaces URI formatted as lexicographically ordered JSON array of string. There isn't
    * any white character between values and symbols, e.g. "["http://opcfoundation.org/UA/IEC61850-7-3","http://opcfoundation.org/UA/IEC61850-7-4"]".
    *
    * @return Hash code.
    */
  override def generateSignature(resource: ResourceConfig, nodes: Map[UaId, UaNode], types: Map[UaId, UaDataType]): String = 
    val identifiers = nodes.keySet ++ types.keySet
    val namespaces = identifiers.map(_.namespaceUri)

    val ordered =
      namespaces
        .map(_.value)
        .toVector
        .sorted

    val text = s"[${ordered.mkString(",")}]"

    val bytes =
      MessageDigest
        .getInstance("MD5")
        .digest(text.getBytes)

    bytes.map(_.toChar).mkString


  override def generate(resource: ResourceConfig, nodes: Map[UaId, UaNode], types: Map[UaId, UaDataType]): ResourceSpec = 
    val paths = generatePaths(nodes)

    var nodeSchemas = Map.empty[String, SchemaSpec]

    val endpoints =
      nodes
        .filter: (id, _) =>
          paths.keySet.contains(id)
        .values
        .map:
          case n: UaVariableNode =>
            val path = paths(n.id)
            val (spec, newSchemas) = generateVariable(n, types, path)
            nodeSchemas = nodeSchemas ++ newSchemas.map(spec => (spec.uri, spec))
            spec.some

          case n: UaMethodNode   =>
            val path = paths(n.id)
            val (spec, newSchemas) = generateMethod(n, types, path)
            nodeSchemas = nodeSchemas ++ newSchemas.map(spec => (spec.uri, spec))
            spec.some

          case _ => None
        .filter(_.isDefined)
        .map:
          case Some(spec) => (spec.path, spec)
          case _ => throw new NotImplementedError("Not implemented filtering of Some[A].")
        .toMap

    val dataTypesSchemas = generateDataTypeSchemas(types)
    val schemas = nodeSchemas ++ dataTypesSchemas

    val signature = generateSignature(resource, nodes, types)
    val name = "OPC UA Resource"
    val description = "Specification of endpoints for OPC UA resource."

    ResourceSpec(name, signature, description, endpoints, schemas)


  private def generateDataTypeSchemas(types: Map[UaId, UaDataType]): Map[String, SchemaSpec] = 
    val schemaGenerator = createSchemaGenerator(types)

    types
      .keys
      .map: id =>
        val schema = schemaGenerator.generate(id)
        val spec = asSchemaSpec(schema)
        (spec.uri, spec)
      .toMap


  private def asSchemaSpec(schema: JsValue): SchemaSpec = 

    def searchDependencies(json: JsValue): Set[String] = 
      val ref = (json \ "$ref").toOption

      if ref.isDefined then 
        val raw = ref.get.as[String]
        return if raw != "#" then Set(raw) else Set.empty

      json match 
        case obj: JsObject =>
          obj
            .values
            .foldLeft(Set.empty[String]): (result, field) =>
              result ++ searchDependencies(field)
        case _ => Set.empty

    val uri = (schema \ "$id").as[String]
    val name = (schema \ "title").as[String]
    val dependencies = searchDependencies(schema)

    SchemaSpec(uri, name, schema, dependencies)


  private def asSchemaSpec(schema: Option[JsValue]): Option[SchemaSpec] = schema.map(asSchemaSpec)


  private def generateVariable(node: UaVariableNode, types: PartialFunction[UaId, UaDataType], path: String): (VariableSpec, Vector[SchemaSpec]) = 
    val schemaGenerator = createSchemaGenerator(types)

    val name = node.browseName.value
    val description = node.description.map(_.value).getOrElse("")
    val js = schemaGenerator.generate(node, path)
    val schema = asSchemaSpec(js)

    (VariableSpec(path, description, name, schema.uri), Vector(schema))


  private def generateMethod(node: UaMethodNode, types: PartialFunction[UaId, UaDataType], path: String): (MethodSpec, Vector[SchemaSpec]) = 
    val schemaGenerator = createSchemaGenerator(types)

    val (inSchema, outSchema) = schemaGenerator.generate(node, path)
    val name = node.browseName.value
    val description = node.description.map(_.value).getOrElse("")
    val input = asSchemaSpec(inSchema)
    val output = asSchemaSpec(outSchema)

    val in = if input.isDefined then Vector(input.get) else Vector()
    val out = if output.isDefined then Vector(output.get) else Vector()

    (MethodSpec(path, description, name, input.map(_.uri), output.map(_.uri)), in ++ out)


  private def generatePaths(nodes: Map[UaId, UaNode]): Map[UaId, String] = 
    val root = UaId(85, OpcUaFoundation)
    val children =
      nodes
        .values
        .map: node =>
          (node.parentId, node.id)
        .groupMap(_._1)(_._2)

    def isEndpoint(id: UaId): Boolean = nodes(id) match 
      case _: UaVariableNode => true
      case _: UaMethodNode   => true
      case _ => false

    def generatePaths(current: UaId, path: Vector[String]): Map[UaId, String] =
      if isEndpoint(current) then
        val pathToCurrent = "/" + path.mkString("/")
        Map(current -> pathToCurrent)
      else 
        children.get(current.some) match 
          case Some(next) => next.foldLeft(Map.empty[UaId, String]): (acc, child) =>
            val node = nodes(child)
            acc ++ generatePaths(child, path :+ node.browseName.value)
          case None => Map.empty
    
    generatePaths(root, Vector.empty)

