package cz.modemtec.adapter.opcua.util

import cz.modemtec.adapter.api.data.AdapterError

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


object FutureExtension extends FutureExtension


trait FutureExtension:

  extension [A](self: Future[A])
    
    def withAdapterError(f: Throwable => AdapterError)(using context: ExecutionContext): Future[Either[AdapterError, A]] = self.transform:
      case Failure(exception) =>
        val error = f(exception)
        val result = Left(error)
        Success(result)

      case Success(value) =>
        val result = Right(value)
        Success(result)

