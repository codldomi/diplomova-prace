package cz.modemtec.adapter.opcua.repository.impl

import cz.modemtec.adapter.opcua.repository.{DatabaseSchema, NodeRepository, QueryExecutor}

import cz.modemtec.scopcua.conversion.ConvertFromUa.ToAll.given Conversion[?, ?]
import cz.modemtec.scopcua.conversion.ConvertToUa.FromAll.given Conversion[?, ?]
import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.node.{UaArgument, UaMethodNode, UaNode, UaObjectNode, UaVariableNode}
import cz.modemtec.scopcua.data.value.UaString
import cz.modemtec.scopcua.data.value.number.integer.{UaInt32, UaUInt32}

import cats.syntax.all.*

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.dbio.DBIO.*
import slick.jdbc.JdbcProfile

import javax.inject.*
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util.chaining.scalaUtilChainingOps


@Singleton()
class NodeRepositoryImpl @Inject()
  (protected val dbConfigProvider: DatabaseConfigProvider)
  (using val executionContext: ExecutionContext)
  extends NodeRepository
    with DatabaseSchema
    with QueryExecutor
    with HasDatabaseConfigProvider[JdbcProfile]:

  import profile.api.*

  private given toUaStringOpt: Conversion[Option[String], Option[UaString]] = (opt: Option[String]) => opt.map(stringToUaString)
  private given toStringOpt: Conversion[Option[UaString], Option[String]] = (opt: Option[UaString]) => opt.map(stringFromUaString)
  private given toVectorUaUInt32: Conversion[Vector[Int], Vector[UaUInt32]] = (vector: Vector[Int]) => vector.map(intToUaUInt32)
  private given toIntOpt: Conversion[Option[UaInt32], Option[Int]] = (opt: Option[UaInt32]) => opt.map(intFromUaInt32)

  extension (self: String)
    private def asVector: Vector[Int] =
      self
        .split(",")
        .filter(_ != "")
        .map(_.toInt)
        .toVector


  extension (self: Vector[UaUInt32])
    private def asString: String =
      self
        .map(_.value)
        .mkString(",")


  private def readNodeId(id: Long): DBIO[UaId] =
    val rows = for
      ident <- Ids if ident.id === id
      ns    <- Namespaces if ns.id === ident.namespaceId
    yield (ident.index, ns.uri)

    rows.result.head.map: (index, uri) =>
      UaId(index, uri)


  private def readNodeId(id: Option[Long]): DBIO[Option[UaId]] = id match
    case Some(value) => readNodeId(value).map(result => Some(result))
    case None        => DBIO.successful(None)


  private def readArguments(id: Long): DBIO[(Vector[UaArgument], Vector[UaArgument])] =
    val arguments = for
      arg   <- Arguments  if arg.methodId === id
      ident <- Ids        if arg.typeId === ident.id
      ns    <- Namespaces if ident.namespaceId === ns.id
    yield (arg.isInput, arg.name, ident.index, ns.uri, arg.ord, arg.valueRank, arg.arrayDimensions)

    arguments
      .result
      .map: rows =>
        val args =
          rows
            .map: (isInput, name, index, uri, ord, valueRank, arrayDimensions) =>
              isInput -> UaArgument(name, isInput, UaId(index, uri), valueRank, arrayDimensions.asVector, ord)
            .toVector
            .groupMap(_._1)(_._2)

        val in = args(true).sortBy(_.ord.value)
        val out = args(false).sortBy(_.ord.value)

        (in, out)


  private def readCommonNode(nodeId: UaId): DBIO[NodeRow] =
    val rows = for
      ident <- Ids        if ident.index === nodeId.index.toInt
      ns    <- Namespaces if ident.namespaceId === ns.id && ns.uri === nodeId.namespaceUri.value
      node  <- Nodes      if node.id === ident.id
    yield node

    rows.result.head


  override def readVariable(nodeId: UaId): Future[UaVariableNode] =
    val selection = for
      node     <- readCommonNode(nodeId)
      parentId <- readNodeId(node.parentId)
      typeId   <- readNodeId(node.typeId)
    yield UaVariableNode(nodeId, parentId, node.browseName, node.displayName, node.description, typeId.get, node.valueRank.get, node.arrayDimensions.get.asVector)

    execute(selection)


  override def readMethod(nodeId: UaId): Future[UaMethodNode] =
    val selection = for
      node            <- readCommonNode(nodeId)
      parentId        <- readNodeId(node.parentId)
      (input, output) <- readArguments(node.id)
    yield UaMethodNode(nodeId, parentId, node.browseName, node.displayName, node.description, input, output)

    execute(selection)


  override def readObject(nodeId: UaId): Future[UaObjectNode] =
    val selection = for
      node     <- readCommonNode(nodeId)
      parentId <- readNodeId(node.parentId)
    yield UaObjectNode(nodeId, parentId, node.browseName, node.displayName, node.description)

    execute(selection)


  override def deleteAll(): Future[Unit] =
    val deletion = for
      _ <- Arguments.delete
      _ <- Nodes.delete
      _ <- Ids.delete
    yield ()

    transaction(deletion)


  private def readNamespaces(): DBIO[Map[String, Long]] =
    Namespaces
      .result
      .map(namespaces => namespaces.map(namespace => namespace.uri -> namespace.id).toMap)


  private def createNode(node: UaNode, createdIds: Map[UaId, Long]): DBIO[Int] =

    extension (self: UaArgument)
      def asArgumentRow(methodId: Long): ArgumentRow =
        ArgumentRow(
          id        = 0,
          isInput   = self.isInput,
          ord       = self.ord,
          name      = self.name,
          methodId  = methodId,
          typeId    = createdIds(self.typeId),
          valueRank = self.valueRank,
          arrayDimensions = self.arrayDimensions.mkString(","))

    node match
      case UaObjectNode(id, parentId, browseName, displayName, description) =>
        val nodeId = createdIds(id)
        val parent = parentId.map(createdIds)
        Nodes += NodeRow(nodeId, browseName, parent, NodeType.Object, None, displayName, description, None, None)

      case UaVariableNode(id, parentId, browseName, displayName, description, typeId, valueRank, arrayDimensions) =>
        val nodeId = createdIds(id)
        val parent = parentId.map(createdIds)
        val dataType = createdIds(typeId)
        Nodes += NodeRow(nodeId, browseName, parent, NodeType.Variable, dataType.some, displayName, description, valueRank.some, arrayDimensions.asString.some)

      case UaMethodNode(nodeId, parentNodeId, browseName, displayName, description, inputArgs, outputArgs) =>
        val id = createdIds(nodeId)
        val parentId = parentNodeId.map(createdIds)
        val input = inputArgs.map(_.asArgumentRow(id))
        val output = outputArgs.map(_.asArgumentRow(id))
        val args = input ++ output
        val actions =
          Seq(
            Nodes += NodeRow(id, browseName, parentId, NodeType.Method, None, displayName, description, None, None),
            Arguments ++= args
          )

        DBIO.sequence(actions).map(_ => id.toInt)


  override def createAll(nodes: Iterable[UaNode]): Future[Unit] =

    def createIds(namespaces: Map[String, Long], ids: Set[UaId]): DBIO[Map[UaId, Long]] =
      val table = Ids returning Ids.map(_.id) into { (row, id) => row.copy(id = id) }
      val rows = ids.map: nodeId =>
        val row = IdRow(0, nodeId.index.toInt, namespaces(nodeId.namespaceUri))
        nodeId -> row

      val actions =
        rows
          .map: (nodeId, row) =>
            (table += row).map(row => nodeId -> row.id)
          .toSeq

      DBIO.sequence(actions).map(pairs => pairs.toMap)

    def readStoredIds(namespaces: Map[String, Long]): DBIO[Map[UaId, Long]] =
      val namespaceIds = namespaces.map(_.swap)
      Ids
        .filter(_.namespaceId.inSet(namespaces.values))
        .result
        .map: rows =>
          rows
            .map(row => (UaId(row.index, namespaceIds(row.namespaceId)), row.id))
            .toMap

    val newIds = nodes.foldLeft(Set.empty[UaId]): (acc, node) =>
      node.parentId match
        case Some(parentId) => acc + node.id + parentId
        case None           => acc + node.id

    val insertion = for
      namespaces <- readNamespaces()
      storedIds  <- readStoredIds(namespaces)
      createdIds <- createIds(namespaces, newIds -- storedIds.keySet)
      _ <- DBIO.sequence:
        val ids = storedIds ++ createdIds
        nodes.map(node => createNode(node, ids))
    yield ()

    transaction(insertion)


  override def readNodeId(guid: String, path: String): Future[UaId] =

    case class Node(id: Long, name: String, parentId: Option[Long])

    def translate(current: Option[Long], path: Seq[String], nodes: Map[Option[Long], Seq[Node]]): DBIO[Option[UaId]] =
      if path.isEmpty then return readNodeId(current)

      val children = nodes(current)

      children
        .filter(_.name == path.head)
        .map:
          case Node(id, _, _) => translate(id.some, path.tail, nodes)
        .head


    def readResourceNamespaces(): DBIO[Set[Long]] =
      val rows = for
        res   <- Resources
        resNs <- ResourceNamespaces if res.id === resNs.resourceId
      yield resNs.namespaceId

      rows.result.map(_.toSet)

    def readResourceNodes(namespaces: Set[Long]): DBIO[Map[Option[Long], Seq[Node]]] =
      Ids
        .join(Nodes).on(_.id === _.id)
        .filter { case (id, _) => id.namespaceId.inSet(namespaces) }
        .map { case (_, node) => (node.parentId, node.id, node.browseName) }
        .result
        .map(_.map { case (parentId, id, browseName) => (parentId, Node(id, browseName, parentId)) })
        .map(_.groupMap(_._1)(_._2))

    val browseNames = Seq("Root", "Objects") :++ path.split("/").filter(_ != "").toSeq
    val selection = for
      namespaces <- readResourceNamespaces()
      nodes      <- readResourceNodes(namespaces)
      nodeId     <- translate(current = None, browseNames, nodes)
    yield nodeId.get

    transaction(selection)


  override def read(uri: String): Future[Vector[UaNode]] =

    def readNode(node: NodeRow): DBIO[UaNode] = node.nodeType match
      case NodeType.Method =>
        for
          nodeId          <- readNodeId(node.id)
          parentId        <- readNodeId(node.parentId)
          (input, output) <- readArguments(node.id)
        yield UaMethodNode(nodeId, parentId, node.browseName, node.displayName, node.description, input, output)

      case NodeType.Object =>
        for
          nodeId   <- readNodeId(node.id)
          parentId <- readNodeId(node.parentId)
        yield UaObjectNode(nodeId, parentId, node.browseName, node.displayName, node.description)

      case NodeType.Variable =>
        for
          nodeId   <- readNodeId(node.id)
          parentId <- readNodeId(node.parentId)
          typeId   <- readNodeId(node.typeId)
        yield UaVariableNode(nodeId, parentId, node.browseName, node.displayName, node.description, typeId.get, node.valueRank.get, node.arrayDimensions.get.asVector)

      case _ => throw new UnsupportedOperationException("Unsupported OPC UA node class.")

    val selection = for
      rows <- Namespaces
        .filter(_.uri === uri)
        .join(Ids).on(_.id === _.namespaceId)
        .join(Nodes).on { case ((_, ident), node) => node.id === ident.id }
        .map { case (_, node) => node }
        .result

      nodes <- rows
        .map(readNode)
        .toVector
        .pipe(created => DBIO.sequence(created))

    yield nodes

    execute(selection)

