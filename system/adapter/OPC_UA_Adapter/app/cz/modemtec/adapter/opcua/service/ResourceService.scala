package cz.modemtec.adapter.opcua.service

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

import com.google.inject.*

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.data.{AdapterError, CallMethodRequest, CallMethodResult, HistoryReadRequest, ReadResult, WriteRequest, WriteResult}
import cz.modemtec.adapter.api.util.MonadTransformer.FutureEither

import cz.modemtec.adapter.opcua.service.impl.ResourceServiceImpl

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

import scala.concurrent.Future


/** The service interface providing managing and usage of resources.
  */
@ImplementedBy(classOf[ResourceServiceImpl])
trait ResourceService:

  /*
       ---
       Methods useful for controllers and API. Contain also error handling.
       ---
   */

  /** Creates or modifies and stores new resource based on the config.
    */
  def createOrUpdateResource(config: ResourceConfig): FutureEither[AdapterError, ResourceConfig]

  /** Deletes resource by its unique identifier.
    *
    * @param guid Resource's identifier.
    */
  def deleteResource(guid: String): FutureEither[AdapterError, Unit]

  /** Reads resource by its unique identifier.
    *
    * @param guid Resource's identifier.
    * @return
    */
  def readResource(guid: String): FutureEither[AdapterError, ResourceConfig]

  /** Reads all stored resources.
    *
    * @return Resources configs.
    */
  def readAll(): FutureEither[AdapterError, Vector[ResourceConfig]]

  /** Reads node's identifier within the information model of the selected server.
    *
    * @param guid The unique server's identifier.
    * @param path The relative path to the node starting in ObjectsFolder node, e.g. /MyObject/MyVariable corresponds
    *             to the absolute path Root/ObjectsFolder/MyObject/MyVariable.
    * @return Node's identifier.
    */
  def readNodeId(guid: String, path: String): FutureEither[AdapterError, UaId]

  /** Checks whether service has stored resource or whether resource can be connected.
    *
    * @param guid The unique server's identifier.
    * @return None, if everything is correct.
    */
  def checkResource(guid: String): FutureEither[AdapterError, Unit]

  /** Reads a value from the resource.
    *
    * @param guid   The unique server's identifier.
    * @param nodeId Node's identifier.
    * @return Operation result.
    */
  def read(guid: String, nodeId: UaId): FutureEither[AdapterError, ReadResult]

  /** Writes a value to the resource.
    *
    * @param request Request description.
    * @param guid    The unique server's identifier.
    * @param nodeId  Node's identifier.
    * @return Operation result.
    */
  def write(guid: String, nodeId: UaId, request: WriteRequest): FutureEither[AdapterError, WriteResult]

  /** Reads history data as stream.
    *
    * @param guid    The unique server's identifier.
    * @param request Request description.
    * @param nodeId  Node's identifier.
    * @return Operation result.
    */
  def historyRead(guid: String, nodeId: UaId, request: HistoryReadRequest): FutureEither[AdapterError, Source[ReadResult, NotUsed]]

  /** Calls method on the resource.
    *
    * @param guid    The unique server's identifier.
    * @param request Request description.
    * @param nodeId  Node identifier.
    * @return Operation result.
    */
  def callMethod(guid: String, nodeId: UaId, request: CallMethodRequest): FutureEither[AdapterError, CallMethodResult]

  /** Cancels subscription to the selected variable.
    *
    * @param guid  The unique server's identifier.
    * @param subId Subscription's identifier.
    */
  def deleteSubscription(guid: String, subId: UaUInt32): FutureEither[AdapterError, Unit]

  /** Creates subscription to the selected variable.
    *
    * @param guid     The unique server's identifier.
    * @param nodeId   Node's identifier.
    * @param callback Callback handling read data.
    * @return Subscription's identifier.
    */
  def createSubscription(guid: String, nodeId: UaId)(callback: ReadResult => _): FutureEither[AdapterError, UaUInt32]

  /*
       ---
       Methods useful for other app components.
       ---
   */

  /** Loads and stores resources from the database.
    */
  def loadResources(): Future[Unit]

  /** Disposes resources loaded into memory.
    */
  def dispose(): Future[Unit]

  /** Dispose unused resources.
    */
  def optimizeResources(): Future[Unit]

  /** Disposes and deletes all stored resources.
    */
  def deleteAll(): Future[Unit]

