package cz.modemtec.adapter.opcua.model

import cz.modemtec.adapter.api.data.config.SecurityPolicy

import cz.modemtec.scopcua.data.config.UaSecurityPolicy


object SecurityPolicyConverter:

  def toUa(policy: SecurityPolicy): UaSecurityPolicy = policy match 
    case SecurityPolicy.None                => UaSecurityPolicy.None
    case SecurityPolicy.Basic256            => UaSecurityPolicy.Basic256
    case SecurityPolicy.Basic256Sha256      => UaSecurityPolicy.Basic256Sha256
    case SecurityPolicy.Aes128Sha256RsaOaep => UaSecurityPolicy.Aes128Sha256RsaOaep
    case SecurityPolicy.Aes256Sha256RsaPss  => UaSecurityPolicy.Aes256Sha256RsaPss
