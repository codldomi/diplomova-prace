package cz.modemtec.adapter.opcua.service.impl

import com.google.inject.*
import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.data.spec.ResourceSpec
import cz.modemtec.adapter.api.util.MonadTransformer.*
import cz.modemtec.adapter.opcua.repository.{DataTypeRepository, NodeRepository, ResourceRepository}
import cz.modemtec.adapter.opcua.service.ResourceSpecService
import cz.modemtec.adapter.opcua.util.FutureExtension.*
import cz.modemtec.adapter.opcua.util.resourcespecgenerator.impl.ResourceSpecGeneratorImpl
import cz.modemtec.scopcua.data.{UaId, UaIdentifiable}
import cz.modemtec.scopcua.util.jsonschemagenerator.impl.UaModemTecJsonSchemaGenerator

import scala.concurrent.{ExecutionContext, Future}
import scala.util.chaining.scalaUtilChainingOps


@Singleton()
class ResourceSpecServiceImpl @Inject() (
  val typeRepo: DataTypeRepository,
  val nodeRepo: NodeRepository,
  val configRepo: ResourceRepository,
) (using val context: ExecutionContext) extends ResourceSpecService:

  private val generator = ResourceSpecGeneratorImpl(types => UaModemTecJsonSchemaGenerator(types))


  private def readBy[A <: UaIdentifiable](uris: Set[String], read: String => Future[Vector[A]]): Future[Map[UaId, A]] =
    uris
      .toVector
      .map(read)
      .pipe(futures => Future.sequence(futures))
      .map: values =>
        values
          .flatten
          .map(value => (value.id, value))
          .toMap


  override def generate(guid: String): FutureEither[AdapterError, ResourceSpec] =
    val input = for
      config <- configRepo.read(guid)
      uris   <- configRepo.readNamespaces(guid)
      nodes  <- readBy(uris, nodeRepo.read)
      types  <- readBy(uris, typeRepo.read)
    yield (config, nodes, types)

    input
      .map: input =>
        (generator.generate _).tupled(input)
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(message = s"An error has occurred during generation ResourceSpec for the resource with GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def signature(guid: String): FutureEither[AdapterError, String] =
    val input = for
      config <- configRepo.read(guid)
      uris   <- configRepo.readNamespaces(guid)
      nodes  <- readBy(uris, nodeRepo.read)
      types  <- readBy(uris, typeRepo.read)
    yield (config, nodes, types)

    input
      .map: input =>
        (generator.generateSignature _).tupled(input)
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(message = s"An error has occurred during generation ResourceSpec's signature for the resource with GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither

