package cz.modemtec.adapter.opcua.controller

import com.google.inject.*

import cz.modemtec.adapter.api.data.CertData
import cz.modemtec.adapter.api.util.AdapterApiFormat

import cz.modemtec.adapter.opcua.service.{PkiService, ResourceService}
import cz.modemtec.adapter.opcua.util.HttpExtension

import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import scala.concurrent.ExecutionContext


@Singleton()
class PkiController @Inject() (
  val controllerComponents: ControllerComponents,
  val pki: PkiService,
  val resources: ResourceService
) (using val context: ExecutionContext) extends BaseController with AdapterApiFormat with HttpExtension:

  /** Reads certificate with its metadata.
    *
    * Responses:
    *
    *   - 200: CertData - Successful operation.
    *   - 404: AdapterError - Certificate doesn't exist.
    *   - 500: AdapterError - Error during operation occurred.
    *
    * @param guid Certificate globally unique identifier.
    */
  def readTrusted(guid: String): Action[AnyContent] = Action.async:
    pki
      .readTrusted(guid)
      .value
      .map(cert => cert.asHttpResult)

  /** Creates or updates certificate with its metadata.
    *
    * Responses:
    *
    *   - 200: CertData - Successful operation.
    *   - 400: AdapterError - Bad data in certificate or metadata.
    *   - 500: AdapterError - Error during operation occurred.
    *
    * @param guid Certificate globally unique identifier.
    */
  def createOrUpdateTrusted(guid: String): Action[CertData] = Action(parse.json[CertData]).async: request =>
    pki
    .createOrUpdateTrusted(guid, request.body)
    .value
    .map(cert => cert.asHttpResult)

  /** Deletes certificate.
    *
    * Responses:
    *
    *   - 200: Nothing - Successful operation
    *   - 500: AdapterError - Error during operation occurred.
    *
    * @param guid Certificate globally unique identifier.
    */
  def deleteTrusted(guid: String): Action[AnyContent] = Action.async:
    pki
      .deleteTrusted(guid)
      .value
      .map(_.asEmptyHttpResult)

  /** Reads all trusted certificates GUIDs.
    *
    * Responses:
    *
    *   - 200: Vector[String] - Successful operation.
    *   - 500: AdapterError - Error during operation occurred.
    */
  def readAllTrustedGuids: Action[AnyContent] = Action.async:
    pki
      .readAllTrustedGuids()
      .value
      .map(guids => guids.asHttpResult)
