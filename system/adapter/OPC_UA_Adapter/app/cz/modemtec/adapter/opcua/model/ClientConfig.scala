package cz.modemtec.adapter.opcua.model

import cz.modemtec.adapter.api.data.config.SecurityPolicy


case class ClientConfig(
  name: String,
  uri: String,
  timeout: Int = 5000,
  policy: SecurityPolicy = SecurityPolicy.None
)
