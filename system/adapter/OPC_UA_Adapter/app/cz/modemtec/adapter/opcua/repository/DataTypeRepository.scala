package cz.modemtec.adapter.opcua.repository

import com.google.inject.*

import cz.modemtec.adapter.opcua.repository.impl.DataTypeRepositoryImpl
import cz.modemtec.scopcua.data.UaDataType

import scala.concurrent.Future


/** The repository interface providing managing OPC UA data types.
  */
@ImplementedBy(classOf[DataTypeRepositoryImpl])
trait DataTypeRepository:

  /** Creates data types.
    *
    * @param types Data types.
    */
  def createAll(types: Iterable[UaDataType]): Future[Unit]

  /** Deletes all stored data types.
    */
  def deleteAll(): Future[Unit]

  /** Reads all stored data types.
    *
    * @return Data types.
    */
  def readAll(): Future[Vector[UaDataType]]

  /** Reads data types for selected namespace.
    *
    * @param uri Namespace's URI.
    * @return Data types.
    */
  def read(uri: String): Future[Vector[UaDataType]]
