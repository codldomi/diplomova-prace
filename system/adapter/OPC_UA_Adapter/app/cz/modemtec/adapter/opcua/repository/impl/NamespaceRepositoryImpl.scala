package cz.modemtec.adapter.opcua.repository.impl

import cz.modemtec.adapter.opcua.repository.{DatabaseSchema, NamespaceRepository, QueryExecutor}

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import slick.jdbc.JdbcProfile

import javax.inject.*
import scala.concurrent.{ExecutionContext, Future}


@Singleton()
class NamespaceRepositoryImpl @Inject()
  (protected val dbConfigProvider: DatabaseConfigProvider)
  (using val executionContext: ExecutionContext)
  extends NamespaceRepository
    with DatabaseSchema
    with QueryExecutor
    with HasDatabaseConfigProvider[JdbcProfile]:

  import profile.api.*


  override def create(namespace: String): Future[Unit] = 
    val insertion = (Namespaces += NamespaceRow(0, namespace)).map(_ => ())
    execute(insertion)


  override def createAll(namespaces: Set[String]): Future[Unit] =
    readAll()
      .flatMap: stored =>
        val news = namespaces -- stored
        val insertion = Namespaces ++= news.map(uri => NamespaceRow(0, uri))
        transaction(insertion)
      .map(_ => ())


  override def deleteAll(): Future[Unit] = 
    val deletion = Namespaces.delete.map(_ => ())
    transaction(deletion)


  override def readAll(): Future[Set[String]] = 
    val selectAll = Namespaces.map(_.uri).result.map(_.toSet)
    transaction(selectAll)


  override def delete(uri: String): Future[Unit] = 
    val deletion = for 
      ids <- readIds(uri)
      _   <- deleteNodes(ids)
      _   <- deleteDataTypes(ids)
      _   <- deleteAttachedNamespaces(uri)
      _   <- Ids.filter(_.id.inSet(ids)).delete
      _   <- Namespaces.filter(_.uri === uri).delete
    yield ()

    transaction(deletion)


  private def readIds(uri: String): DBIO[Set[Long]] = 
    val selection = for 
      ns <- Namespaces if ns.uri === uri
      id <- Ids        if id.namespaceId === ns.id
    yield id.id

    selection.result.map(_.toSet)


  private def deleteAttachedNamespaces(uri: String): DBIO[Unit] = 
    val selection = for 
      ns       <- Namespaces         if ns.uri === uri
      attached <- ResourceNamespaces if attached.namespaceId === ns.id
    yield attached.id

    selection
      .result
      .flatMap(attachedIds => ResourceNamespaces.filter(_.id.inSet(attachedIds)).delete)
      .map(_ => ())


  private def deleteNodes(ids: Set[Long]): DBIO[Unit] =
    for 
      _ <- Arguments.filter(_.methodId.inSet(ids)).delete 
      _ <- Nodes.filter(_.id.inSet(ids)).delete 
    yield ()


  private def deleteDataTypes(ids: Set[Long]): DBIO[Unit] =
    for 
      _ <- StructFields.filter(_.structureId.inSet(ids)).delete 
      _ <- EnumFields.filter(_.dataTypeId.inSet(ids)).delete 
      _ <- DataTypes.filter(_.id.inSet(ids)).delete 
    yield ()

