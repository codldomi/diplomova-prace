package cz.modemtec.adapter.opcua.repository.impl

import cz.modemtec.adapter.api.data.config.{AuthConfig, ResourceConfig}
import cz.modemtec.adapter.api.data.config.SecurityPolicy

import cz.modemtec.adapter.opcua.repository.{DatabaseSchema, QueryExecutor, ResourceRepository}

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.dbio.DBIO.*
import slick.jdbc.JdbcProfile

import javax.inject.*
import scala.concurrent.{ExecutionContext, Future}


@Singleton()
class ResourceRepositoryImpl @Inject()
  (protected val dbConfigProvider: DatabaseConfigProvider)
  (using val executionContext: ExecutionContext)
  extends ResourceRepository
    with DatabaseSchema
    with QueryExecutor
    with HasDatabaseConfigProvider[JdbcProfile]:

  import profile.api.*


  private def configToRow(config: ResourceConfig): ResourceRow = 
    val policyType = config.policy.map:
      case SecurityPolicy.None                => SecurityPolicyType.None
      case SecurityPolicy.Basic256            => SecurityPolicyType.Basic256
      case SecurityPolicy.Basic256Sha256      => SecurityPolicyType.Basic256Sha256
      case SecurityPolicy.Aes128Sha256RsaOaep => SecurityPolicyType.Aes128Sha256RsaOaep
      case SecurityPolicy.Aes256Sha256RsaPss  => SecurityPolicyType.Aes256Sha256RsaPss
    
    val (username, password) = config.auth match 
      case Some(auth) => (Some(auth.username), Some(auth.password))
      case None       => (None, None)
    
    ResourceRow(id = 0, config.guid, config.connstr, policyType, username, password)


  private def rowToConfig(row: ResourceRow): ResourceConfig = 
    val policy = row.securityPolicy.map:
      case SecurityPolicyType.Basic256            => SecurityPolicy.Basic256
      case SecurityPolicyType.Basic256Sha256      => SecurityPolicy.Basic256Sha256
      case SecurityPolicyType.Aes128Sha256RsaOaep => SecurityPolicy.Aes128Sha256RsaOaep
      case SecurityPolicyType.Aes256Sha256RsaPss  => SecurityPolicy.Aes256Sha256RsaPss
      case SecurityPolicyType.None                => SecurityPolicy.None

    val auth = (row.username, row.password) match 
      case (Some(username), Some(password)) => Some(AuthConfig(username, password))
      case (None,           Some(password)) => Some(AuthConfig("", password))
      case (Some(username), None          ) => Some(AuthConfig(username, ""))
      case (None,           None          ) => None

    ResourceConfig(row.server, row.url, policy, auth)


  private def readNamespaces(uris: Set[String]): DBIO[Map[String, Long]] =
    Namespaces
      .filter(_.uri.inSet(uris))
      .result
      .map: namespaces => 
        namespaces
          .map: namespace =>
            (namespace.uri, namespace.id)
          .toMap


  private def modifyNamespaces(resourceId: Long, namespaces: Map[String, Long]): DBIO[Unit] =
    for 
      _ <- ResourceNamespaces.filter(_.resourceId === resourceId).delete 
      _ <- ResourceNamespaces ++= namespaces.map: (_, nsId) =>
        ResourceNamespaceRow(0, resourceId, nsId)
    yield ()


  override def create(config: ResourceConfig): Future[Unit] = 
    val row = configToRow(config)
    val insertion = (Resources += row).map(_ => ())

    execute(insertion)


  override def readNamespaces(guid: String): Future[Set[String]] = 
    val rows = for 
      res   <- Resources          if res.guid === guid
      resns <- ResourceNamespaces if resns.resourceId === resns.id
      ns    <- Namespaces         if resns.namespaceId === ns.id
    yield ns.uri

    val selection = rows.result.map(_.toSet)

    execute(selection)


  override def delete(guid: String): Future[Unit] = 
    val serverSelection =
      Resources
        .filter(_.guid === guid)
        .map(_.id)
        .result
        .headOption

    val namespacesDeletion = (serverId: Long) =>
      ResourceNamespaces
        .filter(_.resourceId === serverId)
        .delete

    val resourceDeletion =
      Resources
        .filter(_.guid === guid)
        .delete
        .map(_ => ())

    val deletion = for 
      serverId <- serverSelection
      _        <- if serverId.isDefined then namespacesDeletion(serverId.get) else DBIO.successful({})
      _        <- resourceDeletion
    yield ()

    transaction(deletion)


  override def read(guid: String): Future[ResourceConfig] = 
    val selectResourceByServer =
      Resources
        .filter(_.guid === guid)
        .result
        .head
        .map(rowToConfig)

    execute(selectResourceByServer)


  override def readAll(): Future[Vector[ResourceConfig]] = 
    val selectAll =
      Resources
        .result
        .map(rows => rows.map(rowToConfig).toVector)

    execute(selectAll)


  override def deleteAll(): Future[Unit] = 
    val deletion = for 
      _ <- Resources.delete
      _ <- ResourceNamespaces.delete
    yield ()

    transaction(deletion)


  override def attachNamespaces(guid: String, uris: Set[String]): Future[Unit] = 
    val attachment = for 
      namespaces <- readNamespaces(uris)
      resourceId <- Resources.filter(_.guid === guid).result.head.map(_.id)
      _          <- modifyNamespaces(resourceId, namespaces)
    yield ()

    transaction(attachment)


  def detachNamespaces(guid: String): Future[Unit] = 
    val serverSelection =
      Resources
        .filter(_.guid === guid)
        .map(_.id)
        .result
        .headOption

    val namespacesDeletion = (serverId: Long) =>
      ResourceNamespaces
        .filter(_.resourceId === serverId)
        .delete
      
    val action = for 
      serverId <- serverSelection
      _        <- if serverId.isDefined then namespacesDeletion(serverId.get) else DBIO.successful({})
    yield ()

    transaction(action)
