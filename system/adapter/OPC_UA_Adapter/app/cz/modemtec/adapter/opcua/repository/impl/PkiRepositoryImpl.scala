package cz.modemtec.adapter.opcua.repository.impl

import com.google.inject.*

import cz.modemtec.adapter.api.data.CertData
import cz.modemtec.adapter.api.util.PkiUtil.{createX509Cert, asBase64}

import cz.modemtec.adapter.opcua.repository.{DatabaseSchema, PkiRepository, QueryExecutor}

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.dbio.DBIO.*
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


@Singleton()
class PkiRepositoryImpl @Inject()
  (protected val dbConfigProvider: DatabaseConfigProvider)
  (using val executionContext: ExecutionContext)
  extends PkiRepository
    with DatabaseSchema
    with QueryExecutor
    with HasDatabaseConfigProvider[JdbcProfile]:

  import profile.api.*


  override def readTrusted(guid: String): Future[CertData] =
    val selection =
      Certificates
        .filter(_.certType === CertificateType.Trusted)
        .filter(_.guid === Some(guid))
        .result
        .head
        .map: row =>
          CertData(guid, cert = createX509Cert(row.encoded))

    execute(selection)


  override def createTrusted(data: CertData): Future[Unit] =
    val row = CertificateRow(
      id = 0,
      guid = Some(data.guid),
      certType = CertificateType.Trusted,
      encoded = data.cert.asBase64)

    val insertion = (Certificates += row).map(_ => ())

    execute(insertion)


  override def deleteTrusted(guid: String): Future[Unit] =
    val deletion =
      Certificates
        .filter(_.guid === Some(guid))
        .delete
        .map(_ => ())

    execute(deletion)


  override def readAllTrustedGuids(): Future[Vector[String]] =
    val selection =
      Certificates
        .filter(_.certType === CertificateType.Trusted)
        .result
        .map: rows =>
          rows
            .map(_.guid.get)
            .toVector

    execute(selection)


  override def readAllTrusted(): Future[Vector[CertData]] =
    val selection =
      Certificates
        .filter(_.certType === CertificateType.Trusted)
        .result
        .map: rows =>
          rows
            .map: row =>
              CertData(row.guid.get, createX509Cert(row.encoded))
            .toVector

    execute(selection)


  override def deleteAll(): Future[Unit] =
    val deletion = Certificates.delete.map(_ => ())
    execute(deletion)

