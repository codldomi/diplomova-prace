package cz.modemtec.adapter.opcua.repository

import play.api.db.slick.HasDatabaseConfigProvider

import slick.jdbc.JdbcProfile


trait DatabaseSchema:
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api.*

  //  CERTIFICATE TABLE -->

  object CertificateType extends Enumeration:
    type CertificateType = Value

    val Trusted: CertificateType.Value = Value(1, "trusted")
    val Rejected: CertificateType.Value = Value(2, "rejected")
    val Issuer: CertificateType.Value = Value(3, "issuer")

  import CertificateType.*

  given certificateTypeMapper: BaseColumnType[CertificateType.CertificateType] = MappedColumnType.base[CertificateType, Int](_.id, CertificateType(_))

  case class CertificateRow(
    id: Long,
    guid: Option[String],
    certType: CertificateType,
    encoded: String
  )

  class CertificateTable(tag: Tag) extends Table[CertificateRow](tag, "t_certificate"):
    def id = column[Long]("c_id", O.PrimaryKey)
    def guid = column[Option[String]]("c_guid")
    def certType = column[CertificateType]("c_type" /*,O.Length(1, varying = false)*/)
    def encoded = column[String]("c_encoded")
    def * = (id, guid, certType, encoded) <> ((CertificateRow.apply _).tupled, CertificateRow.unapply)


  lazy val Certificates = TableQuery[CertificateTable]

  //  DATATYPE TABLE  -->

  object DataTypeType extends Enumeration:
    type DataTypeType = Value

    val BuiltIn: DataTypeType.Value = Value(1, "builtin")
    val Struct: DataTypeType.Value = Value(2, "struct")
    val Enum: DataTypeType.Value = Value(3, "enum")


  import DataTypeType.*

  given dataTypeTypeMapper: BaseColumnType[DataTypeType.DataTypeType] = MappedColumnType.base[DataTypeType, Int](_.id, DataTypeType(_))

  case class DataTypeRow(
    id: Long,
    superId: Option[Long],
    name: String,
    isAbstract: Boolean,
    dataTypeType: DataTypeType
  )

  class DataTypeTable(tag: Tag) extends Table[DataTypeRow](tag, "t_data_type"):
    def id = column[Long]("c_id", O.PrimaryKey)
    def superId = column[Option[Long]]("c_super_id", O.Default(None))
    def name = column[String]("c_name")
    def isAbstract = column[Boolean]("c_is_abstract")
    def dataTypeType = column[DataTypeType]("c_type"/*,O.Length(1, varying = false)*/)
    def * = (id, superId, name, isAbstract, dataTypeType) <> ((DataTypeRow.apply _).tupled, DataTypeRow.unapply)


  lazy val DataTypes = TableQuery[DataTypeTable]

  //  ENUM FIELD TABLE

  case class EnumFieldRow(
    id: Long,
    dataTypeId: Long,
    name: String,
    value: Int
  )

  class EnumFieldTable(tag: Tag) extends Table[EnumFieldRow](tag, "t_enum_field"):
    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def dataTypeId = column[Long]("c_data_type_id")
    def name = column[String]("c_name")
    def value = column[Int]("c_value")
    def * = (id, dataTypeId, name, value) <> ((EnumFieldRow.apply _).tupled, EnumFieldRow.unapply)


  lazy val EnumFields = TableQuery[EnumFieldTable]

  //  STRUCT FIELD TABLE

  case class StructFieldRow(
    id: Long,
    structureId: Long,
    typeId: Long,
    name: String,
    valueRank: Int,
    arrayDimensions: String,
    isOptional: Boolean,
    ord: Int
  )

  class StructFieldTable(tag: Tag) extends Table[StructFieldRow](tag, "t_struct_field"):
    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def structureId = column[Long]("c_structure_id")
    def typeId = column[Long]("c_type_id")
    def name = column[String]("c_name")
    def valueRank = column[Int]("c_value_rank")
    def arrayDimensions = column[String]("c_array_dimensions")
    def isOptional = column[Boolean]("c_is_optional")
    def ord = column[Int]("c_ord")
    def * = (id, structureId, typeId, name, valueRank, arrayDimensions, isOptional, ord) <> ((StructFieldRow.apply _).tupled, StructFieldRow.unapply)


  lazy val StructFields = TableQuery[StructFieldTable]

  //  NAMESPACE TABLE  -->

  case class NamespaceRow(
    id: Long,
    uri: String
  )

  class NamespaceTable(tag: Tag) extends Table[NamespaceRow](tag, "t_namespace"):
    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def uri = column[String]("c_uri", O.Unique)
    def * = (id, uri) <> ((NamespaceRow.apply _).tupled, NamespaceRow.unapply)


  lazy val Namespaces = TableQuery[NamespaceTable]

  //  ID TABLE  -->

  case class IdRow(
    id: Long,
    index: Int,
    namespaceId: Long
  )

  class IdTable(tag: Tag) extends Table[IdRow](tag, "t_id"):
    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def index = column[Int]("c_index")
    def namespaceId = column[Long]("c_namespace_id")
    def * = (id, index, namespaceId) <> ((IdRow.apply _).tupled, IdRow.unapply)


  lazy val Ids = TableQuery[IdTable]

  //  NODE TABLE  -->

  object NodeType extends Enumeration:
    type NodeType = Value

    val Object: NodeType.Value = Value(1, "object")
    val Method: NodeType.Value = Value(2, "method")
    val Variable: NodeType.Value = Value(3, "variable")


  import NodeType.*

  given nodeTypeMapper: BaseColumnType[NodeType.NodeType] = MappedColumnType.base[NodeType, Int](_.id, NodeType(_))

  case class NodeRow(
    id: Long,
    browseName: String,
    parentId: Option[Long],
    nodeType: NodeType,
    typeId: Option[Long],
    displayName: String,
    description: Option[String],
    valueRank: Option[Int],
    arrayDimensions: Option[String]
  )

  class NodeTable(tag: Tag) extends Table[NodeRow](tag, "t_node"):
    def id = column[Long]("c_id", O.PrimaryKey)
    def browseName = column[String]("c_browse_name")
    def parentId = column[Option[Long]]("c_parent_id")
    def nodeType = column[NodeType]("c_type"/*,O.Length(1, varying = false)*/)
    def typeId = column[Option[Long]]("c_data_type_id", O.Default(None))
    def displayName = column[String]("c_display_name")
    def description = column[Option[String]]("c_description")
    def valueRank = column[Option[Int]]("c_value_rank")
    def arrayDimensions = column[Option[String]]("c_array_dimensions")
    def * = (id, browseName, parentId, nodeType, typeId, displayName, description, valueRank, arrayDimensions) <> ((NodeRow.apply _).tupled, NodeRow.unapply)


  lazy val Nodes = TableQuery[NodeTable]

  //  ARGUMENT TABLE  -->

  case class ArgumentRow(
    id: Long,
    isInput: Boolean,
    ord: Int,
    name: String,
    methodId: Long,
    typeId: Long,
    valueRank: Int,
    arrayDimensions: String
  )

  class ArgumentTable(tag: Tag) extends Table[ArgumentRow](tag, "t_argument"):

    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def isInput = column[Boolean]("c_is_input")
    def ord = column[Int]("c_ord")
    def name = column[String]("c_name")
    def methodId = column[Long]("c_node_id")
    def typeId = column[Long]("c_type_id")
    def valueRank = column[Int]("c_value_rank")
    def arrayDimensions = column[String]("c_array_dimensions")
    def * = (id, isInput, ord, name, methodId, typeId, valueRank, arrayDimensions) <> ((ArgumentRow.apply _).tupled, ArgumentRow.unapply)


  lazy val Arguments = TableQuery[ArgumentTable]

  //  RESOURCE TABLE  -->

  object SecurityPolicyType extends Enumeration:
    type SecurityPolicyType = Value

    val None: SecurityPolicyType.Value = Value(1, "None")
    val Basic256: SecurityPolicyType.Value = Value(2, "Basic256")
    val Basic256Sha256: SecurityPolicyType.Value = Value(3, "Basic256Sha256")
    val Aes128Sha256RsaOaep: SecurityPolicyType.Value = Value(4, "Aes128Sha256RsaOaep")
    val Aes256Sha256RsaPss: SecurityPolicyType.Value = Value(5, "Aes256Sha256RsaPss")


  import SecurityPolicyType.*

  given securityPolicyTypeMapper: BaseColumnType[SecurityPolicyType.SecurityPolicyType] = MappedColumnType.base[SecurityPolicyType, Int](_.id, SecurityPolicyType(_))

  case class ResourceRow(
    id: Long,
    server: String,
    url: String,
    securityPolicy: Option[SecurityPolicyType],
    username: Option[String],
    password: Option[String]
  )

  class ResourceTable(tag: Tag) extends Table[ResourceRow](tag, "t_resource"):
    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def guid = column[String]("c_guid", O.Unique)
    def url = column[String]("c_url")
    def securityPolicy = column[Option[SecurityPolicyType]]("c_security_policy")
    def username = column[Option[String]]("c_username")
    def password = column[Option[String]]("c_password")
    def * = (id, guid, url, securityPolicy, username, password) <> ((ResourceRow.apply _).tupled, ResourceRow.unapply)


  lazy val Resources = TableQuery[ResourceTable]

  //  RESOURCE NAMESPACE TABLE  -->

  case class ResourceNamespaceRow(
    id: Long,
    resourceId: Long,
    namespaceId: Long
  )

  class ResourceNamespaceTable(tag: Tag) extends Table[ResourceNamespaceRow](tag, "t_resource_namespace"):
    def id = column[Long]("c_id", O.PrimaryKey, O.AutoInc)
    def resourceId = column[Long]("c_resource_id")
    def namespaceId = column[Long]("c_namespace_id")
    def * = (id, resourceId, namespaceId) <> ((ResourceNamespaceRow.apply _).tupled, ResourceNamespaceRow.unapply)


  lazy val ResourceNamespaces = TableQuery[ResourceNamespaceTable]

