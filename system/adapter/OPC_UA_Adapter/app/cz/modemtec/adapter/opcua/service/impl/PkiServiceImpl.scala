package cz.modemtec.adapter.opcua.service.impl

import com.google.common.collect.ImmutableList
import com.google.inject.*

import cz.modemtec.adapter.api.data.{AdapterError, CertData}
import cz.modemtec.adapter.api.util.MonadTransformer.*

import cz.modemtec.adapter.opcua.repository.PkiRepository
import cz.modemtec.adapter.opcua.service.PkiService
import cz.modemtec.adapter.opcua.util.FutureExtension.*

import cz.modemtec.scopcua.util.trustlistmanager.UaTrustListManager

import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString

import java.security.cert.{X509CRL, X509Certificate}
import java.util

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.jdk.CollectionConverters.SeqHasAsJava


@Singleton()
class PkiServiceImpl @Inject() (
  val repository: PkiRepository
) (using val context: ExecutionContext) extends PkiService:

  private val manager = new UaTrustListManager:

    //  read-only for trusted certificates - todo: change in the future to provide more functionalities with PKI

    override def getIssuerCrls: ImmutableList[X509CRL] = ImmutableList.builder().build()

    override def getTrustedCrls: ImmutableList[X509CRL] = ImmutableList.builder().build()

    override def getIssuerCertificates: ImmutableList[X509Certificate] = ImmutableList.builder().build()

    override def getTrustedCertificates: ImmutableList[X509Certificate] =
      val future =
        repository
          .readAllTrusted()
          .map: data =>
            val certs = data.map(_.cert)
            ImmutableList.copyOf(certs.asJava)

      Await.result(future, 2.minutes)

    override def getRejectedCertificates: ImmutableList[X509Certificate] = ImmutableList.builder().build()

    override def setIssuerCrls(issuerCrls: util.List[X509CRL]): Unit = {}

    override def setTrustedCrls(trustedCrls: util.List[X509CRL]): Unit = {}

    override def setIssuerCertificates(issuerCertificates: util.List[X509Certificate]): Unit = {}

    override def setTrustedCertificates(trustedCertificates: util.List[X509Certificate]): Unit = {}

    override def addIssuerCertificate(certificate: X509Certificate): Unit = {}

    override def addTrustedCertificate(certificate: X509Certificate): Unit = {}

    override def addRejectedCertificate(certificate: X509Certificate): Unit = {}

    override def removeIssuerCertificate(thumbprint: ByteString): Boolean = false

    override def removeTrustedCertificate(thumbprint: ByteString): Boolean = false

    override def removeRejectedCertificate(thumbprint: ByteString): Boolean = false

  end manager


  override def trustListManager: UaTrustListManager = manager


  override def createOrUpdateTrusted(guid: String, data: CertData): FutureEither[AdapterError, CertData] = 
    val action = for 
      _    <- repository.deleteTrusted(guid)
      _    <- repository.createTrusted(data)
      cert <- repository.readTrusted(guid)
    yield cert

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the modifying certificate with $guid in the trusted list. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def readTrusted(guid: String): FutureEither[AdapterError, CertData] =
    repository
      .readTrusted(guid)
      .withAdapterError: exception =>
        AdapterError.CertNotFound(s"The certificate with GUID $guid is possibly not found. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def deleteTrusted(guid: String): FutureEither[AdapterError, Unit] =
    repository
      .deleteTrusted(guid)
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the deletion of the certificate with GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def readAllTrustedGuids(): FutureEither[AdapterError, Vector[String]] =
    repository
      .readAllTrustedGuids()
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the reading GUIDs of all stored trusted certificates. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def deleteAll(): Future[Unit] = repository.deleteAll()

