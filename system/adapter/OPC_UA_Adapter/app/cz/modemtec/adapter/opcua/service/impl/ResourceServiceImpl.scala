package cz.modemtec.adapter.opcua.service.impl

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.data.{AdapterError, CallMethodRequest, CallMethodResult, HistoryReadRequest, ReadResult, WriteRequest, WriteResult}
import cz.modemtec.adapter.api.util.MonadTransformer.*

import cz.modemtec.adapter.opcua.boot.OpcUaLoader
import cz.modemtec.adapter.opcua.model.SecurityPolicyConverter
import cz.modemtec.adapter.opcua.repository.{NodeRepository, ResourceRepository}
import cz.modemtec.adapter.opcua.service.{PkiService, ResourceService}
import cz.modemtec.adapter.opcua.service.impl.util.*
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.FutureExtension.*

import cz.modemtec.scopcua.data.UaId
import cz.modemtec.scopcua.data.config.{UaAuthConfig, UaClientConfig}
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

import org.reactivestreams.{Publisher, Subscriber}

import play.api.Logging
import play.api.inject.ApplicationLifecycle

import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}
import com.google.inject.*

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.jdk.CollectionConverters.MapHasAsScala


@Singleton()
class ResourceServiceImpl @Inject() (
  val lifecycle: ApplicationLifecycle,
  val loader: OpcUaLoader,
  val dataTypeManager: DataTypeManager,
  val nodes: NodeRepository,
  val configs: ResourceRepository,
  val pki: PkiService
) (using val context: ExecutionContext) extends ResourceService with Logging:

  private val defaultConfig =
    val cfg = loader.loadClientConfig()
    val withTrustList = cfg.encryption.map: encryption =>
      encryption.copy(trustList = pki.trustListManager)
    cfg.copy(encryption = withTrustList)

  private val clients = new ConcurrentHashMap[String, ProxyClient]()


  private def toUaClientConfig(config: ResourceConfig): UaClientConfig =
    val auth = config.auth.map(auth => UaAuthConfig(auth.username, auth.password))
    val policy = config.policy.map(SecurityPolicyConverter.toUa).getOrElse(defaultConfig.policy)

    defaultConfig.copy(auth = auth, policy = policy, url = config.connstr)


  override def read(guid: String, nodeId: UaId): FutureEither[AdapterError, ReadResult] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))

    val action = for 
      variable <- nodes.readVariable(nodeId)
      result   <- client.read(variable)
    yield result

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the operation read on node with NodeId $nodeId and resource GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither
  

  override def write(guid: String, nodeId: UaId, request: WriteRequest): FutureEither[AdapterError, WriteResult] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))

    val action = for 
      variable <- nodes.readVariable(nodeId)
      result   <- client.write(variable, request)
    yield result

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the operation write on node with NodeId $nodeId and resource GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither
  

  override def callMethod(guid: String, nodeId: UaId, request: CallMethodRequest): FutureEither[AdapterError, CallMethodResult] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))

    val action = for 
      method <- nodes.readMethod(nodeId)
      result <- client.callMethod(method, request)
    yield result

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the operation callMethod on node with NodeId $nodeId and resource GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither
  
  
  override def loadResources(): Future[Unit] =
    configs
      .readAll()
      .map(_.foreach(add))


  private def add(config: ResourceConfig): Unit = 
    clients.remove(config.guid)
    val clientConfig = toUaClientConfig(config)
    val proxy = ProxyClientImpl(dataTypeManager, config.guid, clientConfig, configs)
    clients.put(config.guid, proxy)


  override def createOrUpdateResource(config: ResourceConfig): FutureEither[AdapterError, ResourceConfig] = 
    val action = for 
      _       <- configs.delete(config.guid)
      _       <- configs.create(config)
      _       <- Future(add(config))
      created <- configs.read(config.guid)
    yield created

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the creating resource. Exception message: ${exception.getMessage}")
      .asFutureEither
  

  override def readResource(guid: String): FutureEither[AdapterError, ResourceConfig] =
    configs
      .read(guid)
      .withAdapterError: _ =>
        AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found.")
      .asFutureEither


  override def deleteResource(guid: String): FutureEither[AdapterError, Unit] =
    val client = clients.remove(guid)

    val action = for 
      _ <- configs.delete(guid)
      _ <- if (client != null) client.dispose() else Future.unit
    yield ()

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during deleting Resource. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def readAll(): FutureEither[AdapterError, Vector[ResourceConfig]] =
    configs
      .readAll()
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during reading all Resources. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def readNodeId(guid: String, path: String): FutureEither[AdapterError, UaId] =
    nodes
      .readNodeId(guid, path)
      .withAdapterError: _ =>
        AdapterError.NodeNotFound(s"Node with path $path and for resource with GUID $guid is not found.")
      .asFutureEither


  override def deleteAll(): Future[Unit] = for 
    _ <- dispose()
    _ <- configs.deleteAll()
  yield ()


  override def dispose(): Future[Unit] = 
    val actions = clients.asScala.map: (_, client) =>
      client.dispose()
    Future.sequence(actions).map(_ => ())
  
  
  override def historyRead(guid: String, nodeId: UaId, request: HistoryReadRequest): FutureEither[AdapterError, Source[ReadResult, NotUsed]] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))

    val action =
      nodes
        .readVariable(nodeId)
        .map: node =>
          val publisher = new Publisher[ReadResult]:
            override def subscribe(s: Subscriber[_ >: ReadResult]): Unit = 
              val client = clients.get(guid)
              val history = client.historyRead(node, request)(data => s.onNext(data))
              
              //  future is completed when history from the selected interval is all gathered
              val result = history.map(_ => s.onComplete())
              
              //  todo: refactor somehow without "magic" numbers
              Await.ready(result, 10.minute)

          Source.fromPublisher(publisher)

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the operation historyRead on node with NodeId $nodeId and resource GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither
    

  def deleteSubscription(guid: String, subId: UaUInt32): FutureEither[AdapterError, Unit] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))
    
    client
      .deleteSubscription(subId)
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the operation cancelSubscription with ID $subId and resource GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def createSubscription(guid: String, nodeId: UaId)(callback: ReadResult => _): FutureEither[AdapterError, UaUInt32] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))

    val action = for 
      variable <- nodes.readVariable(nodeId)
      subId    <- client.createSubscription()
      _        <- client.createMonitoredItem(subId, variable) { (result, _, _) => callback(result) }
    yield subId

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"An error occurred during the operation subscribe on node with NodeId $nodeId and resource GUID $guid. Exception message: ${exception.getMessage}")
      .asFutureEither


  override def checkResource(guid: String): FutureEither[AdapterError, Unit] = 
    val client = clients.get(guid)

    if client == null then return FutureEither.left(AdapterError.ResourceNotFound(s"Resource with GUID $guid is not found."))
    
    client
      .check()
      .withAdapterError: _ =>
        AdapterError.ResourceNotConnected(s"Resource with GUID $guid can't be connected or namespaces can't be updated.")
      .asFutureEither

  
  override def optimizeResources(): Future[Unit] =
    val actions =
      clients
        .asScala
        .map:
          case (_, client) =>
            client.optimize()

    Future.sequence(actions).map(_ => ())
      
