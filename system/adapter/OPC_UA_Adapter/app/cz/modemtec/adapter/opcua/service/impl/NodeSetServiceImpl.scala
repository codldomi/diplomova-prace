package cz.modemtec.adapter.opcua.service.impl

import com.google.inject.*

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.util.MonadTransformer.*

import cz.modemtec.adapter.opcua.model.Constants
import cz.modemtec.adapter.opcua.repository.{DataTypeRepository, NamespaceRepository, NodeRepository}
import cz.modemtec.adapter.opcua.service.NodeSetService
import cz.modemtec.adapter.opcua.util.datatypemanager.DataTypeManager
import cz.modemtec.adapter.opcua.util.FutureExtension.*

import cz.modemtec.scopcua.data.node.{UaNode, UaNodeSet}
import cz.modemtec.scopcua.util.nodesetextractor.impl.UaNodeSet1v04Extractor
import cz.modemtec.scopcua.data.UaDataType

import play.api.Logging

import scala.xml.Node as XmlNode
import scala.concurrent.{ExecutionContext, Future}
import scala.util.chaining.scalaUtilChainingOps
import scala.util.{Failure, Success, Try}


@Singleton()
class NodeSetServiceImpl @Inject() (
  val dataTypeManager: DataTypeManager,
  val namespaceRepo: NamespaceRepository,
  val nodeRepo: NodeRepository,
  val dataTypeRepo: DataTypeRepository
) (using val context: ExecutionContext) extends NodeSetService with Logging with Constants:


  override def extract(xml: XmlNode): Either[AdapterError, UaNodeSet] =
    val result = Try:
      UaNodeSet1v04Extractor.extract(xml)

    result match
      case Success(value) => Right(value)
      case Failure(_) =>
        val error = AdapterError.BadInputData(message = "The format of UaNodeSet2.xml is invalid. Supported version: 1.04.")
        Left(error)


  override def create(nodeSet: UaNodeSet): FutureEither[AdapterError, Unit] =
    logger.debug(s"Inserting nodeset ${nodeSet.modelUri} into database.")

    val action = for
      _ <- {
        logger.debug(s"Inserting ${nodeSet.uris.size} URIs into database.")
        namespaceRepo.createAll(nodeSet.uris)
      }
      _ <- {
        logger.debug(s"Inserting ${nodeSet.dataTypes.size} data types into database.")
        dataTypeRepo.createAll(nodeSet.dataTypes.values)
      }
      _ <- Future {
        nodeSet.dataTypes.values.map(dataTypeManager.add)
      }
      _ <- {
        logger.debug(s"Inserting ${nodeSet.nodes.size} nodes into database.")
        nodeRepo.createAll(nodeSet.nodes.values)
      }
    yield logger.debug(s"Nodeset ${nodeSet.modelUri} inserted.")

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"During creating of NodeSet an exception was thrown. Its message: ${exception.getMessage}")
      .asFutureEither


  override def readNamespaces(): FutureEither[AdapterError, Set[String]] =
    val action = namespaceRepo.readAll()

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"During reading of all namespaces an exception was thrown. Its message: ${exception.getMessage}")
      .asFutureEither


  override def delete(uri: String): FutureEither[AdapterError, Unit] =
    val action = namespaceRepo.delete(uri)

    action
      .withAdapterError: exception =>
        AdapterError.ErrorDuringOperation(s"During deleting of namespace an exception was thrown. Its message: ${exception.getMessage}")
      .asFutureEither


  override def deleteAll(): Future[Unit] =
    logger.debug("Deleting all namespaces with nodes and data types from database.")

    dataTypeManager.clear()
    logger.debug("Data types cleaned from the memory.")

    for
      _ <- dataTypeRepo.deleteAll()
      _ <- nodeRepo.deleteAll()
      _ <- namespaceRepo.deleteAll()
    yield logger.debug("All namespaces with nodes and data types deleted from database.")


  override def readNodes(uri: String): Future[Vector[UaNode]] = nodeRepo.read(uri)


  override def readDataTypes(uri: String): Future[Vector[UaDataType]] =
    dataTypeManager
      .filter(dataType => dataType.id.namespaceUri.value == uri)
      .pipe(Future.successful)


  override def loadDataTypes(): Future[Unit] =
    dataTypeRepo
      .readAll()
      .map(_.foreach(dataTypeManager.add))

