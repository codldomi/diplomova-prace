package cz.modemtec.adapter.opcua.util

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.util.AdapterApiFormat.formatAdapterError

import play.api.libs.json.{Json, Writes}
import play.api.mvc.{Result, Results}
import play.api.mvc.Results.*


object HttpExtension extends HttpExtension


trait HttpExtension:

  extension (self: Option[AdapterError])

    def asHttpResult: Result = self match 
      case Some(error) =>
        val json = Json.toJson(error)
        Results.Status(error.code)(json)
      case None => Ok


  extension (self: AdapterError)

    def asHttpResult: Result = 
      val json = Json.toJson(self)
      Results.Status(self.code)(json)
  

  extension [A](self: Either[AdapterError, A])

    def asEmptyHttpResult: Result = self match 
      case Right(_) => Results.Ok
      case Left(error) =>
        val json = Json.toJson(error)
        Results.Status(error.code)(json)
    
    def asHttpResult(using writes: Writes[A]): Result = self match
      case Right(value) =>
        val json = Json.toJson(value)
        Results.Ok(json)

      case Left(error) =>
        val json = Json.toJson(error)
        Results.Status(error.code)(json)
    

