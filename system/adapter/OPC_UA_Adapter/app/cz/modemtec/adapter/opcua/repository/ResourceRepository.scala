package cz.modemtec.adapter.opcua.repository

import com.google.inject.*

import cz.modemtec.adapter.api.data.config.ResourceConfig

import cz.modemtec.adapter.opcua.repository.impl.ResourceRepositoryImpl

import scala.concurrent.Future


/** The repository interface providing managing of resources.
  */
@ImplementedBy(classOf[ResourceRepositoryImpl])
trait ResourceRepository:

  /** Creates new resource based on the configuration.
    *
    * @param config Resource configuration.
    */
  def create(config: ResourceConfig): Future[Unit]

  /** Adds namespaces to the server.
    *
    * @param guid Server's identifier.
    * @param uris Namespaces URIs.
    */
  def attachNamespaces(guid: String, uris: Set[String]): Future[Unit]

  /** Removes namespaces from the server.
    *
    * @param guid Server's identifier.
    */
  def detachNamespaces(guid: String): Future[Unit]

  /** Reads namespaces of the selected resource.
    *
    * @param guid Server's identifier.
    * @return Set of namespace URIs.
    */
  def readNamespaces(guid: String): Future[Set[String]]

  /** Deletes resource.
    *
    * @param guid Server's identifier.
    */
  def delete(guid: String): Future[Unit]

  /** Reads resource by its server.
    *
    * @param guid Server's identifier.
    * @return Resource's configuration.
    */
  def read(guid: String): Future[ResourceConfig]

  /** Reads all stored resources.
    *
    * @return Resources configurations.
    */
  def readAll(): Future[Vector[ResourceConfig]]

  /** Deletes all stored resources.
    */
  def deleteAll(): Future[Unit]

