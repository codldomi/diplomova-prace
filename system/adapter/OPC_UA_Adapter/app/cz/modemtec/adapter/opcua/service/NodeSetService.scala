package cz.modemtec.adapter.opcua.service

import com.google.inject.*

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.util.MonadTransformer.FutureEither

import cz.modemtec.adapter.opcua.service.impl.NodeSetServiceImpl

import cz.modemtec.scopcua.data.UaDataType
import cz.modemtec.scopcua.data.node.{UaNode, UaNodeSet}

import scala.xml.Node as XmlNode
import scala.concurrent.Future


/** The service interface providing managing and storing of the nodes and data types.
  */
@ImplementedBy(classOf[NodeSetServiceImpl])
trait NodeSetService:

  /*
      ---
      Methods useful for controllers and API. Contain also error handling.
      ---
   */

  /** Extracts node set from the XML doc.
    *
    * @param xml XML doc.
    * @return Extracted node set.
    */
  def extract(xml: XmlNode): Either[AdapterError, UaNodeSet]

  /** Stores node set.
    *
    * @param nodeSet The node set.
    */
  def create(nodeSet: UaNodeSet): FutureEither[AdapterError, Unit]

  /** Deletes the whole namespace.
    *
    * @param uri Namespace URI.
    */
  def delete(uri: String): FutureEither[AdapterError, Unit]

  /** Reads all stores namespaces.
    *
    * @return Namespaces URIs.
    */
  def readNamespaces(): FutureEither[AdapterError, Set[String]]

  /*
      ---
      Methods useful for other app components.
      ---
   */

  /** Loads and stores data types from the database.
    */
  def loadDataTypes(): Future[Unit]

  /** Deletes all stored node sets.
    */
  def deleteAll(): Future[Unit]

  /** Reads nodes from the selected namespace.
    *
    * @param uri Namespace URI.
    */
  def readNodes(uri: String): Future[Vector[UaNode]]

  /** Reads data types from the selected namespace.
    *
    * @param uri Namespace URI.
    * @return
    */
  def readDataTypes(uri: String): Future[Vector[UaDataType]]

