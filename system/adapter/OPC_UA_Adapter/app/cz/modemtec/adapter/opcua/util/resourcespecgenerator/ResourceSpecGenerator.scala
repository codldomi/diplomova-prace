package cz.modemtec.adapter.opcua.util.resourcespecgenerator

import cz.modemtec.adapter.api.data.config.ResourceConfig
import cz.modemtec.adapter.api.data.spec.ResourceSpec

import cz.modemtec.scopcua.data.{UaDataType, UaId}
import cz.modemtec.scopcua.data.node.UaNode

/** Generates specification of the resource.
  */
trait ResourceSpecGenerator:

  /** Generates specification for the given resource, nodes and data types.
    */
  def generate(resource: ResourceConfig, nodes: Map[UaId, UaNode], types: Map[UaId, UaDataType]): ResourceSpec

  /** Generates unique signature of the specification.
    */
  def generateSignature(resource: ResourceConfig, nodes: Map[UaId, UaNode], types: Map[UaId, UaDataType]): String

