package cz.modemtec.adapter.opcua.service

import com.google.inject.*

import cz.modemtec.adapter.api.data.AdapterError
import cz.modemtec.adapter.api.data.spec.ResourceSpec
import cz.modemtec.adapter.api.util.MonadTransformer.FutureEither

import cz.modemtec.adapter.opcua.service.impl.ResourceSpecServiceImpl


@ImplementedBy(classOf[ResourceSpecServiceImpl])
trait ResourceSpecService:

  /*
       ---
       Methods useful for controllers and API. Contain also error handling.
       ---
   */

  /** Generates resource specification for the given server.
    *
    * @param guid Server's unique identifier.
    * @return Resource's specification.
    */
  def generate(guid: String): FutureEither[AdapterError, ResourceSpec]

  /** Generates resource specification signature for the given server.
    *
    * @param guid Server's unique identifier,
    * @return Resource specification signature.
    */
  def signature(guid: String): FutureEither[AdapterError, String]

  /*
       ---
       Methods useful for other app components.
       ---
   */

  //  nothing here yet

