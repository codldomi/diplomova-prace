package cz.modemtec.adapter.opcua.service.impl.util

import cz.modemtec.adapter.api.data.{CallMethodRequest, CallMethodResult, HistoryReadRequest, ReadResult, WriteRequest, WriteResult}

import cz.modemtec.scopcua.data.node.{UaMethodNode, UaVariableNode}
import cz.modemtec.scopcua.data.value.number.integer.UaUInt32

import scala.concurrent.Future


trait ProxyClient:

  /** Checks connection and eventually creates new.
    */
  def check(): Future[Unit]

  /** Optimizes memory and other usage.
    */
  def optimize(): Future[Unit]

  /** Forces to dispose the client - doesn't care about current processing operations. Useful for program termination sequence.
    */
  def dispose(): Future[Unit]

  /** Reads value with given parameters.
    *
    * @param node Node data.
    * @return
    */
  def read(node: UaVariableNode): Future[ReadResult]

  /** Writes value with given parameters.
    *
    * @param node    Node data.
    * @param request Request parameters.
    * @return
    */
  def write(node: UaVariableNode, request: WriteRequest): Future[WriteResult]

  /** Calls method with given parameters.
    *
    * @param node    Node data.
    * @param request Request parameters.
    * @return
    */
  def callMethod(node: UaMethodNode, request: CallMethodRequest): Future[CallMethodResult]

  /** Reads history with given parameters.
    *
    * @param node    Node data.
    * @param request Request parameters.
    * @param callback Function (read value) => _
    * @return
    */
  def historyRead(node: UaVariableNode, request: HistoryReadRequest)(callback: ReadResult => _): Future[Unit]

  /** Add callback to monitored item.
    *
    * @param node  Node data.
    * @param subId Subscription ID.
    * @param callback Function (new value, monitored item ID, subscription ID) => _
    * @return Monitored item ID.
    */
  def createMonitoredItem(subId: UaUInt32, node: UaVariableNode)(callback: (ReadResult, UaUInt32, UaUInt32) => _): Future[UaUInt32]

  /** Deletes monitored item.
    *
    * @param subId Subscription ID.
    * @param monId Monitored item ID.
    */
  def deleteMonitoredItem(subId: UaUInt32, monId: UaUInt32): Future[Unit]

  /** Deletes subscription with all connected monitored items.
    *
    * @param subId Subscription ID.
    */
  def deleteSubscription(subId: UaUInt32): Future[Unit]

  /** Creates new subscription.
    *
    * @return Subscription ID.
    */
  def createSubscription(): Future[UaUInt32]

