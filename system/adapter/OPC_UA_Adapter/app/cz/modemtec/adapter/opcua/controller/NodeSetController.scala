package cz.modemtec.adapter.opcua.controller

import com.google.inject.*

import cz.modemtec.adapter.opcua.service.NodeSetService
import cz.modemtec.adapter.opcua.util.HttpExtension

import cz.modemtec.adapter.api.util.AdapterApiFormat

import cz.modemtec.adapter.opcua.api.util.OpcUaApiFormat
import cz.modemtec.adapter.opcua.api.data.DeleteNamespaceRequest

import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}
import scala.xml.NodeSeq


@Singleton()
class NodeSetController @Inject() (
  val controllerComponents: ControllerComponents,
  val nodeSetService: NodeSetService
)(using val context: ExecutionContext) extends BaseController with AdapterApiFormat with OpcUaApiFormat with HttpExtension:

  /** Creates new or modifies namespace.
    *
    * Responses:
    *
    *   - 200: Nothing - Successful operation.
    *   - 500: AdapterError - Error during operation occurred.
    */
  def createOrUpdate(): Action[NodeSeq] = Action.async(parse.xml): request =>
    //  the content max size is currently set to 5MB
    val nodeSet = nodeSetService.extract(request.body.head)

    if nodeSet.isLeft then
      Future.successful(nodeSet.asEmptyHttpResult)
    else
      val action = for
        _ <- nodeSetService.delete(nodeSet.toOption.get.modelUri)
        _ <- nodeSetService.create(nodeSet.toOption.get)
      yield ()

      action.value.map(_.asEmptyHttpResult)

  /** Reads all stored namespaces URIs.
    *
    * Responses
    *
    *   - 200: Set[String] - Successful operation.
    *   - 500: AdapterError - Error during operation occurred.
    */
  def readAll(): Action[AnyContent] = Action.async:
    nodeSetService
      .readNamespaces()
      .value
      .map(namespaces => namespaces.asHttpResult)

  /** Deletes namespace with all data types and nodes. The dependent namespaces must be deleted beforehand.
    *
    * Responses:
    *
    *   - 200: Nothing - Successful operation
    *   - 500: AdapterError - Error during operation occurred.
    */
  def delete(): Action[DeleteNamespaceRequest] = Action.async(parse.json[DeleteNamespaceRequest]): request =>
    nodeSetService
      .delete(request.body.uri)
      .value
      .map(_.asEmptyHttpResult)

