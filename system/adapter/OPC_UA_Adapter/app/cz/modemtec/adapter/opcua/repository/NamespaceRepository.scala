package cz.modemtec.adapter.opcua.repository

import com.google.inject.*

import cz.modemtec.adapter.opcua.repository.impl.NamespaceRepositoryImpl

import scala.concurrent.Future


/** The repository interface providing managing of OPC UA namespaces.
  *
  * @note All operations are idempotent.
  */
@ImplementedBy(classOf[NamespaceRepositoryImpl])
trait NamespaceRepository:

  /** Creates namespace for the URI.
    *
    * @param uri Namespace URI.
    */
  def create(uri: String): Future[Unit]

  /** Creates namespaces for URIs.
    *
    * @param namespaces Namespaces URIs.
    */
  def createAll(namespaces: Set[String]): Future[Unit]

  /** Reads all stored namespaces.
    *
    * @return Namespaces URIs.
    */
  def readAll(): Future[Set[String]]

  /** Deletes all stored namespaces.
    */
  def deleteAll(): Future[Unit]

  /** Deletes namespace by its URI.
    *
    * @param uri Namespace URI.
    */
  def delete(uri: String): Future[Unit]
