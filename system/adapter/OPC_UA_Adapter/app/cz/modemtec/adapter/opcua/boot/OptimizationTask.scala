package cz.modemtec.adapter.opcua.boot

import org.apache.pekko.actor.{ActorSystem, Cancellable}

import cz.modemtec.adapter.opcua.service.ResourceService

import play.api.{Configuration, Logging}
import play.api.inject.ApplicationLifecycle

import javax.inject.*

import scala.concurrent.duration.*
import scala.concurrent.{ExecutionContext, Future}


@Singleton()
class OptimizationTask @Inject()(
  val lifecycle: ApplicationLifecycle,
  val actors: ActorSystem,
  val service: ResourceService,
  val config: Configuration
)(using val context: ExecutionContext) extends Logging:

  val minutes: FiniteDuration = config.get[Int]("optimization.interval").minutes

  val task: Cancellable =
    actors
      .scheduler
      .scheduleAtFixedRate(initialDelay = 10.minutes, interval = minutes): () =>
        process()
  
  lifecycle.addStopHook: () =>
    Future.successful(task.cancel())
  
  
  def process(): Unit = 
    logger.debug("Optimization Task is called.")
    service.optimizeResources()
  

