name := "OPC UA Adapter"

scalaVersion := "3.3.1"

version := "0.1.0"

lazy val `opc-ua-adapter` = (project in file(".")).enablePlugins(PlayScala, LauncherJarPlugin)

maintainer := "dominik.codl@modemtec.cz"

//----------------------------------------------------------------------------------------------------------------------
//  DEPENDENCIES

resolvers ++= Seq(
  "jitpack.io" at "https://jitpack.io" // Scalable OPC UA depends on https://github.com/jimblackler/jsonschemafriend
)

//  default Play dependencies
libraryDependencies ++= Seq(jdbc , ehcache , ws , specs2 % Test, guice)

//  custom dependencies
libraryDependencies ++= Seq(

  //  ModemTec

  "cz.modemtec" %% "scalable-opc-ua" % "0.5.0",
  "cz.modemtec" %% "adapter-api" % "0.1.0",

  //  third party

  "org.playframework" %% "play-slick" % "6.0.0-M2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "org.xerial" % "sqlite-jdbc" % "3.40.1.0",
  "ch.qos.logback" % "logback-classic" % "1.3.5",
  "org.slf4j" % "slf4j-api" % "2.0.6",
  "org.scalatestplus.play" %% "scalatestplus-play" % "7.0.0" % "test",
  "org.scalatestplus" %% "mockito-4-6" % "3.2.14.0" % "test"
)

scalacOptions ++= Seq("-feature", "-language:implicitConversions")

//----------------------------------------------------------------------------------------------------------------------
//  DOCKERIZE THE APP

import com.typesafe.sbt.packager.docker.DockerChmodType
import com.typesafe.sbt.packager.docker.DockerPermissionStrategy

dockerChmodType := DockerChmodType.UserGroupWriteExecute
dockerPermissionStrategy := DockerPermissionStrategy.CopyChown

Docker / maintainer := "dominik.codl@modemtec.cz"
Docker / packageName := "opc-ua-adapter"
Docker / version := sys.env.getOrElse("BUILD_NUMBER", "0")
Docker / daemonUserUid  := None
Docker / daemonUser := "daemon"

dockerExposedPorts := Seq(9000)
dockerBaseImage := "eclipse-temurin:17-jre-alpine"
//dockerBaseImage := "openjdk:8-jre-alpine"
dockerRepository := sys.env.get("ecr_repo")
dockerUpdateLatest := true

